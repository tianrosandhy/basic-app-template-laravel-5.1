<?php
return array(
	0 => array(
		"label" 	=> "Dashboard",
		"icon" 		=> "lnr-home",
		"target" 	=> "/",
		"submenu"	=> null,
		"auth"		=> 10
		),

	1 => array(
		"label"		=> "Data Master",
		"icon"		=> "lnr-database",
		"target"	=> null,
		"auth"		=> 10,
		"submenu"	=> array(
			11 => array(
				"label" 	=> "Karyawan",
				"target"	=> "master/karyawan",
				"auth"		=> 10,
				),
			110 => array(
				"label"		=> "Upload Karyawan",
				"target"	=> "upload-karyawan",
				"auth"		=> 10,
			),
			111 => array(
				"label"		=> "Batch Update Karyawan",
				"target"	=> "update-karyawan",
				"auth"		=> 2,
			),
			
			12 => array(
				"label" 	=> "Kode Karyawan",
				"target"	=> "master/kode_karyawan",
				"auth"		=> 10,
				),
			13 => array(
				"label" 	=> "Cabang",
				"target"	=> "master/divisi",
				"auth"		=> 10,
				),
			14 => array(
				"label" 	=> "Jabatan",
				"target"	=> "master/jabatan",
				"auth"		=> 10,
				),
			19 => array(
				"label" 	=> "Golongan",
				"target"	=> "master/golongan",
				"auth"		=> 3,
				),
			15 => array(
				"label" 	=> "Shift Kerja",
				"target"	=> "master/shift",
				"auth"		=> 10,
				),
			16 => array(
				"label" 	=> "Hari Raya",
				"target"	=> "master/hari_raya",
				"auth"		=> 10,
				),
			17 => array(
				"label" 	=> "Kode Absensi",
				"target"	=> "master/kode_absensi",
				"auth"		=> 10,
				),
			18 => array(
				"label" 	=> "Gaji",
				"target"	=> "master/gaji",
				"auth"		=> 2,
				)
			)
		),
	2 => array(
		"label"		=> "Mutasi & Presensi",
		"icon"		=> "lnr-calendar-full",
		"target"	=> null,
		"auth"		=> 10,
		"submenu"	=> array(
			21 => array(
				"label" 	=> "Mutasi Karyawan",
				"target"	=> "mutasi/karyawan",
				"auth"		=> 10,
				),
			22 => array(
				"label" 	=> "Rancangan Jadwal",
				"target"	=> "mutasi/jadwal",
				"auth"		=> 10,
				),
			29 => array(
				"label" 	=> "Upload File Jadwal",
				"target"	=> "mutasi/upload2",
				"auth"		=> 10,
				),

			23 => array(
				"label" 	=> "Presensi Harian",
				"target"	=> "mutasi/presensi",
				"auth"		=> 10,
				),
			24 => array(
				"label" 	=> "Upload File Presensi",
				"target"	=> "mutasi/upload",
				"auth"		=> 10,
				),
			28 => array(
				"label" 	=> "Ketidakhadiran",
				"target"	=> "mutasi/tidakhadir",
				"auth"		=> 10,
				),
			25 => array(
				"label" 	=> "Keterlambatan",
				"target"	=> "mutasi/terlambat",
				"auth"		=> 10,
				),
			27 => array(
				"label" 	=> "Absen Tidak Lengkap",
				"target"	=> "mutasi/lupa-absen",
				"auth"		=> 10,
				),
			26 => array(
				"label" 	=> "Lembur",
				"target"	=> "mutasi/lembur",
				"auth"		=> 10,
				),
			),
		),

	3 => array(
		"label"		=> "Penggajian",
		"icon"		=> "lnr-briefcase",
		"target"	=> null,
		"auth"		=> 10,
		"submenu"	=> array(
			31 => array(
				"label" 	=> "Mutasi Gaji Tetap",
				"target"	=> "gaji/mutasi",
				"auth"		=> 3,
				),
			32 => array(
				"label" 	=> "Bonus & Potongan Bulanan",
				"target"	=> "gaji/bulanan",
				"auth"		=> 10,
				),
			38 => array(
				"label"		=> "Upload Bonus & Potongan",
				"target"	=> "upload-gaji/bulanan",
				"auth"		=> 10
			),
			34 => array(
				"label" 	=> "Tunjangan Hari Raya",
				"target"	=> "gaji/thr",
				"auth"		=> 3,
			),
			35 => array(
				'label'		=> 'Bonus Karyawan Resign',
				'target'	=> 'gaji/resign',
				'auth'		=> 3,
			),
			36 => array(
				'label' 	=> 'Kalkulasi BPJS',
				'target'	=> 'gaji/bpjs',
				'auth' 		=> 3,
			)
		),

	),

	4 => array(
		"label"		=> "Laporan",
		"icon"		=> "lnr-pie-chart",
		"target"	=> null,
		"auth"		=> 10,
		"submenu"	=> array(
			27 => array(
				"label" 	=> "Rekap Kehadiran",
				"target"	=> "mutasi/rekap",
				"auth"		=> 10,
			),
			43 => array(
				"label" 	=> "Rekap Keterlambatan",
				"target"	=> "mutasi/rekap-terlambat",
				"auth"		=> 10,
			),
			46 => array(
				"label" 	=> "Rekap Lupa Absen",
				"target"	=> "mutasi/rekap-lupa-absen",
				"auth"		=> 10,
			),

			33 => array(
				"label" 	=> "Rekap Gaji Karyawan",
				"target"	=> "gaji/rekap",
				"auth"		=> 3,
				),
			41 => array(
				"label" 	=> "Laporan utk Cik Amy",
				"target"	=> "report/finance",
				"auth"		=> 1,
				),
			42 => array(
				"label" 	=> "Laporan utk Finance",
				"target"	=> "report/amy",
				"auth"		=> 1,
				),
			44 => array(
				"label" => "Laporan Payroll",
				"target" => "report/payroll",
				"auth" => 1,
				'hide_on_panic' => true
			),
			45 => [
				'label' => 'Laporan Tahunan',
				'target' => 'gaji/tahunan',
				'auth' => 1
			]
		),
	),

	5 => array(
		"label" 	=> "Setting",
		"icon" 		=> "lnr-cog",
		"target" 	=> "setting",
		"submenu"	=> null,
		"auth"		=> 2
		),

);