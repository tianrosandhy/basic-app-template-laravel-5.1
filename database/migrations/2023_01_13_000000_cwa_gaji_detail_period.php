<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CwaGajiDetailPeriod extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('cwa_detail_gaji', function (Blueprint $table) {
            $table->date('active_until_period')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // 
        Schema::table('cwa_detail_gaji', function (Blueprint $table) {
            $table->date('active_until_period')->nullable();
        });
    }
}
