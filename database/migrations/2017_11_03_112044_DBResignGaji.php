<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DBResignGaji extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if(Schema::hasTable('cwa_resign_gaji')){
            return;
        }
        Schema::create('cwa_resign_gaji', function(Blueprint $tb){
            $tb->increments('id');
            $tb->integer('id_karyawan');
            $tb->integer('id_mutasi');
            $tb->integer('bonus');
            $tb->date('tgl_berlaku');
            $tb->tinyinteger('stat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('cwa_resign_gaji');
    }
}
