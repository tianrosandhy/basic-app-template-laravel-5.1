<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EmailGaji extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('cwa_email_gaji', function(Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('karyawan_id');
            $table->integer('bulan');
            $table->integer('tahun');
            $table->string('email')->nullable();
            $table->tinyInteger('is_sent')->nullable()->comment('0=pending, 1=sent, 9=failed');
            $table->text('note')->nullable();
            $table->timestamps();

            $table->foreign('karyawan_id')->references('id')->on('cwa_karyawan')
                ->onDelete('cascade');
            $table->unique(['tahun', 'bulan', 'karyawan_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('cwa_email_gaji');
    }
}
