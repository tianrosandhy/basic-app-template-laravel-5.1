<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DBGolongan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('cwa_golongan', function(Blueprint $tb){
            $tb->increments('id');
            $tb->string('kode_golongan',10);
            $tb->string('description');
            $tb->integer('gaji_pokok');
            $tb->integer('tunjangan_jabatan');
            $tb->tinyinteger('stat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('cwa_golongan');
    }
}
