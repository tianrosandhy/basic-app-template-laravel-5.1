<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DBDetaiLGaji extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('cwa_detail_gaji', function(Blueprint $tb){
            $tb->increments('id');
            $tb->integer('id_karyawan');
            $tb->integer('id_gaji');
            $tb->date('tanggal');
            $tb->double('nilai');
            $tb->tinyinteger('stat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('cwa_detail_gaji');
    }
}
