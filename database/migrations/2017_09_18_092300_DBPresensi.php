<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DBPresensi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create("cwa_presensi", function(Blueprint $tb){
            $tb->increments("id");
            $tb->integer("id_rancangan_jadwal");
            $tb->time("must_masuk")->nullable();
            $tb->time("must_pulang")->nullable();
            $tb->time("real_masuk")->nullable();
            $tb->time("real_pulang")->nullable();
            $tb->date("tanggal");
            $tb->tinyinteger("stat");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop("cwa_presensi");
    }
}
