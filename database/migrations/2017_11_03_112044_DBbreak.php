<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DBbreak extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('cwa_break', function(Blueprint $tb){
            $tb->increments('id');
            $tb->integer('id_karyawan');
            $tb->string('type');
            $tb->date('tgl_start');
            $tb->date('tgl_end');
            $tb->string('description');
            $tb->tinyinteger('stat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('cwa_break');
    }
}
