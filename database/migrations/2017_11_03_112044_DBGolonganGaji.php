<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DBGolonganGaji extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('cwa_golongan_gaji', function(Blueprint $tb){
            $tb->increments('id');
            $tb->integer('id_golongan');
            $tb->integer('gaji_pokok');
            $tb->integer('tunjangan_jabatan');
            $tb->date('tgl_berlaku');
            $tb->tinyinteger('stat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('cwa_golongan_gaji');
    }
}
