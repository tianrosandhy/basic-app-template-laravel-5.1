<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DBTerlambat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create("cwa_terlambat", function(Blueprint $tb){
            $tb->increments("id");
            $tb->string("uniq");
            $tb->date("tanggal");
            $tb->integer("id_karyawan");
            $tb->integer("id_divisi");
            $tb->string("reason")->nullable();
            $tb->tinyinteger("acc")->nullable();
            $tb->tinyinteger("stat");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop("cwa_terlambat");
    }
}
