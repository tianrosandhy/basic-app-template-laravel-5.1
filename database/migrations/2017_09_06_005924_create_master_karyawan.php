<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterKaryawan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('cwa_karyawan', function(Blueprint $tb){
            $tb->increments('id');
            $tb->string('nama');
            $tb->string('nama_panggilan')->nullable();
            $tb->string('ktp')->nullable();
            $tb->string('ttl')->nullable();
            $tb->tinyInteger('jk');
            $tb->string('alamat');
            $tb->string('alamat_asal')->nullable();
            $tb->string('telp',30);
            $tb->string('no_rek')->nullable();
            $tb->tinyinteger('status_transfer')->nullable();
            $tb->date('tgl_masuk');
            $tb->tinyInteger('stat');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('cwa_karyawan');
    }
}
