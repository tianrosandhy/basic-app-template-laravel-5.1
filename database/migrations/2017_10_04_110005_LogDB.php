<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LogDB extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('log_system', function (Blueprint $tb){
            $tb->increments('id');
            $tb->string('username');
            $tb->datetime('tgl');
            $tb->string('uri');
            $tb->string('action');
            $tb->string('data');
            $tb->tinyinteger('stat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('log_system');
    }
}
