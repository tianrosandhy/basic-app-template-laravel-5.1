<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HR extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create("cwa_hr", function(Blueprint $tb){
            $tb->increments("id");
            $tb->string("nama_hr");
            $tb->date("tgl_start");
            $tb->date("tgl_end");
            $tb->tinyinteger("occurence");
            $tb->tinyinteger("stat");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('cwa_hr');
    }
}
