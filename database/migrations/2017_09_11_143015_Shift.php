<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Shift extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create("cwa_shift", function(Blueprint $tb){
            $tb->increments("id");
            $tb->string("nama_shift");
            $tb->string("kode_shift",10);
            $tb->time("jam_masuk");
            $tb->time("jam_pulang");
            $tb->string("description");
            $tb->tinyinteger("stat");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop("cwa_shift");
    }
}
