<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnVisibilityOnCwaMasterGaji extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('cwa_master_gaji', function (Blueprint $table) {
            $table->tinyInteger('is_visible')->nullable()->default('1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // 
        Schema::table('cwa_master_gaji', function (Blueprint $table) {
            $table->dropColumn('is_visible');
        });
    }
}
