<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateControlCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('cwa_control_code', function(Blueprint $tb){
            $tb->increments('id');
            $tb->integer('id_karyawan');
            $tb->string('code',40);
            $tb->string('type',40);
            $tb->tinyInteger('stat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('cwa_control_code');
    }
}
