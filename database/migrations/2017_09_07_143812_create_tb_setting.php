<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbSetting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('cwa_setting', function(Blueprint $tb){
            $tb->increments('id');
            $tb->string('param', 30);
            $tb->string('label', 50);
            $tb->string('tag');
            $tb->string('value');
            $tb->string('def_value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('cwa_setting');
    }
}
