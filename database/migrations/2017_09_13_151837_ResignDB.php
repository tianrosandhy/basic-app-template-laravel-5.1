<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ResignDB extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create("cwa_resign", function(Blueprint $tb){
            $tb->increments("id");
            $tb->integer("id_karyawan");
            $tb->date("tgl_resign");
            $tb->string("keterangan", 500);
            $tb->tinyinteger("stat");
        });
    }

    public function down()
    {
        Schema::drop("cwa_resign");
    }
}
