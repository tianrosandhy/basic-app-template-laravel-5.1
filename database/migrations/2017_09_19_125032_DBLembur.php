<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DBLembur extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create("cwa_lembur", function(Blueprint $tb){
            $tb->increments("id");
            $tb->integer("id_presensi");
            $tb->date("tgl");
            $tb->integer("lama_lembur");
            $tb->tinyinteger("type");
            $tb->tinyinteger("stat");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop("cwa_lembur");
    }
}
