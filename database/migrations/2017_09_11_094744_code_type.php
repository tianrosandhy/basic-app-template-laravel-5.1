<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CodeType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('cwa_code_type', function(Blueprint $tb){
            $tb->increments('id');
            $tb->string('code_name', 40);
            $tb->string('code_description');
            $tb->string('type',40);
            $tb->tinyInteger('stat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('cwa_code_type');
    }
}
