<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Log;

class ManageIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $maps = [
            'cwa_control_code' => [
                'id_karyawan', 'stat'
            ],
            'cwa_detail_gaji' => [
                'id_karyawan', 'id_gaji'
            ],
            'cwa_golongan_gaji' => [
                'id_golongan'
            ],
            'cwa_karyawan' => [
                'status_transfer',
                'stat'
            ],
            'cwa_lembur' => [
                'id_presensi',
            ],
            'cwa_mutasi_karyawan' => [
                'id_karyawan', 'id_divisi', 'id_jabatan', 'id_golongan', 'stat'
            ],
            'cwa_presensi' => [
                'id_rancangan_jadwal'
            ],
            'cwa_rancangan_jadwal' => [
                'id_karyawan', 'id_divisi', 'id_kode_absen', 'id_shift', 'tanggal'
            ],
            'cwa_resign' => [
                'id_karyawan'
            ],
            'cwa_resign_gaji' => [
                'id_karyawan'
            ],
            'cwa_terlambat' => [
                'uniq', 'tanggal', 'id_karyawan', 'id_divisi'
            ],
            'cwa_tidak_hadir' => [
                'id_karyawan', 'id_divisi', 'tanggal'
            ],            
        ];

        foreach($maps as $tbname => $listKeys){
            try{
                Schema::table($tbname, function($table) use($listKeys){
                    $table->index($listKeys);
                });
            }catch(\Exception $e){
                Log::info("Error Migration ManageIndex in table ".$tbname." : " . $e->getMessage());
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        // Schema::drop("cwa_karyawan");
    }
}
