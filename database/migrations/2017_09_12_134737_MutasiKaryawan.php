<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MutasiKaryawan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create("cwa_mutasi_karyawan", function(Blueprint $tb){
            $tb->increments("id");
            $tb->integer("id_karyawan");
            $tb->integer("id_divisi");
            $tb->integer("id_jabatan");
            $tb->integer("id_golongan");
            $tb->date("tgl_berlaku");
            $tb->string("status");
            $tb->tinyinteger("stat");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("cwa_mutasi_karyawan");
    }
}
