<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DBDetailTidakhadir extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create("cwa_tidak_hadir", function(Blueprint $tb){
            $tb->increments("id");
            $tb->integer("id_karyawan");
            $tb->integer("id_divisi");
            $tb->date("tanggal");
            $tb->integer("id_kode_absen");
            $tb->tinyinteger("stat");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop("cwa_tidak_hadir");
    }
}
