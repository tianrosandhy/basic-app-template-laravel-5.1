<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class JadwalDB extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create("cwa_rancangan_jadwal", function(Blueprint $tb){
            $tb->increments("id");
            $tb->integer("id_karyawan");
            $tb->integer("id_divisi");
            $tb->integer("id_kode_absen")->nullable();
            $tb->integer("id_shift")->nullable();
            $tb->date("tanggal");
            $tb->tinyinteger("stat");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop("cwa_rancangan_jadwal");
    }
}
