<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KodeAbsen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create("cwa_kode_absen", function(Blueprint $tb){
            $tb->increments("id");
            $tb->string("kode_absen",10);
            $tb->string("label",50);
            $tb->tinyinteger("bobot");
            $tb->string("color",10);
            $tb->tinyinteger("include_jadwal");
            $tb->tinyinteger("stat");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop("cwa_kode_absen");
    }
}
