<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KaryawanUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cwa_karyawan', function (Blueprint $table) {
            $table->tinyInteger('is_bpjs_kesehatan')->default('0');
            $table->tinyInteger('is_bpjs_tk')->default('0');
            $table->date('tgl_daftar_bpjs_kesehatan')->nullable();
            $table->date('tgl_daftar_bpjs_tk')->nullable();
            $table->string('status_ptkp', 10)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cwa_karyawan', function (Blueprint $table) {
            $table->dropColumn('is_bpjs_kesehatan');
            $table->dropColumn('is_bpjs_tk');
            $table->dropColumn('tgl_daftar_bpjs_kesehatan');
            $table->dropColumn('tgl_daftar_bpjs_tk');
            $table->dropColumn('status_ptkp');
        });
    }
}
