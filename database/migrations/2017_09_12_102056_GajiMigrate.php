<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GajiMigrate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create("cwa_master_gaji", function(Blueprint $tb){
            $tb->increments("id");
            $tb->string("label",40);
            $tb->string("description");
            $tb->integer("default_value");
            $tb->tinyinteger("type"); //1. plus  2.minus
            $tb->tinyinteger("occurence"); //1. fixed   2. monthly
            $tb->tinyinteger("divide_by_absen"); //ketidakhadiran mengurangi bagian gaji
            $tb->tinyinteger("invalid_by_alpa"); //dianggap tidak valid jika alpa melebihi x kali
            $tb->tinyinteger("stat");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop("cwa_master_gaji");
    }
}
