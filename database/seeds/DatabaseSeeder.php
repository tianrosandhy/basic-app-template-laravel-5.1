<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(ControlCode::class);
        $this->call(HariRayaSeeder::class);
        $this->call(KodeAbsenSeed::class);
        $this->call(MasterGajiSeed::class);
        $this->call(SettingDefault::class);
        $this->call(UserDetail::class);
        // $this->call(UserTableSeeder::class);

        Model::reguard();
    }
}
