<?php

use Illuminate\Database\Seeder;

class SettingDefault extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('cwa_setting')->insert(
        	[
        		'param' => 'paging',
        		'label' => 'Jumlah data per halaman',
        		'tag' => '',
        		'value' => 0,
        		'def_value' => 10
        	],
        	[
        		'param' => 'max_login_try',
        		'label' => 'Jumlah kesalahan log in maksimal',
        		'tag' => '',
        		'value' => 0,
        		'def_value' => 10
        	]
        );
    }
}
