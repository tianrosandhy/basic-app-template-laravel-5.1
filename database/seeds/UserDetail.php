<?php

use Illuminate\Database\Seeder;

class UserDetail extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $now = date("Y-m-d H:i:s");
        DB::table('cwa_user')->insert([
        	'username' => 'admin',
        	'email' => 'admin@localhost',
        	'password' => bcrypt('admin'),
        	'created_at' => $now,
        	'updated_at' => $now,
            'priviledge' => 1
        ]);
    }
}
