<?php

use Illuminate\Database\Seeder;

class ControlCode extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table("cwa_code_type")
        ->insert([
        	'code_name' => 'Kode Master',
        	'code_description' => 'Kode master perusahaan yang disesuaikan dengan kebutuhan',
        	'type' => 'mandatory',
        	'stat' => 0
        ]);
    }
}
