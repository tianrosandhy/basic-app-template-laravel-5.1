<?php

use Illuminate\Database\Seeder;

class KodeAbsenSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table("cwa_kode_absen")->insert([
        	[
        	'kode_absen' => 'A',
        	'label' => 'Alpa',
        	'bobot' => 0,
        	'color' => '#D00000',
            'include_jadwal' => 0,
        	'stat' => 0
        	],
        	[
        	'kode_absen' => 'S',
        	'label' => 'Sakit',
        	'bobot' => 1,
        	'color' => '#FFCC42',
            'include_jadwal' => 0,
        	'stat' => 0
        	],
        	[
        	'kode_absen' => 'I',
        	'label' => 'Izin',
        	'bobot' => 0,
        	'color' => '#EA7E00',
            'include_jadwal' => 0,
        	'stat' => 0
        	],
        	[
            'kode_absen' => 'C',
            'label' => 'Cuti',
            'bobot' => 1,
            'color' => '#6BA500',
            'include_jadwal' => 1,
            'stat' => 0
            ],
            [
            'kode_absen' => 'T',
            'label' => 'Terlambat',
            'bobot' => 1,
            'color' => '#888',
            'include_jadwal' => 0,
            'stat' => 0
            ],
            [
            'kode_absen' => 'HR',
            'label' => 'Hari Raya',
            'bobot' => 1,
            'color' => '#F70B7A',
            'include_jadwal' => 1,
            'stat' => 0
            ],
            [
            'kode_absen' => 'L',
            'label' => 'Libur',
            'bobot' => 1,
            'color' => '#30F5F5',
            'include_jadwal' => 1,
            'stat' => 0
            ],
        ]);
    }
}
