<?php

use Illuminate\Database\Seeder;

class MasterGajiSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table("cwa_master_gaji")->insert([
        	[
        	'label' => "Gaji Pokok",
        	'description' => '',
        	'default_value' => 0,
        	'type' => 1,
        	'occurence' => 1,
        	'divide_by_absen' => 1,
        	'invalid_by_alpa' => 0,
        	'stat' => 1
        	],
        	[
        	'label' => "Insentif",
        	'description' => 'Bayaran tambahan untuk mengapresiasi kerajinan karyawan',
        	'default_value' => 200000,
        	'type' => 1,
        	'occurence' => 1,
        	'divide_by_absen' => 0,
        	'invalid_by_alpa' => 1,
        	'stat' => 1
        	],
        	[
        	'label' => "Tunjangan Jabatan",
        	'description' => 'Tunjangan tambahan untuk karyawan',
        	'default_value' => 0,
        	'type' => 1,
        	'occurence' => 1,
        	'divide_by_absen' => 0,
        	'invalid_by_alpa' => 0,
        	'stat' => 1
        	]

        ]);
    }
}
