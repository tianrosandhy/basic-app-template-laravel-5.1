<?php

use Illuminate\Database\Seeder;

class HariRayaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table("cwa_hr")->insert([
        [
            "nama_hr" => "Tahun Baru",
            "tgl_start" => "2017-01-01",
            "tgl_end" => "2017-01-01",
            "occurence" => 1,
            "stat" => 0
        ],
        [
            "nama_hr" => "Natal",
            "tgl_start" => "2017-12-25",
            "tgl_end" => "2017-12-25",
            "occurence" => 1,
            "stat" => 0
        ],
        [
            "nama_hr" => "Hari Buruh",
            "tgl_start" => "2017-05-01",
            "tgl_end" => "2017-05-01",
            "occurence" => 1,
            "stat" => 0
        ]
        ]);

    }
}
