<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<base href="{{ URL::to('/') }}/">
<title>{{ isset($title) ? $title : "HRIS Application" }}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<link href="css/font-awesome.css" rel="stylesheet"> 
<link rel="stylesheet" href="css/app.css" type="text/css">
</head>
<body class="print">

@yield('content')


<script>
	window.print();
</script>
</body>
</html>