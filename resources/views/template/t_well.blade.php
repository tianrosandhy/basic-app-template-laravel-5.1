<div class="well">
	<h3>{{ $wellname }}</h3>
	<form action="" method="get" class="form form-horizontal">
		@if(isset($default['bulan']))
		<div class="form-group">
			<label class="control-label col-sm-2">
				Bulan
			</label>
			<div class="col-sm-5">
				<select name="bulan" class="form-control">
					@foreach(nama_bulan() as $i=>$bln)
					<option value="{{ $i }}" {{$i==$default['bulan'] ? "selected" : ""}} >{{ $bln }}</option>
					@endforeach
				</select>
			</div>
		</div>
		@endif
		<div class="form-group">
			<label class="control-label col-sm-2">
				Tahun
			</label>
			<div class="col-sm-3">
				<input type="number" name="tahun" min=1900 max=2100 class="form-control" value="{{ $default['tahun'] }}">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-2">
				Divisi
			</label>
			<div class="col-sm-5">
				<select name="divisi" class="form-control">
					@foreach($listdiv as $id=>$dv)
					<option value="{{ $id }}" {{ $id == $default['divisi'] ? "selected" : "" }} >{{ $dv }}</option>
					@endforeach
				</select>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3 col-sm-push-2">
				<button class="btn btn-primary">Filter</button>
				<a href="{{ Request::segment(1)."/".Request::segment(2) }}" class="btn btn-danger">Reset</a>
			</div>
		</div>
		

	</form>
</div>