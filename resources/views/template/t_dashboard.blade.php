<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>{{ $title }}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<link href="css/style.css" rel='stylesheet' type='text/css' />
<link href="css/font-awesome.css" rel="stylesheet"> 
<link rel="stylesheet" href="css/alertify.min.css" type='text/css' />
<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
<link rel="stylesheet" href="css/app.css" type='text/css' />
<script src="js/Chart.min.js"></script>
<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
<style>
.demo-alert{
    position:fixed;
    bottom:0;
    left:0;
    width:100%;
    z-index:200;
    margin-bottom:0;
    background:#d00;
    color:#fff;
    font-weight:bold;
    text-align:center;
    padding:.5em 1em;
}
</style>
<script src="js/wow.min.js"></script>
	<script>
		 new WOW().init();
	</script>
<script src="js/jquery-1.10.2.min.js"></script>
<!-- Placed js at the end of the document so the pages load faster -->

</head> 
   
 <body class="sticky-header left-side-collapsed"  onload="initMap()">
    <section>
    <!-- left side start-->
		<div class="left-side sticky-left-side">

			<!--logo and iconic logo start-->
			<div class="logo">
				<h1><a href="{{ URL::to('/') }}"><span>Citra Warna</span></a></h1>
			</div>
			<div class="logo-icon text-center">
				<a href="{{ URL::to('/') }}"><i class="lnr lnr-home"></i> </a>
			</div>

			<!--logo and iconic logo end-->
			<div class="left-side-inner">

				<!--sidebar nav start-->
					<ul class="nav nav-pills nav-stacked custom-nav">
						@foreach(config('menu') as $index => $menu)
							@if(isset($menu['hide_on_panic']) && is_panic())
							@else
							@if(Auth::user()->priviledge <= $menu['auth'])
							<li class="{{ isset($menu['submenu']) ? 'menu-list' : '' }} @if(isset($curmenu)) {{ $curmenu == $index ? 'act' : '' }} @endif">
									@if(isset($menu['submenu']))
									<a href="#">
										<i class="lnr {{ $menu['icon'] }}"></i>
										<span>{{ $menu['label'] }}</span>
									</a>
										<ul class="sub-menu-list">
											@foreach($menu['submenu'] as $ind => $submenu)
												@if(isset($submenu['hide_on_panic']) && is_panic())
												@else
												@if(Auth::user()->priviledge <= $submenu['auth'])
													<li><a @if(isset($cursubmenu)) {!! $cursubmenu == $ind ? 'style="font-weight:bold; background:#6B9D31"' : '' !!} @endif href="{{ URL::to($submenu['target']) }}">{{ $submenu['label'] }}</a></li>
												@endif
												@endif
											@endforeach
										</ul>
									@else
									<a href="{{ URL::to($menu['target']) }}">
										<i class="lnr {{ $menu['icon'] }}"></i>
										<span>{{ $menu['label'] }}</span>
									</a>
									@endif
							</li>
							@endif
							@endif
						@endforeach
					</ul>
				<!--sidebar nav end-->
			</div>
		</div>
		<!-- left side end-->
    
		<!-- main content start-->
		<div class="main-content">
			<!-- header-starts -->
			<div class="header-section">
			 
			<!--toggle button start-->
			<a class="toggle-btn  menu-collapsed"><i class="fa fa-bars"></i></a>
			<!--toggle button end-->

			<!--notification menu start -->
			<div class="menu-right">
				<div class="user-panel-top">  	
					<div class="profile_details">		
						<ul>
							<li class="dropdown profile_details_drop">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
									<div class="profile_img">	
										 <div class="user-name">
											<p>
												{{ ucfirst(auth()->user()['username']) }}
												<span>{{ get_priviledge()[1] }}</span>
											</p>
										 </div>
										 <i class="lnr lnr-chevron-down"></i>
										 <i class="lnr lnr-chevron-up"></i>
										<div class="clearfix"></div>	
									</div>	
								</a>
								<ul class="dropdown-menu drp-mnu">
									<li> <a href="setting"><i class="fa fa-cog"></i> Settings</a> </li> 
									<li> <a href="logout"><i class="fa fa-sign-out"></i> Logout</a> </li>
								</ul>
							</li>
							<div class="clearfix"> </div>
						</ul>
					</div>		
					<div class="clearfix"></div>
				</div>
			  </div>
			<!--notification menu end -->
			</div>
		<!-- //header-ends -->
			<div id="page-wrapper">


			@yield('content')
	

			</div>
			 <!--body wrapper end-->
		</div>
        <!--footer section start-->
			<footer>
			   <p>&copy 2017 Citra Warna Jaya Abadi</p>
			</footer>
        <!--footer section end-->

      <!-- main content end-->
   </section>

@if (env('DEMO'))
<div class="demo-alert">This app is running in DEMO / Staging Environtment.</div>
@endif

<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>
<script src="js/alertify.min.js"></script>
<script src="js/featherlight.js"></script>
<script>
$(function(){
	@if(session('error'))
		alertify.error("{{ session('error') }}");
	@elseif(session('success'))
		alertify.success("{{ session('success') }}");
	@endif
});
</script>
</body>
</html>