<div class="well">
	<h3>{{ $wellname }}</h3>
	<form action="" method="get" class="form form-horizontal">
		<div class="form-group">
			<label class="control-label col-sm-2">
				Tanggal
			</label>
			<div class="col-sm-5">
				<div class="input-group">
					<span class="input-group-btn">
						<a href="{{Request::segment(1)}}/{{ Request::segment(2) }}?divisi={{ $default['divisi'] }}&tgl={{ date("Y-m-d", strtotime($default['tgl'])-86400 ) }}" class="btn btn-primary">
							<i class="fa fa-caret-left"></i>
						</a>
					</span>
					<input type="date" name="tgl" class="form-control" value="{{ $default['tgl'] }}">
					<span class="input-group-btn">
						<a href="{{Request::segment(1)}}/{{ Request::segment(2) }}?divisi={{ $default['divisi'] }}&tgl={{ date("Y-m-d", strtotime($default['tgl'])+86400 ) }}" class="btn btn-primary">
							<i class="fa fa-caret-right"></i>
						</a>
					</span>
				</div>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-sm-2">
				Divisi
			</label>
			<div class="col-sm-5">
				<select name="divisi" class="form-control">
					@foreach($listdiv as $id=>$dv)
					<option value="{{ $id }}" {{ $id == $default['divisi'] ? "selected" : "" }} >{{ $dv }}</option>
					@endforeach
				</select>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3 col-sm-push-2">
				<button class="btn btn-primary">Filter</button>
				<a href="{{ Request::segment(1)."/".Request::segment(2) }}" class="btn btn-danger">Reset</a>
			</div>
		</div>
		

	</form>
</div>