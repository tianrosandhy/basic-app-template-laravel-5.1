<?php
$ok = true;
?>
@if(env('DEMO_TIME'))
	<?php
	$now = time();
	$integer = strtotime(env('DEMO_TIME'));
	$limit = date('d F Y H:i:s', $integer);

	$treshold = 5;
	?>
	@if($now < $integer)
	@if(($integer-$now) < ($treshold * 86400))
	<div class="alert alert-warning">
		Your trial will be terminated in {{ $limit }}. Please buy or renew your license before the date given. 
	</div>
	@endif
	@else
	<?php $ok = false; ?>
	<div class="alert alert-danger">
		Sorry, your trial period has been terminated. Please buy or renew your license to continue the application.
	</div>	
	@endif
@endif

@if($ok)
<input type="submit" value="Login to your account" style="width:100%">
@endif