<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>{!! isset($title) ? $title : "Aplikasi" !!}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<link href="css/app.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<!-- lined-icons -->
<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
<!-- //lined-icons -->
<style>
.demo-alert{
    position:fixed;
    bottom:0;
    left:0;
    width:100%;
    z-index:200;
    margin-bottom:0;
    background:#d00;
    color:#fff;
    font-weight:bold;
    text-align:center;
    padding:.5em 1em;
}
</style>
<!-- chart -->
<script src="js/Chart.js"></script>
<!-- //chart -->
<!--animate-->
<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
<script src="js/wow.min.js"></script>
	<script>
		 new WOW().init();
	</script>
<!--//end-animate-->
<script src="js/jquery-1.10.2.min.js"></script>
<!-- Placed js at the end of the document so the pages load faster -->

</head> 
   
 <body class="sign-in-up">
    <section>
			<div id="page-wrapper" class="sign-in-wrapper">
				<div class="graphs">
					<div class="sign-in-form">
						<div class="sign-in-form-top" align="center">
							<img src="images/logo-new.jpg" alt="Logo CWA" height=100>
						</div>
						<div class="signin">
							{!! Form::open(['url' => 'login']) !!}
							
							@if(session('error'))
								<div class="alert alert-danger">{{ session('error') }}</div>
							@endif
							@if(session('info'))
								<div class="alert alert-info">{{ session('info') }}</div>
							@endif
				

							<div class="log-input">
							   <input type="text" class="user" name="username" placeholder="Username"/>
								<div class="clearfix"> </div>
							</div>
							<div class="log-input">
							   <input type="password" name="password" class="lock" placeholder="Password" />
								<div class="clearfix"> </div>
							</div>

						 	@include ('template.t_demo')
						{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		<!--footer section start-->
			<footer>
			   <p>&copy 2017 Citra Warna Jaya Abadi</p>
			</footer>
        <!--footer section end-->
	</section>

@if (env('DEMO'))
<div class="demo-alert">This app is running in DEMO / Staging Environtment.</div>
@endif
	
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>