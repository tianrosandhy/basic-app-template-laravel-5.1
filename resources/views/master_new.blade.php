@extends('template/t_panel')

@section('content')
<h2>{{ $title }}</h2>

@if(isset($back_button))
	<a href="javascript:history.go(-1);" class="btn btn-default btn-sm">
		<span class="fa fa-caret-left"></span>
		Back
	</a>
@endif

@if(isset($description))
<div class="alert alert-info">{{ $description }}</div>
@endif



@if(isset($action_button))
<div class="padd" style="padding:1em 0;">
	<a href="{{ URL::to($action_button) }}" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>	
</div>
@endif

@if(isset($getform))
<div class="well">
	<h3>{{ $getform['title'] }}</h3>
	<form action="" method="get" class="form form-horizontal">
		@foreach($getform['form'] as $data)
			<div class="form-group">
				<label class="control-label col-sm-2">{{ $data['label'] }}</label>
				<div class="col-sm-10">
				@if(!isset($data['value']))
					{!! input_helper($data['label'], $data, Session::get($data['field'])) !!}
				@else
					{!! input_helper($data['label'], $data, $data['value']) !!}
				@endif
				</div>
			</div>
		@endforeach
		<div class="form-group">
			<div class="col-sm-10 col-sm-push-2">
				<button class="btn btn-warning">
					<i class="fa fa-filter"></i> Filter
				</button>
				@if(isset($getform['url']))
				<a href="{!! $getform['url'] !!}" class="btn btn-danger">Reset</a>
				@endif
			</div>
		</div>
	</form>
</div>
@endif



<table class="table data-table dataTable table-hover">
	<thead>
		<tr>
			@foreach($columns as $key=>$val)
				<th>{{ $key }}</th>
			@endforeach
		</tr>
	</thead>
	<thead class="search">
		<tr>
			@foreach($columns as $key=>$val)
				<th>{{ $val['search'] ? $key : "" }}</th>
			@endforeach
		</tr>
	</thead>
	<tbody>
		
	</tbody>
</table>


@if(isset($script))
<script>
	var tb_data;
	var sudahload = false;

	$(function(){
		//jump to plugin
		jQuery.fn.dataTable.Api.register( 'page.jumpToData()', function ( n ) {
		    if ( n > 0 ) {
		    	console.log(n);
		        this.page( n ).draw(false);
		    }
		    return this;
		} );

		$("table .search th").each(function(key, value){
			var title = $(value).text();
			if(title.length > 0){
				$(value).html('<input class="form-control search" id="'+title.replace(' ','_').toLowerCase()+'" type="text" placeholder="Search '+title+'" />');
			}
		});

		$("table .search input, table .search select").on(
			"change", $.debounce(250, function(){
			tb_data.ajax.reload();
		}));

		$("table .search input").on(
			"keyup", $.debounce(250, function(){
			tb_data.ajax.reload();
		}));


		tb_data = $("table.data-table").DataTable({
			'processing': true,
			'serverSide': true,
			'autoWidth' : false,
			'searching'	: false,
			'filter'	: false,
			'pagingType': 'numbers',
			'ajax'		: {
				type : 'GET',
				url	: '{{ $datatable_ajax['tb'] }}',
				dataType : 'json',
				data : function(data){
					{!! $datatable_search !!}
				}
			},
			'drawCallback' : function(setting){
				@if(isset($_GET['page']))
				load_tb_page({{ $_GET['page'] }});
				@endif
			}
		});


		$("body").on('click', '.btn-edit', function(e){
			e.preventDefault();
			window.location = $(this).attr('href')+'?page='+get_tb_page();
		});


	});

	function load_tb_page($page){
		//FUCKING LOGIC!!!!
		if(sudahload == false){
			sudahload = true;
			tb_data.page.jumpToData($page);

			selector = ".paginate_button:contains("+$page+")";
			console.log(selector);
			setTimeout(function(){
		        $(selector).each(function(){
		        	console.log($(this).html == $page);
		        	if($(this).html() == $page){
		        		$(this).click();
		        	}
		        });
			}, 250);
		}
	}

	function get_tb_page(){
		data = tb_data.page.info();
		page = data.page + 1;
		return page;
	}
</script>
@endif

@stop


