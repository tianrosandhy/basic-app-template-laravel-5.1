@extends('template/t_print')

@section('content')



<div class="slip-pack">
	<?php $x = 0; ?>
	@foreach($rows as $row)
	<div class="slip">
		<h2>{{ $title }}</h2>
		<span class="label label-primary">Detail Karyawan</span>
		<table class="padd detail-gaji">
			<tr>
				<th>Nama Karyawan</th>
				<td>{{ $row->nama }}</td>
			</tr>
			<tr>
				<th>Divisi</th>
				<td>{{ $row->nama_divisi }}</td>
			</tr>
			<tr>
				<th>Jabatan</th>
				<td>{{ $row->nama_jabatan }}</td>
			</tr>
			<tr>
				<th>Tanggal</th>
				<td>{{ indo_date($default['tgl']) }}</td>
			</tr>
		</table>
	

		<span class="label label-primary">Detail THR</span>
		<table class="padd detail-gaji">
			<tr>
				<td>Lama Bekerja</td>
				<td>{{ $content[$row->id_karyawan]['lama_bekerja'] }}</td>
			</tr>
			<tr>
				<td>Total Upah</td>
				<td>{{ rupiah($content[$row->id_karyawan]['thr']) }}</td>
			</tr>
			<tr>
				<td>THR Diterima</td>
				<td><b>{{ rupiah($content[$row->id_karyawan]['get']) }}</b></td>
			</tr>
		</table>
	</div>
	<?php
	$x++;
	if($x % 18 == 0){
		echo "</div><div class='slip-pack'>";
	}
	?>
	@endforeach
</div>
@stop