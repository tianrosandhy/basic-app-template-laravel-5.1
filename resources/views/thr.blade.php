@extends('template/t_panel')

@section('content')

<h2>{{ $title }}</h2>


@include("template/$getform")


@if(Request::get('tgl'))

<div class="padd" align="center">
	<a href="gaji/thr/slip?tgl={{$default['tgl']}}&divisi={{$default['divisi']}}" class="btn btn-primary" target="_blank">
		<span class="fa fa-print"></span>
		Cetak Slip THR
	</a>	

	<a href="gaji/thr/print?tgl={{$default['tgl']}}&divisi={{$default['divisi']}}" class="btn btn-info">
		<i class="fa fa-print"></i> Print Laporan THR
	</a>
</div>

<table class="data table">
	<thead>
		<tr>
			<th>Nama Karyawan</th>
			<th>Jabatan</th>
			<th>Divisi</th>

			<th>Lama bekerja</th>
			<th>Jumlah Upah</th>
			<th>THR Diterima</th>
		</tr>
	</thead>
	<tbody>
	<?php $totall = 0;?>
	@foreach($mutasi as $mt)
		<?php $totall += $thr[$mt->id_karyawan]['get']; ?>
		<tr>
			<td>{{ $mt->nama }}</td>
			<td>{{ $mt->nama_jabatan }}</td>
			<td>{{ $mt->nama_divisi }}</td>

			<td>{{ $thr[$mt->id_karyawan]['lama_bekerja'] }}</td>
			<td>{{ rupiah($thr[$mt->id_karyawan]['thr']) }}</td>
			<td><b>{{ rupiah($thr[$mt->id_karyawan]['get']) }}</b></td>
		</tr>
	@endforeach
	</tbody>
	<tfoot>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td><mark><b>{{ rupiah($totall) }}</b></mark></td>
		</tr>
	</tfoot>
</table>

@endif

@stop
