<table>
	<thead>
		<tr>
			<th>No</th>
			<th>Nama Karyawan</th>
			<th>Cabang</th>
			<th>Jabatan</th>
			<th>Golongan</th>
			<th>Stt Transfer</th>
			<th>No Rek</th>
			<th>Presensi</th>
			<th>NPWP</th>
			<th>NIK</th>
			<th>Departemen</th>
			<th>Email</th>
			@foreach($listgaji as $type => $dtgj)
				@foreach($dtgj as $key => $value)
				<th>{{ $value }}</th>
				@endforeach
			@endforeach
			<th>Take Home Pay</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$temp = [
			'take_home_pay' => 0
		];
		$i = 1;
		?>
		@foreach($data as $row)
		<tr>
			<td>{{ $i++ }}</td>
			<td>{{ $row['name'] }}</td>
			<td>{{ $row['divisi'] }}</td>
			<td>{{ $row['jabatan'] }}</td>
			<td>{{ $row['kode_golongan'] }}</td>
			<td>{{ ($row['status_transfer'] == 1 ? 'Transfer' : 'Cash') }}</td>
			<td>{{ $row['no_rek'] }}</td>
			<td>{{ $row['kesimpulan']['presensi'] }} / {{ $row['kesimpulan']['batas_bobot'] }}</td>

			<td>{{ $row['npwp'] }}</td>
			<td>{{ $row['nik'] }}</td>
			<td>{{ $row['departemen'] }}</td>
			<td>{{ $row['email'] }}</td>
			@foreach($row['gaji'] as $type => $dtgj)
				@foreach($dtgj as $keyname => $gajivalue)
				<td>{{ round($gajivalue) }}</td>
				<?php
				if(isset($temp[$keyname])){
					$temp[$keyname] += $gajivalue;
				}
				else{
					$temp[$keyname] = $gajivalue;
				}
				?>
				@endforeach
			@endforeach
			<td>{{ round($row['kesimpulan']['total']) }}</td>
			<?php
			$temp['take_home_pay'] += $row['kesimpulan']['total'];
			?>
		</tr>
		@endforeach
	</tbody>
	<tfoot>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td align="right"><strong>TOTAL</strong></td>
			@foreach($listgaji as $type => $dtgj)
				@foreach($dtgj as $key => $value)
				<td><strong>{{ isset($temp[$value]) ? round($temp[$value]) : 0 }}</strong></td>
				@endforeach
			@endforeach
			<td><strong>{{ round($temp['take_home_pay']) }}</strong></td>
		</tr>
	</tfoot>
</table>