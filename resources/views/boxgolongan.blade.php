<div class="container">
	<h2>{{ $title }}</h2>
	Kode Golongan : <strong>{{ $row->kode_golongan }}</strong>
	<br>
	Deskripsi : <strong>{{ $row->description }}</strong>
	<br><br>

	<div class="well">
		<form action="{{ URL::to($action_button) }}" method="get" class="form-horizontal {{ ($type == 'update') ? 'form-ajax' : '' }}">
			{{ csrf_field() }}
			<input type="hidden" name="id_golongan" value="{{ $row->id }}">
				<div class="form-group">
					<div class="col-sm-2">
						Gaji Pokok
					</div>
					<div class="col-sm-10">
						<input type="number" class="form-control" name="gaji_pokok">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-2">
						Tunjangan Jabatan
					</div>
					<div class="col-sm-10">
						<input type="number" class="form-control" name="tunjangan_jabatan">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-2">
						Berlaku Per Tanggal
					</div>
					<div class="col-sm-10">
						<input type="date" class="form-control" name="tgl_berlaku" value="{{ date("Y-m-d") }}">
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-10 col-sm-push-2">
						<button class="btn btn-primary">
							<span class="fa fa-save"></span>
							Simpan
						</button>
					</div>
				</div>

		</form>
	</div>

	<table class="data table data-table">
		<thead>
			<tr>
				<th>Gaji Pokok</th>
				<th>Tunjangan Jabatan</th>
				<th>Berlaku Tanggal</th>
				<th>Status</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
		@foreach($datagaji as $row)
			<tr>
				<td>Rp {{ number_format($row->gaji_pokok) }}</td>
				<td>Rp {{ number_format($row->tunjangan_jabatan) }}</td>
				<td>{{ $row->tgl_berlaku }}</td>
				<td>{{ in_array(date("Y-m-d", strtotime($row->tgl_berlaku)), $curr[$row->id_golongan]) ? 'active' : '' }}</td>
				<th><a href="master/golongan/del-detailgaji/" data-id="{{ $row->id_golongan }}" data-gaji="{{ $row->gaji_pokok }}" data-tunjangan="{{ $row->tunjangan_jabatan }}" data-tgl="{{ $row->tgl_berlaku }}" class="btn btn-sm btn-danger delete-button gol-btn">Hapus</a></th>
			</tr>
		@endforeach
		</tbody>
	</table>

</div>
<script>
	function delete_golongan(ctx){
		$.ajax({
			url : ctx.attr('href'),
			dataType : 'json',
			data : {
				id : ctx.data('id'),
				gaji : ctx.data('gaji'),
				tunjangan : ctx.data('tunjangan'),
				tgl : ctx.data('tgl')
			}
		}).done(function(resp){
			if(resp.type == 'success'){
				alertify.success(resp.message);
				ctx.closest('tr').remove();
			}
			else{
				alertify.error(resp.message);
			}
		});
	}


	$(".delete-button").click(function(e){
		e.preventDefault();
		target = $(this).attr("href");
		var x = confirm('Apakah Anda benar-benar ingin menghapus data ini? Data yang sudah dihapus tidak dapat dikembalikan lagi');

		trparent = $(this).closest("tr");

		if(x == true){
			delete_golongan($(this));
		}
		else{
			return false;
		}
	});

	$(".form-ajax").on("submit", function(e){
		e.preventDefault();
		var formdata = $(this).serialize();
		var target = $(this).attr('action');
		$.ajax({
			url : target,
			type : 'GET',
			dataType : 'json',
			data : formdata
		}).done(function(resp){
			console.log(resp);
			if(resp['type'] == 'error')
				alertify.error(resp['message']);
			if(resp['type'] == 'success')
				alertify.success(resp['message']);
		});
	});
</script>