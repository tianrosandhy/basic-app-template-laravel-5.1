@if (isset($email_report[$id_karyawan]))
    @if ($email_report[$id_karyawan]->is_sent == 0)
    <button type="button" class="btn btn-warning" style="background:#f90; border:none;" title="Email sudah dalam antrian, akan segera dikirimkan dalam beberapa waktu kedepan" data-toggle="tooltip" data-placement="top">Pending</button>
    @elseif ($email_report[$id_karyawan]->is_sent == 1)
    <button type="button" class="btn btn-success" title="Email sudah dikirim ke {{ $email_report[$id_karyawan]->email }}" data-toggle="tooltip" data-placement="top">Sent</button>
    @else
    <button type="button" class="btn btn-danger" title="Email tidak terkirim : {{ strlen($email_report[$id_karyawan]->note) > 0 ? $email_report[$id_karyawan]->note : 'Kesalahan sistem' }}" data-toggle="tooltip" data-placement="top">Failed</button>
    @endif
@else
<em><small class="mute">Not Scheduled Yet</small></em>
@endif
