<div style="min-width:600px">
	
</div>
<h2>Break Management</h2>

<form action="{{ URL::to($action_button) }}" method="post" class="form form-horizontal form-ajax">
    <input type="hidden" name="id" value="{{ $data->id }}">
	<div class="form-group">
		<label class="col-sm-3 control-label">
			Nama Karyawan
		</label>
		<div class="col-sm-9">
			{{ $data->nama }}
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3 control-label">
			Tanggal Masuk
		</label>
		<div class="col-sm-9">
			{{ indo_date($data->tgl_masuk) }}
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-3 control-label">
			Jenis
		</label>
		<div class="col-sm-9">
			<select required name="jenis" class="form-control">
			    <option value="">- Pilih Salah Satu -</option>
			    <option value="break" {{ isset($old['type']) ? $old['type'] == 'break' ? 'selected' : '' : '' }}>Break</option>
			    <option value="cuti hamil" {{ isset($old['type']) ? $old['type'] == 'cuti hamil' ? 'selected' : '' : '' }}>Cuti Hamil</option>
			</select>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-3 control-label">
			Tanggal Mulai Break
		</label>
		<div class="col-sm-9">
			<input type="date" required name="tgl_start" class="form-control" value="{{ Session::get('tgl_start') ? Session::get('tgl_start') : $old['tgl_start'] }}">
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-3 control-label">
			Tanggal Akhir Break
		</label>
		<div class="col-sm-9">
			<input type="date" required name="tgl_end" class="form-control" value="{{ Session::get('tgl_end') ? Session::get('tgl_end') : $old['tgl_end'] }}">
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3 control-label">
			Keterangan
		</label>
		<div class="col-sm-9">
			<textarea name="keterangan" class="form-control">{{ Session::get('keterangan') ? Session::get('keterangan') : $old['description'] }}</textarea>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-9 col-sm-push-3">
			<button name="proses" class='btn btn-primary'>Proses</button>
			<a href="{{ url('mutasi/karyawan/removebreak/'.$data->id) }}" class="btn btn-danger btn-sm">Hapus Break</a>
		</div>
	</div>
</form>