@extends('template/t_panel')

@section('content')
<h2>{{ $title }}</h2>

@if(isset($description))
<div class="alert alert-info">{{ $description }}</div>
@endif


@include('template/t_well')

<table class="data table table-hover">
	<thead>
		<tr>
			<th>Divisi</th>
			<th>Nama Karyawan</th>
			<th>Status Trf</th>
			<th>Take Home Pay</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php $realtotal = 0; ?>
		@foreach($row as $r)
			<?php
			if(isset($gaji[$r->id_karyawan])){
				$realtotal += array_sum($gaji[$r->id_karyawan]);
			}
			?>
		@if(!is_panic() || (is_panic() && $r->status_transfer == 1))
		<tr>
			<td>{{ $r->nama_divisi }}</td>
			<td><a href="detail/{{ $r->id_karyawan }}" data-featherlight="ajax">{{ $r->nama }}</a></td>
			<td>{!! ($r->status_transfer == 1) ? '<span class="label label-success">Transfer</span>' : '<span class="label label-warning">Cash</span>' !!}</td>

			@if(isset($gaji[$r->id_karyawan]))
			<td>Rp {{ number_format(array_sum($gaji[$r->id_karyawan]), 0, ".", ",") }}</td>
			<td>
				<a href="#" class="btn btn-primary" data-show-detail-gaji data-gaji="{{ json_encode($gaji[$r->id_karyawan]) }}" data-total="{{ array_sum($gaji[$r->id_karyawan]) }}">Detail</a>
			</td>
			@else
			<td></td>
			<td></td>
			@endif

		</tr>
		@endif
		@endforeach
	</tbody>
	<tfoot>
		<td></td>
		<td></td>
		<td></td>
		<td><b>Rp {{ number_format($realtotal, 0, ",", ".") }}</b></td>
		<td></td>
	</tfoot>
</table>



<div id="price-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detail Gaji</h4>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
@stop

@push ('script')
<script>
$(function(){
	$(document).on('click', "[data-show-detail-gaji]", function(e){
		e.preventDefault();
		ctn = JSON.parse($(this).attr('data-gaji'));
		$("#price-modal .modal-body").html('');

		$.each(ctn, function(k, v){
			$("#price-modal .modal-body").append('<div class="row"><div class="col-sm-6"><strong>'+k+'</strong></div> <div class="col-sm-6">: Rp '+numberWithCommas(v)+'</div></div>');
		});

		$("#price-modal .modal-body").append('<div class="row" style="margin-top:1em"><div class="col-sm-6">TOTAL</div><div class="col-sm-6">: <strong>Rp '+numberWithCommas($(this).attr('data-total'))+'</strong></div></div>');

		$("#price-modal").modal();
	});
});

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}
</script>
@endpush