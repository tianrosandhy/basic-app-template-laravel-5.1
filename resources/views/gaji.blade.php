@extends('template/t_panel')

@section('content')

<h2>{{ $title }}</h2>


@include('template/t_well')

<table class="data table">
	<thead>
		<tr>
			<th>Divisi</th>
			<th>Nama Karyawan</th>
			<th>Jabatan</th>
			<th>Gaji Pokok</th>
			<th>Tunjangan Jabatan</th>
			@foreach($master_gaji as $msg)
				@if (get_priviledge()[0] >= 3 && in_array(strtolower($msg['label']), config('app.hr_ignore')))
				@else
				<th>
					{{ $msg['label'] }}
					@if($msg['type'] == 1)
						<span class="label label-success">+</span>
					@else
						<span class="label label-danger">-</span>
					@endif
				</th>
				@endif
			@endforeach
			@if (get_priviledge()[0] < 3)
			<th>Total</th>
			@endif
			<th>Tgl Berlaku</th>
			<th class="exclude"></th>
		</tr>
	</thead>
	<tbody>
	@foreach($mutasi as $mt)
		<tr>
			<td>{{ $mt->nama_divisi }}</td>
			<td>{{ $mt->nama }}</td>
			<td>{{ $mt->nama_jabatan }}</td>
			<td>{{ isset($data_gaji[$mt->id_karyawan]['gaji_pokok']) ? floor($data_gaji[$mt->id_karyawan]['gaji_pokok']) : 0 }}</td>
			<td>{{ isset($data_gaji[$mt->id_karyawan]['tunjangan_jabatan']) ? floor($data_gaji[$mt->id_karyawan]['tunjangan_jabatan']) : 0 }}</td>
			@foreach($master_gaji as $msg)
				@if (get_priviledge()[0] >= 3 && in_array(strtolower($msg['label']), config('app.hr_ignore')))
				@else
					<td>
					<?php $decrement = 0; ?>
					@if(isset($data_gaji[$mt->id_karyawan][$msg['id']]))
						{{floor($data_gaji[$mt->id_karyawan][$msg['id']]['value'])}}
					@else
						-
					@endif
				@endif
				</td>
			@endforeach
			
			@if (get_priviledge()[0] < 3)
			<?php
			//manage total
			$totalgaji = isset($data_gaji[$mt->id_karyawan]['total']) ? $data_gaji[$mt->id_karyawan]['total'] : 0;
			if(Auth::user()->priviledge > 1){
				$totalgaji = $totalgaji - $decrement;
			}
			?>
			<td><b>{{ floor($totalgaji) }}</b></td>
			@endif
			<td><span class="label label-default">{{ isset($data_gaji[$mt->id_karyawan]['tgl']) ? substr(indo_date($data_gaji[$mt->id_karyawan]['tgl']), 2) : "" }}</span></td>
			<td class="exclude">
				<a href="{{ $action_button }}&id={{$mt->id_karyawan}}" class="btn btn-primary btn-sm" data-featherlight="ajax"><span class="fa fa-plus"></span> Input Data Gaji</a> <a href="{{ Request::segment(1) }}/{{ Request::segment(2) }}/history/{{ $mt->id_karyawan }}" data-featherlight="ajax" class="btn btn-warning btn-sm"><span class="fa fa-eye"></span> History</a>
			</td>
		</tr>
	@endforeach
	</tbody>
</table>


@stop
