@extends('template/t_panel')

@section('content')
<h2>{{ $title }}</h2>


@if(isset($description))
<div class="alert alert-info">{{ $description }}</div>
@endif


<div class="well">
	<h3>Filter Data</h3>
	<form action="" method="get" class="form form-horizontal">
		<div class="form-group">
			<label class="control-label col-sm-2">
				Tanggal
			</label>
			<div class="col-sm-5">
				<div class="input-group">
					<span class="input-group-btn">
						<a href="mutasi/{{ Request::segment(2) }}?divisi={{ $default['divisi'] }}&tgl={{ date("Y-m-d", strtotime($default['tgl'])-86400 ) }}" class="btn btn-primary">
							<i class="fa fa-caret-left"></i>
						</a>
					</span>
					<input type="date" name="tgl" class="form-control" value="{{ $default['tgl'] }}">
					<span class="input-group-btn">
						<a href="mutasi/{{ Request::segment(2) }}?divisi={{ $default['divisi'] }}&tgl={{ date("Y-m-d", strtotime($default['tgl'])+86400 ) }}" class="btn btn-primary">
							<i class="fa fa-caret-right"></i>
						</a>
					</span>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-2">
				Divisi
			</label>
			<div class="col-sm-5">
				<select name="divisi" class="form-control">
					@foreach($listdiv as $id=>$dv)
					<option value="{{ $id }}" {{ $id == $default['divisi'] ? "selected" : "" }} >{{ $dv }}</option>
					@endforeach
				</select>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3 col-sm-push-2">
				<button class="btn btn-primary">Filter</button>
				<a href="mutasi/presensi" class="btn btn-danger">Reset</a>
			</div>
		</div>
		

	</form>
</div>

<table class="table data table-hover">
	<thead>
		<tr>
			<th>Divisi</th>
			<th nowrap>Nama Karyawan</th>
			@foreach($list_date as $ld)
			<th nowrap {!! $ld == $default['tgl'] ? 'class="act"' : "" !!} ><a href="mutasi/presensi?divisi={{ $default['divisi'] }}&tgl={{ date("Y-m-d", strtotime($ld) ) }}">{{ date("d M Y",strtotime($ld)) }}</a></th>
			@endforeach
		</tr>
	</thead>
	<tbody>
		@foreach($mutasi as $data)
		<tr>
			<td>{{ $data->nama_divisi }}</td>
			<td>{{ $data->nama }}</td>
			@foreach($list_date as $ld)
				<td class="absence" {!! $ld == $default['tgl'] ? "style=\"background:#aaa\"" : "" !!}>
				@if(in_array($ld, $data->tgl))
					{{-- */$index = strtotime($ld)."-".$data->id_karyawan."-".$data->id_divisi; /* --}}
					@if(isset($rencana[$index]))
						@if($rencana[$index]['type'] == "shift")
							@if(isset($content[$index."-masuk"]) or isset($content[$index."-pulang"]))
								<div style="background:#A5E24B">Hadir</div>
							@else

								<select name="{{ $index }}" class="form-control ctrlbox">
									@foreach($kode_absen as $idd=>$kd)
									<option value="{{$idd}}" data-bg="{{$kd['color']}}" {{ isset($tidak_hadir[$data->id_karyawan][$data->id_divisi][strtotime($ld)]) ? ($tidak_hadir[$data->id_karyawan][$data->id_divisi][strtotime($ld)] == $idd) ? "selected" : "" : "" }}>{{ $kd['label'] }}</option>
									@endforeach
								</select>

							@endif

						@else
							<div style="background:{!! $rencana[$index]['bg'] !!}">{{ $kode_absen[$rencana[$index]['pk']]['label'] }}</div>
						@endif
					@else
						<small><em><a href="mutasi/jadwal?bulan={{date("n", strtotime($ld))}}&tahun={{date("Y", strtotime($ld))}}">Rancangan jadwal</a> belum dibuat</em></small>
					@endif
				@endif
				</td>
			@endforeach
		</tr>
		@endforeach
	</tbody>
</table>


<script>
$(function(){
	$(".timepicker").timepicker({
		timeFormat : 'H:i',
		step : 1,
	});


	$('body').on('change', ".jam_masuk, .jam_pulang", function(){
		var uniq = $(this).attr("data-uniq");
		var vall = $(this).val();
		vartgl = $(this).attr("data-tanggal");
		varkar = $(this).attr("data-karyawan");
		vardiv = $(this).attr("data-divisi");
		vartype = $(this).attr("data-type");

		varthis = $(this);

		$.ajax({
			url : "api/presensi",
			method : "GET",
			dataType : "json",
			data : {
				uniq : uniq,
				id_karyawan : varkar,
				id_divisi : vardiv,
				tanggal : vartgl, 
				type : vartype,
				nilai : vall
			}
		}).done(function(data){
			console.log(data);
			varthis.removeClass("fill err");
			varthis.addClass(data['bg']);

			if(data['error'])
				alertify.error(data['error'], 1);
			if(data['success'])
				alertify.success(data['success'], 1);
		});

		$(this).removeClass("fill");
		$(this).addClass("fill");


	});

	$(".ctrlbox").each(function(){
		if($(this).val() > 0){
			inx = $(this).val();
			bg = $(this).children("option[value="+inx+"]").attr("data-bg");
			$(this).css("border", "3px solid "+bg);
		}
	});

	$('body').on('change', ".ctrlbox", function(){
		//hai ajax
		val = $(this).val();
		uniq = $(this).attr("name");
		ini = $(this);

		$.ajax({
			url : "api/tidakhadir",
			dataType : "json",
			method : "POST",
			data : {
				dataval : val,
				datauniq : uniq
			}
		}).done(function(data){

			bg = ini.children("option[value="+val+"]").attr("data-bg");
			ini.css("border", "3px solid "+bg);

			if(data['error'])
				alertify.error(data['error'], 1);
			if(data['success'])
				alertify.success(data['success'], 1);
		});


	});

});
</script>

@stop