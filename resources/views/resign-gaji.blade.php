@extends ('template/t_panel')
@section ('content')

<h2>Data Bonus Terakhir Karyawan Resign</h2>

<table class="data table-hover">
	<thead>
		<tr>
			<th>Nama Karyawan</th>
			<th>Divisi</th>
			<th>Bonus Terakhir</th>
			<th>Per Tanggal</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
	@foreach($data_resign as $row)
		@if(isset($current_mutasi[$row->id_karyawan]))
		<tr>
			<td>{{ $row->karyawan->nama }}</td>
			<td>{{ $current_mutasi[$row->id_karyawan]->getDivisi->nama_divisi }}</td>
			<td>{{ isset($data_gaji[$row->id_karyawan]) ? rupiah($data_gaji[$row->id_karyawan]) : '-' }}</td>
			<td>{{ isset($data_tanggal[$row->id_karyawan]) ? indo_date($data_tanggal[$row->id_karyawan]) : '-' }}</td>
			<td><a data-id="{{ $row->id_karyawan }}" data-nama="{{ $row->karyawan->nama }}" data-divisi="{{ $current_mutasi[$row->id_karyawan]->nama_divisi }}" data-bonus="{{ isset($data_gaji[$row->id_karyawan]) ? $data_gaji[$row->id_karyawan] : 0 }}" data-tanggal="{{ isset($data_tanggal[$row->id_karyawan]) ? $data_tanggal[$row->id_karyawan] : date('Y-m-d') }}" data-toggle="modal" class="btn btn-primary btn-set-gaji" data-target="#modalGaji">Set Gaji</a></td>
		</tr>
		@endif
	@endforeach
	</tbody>
</table>


<div id="modalGaji" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">

        	<form method="post" action="{{ url('gaji/resign/store') }}" class="ajax-form">
        		{{ csrf_field() }}
        		<input type="hidden" name="id_karyawan">
        		<div class="modal-header">
	        		<h3>Set Bonus Terakhir Karyawan</h3>
        		</div>

        		<div class="modal-body">
	        		<div class="row">
	        			<div class="col-sm-3">
	        				<label>Nama Karyawan</label>
	        			</div>
	        			<div class="col-sm-9">
	        				<span class="holder-nama"></span>
	        			</div>
	        		</div>
	        		<div class="row">
	        			<div class="col-sm-3">
	        				<label>Divisi</label>
	        			</div>
	        			<div class="col-sm-9">
	        				<span class="holder-divisi"></span>
	        			</div>
	        		</div>
	        		<div class="row">
	        			<div class="col-sm-3">
	        				<label>Bonus Terakhir</label>
	        			</div>
	        			<div class="col-sm-9">
	        				<input type="number" class="form-control" name="bonus" value="">
	        			</div>
	        		</div>
	        		<div class="row">
	        			<div class="col-sm-3">
	        				<label>Berlaku per Tanggal</label>
	        			</div>
	        			<div class="col-sm-9">
	        				<input type="date" class="form-control" name="tgl_berlaku" value="">
	        			</div>
	        		</div>
        		</div>

        		<div class="modal-footer">
        			<button class="btn btn-primary">Proses</button>
        		</div>

            </form>
        </div>
    </div>
</div>


<script>
	$(function(){
		$('body').on('click', ".btn-set-gaji", function(){
			$("input[name='id_karyawan']").val($(this).attr('data-id'));
			$("span.holder-nama").html($(this).attr('data-nama'));
			$("span.holder-divisi").html($(this).attr('data-divisi'));
			$("input[name='bonus']").val($(this).attr('data-bonus'));
			$("input[name='tgl_berlaku']").val($(this).attr('data-tanggal'));
		});


		$("form.ajax-form").on('submit', function(e){
			e.preventDefault();
			$data = $(this).serialize();
			$.ajax({
				url : '{{ url('gaji/resign/store') }}',
				dataType : 'json',
				data : $data,
				type : 'POST'
			}).done(function(resp){
				if(resp.type == 'success'){
					alertify.success(resp.message);
				}
				if(resp.type == 'error'){
					alertify.error(resp.message);
				}

				location.reload();
			});
		})
	});
</script>

@stop