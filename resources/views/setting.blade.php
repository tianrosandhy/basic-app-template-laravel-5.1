@extends ('template/t_panel')
@section ('content')



<h2>{{ $title }}</h2>

<div class="panel panel-primary">
	<div class="panel-heading">User Management Setting</div>
	<div class="panel-body">
		<a href="setting/user" class="btn btn-primary">
			<span class="fa fa-plus"></span>
			Tambah User
		</a>
		<table class="table">
			<thead>
				<tr>
					<th>Username</th>
					<th>Email</th>
					<th>Created at</th>
					<th>Last Update</th>
					<th>Level</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
			@if(count($users) > 0)
			@foreach($users as $user)
				<tr>
					<td>{{ $user->username }}</td>
					<td>{{ $user->email }}</td>
					<td>{{ $user->created_at }}</td>
					<td>{{ $user->updated_at }}</td>
					<td><span class="label label-primary">{{ $user->priviledge > 0 ? (isset($list_priviledge[$user->priviledge]) ? $list_priviledge[$user->priviledge] : 'Demo User') : 'Super Admin' }}</span></td>
					<td>
						<a href="setting/update/{{ $user->id }}" class="btn btn-primary">Update user</a>
						<a href="setting/destroy/{{ $user->id }}" class="btn btn-danger delete-button"><span class="fa fa-times"></span> Hapus</a>
					</td>
				</tr>
			@endforeach
			@endif
			</tbody>
		</table>
	</div>
</div>


@if($curr[0] <= 1)

<form action="setting/store" method="post">
	@foreach($param as $tag=>$data)
	<div class="panel panel-primary">
		<div class="panel-heading">Pengaturan {{ ucfirst($tag) }}</div>
		<div class="panel-body">
			@foreach($data as $label => $row)
			<div class="row space">
				<div class="col-sm-3">
					<b>{{ $row['label'] }}</b>
				</div>
				<div class="col-sm-9">
					<input type="text" class="form-control" name="setting[{{$row['id']}}]" placeholder="Default : {{$row['def_value']}}" value="{{ $row['value'] }}">
				</div>
			</div>
			@endforeach
		</div>
	</div>
	@endforeach

	<div align="center" class="space">
		<button class="btn btn-lg btn-primary">
			<span class="fa fa-save"></span>
			Simpan
		</button>
	</div>
</form>

@endif


<form action="setting/backuprestore" method="post" enctype="multipart/form-data">
	<div class="panel panel-primary">
		<div class="panel-heading">Backup & Restore Database</div>
		<div class="panel-body">
			<ul class="nav nav-tabs">
			  <li class="active"><a data-toggle="tab" href="#backup">Backup</a></li>
			  <li><a data-toggle="tab" href="#restore">Restore</a></li>
			</ul>

			<div class="tab-content">
			  <div id="backup" class="tab-pane fade in active">
			  	<h3><strong>List Modul Backup</strong></h3>
			  	<?php $n = 0 ;?>
			  	<div class="row">
			  		<div class="col-sm-6">
			  			
			  	@foreach($backup_list as $md_name => $back_module)
			  		@if($backup_priv[$md_name] >= $curr[0])
			  		<fieldset>
			  			<legend>
			  				{{ ucwords($md_name) }}
			  				<small>
				  				<label class="badge badge-default">
				  					<input type="checkbox" data-group-target="{{ $md_name }}"> Pilih Semua
				  				</label>
			  				</small>
			  			</legend>

			  			@foreach($back_module as $mdname)
			  			<label class="d-block">
			  				<input type="checkbox" data-group="{{ $md_name }}" name="module[{{$mdname}}]">
			  				{{$mdname}}
			  			</label>
			  			@endforeach
			  		</fieldset>
			  		<?php
			  		$n++;
			  		if($n == 1){echo "</div><div class='col-sm-6'>";}
			  		?>
			  		@endif
			  	@endforeach
			  		</div>
			  	</div>
			  	<div align="center">
			  		<button class="btn btn-primary" name="backup" value="backup">Backup Database</button>
			  	</div>
			  </div>
			  <div id="restore" class="tab-pane fade">
		  		<input type="file" accept=".backup" name="restore_file" class="form-control">
		  		<button class="btn btn-primary" name="restore" value="restore">Restore</button>

			  </div>
			</div>			
		</div>
	</div>
</form>




@if(Session::has('list_restore'))
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
        	<form method="post" action="setting/restore" enctype="multipart/form-data">
        		<input type="hidden" name="tb_list" value="{{ Session::get('list_restore') }}">
        		<input type="hidden" name="file_restore" value="{{ Session::get('file_restore') }}">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                <h4 class="modal-title">Restore Data?</h4>
	            </div>
	            <div class="modal-body">
	                <p>Nama tabel dibawah ini adalah nama-nama tabel yang akan direstore. Seluruh data-data tabel bersangkutan yang belum dibackup akan dihapus, dan digantikan dengan data backupan yang akan direstore. </p>

	                @foreach(json_decode(Session::get('list_restore')) as $tbname)
	                	<span class="label label-primary">{{ $tbname }}</span>
	                @endforeach

	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                <button type="submit" name="restore_action" value="restore_action" class="btn btn-primary">Lanjutkan Restore Database</button>
	            </div>
            </form>
        </div>
    </div>
</div>
@endif

<script>
	$(function(){
		$("#myModal").modal('show');

		run_check();
		$("input[data-group]").change(function(){
			run_check();
		});

		function run_check(){
			$("input[data-group-target]").each(function(){
				nmgrup = $(this).data("group-target");
				curr = true;
				$("input[data-group='"+nmgrup+"']").each(function(){
					if(!this.checked)
						curr = false;
				});
				$(this).prop('checked', curr);
			});
		}

		$('input[data-group-target]').change(function(){
			grpname = $(this).data('group-target');
			$('input[data-group="'+grpname+'"]').prop('checked', this.checked);
		});

	});
</script>

@stop