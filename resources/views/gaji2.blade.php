@extends('template/t_panel')

@section('content')

<h2>{{ $title }}</h2>


@include('template/t_well')

<table class="data table">
	<thead>
		<tr>
			<th>Divisi</th>
			<th>Nama Karyawan</th>
			<th>Jabatan</th>
			@foreach($master_gaji as $msg)
				<th>
					{{ $msg['label'] }}
					@if($msg['type'] == 1)
						<span class="label label-success">+</span>
					@else
						<span class="label label-danger">-</span>
					@endif
				</th>
			@endforeach
			<th>Total</th>
			<th>Tgl Berlaku</th>
			<th class="exclude"></th>
		</tr>
	</thead>
	<tbody>
	@foreach($mutasi as $mt)
		<tr>
			<td>{{ $mt->nama_divisi }}</td>
			<td>{{ $mt->nama }}</td>
			<td>{{ $mt->nama_jabatan }}</td>
			@foreach($master_gaji as $msg)
				<td>
				@if(isset($data_gaji[$mt->id_karyawan][$msg['id']]))
					{{ floor($data_gaji[$mt->id_karyawan][$msg['id']]['value']) }}
				@else
					-
				@endif
				</td>
			@endforeach
			<td><b>{{ isset($data_gaji[$mt->id_karyawan]['total']) ? floor($data_gaji[$mt->id_karyawan]['total']) : 0 }}</b></td>
			<td><span class="label label-default">{{ isset($data_gaji[$mt->id_karyawan]['tgl']) ? substr(indo_date($data_gaji[$mt->id_karyawan]['tgl']), 2) : "" }}</span></td>
			<td class="exclude">
				<a href="{{ $action_button }}&id={{$mt->id_karyawan}}" class="btn btn-primary btn-sm" data-featherlight="ajax"><span class="fa fa-plus"></span> Input Data Gaji</a> <a href="{{ Request::segment(1) }}/{{ Request::segment(2) }}/history/{{ $mt->id_karyawan }}" data-featherlight="ajax" class="btn btn-warning btn-sm"><span class="fa fa-eye"></span> History</a>
			</td>
		</tr>
	@endforeach
	</tbody>
</table>


@stop
