<div style="margin: 32px; height: 1px; background: #f5f5f5"></div>
<p style="margin-bottom: 16px;">
	@if(isset($footer_text))
		{!! $footer_text !!}
	@else
	Apabila ada kesulitan atau ada yang ingin ditanyakan, silakan menghubungi Admin / tim Human Resource kembali. Terima kasih.
	@endif
</p>
