<!DOCTYPE html>
<html>
<head>
	@include ('mail.partials.metadata')
</head>
<body>
	<table width="500" style="margin: 32px auto; border: solid 1px #f5f5f5; box-shadow: 4px 4px 8px rgba(0,0,0,.05); color: #333333; border-radius: 8px">
		@include ('mail.partials.header')
		<tr>
			<td style="padding: 0 16px">
				<table>
					<tr>
						<td>
							<div class="content">
								@if(isset($title))
								<strong>{{ $title }}</strong>
								<br>
								@endif

								@if(isset($content))
									{!! $content !!}
								@endif
							</div>

							@include ('mail.partials.footer')
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<table width="500" style="margin: auto;">
		<tr>
			<td>
				<p style="text-align: center; border-top: solid 1px #f5f5f5; color: #999999; margin-bottom: 32px; padding-top: 32px;">
					Copyright &copy; PT Citra Warna Jaya Abadi {{ date('Y') }}
				</p>
			</td>
		</tr>
	</table>
</body>
</html>

