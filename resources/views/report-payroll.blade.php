@extends('template/t_panel')

@section('content')
<h2>{{ $title }}</h2>

<div class="well">
	<h3>Filter Data</h3>
	<form action="" method="get">
		<div class="row">
			<div class="col-sm-3">Bulan</div>
			<div class="col-sm-6">
				<select name="bulan" id="" class="form-control">
					@foreach(nama_bulan() as $idbul => $nmbul)
					<option value="{{ $idbul }}" {{ $default['bulan'] == $idbul ? 'selected' : '' }} >{{ $nmbul }}</option>
					@endforeach
				</select>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3">Tahun</div>
			<div class="col-sm-6">
				<input type="number" name="tahun" min=1970 max=2100 class="form-control" value="{{ $default['tahun'] }}">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3"></div>
			<div class="col-sm-6">
				<button class="btn btn-primary btn-generate">Generate</button>
				<a href="report/amy" class="btn btn-danger">Refresh</a>
			</div>
		</div>
	</form>
</div>

<div class="download_btn" align="center" style="display:none;">
	<a href="" target="_blank" class="btn btn-primary btn-lg">Download Report</a>
</div>

<script>
$(function(){
	$(".btn-generate").on('click', function(e){
		e.preventDefault();
		formdata = $('form').serialize();
		$.ajax({
			url : '{{ url('report/payroll/excel') }}',
			type : 'GET',
			dataType : 'json',
			data : formdata
		}).done(function(resp){
			$(".download_btn").slideDown();
			$(".download_btn a").attr('href', resp);
		});
	});

	$(".download_btn a").on('click', function(){
		$(".download_btn").slideUp();
	});
});	
</script>
@endsection