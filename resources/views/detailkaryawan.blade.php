
<div class="container">
	
	<div class="row">
		<div class="col-sm-6">
			<div class="panel panel-primary">
				<div class="panel-heading"><h3>Informasi Detail Karyawan</h3></div>
				<div class="panel-body">
					<table class="dt-table">
						<tr>
							<th>Nama Karyawan</th>
							<td> : </td>
							<td>{{ isset($data['nama']) ? $data['nama'] : "" }}</td>
						</tr>
						<tr>
							<th>TTL</th>
							<td> : </td>
							<td>{{ isset($data['ttl']) ? $data['ttl'] : "" }}</td>
						</tr>
						<tr>
							<th>Jenis Kelamin</th>
							<td> : </td>
							<td>{{ isset($data['jk']) ? $data['jk'] : "" }}</td>
						</tr>
						<tr>
							<th>Alamat</th>
							<td> : </td>
							<td>{{ isset($data['alamat']) ? $data['alamat'] : "" }}</td>
						</tr>
						<tr>
							<th>Telepon</th>
							<td> : </td>
							<td>{{ isset($data['telp']) ? $data['telp'] : "" }}</td>
						</tr>
						<tr>
							<th>Tanggal Masuk</th>
							<td> : </td>
							<td>{{ isset($data['tgl_masuk']) ? $data['tgl_masuk'] : "" }}</td>
						</tr>
					</table>
				</div>
			</div>			


			@if(Auth::user()['priviledge'] <= 1)
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3>Detail Data Gaji Karyawan</h3>
				</div>
				<div class="panel-body">
					<table>
						<tr>
							<td>Berlaku Per Tanggal</td>
							<td> : </td>
							<td>{{ indo_date($tgl_gaji, "half") }}</td>
						</tr>

						@foreach($gaji as $lbl => $nilai)
						<tr>
							<td>{{ $lbl }}</td>
							<td></td>
							<td>{{ rupiah($nilai) }}</td>
						</tr>
						@endforeach

						<tr>
							<td>Total Gaji</td>
							<td> : </td>
							<td><strong>{{ rupiah($total_gaji) }}</strong></td>
						</tr>
					</table>
				</div>

			</div>
			@endif

		</div>
		<div class="col-sm-6">

			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3>Rekap Mutasi Karyawan</h3>
				</div>
				<div class="panel-body">
					<table class="data table">
						<thead>
							<tr>
								<th>No</th>
								<th>Tgl Berlaku</th>
								<th>Divisi</th>
								<th>Jabatan</th>
							</tr>
						</thead>
						<tbody>
							@foreach($mutasi as $row)
							<tr>
								<td>{{ $row['no'] }}</td>
								<td>{{ $row['tgl_berlaku'] }}</td>
								<td>{{ $row['nama_divisi'] }}</td>
								<td>{{ $row['nama_jabatan'] }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>		
				</div>
			</div>

		</div>
	</div>



</div>