@extends('template/t_panel')

@section('content')
<h2>{{ $title }}</h2>

@if(isset($description))
<div class="alert alert-info">{{ $description }}</div>
@endif


@include('template/t_well')

<div class="space" align="center">
	@if (get_priviledge()[0] < 3)
	<a href="gaji/slip?tahun={{$default['tahun']}}&bulan={{$default['bulan']}}&divisi={{$default['divisi']}}" class="btn btn-primary" target="_blank">
		<span class="fa fa-print"></span>
		Cetak Slip Gaji Lama
	</a>
	@endif
    <a href="#" data-toggle="modal" data-target="#modal-gaji" class="btn btn-warning">
        <span class="iconify" data-icon="ant-design:mail-twotone"></span>
        Blast Slip Gaji ke Email
    </a>
	@if (get_priviledge()[0] < 3)
	<a href="gaji/excel-slip?tahun={{$default['tahun']}}&bulan={{$default['bulan']}}&divisi={{$default['divisi']}}" class="btn btn-info" target="_blank">
		<span class="iconify" data-icon="bi:file-earmark-excel-fill"></span>
		Download Laporan Gaji
	</a>
	@endif
</div>

@if ($email_report_summary[0] > 0 || $email_report_summary[1] > 0 || $email_report_summary[9] > 0)
    <!-- There is pending email sending -->
    <?php
    $pending_width = $email_report_summary[0] / $email_report_summary['total'] * 100;
    $sent_width = $email_report_summary[1] / $email_report_summary['total'] * 100;
    $failed_width = $email_report_summary[9] / $email_report_summary['total'] * 100;
    ?>
    <div class="well">
        <strong>Email Blast Summary Information</strong>
        <div class="progress">
            @if ($pending_width > 0)
            <div class="progress-bar bg-warning progress-bar-striped progress-bar-animated" role="progressbar" style="width: {{ $pending_width }}%" aria-valuenow="{{ $pending_width }}" aria-valuemin="0" aria-valuemax="100">Pending ({{ $email_report_summary[0] }} / {{ $email_report_summary['total'] }})</div>
            @endif
            @if ($sent_width > 0)
            <div class="progress-bar bg-success progress-bar-striped progress-bar-animated" role="progressbar" style="width: {{ $sent_width }}%" aria-valuenow="{{ $sent_width }}" aria-valuemin="0" aria-valuemax="100">Sent ({{ $email_report_summary[1] }} / {{ $email_report_summary['total'] }})</div>
            @endif
            @if ($failed_width > 0)
            <div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" role="progressbar" style="width: {{ $failed_width }}%" aria-valuenow="{{ $failed_width }}" aria-valuemin="0" aria-valuemax="100">Failed ({{ $email_report_summary[9] }} / {{ $email_report_summary['total'] }})</div>
            @endif
        </div>

        <table style="margin-bottom:1em;">
            <tr>
                <td style="padding:0.25em 0.5em;">Pending</td>
                <td style="padding:0.25em 0.5em;"> : </td>
                <td style="padding:0.25em 0.5em;">{{ $email_report_summary[0] }} email</td>
            </tr>
            <tr>
                <td style="padding:0.25em 0.5em;">Sent Successfully</td>
                <td style="padding:0.25em 0.5em;"> : </td>
                <td style="padding:0.25em 0.5em;">{{ $email_report_summary[1] }} email</td>
            </tr>
            <tr>
                <td style="padding:0.25em 0.5em;">Failed</td>
                <td style="padding:0.25em 0.5em;"> : </td>
                <td style="padding:0.25em 0.5em;">{{ $email_report_summary[9] }} email</td>
            </tr>
        </table>

        <small class="alert alert-info" style="margin-top:1em;">Data ini akan terus diupdate apabila email sudah dikirimkan sesuai antrian yang berjalan</small>
    </div>

@endif

<table class="data table table-hover">
	<thead>
		<tr>
			<th>Cabang</th>
			<th>Nama Karyawan</th>
			<th>Departemen</th>
			<th>Status Trf</th>
			<th>Presensi</th>
			@if (get_priviledge()[0] < 3)
			<th>Total Gaji</th>
			<th>Potongan</th>
			<th>Take Home Pay</th>
			@endif
            <th>Email Sent</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php $realtotal = 0; ?>
		@if(!is_panic())
		@foreach($data_resign as $dt)
		<?php
			$realtotal += $dt->bonus;
		?>
		<tr>
			<td>- resigned -</td>
			<td>{{ $dt->getKaryawan->nama }}</a></td>
			<td>{{ isset($dt->getKaryawan->departemen) ? $dt->getKaryawan->departemen : null }}</td>
			<td><span class="label label-warning">Cash</span></td>

			@if(isset($gaji[$dt->id_karyawan]))
				<td>{{ $gaji[$dt->id_karyawan]['bobot'] }} / {{ $gaji[$dt->id_karyawan]['batas_bobot'] }}</td>
				
				@if (get_priviledge()[0] < 3)
				<td>{{ floor($gaji[$dt->id_karyawan]['surplus']) }}</td>
				<td>{{ floor($gaji[$dt->id_karyawan]['defisit']) }}</td>
				<td>{{ floor($gaji[$dt->id_karyawan]['total']) }}</td>
				@endif
				<td>
					@include ('partials.email-report-button', ['id_karyawan' => $dt->id_karyawan])
				</td>
				<td>
					@if (get_priviledge()[0] < 3)
					<a target="_blank" href="gaji/slip-online/{{$dt->id_karyawan}}/{{$default['tahun']}}/{{$default['bulan']}}?without_password=1" class="btn btn-danger">e-PaySlip</a>
					@endif
					<a href="{{ route('gaji.blast-single', ['id' => $dt->id_karyawan, 'tahun' => $default['tahun'], 'bulan' => $default['bulan'] ]) }}" class="btn btn-primary">Kirim ke Email Manual</a>
				</td>
			@else
				<td></td>
				@if (get_priviledge()[0] < 3)				
					<td></td>
					<td></td>
					<td></td>
				@endif
				<td></td>
				<td></td>
			@endif

		</tr>		
		@endforeach
		@endif


		@foreach($row as $r)
			<?php
			if(isset($gaji[$r->id_karyawan])){
				$realtotal += $gaji[$r->id_karyawan]['total'];	
			}
			?>
		@if(!is_panic() || (is_panic() && $r->status_transfer == 1))
		<tr>
			<td>{{ $r->nama_divisi }}</td>
			<td>
                @if (strlen($r->email) == 0)
                <span title="Belum ada email" class="iconify text-danger" data-icon="fluent:mail-warning-24-filled"></span>
                @else
                <span title="Bisa kirim via email" class="iconify text-success" data-icon="fluent:mail-24-filled"></span>
                @endif
                <a href="detail/{{ $r->id_karyawan }}" data-featherlight="ajax">{{ $r->nama }}</a>
            </td>
			<td>{{ isset($r->departemen) ? $r->departemen : '-' }}</td>
			<td>{!! ($r->status_transfer == 1) ? '<span class="label label-success">Transfer</span>' : '<span class="label label-warning">Cash</span>' !!}</td>

			@if(isset($gaji[$r->id_karyawan]))
				<td>{{ $gaji[$r->id_karyawan]['bobot'] }} / {{ $gaji[$r->id_karyawan]['batas_bobot'] }}</td>
				@if (get_priviledge()[0] < 3)
				<td>{{ floor($gaji[$r->id_karyawan]['surplus']) }}</td>
				<td>{{ floor($gaji[$r->id_karyawan]['defisit']) }}</td>
				<td>{{ floor($gaji[$r->id_karyawan]['total']) }}</td>
				@endif
				<td>
					@include ('partials.email-report-button', ['id_karyawan' => $r->id_karyawan])            
				</td>
				<td>
					@if (get_priviledge()[0] < 3)
					<a target="_blank" href="gaji/slip-online/{{$r->id_karyawan}}/{{$default['tahun']}}/{{$default['bulan']}}?without_password=1" class="btn btn-danger">e-PaySlip</a>
					@endif
					<a href="{{ route('gaji.blast-single', ['id' => $r->id_karyawan, 'tahun' => $default['tahun'], 'bulan' => $default['bulan'] ]) }}" class="btn btn-primary">Kirim ke Email Manual</a>
				</td>
			@else
				<td></td>
				@if (get_priviledge()[0] < 3)
					<td></td>
					<td></td>
					<td></td>
				@endif
				<td></td>
				<td></td>
			@endif

		</tr>
		@endif
		@endforeach
	</tbody>
	<tfoot>
		<td></td>
		<td></td>
		<td></td>
		@if (get_priviledge()[0] < 3)
			<td></td>
			<td></td>
			<td><b>{{ floor($realtotal) }}</b></td>
		@endif
        <td></td>
		<td></td>
	</tfoot>
</table>

<div class="modal fade" id="modal-gaji">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <span>Anda yakin ingin melakukan blast Slip Gaji?</span>
                <button class="button close" type="button">&times;</button>
            </div>
            <div class="modal-body">
                <p>Apakah Anda yakin ingin melakukan blast slip gaji ke seluruh email karyawan?</p>
                <p>Jika data slip gaji sudah siap, maka schedule pengiriman email slip gaji akan dijalankan secara bertahap.</p>
            </div>
            <div class="modal-footer">
                <a href="{{ route('gaji.blast', ['tahun' => $default['tahun'], 'bulan' => $default['bulan']]) }}" class="btn btn-success">Ya, Blast Email Sekarang</a>
                <a href="#" data-dismiss="modal" class="btn btn-secondary">Tidak, Batalkan Blast</a>                
            </div>
        </div>
    </div>
</div>

@stop