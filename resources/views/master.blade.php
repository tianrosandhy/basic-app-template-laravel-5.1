@extends('template/t_panel')

@section('content')
<h2>{{ $title }}</h2>

@if(isset($back_button))
	<a href="javascript:history.go(-1);" class="btn btn-default btn-sm">
		<span class="fa fa-caret-left"></span>
		Back
	</a>
@endif

@if(isset($description))
<div class="alert alert-info">{{ $description }}</div>
@endif



@if(isset($action_button))
<div class="padd" style="padding:1em 0;">
	<a href="{{ URL::to($action_button) }}" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>	
</div>
@endif

@if(isset($getform))
<div class="well">
	<h3>{{ $getform['title'] }}</h3>
	<form action="" method="get" class="form form-horizontal">
		@foreach($getform['form'] as $data)
			<div class="form-group">
				<label class="control-label col-sm-2">{{ $data['label'] }}</label>
				<div class="col-sm-10">
				@if(!isset($data['value']))
					{!! input_helper($data['label'], $data, Session::get($data['field'])) !!}
				@else
					{!! input_helper($data['label'], $data, $data['value']) !!}
				@endif
				</div>
			</div>
		@endforeach
		<div class="form-group">
			<div class="col-sm-10 col-sm-push-2">
				<button class="btn btn-warning">
					<i class="fa fa-filter"></i> Filter
				</button>
				@if(isset($getform['url']))
				<a href="{!! $getform['url'] !!}" class="btn btn-danger">Reset</a>
				@endif
			</div>
		</div>
	</form>
</div>
@endif



<table class="data table table-hover">
	<thead>
		<tr>
		@foreach($table as $label => $data)
			@if($data['table'] == true)
				<th>{{ $label }}</th>
			@endif
		@endforeach
		</tr>
	</thead>
	<tbody>
		@foreach($content as $data)
		<tr>
			@foreach($table as $field)
				@if($field['table'] == true)
					<td>{!! isset($data[$field['field']]) ? $data[$field['field']] : "" !!}</td>
				@endif
			@endforeach
		</tr>
		@endforeach
	</tbody>
</table>
<script>
var BASE_URL = '{{ url('/') }}';
$(function(){
	initSwitchery();
	$(document).on('change', "[data-switchery]", function(e){
		val = $(this).prop('checked');
		$.ajax({
			url : window.BASE_URL + '/switch',
			type : 'POST',
			dataType : 'json',
			data : {
				table : $(this).attr('data-table'),
				field : $(this).attr('data-field'),
				id : $(this).attr('data-id'),
				value : val ? 1 : 0
			},
			success : function(resp){
				console.log(resp);
			},
			error : function(resp){
				console.log(resp);
			}
		});
	});
});
function initSwitchery(){
	if($("[data-switchery]").length){
		$("[data-switchery]").each(function(){
			el = $(this).get(0);
			new Switchery(el);
		});
	}
}
</script>

@if(isset($script))
<script>
	$(function(){
		$("body").on("keypress", ".ajaxBx", function(e){
			if(e.which == 13){
				uniq = $(this).attr("data-uniq");
				isi = $(this).val();
				type = $(this).attr("data-type");

				run_reason(uniq, isi, type);
			}
		});
		$("body").on('click', '.ajaxBtn', function(e){
			e.preventDefault();
			uniq = $(this).closest("span").prev("input").attr("data-uniq");
			isi = $(this).closest("span").prev("input").val();
			type = $(this).closest("span").prev("input").attr("data-type");
			run_reason(uniq, isi, type);
		});
		$("body").on('change', '.ajaxChk', function(e){
			pro = $(this).prop("checked");
			if(pro)
				val = 1;
			else
				val = 0;
			uniq = $(this).attr("data-uniq");
			type = $(this).attr("data-type");

			run_reason(uniq, val, type);

		});


		function run_reason(uniq, content, dttype){
			$.ajax({
				url : "api/terlambat",
				method : "POST",
				dataType : "json",
				data : {
					datauniq : uniq,
					datacontent : content,
					type : dttype
				}
			}).done(function(data){
				if(data['success']){
					alertify.success(data['success'], 1);
				}
				if(data['error']){
					alertify.error(data['error'], 1);
				}
			});
		}


		$("table").on("change", '.kode_box', function(){
			dttype = $(this).attr('data-type');
			dtkary = $(this).attr('data-karyawan');
			id_kode = $(this).val();

			$.ajax({
				url : 'api/kode',
				method : 'GET',
				dataType : 'json',
				data : {
					type : dttype,
					karyawan : dtkary,
					kode : id_kode
				}
			}).done(function(data){
				console.log(data);
				if(data['success']){
					alertify.success(data['success'], 1);
				}
				if(data['error']){
					alertify.error(data['error'], 1);
				}
			});
		});


		//delegate change ajax from table
		$("table").on("change", '.ajaxLbr', function(){
			dtuniq = $(this).attr("data-uniq");
			dttype = $(this).attr("data-type"); //ga kepake
			dtval = parseInt($(this).val());

			max = parseInt($(this).attr("max"));
			if(dtval > max){
				alertify.alert("Invalid Operation", "Realisasi jam lembur yang diizinkan tidak boleh melewati batas lembur yang tercatat oleh sistem" + dtval + " > " + max);
				$(this).val($(this).attr("data-default"));
			}
			else{
				$.ajax({
					url : "api/lembur",
					method : "GET",
					dataType : "json",
					data : {
						uniq : dtuniq,
						type : dttype,
						value : dtval
					}
				}).done(function(data){
					console.log(data);
					if(data['success']){
						alertify.success(data['success'], 1);
					}
					if(data['error']){
						alertify.error(data['error'], 1);
					}
				});
			}


		});

		$("table").on("change", ".ajaxChose", function(){
			dtval = $(this).val();
			dtuniq = $(this).attr("data-uniq");
			dttype = "type";
			$.ajax({
				url : "api/lembur",
				method : "GET",
				dataType : "json",
				data : {
					uniq : dtuniq,
					type : dttype,
					value : dtval
				}
			}).done(function(data){
				console.log(data);
				if(data['success']){
					alertify.success(data['success'], 1);
				}
				if(data['error']){
					alertify.error(data['error'], 1);
				}
			});
		});
        
        
        $("body").on("change", "select.status_box", function(){
            targ = $(this).data('id');
            val = $(this).val();
            console.log([targ, val]);
        });
        
        
        $("body").on("submit", "form.form-ajax", function(e){
            e.preventDefault();
            form = $(this);
            
            $.ajax({
                url : form.attr('action'),
                type : form.attr('method'),
                data : form.serialize(),
                dataType : 'json'
            }).done(function(dt){
                console.log(dt);
                if(dt['error']){
                    console.log(dt['error']);
                }
                if(dt['success']){
                    alertify.success(dt['success']);
                    var current = $.featherlight.current();
                        current.close();
                }
            });
        });
	});
</script>
@endif

@stop


