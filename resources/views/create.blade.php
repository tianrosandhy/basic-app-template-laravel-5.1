@extends('template/t_panel')

@section('content')
<a href="javascript:history.go(-1)" class="btn btn-default btn-sm">
	<span class="fa fa-caret-left"></span>
	Back
</a>
<h2>{{ $title }}</h2>

@if(isset($info))
	<div class="alert alert-info">
		{{ $info }}
	</div>
@endif


<form action="{{ URL::to($action_button) }}{{ isset($_GET['page']) ? '?page='.$_GET['page'] : '' }}" method="post" class="form-horizontal">
	{{ csrf_field() }}

	@foreach($form as $label => $data)
		@if($data['form'])
		<div class="form-group">
			<label class="control-label col-sm-2">{{ $label }}</label>
			<div class="col-sm-10">
			@if(!isset($default_value))
				{!! input_helper($label, $data, Session::get($data['field'])) !!}
			@else
				{!! input_helper($label, $data, $default_value[$data['field']]) !!}
			@endif
			</div>
		</div>
		@endif
	@endforeach
		<div class="form-group">
			<div class="col-sm-10 col-sm-push-2">
				<button class="btn btn-primary">
					<span class="fa fa-save"></span>
					Proses
				</button>
			</div>
		</div>

</form>

@stop