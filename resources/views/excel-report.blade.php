<?php
header( "Content-Type: application/vnd.ms-excel" ); 
header( "Content-disposition: attachment; filename=spreadsheet.xls" );
?>
<h2>Data Gaji Cash</h2>
<?php
$total_gaji = [];
?>
@foreach($data[0] as $divisi => $konten)
	<table>
		<thead>
			<tr>
				<th colspan=2>{{ $divisi }}</th>
			</tr>
			<tr>
				<th>Nama Karyawan</th>
				<th>Jumlah Gaji</th>
			</tr>
		</thead>
		<tbody>
		@foreach($konten as $idkaryawan => $row)
			<tr>
				<td>{{ $row['nama'] }}</td>
				<td>{{ number_format($row['total_gaji']) }}</td>
			</tr>
			<?php 
			if(isset($total_gaji[$divisi]))
				$total_gaji[$divisi] += $row['total_gaji'];
			else
				$total_gaji[$divisi] = $row['total_gaji'];
			?>
		@endforeach
		</tbody>
		<tfoot>
			<tr>
				<td></td>
				<td><strong>{{ isset($total_gaji[$divisi]) ? number_format($total_gaji[$divisi]) : 0 }}</strong></td>
			</tr>
		</tfoot>
	</table>
@endforeach

<br>
<br>
<h2>Data Gaji Transfer</h2>
<?php
$total_gaji = [];
?>
@foreach($data[1] as $divisi => $konten)
	<table>
		<thead>
			<tr>
				<th colspan=2>{{ $divisi }}</th>
			</tr>
			<tr>
				<th>Nama Karyawan</th>
				<th>Jumlah Gaji</th>
			</tr>
		</thead>
		<tbody>
		@foreach($konten as $idkaryawan => $row)
			<tr>
				<td>{{ $row['nama'] }}</td>
				<td>{{ number_format($row['total_gaji']) }}</td>
			</tr>
			<?php 
			if(isset($total_gaji[$divisi]))
				$total_gaji[$divisi] += $row['total_gaji'];
			else
				$total_gaji[$divisi] = $row['total_gaji'];
			?>
		@endforeach
		</tbody>
		<tfoot>
			<tr>
				<td></td>
				<td><strong>{{ isset($total_gaji[$divisi]) ? number_format($total_gaji[$divisi]) : 0 }}</strong></td>
			</tr>
		</tfoot>
	</table>
@endforeach

<br>
<br>
