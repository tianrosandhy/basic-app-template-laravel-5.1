<div class="container">
	<h2>{{ $title }}</h2>

	<table>
		<tr>
			<td>Nama Karyawan</td>
			<td> : </td>
			<td><b>{{ $datakar->nama }}</b></td>
		</tr>
		<tr>
			<td>TTL</td>
			<td> : </td>
			<td>{{ $datakar->ttl }}</td>
		</tr>
		<tr>
			<td>Tanggal Masuk</td>
			<td> : </td>
			<td>{{ indo_date($datakar->tgl_masuk) }}</td>
		</tr>
	</table>


	<table class="data box table">
		<thead>
			<tr>
				<th>Tanggal Berlaku</th>
				@foreach($master_gaji as $msg)
				@if (get_priviledge()[0] >= 3 && in_array(strtolower($msg['label']), config('app.hr_ignore')))
					@else
					<th>
						{{ $msg['label'] }}
						@if($msg['type'] == 1)
							<span class="label label-success">+</span>
						@else
							<span class="label label-danger">-</span>
						@endif
					</th>
					@endif
				@endforeach
				<th></th>
			</tr>
		</thead>
		<tbody>
		@foreach($gaji as $tgl=>$gj)
			<tr>
				<td>{{ date("Y-m", strtotime($tgl)) }}</td>
				@foreach($master_gaji as $ind=>$msg)
					@if (get_priviledge()[0] >= 3 && in_array(strtolower($msg['label']), config('app.hr_ignore')))
					@else
					<td>
						@if(isset($gj[$ind]))
							Rp {{ number_format($gj[$ind], 0, ",", ".") }}
						@else
							-
						@endif
					</td>
					@endif
				@endforeach
				<td>
					<a href="gaji/{{ Request::segment(2) }}/remove/{{$datakar->id}}/{{ strtotime($tgl) }}" class="btn btn-danger btn-sm"><span class="fa fa-times"></span> Hapus</a>
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>	
</div>


<script>
	$("table.box.data").DataTable({
		paging : true,
		pagingType : "full_numbers",
		lengthMenu : [10, 25, 50, 75, 100],
		dom : 'Bfrtip',
		buttons : [
			'copyHtml5',
			'excelHtml5',
			'pdfHtml5'
		]
	});
</script>