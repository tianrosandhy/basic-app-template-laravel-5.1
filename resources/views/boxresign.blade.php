<div style="min-width:600px">
	
</div>
<h2>Resign Management</h2>

<form action="{{ URL::to($action_button) }}" method="post" class="form form-horizontal">
	<div class="form-group">
		<label class="col-sm-3 control-label">
			Nama Karyawan
		</label>
		<div class="col-sm-9">
			{{ $data->nama }}
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3 control-label">
			Tanggal Masuk
		</label>
		<div class="col-sm-9">
			{{ indo_date($data->tgl_masuk) }}
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3 control-label">
			Tanggal Resign
		</label>
		<div class="col-sm-9">
			<input type="date" required class='form-control' name="tgl_resign" value="{{ Session::get('tgl_resign') }}">
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3 control-label">
			Keterangan
		</label>
		<div class="col-sm-9">
			<textarea name="keterangan" class="form-control">{{ Session::get('keterangan') }}</textarea>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-9 col-sm-push-3">
			<button name="proses" class='btn btn-primary'>Proses</button>
			@if($cek)
			<button name="batal" class="btn btn-danger">Batalkan Resign</button>
			@endif
		</div>
	</div>
</form>