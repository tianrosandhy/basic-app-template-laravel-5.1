@extends('template/t_dashboard')

@section('content')

@if(isset($notif))
	@foreach($notif as $msg)
		<div class="alert alert-info">
			{{ $msg }}
		</div>
	@endforeach
@endif

<div class="col_3">
	<div class="col-md-3 widget widget1">
		<div class="r3_counter_box">
			<i class="fa fa-users"></i>
			<div class="stats">
			  <h5>{{ $data['flat']['karyawan']['value'] }}</h5>
			  <div class="grow">
				<a href="master/karyawan"><p>{{ $data['flat']['karyawan']['label'] }}</p></a>
			  </div>
			</div>
		</div>
	</div>
	<div class="col-md-3 widget widget1">
		<div class="r3_counter_box">
			<i class="fa fa-times-circle"></i>
			<div class="stats">
			  <h5>{{ $data['flat']['resign']['value'] }}</h5>
			  <div class="grow grow1">
				<a href="master/karyawan"><p>{{$data['flat']['resign']['label']}}</p></a>
			  </div>
			</div>
		</div>
	</div>
	<div class="col-md-3 widget widget1">
		<div class="r3_counter_box">
			<i class="fa fa-briefcase"></i>
			<div class="stats">
			  <h5>{{ $data['flat']['divisi']['value'] }}</h5>
			  <div class="grow grow3">
				<a href="master/divisi"><p>{{ $data['flat']['divisi']['label'] }}</p>
				</a>
			  </div>
			</div>
		</div>
	 </div>
	 <div class="col-md-3 widget">
		<div class="r3_counter_box">
			<i class="fa fa-clock-o"></i>
			<div class="stats">
			  <h5>{{ $data['flat']['terlambat']['value'] }}</h5>
			  <div class="grow grow2">
				<a href="mutasi/terlambat">
				<p>{{ $data['flat']['terlambat']['label'] }}</p>
				</a>
			  </div>
			</div>
		</div>
	</div>
	<div class="clearfix"> </div>
</div>



<div class="switches">
	<div class="col-4">
		<div class="col-md-12">
			<div class="switch-right-grid">
				<div class="switch-right-grid1">
					<h3>{{ $data['graph']['absensi']['label'] }}</h3>
				</div>
			</div>
			<div class="sparkline">
				<canvas id="absen" width="100%" height="300"></canvas>
				<script>
				new Chart($("#absen"),{
					"type":"bar",
					"data":{
						"labels": {!! $data['graph']['absensi']['dataLabel'] !!},
						"datasets":[{
							"label":"Jumlah Kehadiran",
							"data":{!! $data['graph']['absensi']['dataValue'] !!},
							"fill":false,
							"backgroundColor":"rgba(255, 99, 132, 0.2)",
							"borderColor":"rgb(255, 99, 132)",
						"borderWidth":1}]
					},
					"options":{
						"responsive" : true,
						"maintainAspectRatio" : false,
						"scales":{
							"yAxes":[{"ticks":{"beginAtZero":true}}]
						}
					}
				});	
				</script>				
			</div>
		</div>
		<div class="col-md-6">
			<div class="switch-right-grid">
				<div class="switch-right-grid1">
					<h3>{{ $data['graph']['jk']['label'] }}</h3>
				</div>
			</div>
			<div class="sparkline">
				<canvas id="jk" width="100%" height="300"></canvas>
				<script>
				new Chart($("#jk"),{
					"type":"polarArea",
					"data":{
						"labels": {!! $data['graph']['jk']['dataLabel'] !!},
						"datasets":[{
							"data":{!! $data['graph']['jk']['dataValue'] !!},
							"fill":false,
							"backgroundColor":{!! $data['graph']['jk']['dataBg'] !!},
						"borderWidth":1}]
					},
					"options":{
						"responsive" : true,
						"maintainAspectRatio" : false,
						"scales":{
							"yAxes":[{"ticks":{"beginAtZero":true}}]
						}
					}
				});	
				</script>				
			</div>
		</div>
		<div class="col-md-6">
			<div class="switch-right-grid">
				<div class="switch-right-grid1">
					<h3>{{ $data['graph']['divisi']['label'] }}</h3>
				</div>
			</div>
			<div class="sparkline">
				<canvas id="divisi" width="100%" height="300"></canvas>
				<script>
				new Chart($("#divisi"),{
					"type":"polarArea",
					"data":{
						"labels": {!! $data['graph']['divisi']['dataLabel'] !!},
						"datasets":[{
							"label":"Jumlah Kehadiran",
							"data":{!! $data['graph']['divisi']['dataValue'] !!},
							"fill":false,
							"backgroundColor":{!! $data['graph']['divisi']['dataBg'] !!},
						"borderWidth":1}]
					},
					"options":{
						"responsive" : true,
						"maintainAspectRatio" : false,
						"scales":{
							"yAxes":[{"ticks":{"beginAtZero":true}}]
						}
					}
				});	
				</script>				
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>

@stop