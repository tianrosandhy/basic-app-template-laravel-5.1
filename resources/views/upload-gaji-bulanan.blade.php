@extends('template/t_panel')

@section('content')

<h2>{{ $title }}</h2>

<form action="{{ isset($data['importedData']) ? url('upload-gaji/bulanan/process') : '' }}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="well" style="padding:1em;">
        <p class="">Anda dapat mengupload data bonus & potongan bulanan karyawan sekaligus dalam format excel. Silakan download contoh format excel, lalu upload sesuai kebutuhan.</p>
        <a href="{{ url('upload-gaji/bulanan/example') }}" class="btn btn-info"><i class="fa fa-download"></i> Download Contoh Format Excel</a>
    </div>

    @if(!isset($data['importedData']))
    <div class="well" style="background:#fff; padding:1.5em">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Upload Excel Document Disini</label>
                    <input type="file" name="excel" class="form-control" accept=".xls, .xlsx">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Periode</label>
                    <div class="row">
                        <div class="col-xs-6">
                            <?php
                            $current_month = intval(date('m'));
                            ?>
                            <select name="bulan" class="form-control">
                                @for($i=1; $i<=12; $i++)
                                <option value="{{ $i }}" {{ $i == $current_month ? 'selected' : '' }}>{{ date('F', strtotime('2020-' . $i . '-01')) }}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="col-xs-6">
                            <input type="number" name="tahun" class="form-control" min="{{ date('Y') - 20 }}" max="{{ date('Y') + 20 }}" value="{{ date('Y') }}">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <button type="submit" class="btn btn-success">Process Data</button>
    </div>
    @else
    <div class="alert alert-info">Dibawah ini adalah <strong>{{ count($data['importedData']) }}</strong> data bonus dan potongan karyawan yang akan disimpan untuk data periode <strong>{{ date('F', strtotime('2020-' . $data['bulan'] . '-01')) }} {{ $data['tahun'] }}</strong>. Klik tombol "Proses Sekarang" untuk mengeksekusi import data ini.</div>
    <input type="hidden" name="data" value="{{ json_encode($data) }}">
    <div style="margin-bottom:1.5em;" class="btn-group">
        <button style="font-size:1.25rem;" class="btn btn-primary">Proses Sekarang</button>
        <a href="{{ url('upload-gaji/bulanan') }}" style="font-size:1.25rem;" class="btn btn-danger">Batalkan</a>
    </div>
    
    <table class="data table table-hover">
        <thead>
            <tr>
                <th>NIK</th>
                <th>Nama</th>
                @foreach($gaji_bulanan as $gj)
                <th>{{ $gj->label }}</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            @foreach($data['importedData'] as $idkar => $listgaji)
            <tr>
                <?php
                $usedKaryawan = null;
                foreach($karyawan as $kar){
                    if($kar->id == $idkar){
                        $usedKaryawan = $kar;
                        break;
                    }
                }
                ?>
                @if(!empty($usedKaryawan))
                    <td>{{ $usedKaryawan->nik }}</td>
                    <td>{{ $usedKaryawan->nama }}</td>
                    @foreach($gaji_bulanan as $gj)
                        <td><?php
                        if(isset($listgaji[$gj->id])){
                            $lsgaji = trim($listgaji[$gj->id]);
                            if(strlen($lsgaji) == 0){
                                $lsgaji = null;
                            }
                            echo number_format($lsgaji);
                        }
                        else{
                            echo '-';
                        }
                        ?></td>
                    @endforeach
                @endif
            </tr>
            @endforeach
        </tbody>
    </table>

    <div style="margin-top:1.5em;" class="btn-group">
        <button style="font-size:1.25rem;" class="btn btn-primary">Proses Sekarang</button>
        <a href="{{ url('upload-gaji/bulanan') }}" style="font-size:1.25rem;" class="btn btn-danger">Batalkan</a>
    </div>

    @endif
</form>

@stop