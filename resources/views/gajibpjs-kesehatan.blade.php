@extends('template/t_panel')

@section('content')
<h2>{{ $title }}</h2>

@if(isset($description))
<div class="alert alert-info">{{ $description }}</div>
@endif


@include('template/t_well')

<div class="space" align="center">
	@if (get_priviledge()[0] < 3)
	<a href="{{ url('gaji/bpjs/export') }}" class="btn btn-info">
		<span class="fa fa-print"></span>
		Export as Excel
	</a>
	@endif
</div>

<table class="data table table-hover">
	<thead>
		<tr>
			<th>Cabang</th>
			<th>Nama Karyawan</th>
			<th>Departemen</th>
			<th>Status Trf</th>
			@if (get_priviledge()[0] < 3)
			<th>Total Basis Perhitungan<br><small>(Gj Pokok + Tj Jabatan + Tj Golongan)</small></th>
			<th>PK</th>
			<th>TK</th>
			@endif
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php $realtotal = 0; ?>
		@foreach($row as $r)
		@if(!is_panic() || (is_panic() && $r->status_transfer == 1))

		@if ($r->is_bpjs_kesehatan <> 1)
			<?php continue; ?>
		@endif

		<tr>
			<td>{{ $r->nama_divisi }}</td>
			<td>
                <a href="detail/{{ $r->id_karyawan }}" data-featherlight="ajax">{{ $r->nama }}</a>
            </td>
			<td>{{ isset($r->departemen) ? $r->departemen : '-' }}</td>
			<td>{!! ($r->status_transfer == 1) ? '<span class="label label-success">Transfer</span>' : '<span class="label label-warning">Cash</span>' !!}</td>

			@if (get_priviledge()[0] < 3)
			@if(isset($gaji[$r->id_karyawan]))
				<td>{{ isset($gaji[$r->id_karyawan]['gaji_bpjs']) ? number_format($gaji[$r->id_karyawan]['gaji_bpjs']) : '-' }}</td>
				<td>{{ isset($gaji[$r->id_karyawan]['bpjs_kesehatan_pk']) ? number_format($gaji[$r->id_karyawan]['bpjs_kesehatan_pk']) : '-' }}</td>
				<td>{{ isset($gaji[$r->id_karyawan]['bpjs_kesehatan_tk']) ? number_format($gaji[$r->id_karyawan]['bpjs_kesehatan_tk']) : '-' }}</td>
			@else
				<td></td>
				<td></td>
				<td></td>
			@endif
			@endif

			<td></td>
		</tr>
		@endif
		@endforeach
	</tbody>
	<tfoot>
		<td></td>
		<td></td>
		<td></td>
		@if (get_priviledge()[0] < 3)
		<td></td>
		<td></td>
		<td></td>
		@endif
		<td></td>
	</tfoot>
</table>

@stop