@extends('template/t_panel')

@section('content')

<h2>{{ $title }}</h2>

<form action="{{ isset($data['importedData']) ? url('update-karyawan/process') : '' }}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="well" style="padding:1em;">
        <p class="">Anda dapat mengupdate data master karyawan sekaligus dalam format excel. Silakan download contoh format excel, lalu upload sesuai kebutuhan.</p>
        <a href="{{ url('update-karyawan/example') }}" class="btn btn-info"><i class="fa fa-download"></i> Download Contoh Format Excel</a>
    </div>

    @if(!isset($data['importedData']) || isset($err[0]))
        <div class="well" style="background:#fff; padding:1.5em">
            <div class="form-group">
                <label>Upload Excel Document Disini</label>
                <input type="file" name="excel" class="form-control" accept=".xls, .xlsx">
            </div>

            <button type="submit" class="btn btn-success">Process Data</button>
        </div>
        @if (isset($err[0]))
        <div class="alert alert-danger">{{ $err[0] }}</div>
        @endif
    @else
        <div class="alert alert-info">Dibawah ini adalah <strong>{{ count($data['importedData']) }}</strong> data master karyawan yang akan diupdate berdasarkan NIK . Klik tombol "Proses Sekarang" untuk mengeksekusi import update data ini.<br><strong>Note : </strong> nomor NIK yang tidak valid akan diabaikan.</div>
        <input type="hidden" name="data" value="{{ json_encode($data) }}">
        <div style="margin-bottom:1.5em;" class="btn-group">
            <button style="font-size:1.25rem;" class="btn btn-primary">Proses Sekarang</button>
            <a href="{{ url('update-karyawan') }}" style="font-size:1.25rem;" class="btn btn-danger">Batalkan</a>
        </div>
        
        <table class="data table table-hover">
            <thead>
                <tr>
                    @foreach ($mapHeader as $fname => $fi)
                    <td>{{ strtoupper(str_replace('_', ' ', $fname)) }}</td>
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @foreach($data['importedData'] as $row)
                <tr>
                    @foreach ($mapHeader as $fname => $fi)
                        @if ($fname == 'jk')
                        <td>
                            @if($row['jk'] == 1)
                            <span class="btn btn-primary btn-sm">Laki-laki</span>
                            @elseif($row['jk'] == 0)
                            <span class="btn btn-danger btn-sm">Perempuan</span>
                            @endif
                        </td>
                        @elseif ($fname == 'status_transfer')
                        <td>
                            @if($row['status_transfer'] == 1)
                            TRANSFER
                            @else
                            CASH
                            @endif
                        </td>
                        @elseif ($fname == 'bpjs_kesehatan' || $fname == 'bpjs_tk')
                        <td>
                            @if (isset($row[$fname]))
                                @if ($row[$fname] == 1)
                                <span class="label label-success">YA</span>
                                @else
                                <span class="label label-danger">TIDAK</span>
                                @endif
                            @endif
                        </td>
                        @elseif ($fname == 'status_ptkp')
                        <td>
                            @if (isset($row[$fname]))
                                {{ config('app.status_ptkp.' . $row[$fname]) ?? '-' }}
                            @endif
                        </td>
                        @else
                        <td>{{ $row[$fname] ?? '' }}</td>
                        @endif
                    @endforeach
                </tr>
                @endforeach
            </tbody>
        </table>

        <div style="margin-top:1.5em;" class="btn-group">
            <button style="font-size:1.25rem;" class="btn btn-primary">Proses Sekarang</button>
            <a href="{{ url('upload-karyawan') }}" style="font-size:1.25rem;" class="btn btn-danger">Batalkan</a>
        </div>
    @endif
</form>

@stop