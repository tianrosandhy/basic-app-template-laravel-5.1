<div class="container">
	<h2>{{ $title }}</h2>
	Nama Karyawan : <strong>{{ $name }}</strong>
	<br>
	Tanggal Masuk : <strong>{{ indo_date($tgl_masuk) }}</strong>
	<br><br>

	<div class="well">
		<form action="{{ URL::to($action_button) }}" method="post" class="form-horizontal {{ ($type == 'update') ? 'form-ajax' : '' }}">
			{{ csrf_field() }}
			<input type="hidden" name="edit_id" value="{{ Session::get('id') }}">
			@foreach($table as $label => $data)
				@if($data['form'])
				<div class="form-group">
					<label class="control-label col-sm-2">{{ $label }}</label>
					<div class="col-sm-10">
					@if(!isset($default_value))
						{!! input_helper($label, $data, Session::get($data['field'])) !!}
					@else
						{!! input_helper($label, $data, $default_value[$data['field']]) !!}
					@endif
					</div>
				</div>
				@endif
			@endforeach
				<div class="form-group">
					<div class="col-sm-10 col-sm-push-2">
						<button class="btn btn-primary">
							<span class="fa fa-save"></span>
							Simpan
						</button>
					</div>
				</div>

		</form>
	</div>

@if($type == "show")
	<h3></h3>
	<table class="data table">
		<thead>
			<tr>
			@foreach($table as $label => $data)
				@if($data['table'] == true)
					<th>{{ $label }}</th>
				@endif
			@endforeach
			</tr>
		</thead>
		<tbody>
			@foreach($content as $data)
			<tr>
				@foreach($table as $field)
					@if($field['table'] == true)
						<td>{!! isset($data[$field['field']]) ? $data[$field['field']] : "" !!}</td>
					@endif
				@endforeach
			</tr>
			@endforeach
		</tbody>
	</table>
@endif

</div>
<script>
	$(".delete-button").click(function(e){
		e.preventDefault();
		target = $(this).attr("href");
		var x = confirm('Apakah Anda benar-benar ingin menghapus data ini? Data yang sudah dihapus tidak dapat dikembalikan lagi');

		trparent = $(this).closest("tr");

		if(x == true){
			//ajax run
			$.ajax({
				method : "POST",
				dataType : "json",
				url : target
			}).done(function(data){
				trparent.slideUp();
			});
		}
		else{
			return false;
		}
	});

	$(".form-ajax").on("submit", function(e){
		e.preventDefault();
		var formdata = $(this).serialize();
		var target = $(this).attr('action');
		$.ajax({
			url : target,
			type : 'POST',
			dataType : 'json',
			data : formdata
		}).done(function(resp){
			console.log(resp);
			if(resp['error'])
				alertify.error(resp['error']);
			if(resp['success'])
				alertify.success(resp['success']);
		});
	});
</script>