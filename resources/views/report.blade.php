@extends('template/t_panel')

@section('content')
<h2>{{ $title }}</h2>

@if(isset($description))
<div class="alert alert-info">{{ $description }}</div>
@endif


@include('template/t_well')


<table class="data table table-hover">
	<thead>
		<tr>
			<th>Nama Karyawan</th>
			<th>Divisi</th>
			<th>Hadir</th>
			<th>Terlambat</th>
			<th>Lembur Byr</th>
			<th>Lembur Tdk Bayar</th>
			@foreach($listkode as $lk)
			<th><div style="background:{{ $lk->color }}; text-align:center;">{{ $lk->kode_absen }}</div></th>
			@endforeach
			<th>Presensi</th>
		</tr>
	</thead>
	<tbody>
		@foreach($row as $r)
			<?php
			$presence = 0;
			?>
			@if(isset($content[$r->id_karyawan]))
				<?php
				$libur = 0;
				$presence = $total = $content[$r->id_karyawan]['hadir'];
				?>
			<tr>
				<td><a href="detail/{{ $r->id_karyawan }}" data-featherlight="ajax">{{ $r->nama }}</a></td>
				<td>{{ $r->nama_divisi }}</td>
				<td>{{ $content[$r->id_karyawan]['hadir'] }}</td>
				<td>{{ $content[$r->id_karyawan]['terlambat'] }}</td>
				<td>{{ isset($content[$r->id_karyawan]['lembur'][1]) ? $content[$r->id_karyawan]['lembur'][1] . " jam" : '-' }}</td>
				<td>{{ isset($content[$r->id_karyawan]['lembur'][2]) ? $content[$r->id_karyawan]['lembur'][2] . " jam" : '-' }}</td>

				@foreach($listkode as $lk)
					<?php
					if(isset($content[$r->id_karyawan][$lk->id])){
						if($lk->kode_absen <> 'L'){
							$presence += ($lk->bobot * $content[$r->id_karyawan][$lk->id]);
						}
						else{
							$libur += $content[$r->id_karyawan][$lk->id];
						}
						$total += ($lk->bobot * $content[$r->id_karyawan][$lk->id]);
					}
					?>
					<td>{{ isset($content[$r->id_karyawan][$lk->id]) ? $content[$r->id_karyawan][$lk->id] : 0 }}</td>
				@endforeach
				<td><div>{{ $presence }} / {{ (count($calendar) - $libur) }}</div></td>
			</tr>
			@endif
		@endforeach
	</tbody>
</table>


@stop