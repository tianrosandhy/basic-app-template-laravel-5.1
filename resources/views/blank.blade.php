@extends('template/t_panel')

@section('content')

<h2>{{ $title }}</h2>

@if(isset($file_template))
	<div class="alert alert-info">
		Pastikan nama divisi, nama karyawan, dan kode absensi yang diketik sudah sama dan sesuai dengan yang ada di program. Perbedaan sedikit saja mengakibatkan inputan diabaikan
	</div>
	<div class="well">
	
       <form class="form-ajax" method="get" action="{{ url('api/excel') }}">
       <div class="row">
           <div class="col-sm-2">
                <b>Bulan</b>
           </div>
           <div class="col-sm-3">
               <select name="bulan" class="form-control">
                   @foreach(nama_bulan() as $ind=>$nmbl)
                       <option value="{{ $ind }}" {{ $ind==date('n') ? 'selected' : '' }}>{{ $nmbl }}</option>
                   @endforeach
               </select>
           </div>
       </div>
       <div class="row">
           <div class="col-sm-2">
                <b>Tahun</b>
           </div>
           <div class="col-sm-3">
               <input class="form-control" type="number" name="tahun" value="{{ date('Y') }}">
           </div>
       </div>
        <div class="row">
            <div class="col-sm-2">
                <b>Output Divisi</b>
            </div>
            <div class="col-sm-3">
                <select name="divisi" id="" class="form-control">
                    <option value="">- Download untuk Seluruh Divisi -</option>
                    @foreach($list_divisi as $id_divisi=>$nama_divisi)
                        <option value="{{ $id_divisi }}">{{ $nama_divisi }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-push-2 col-sm-3">
                <button class="btn btn-primary">Download File Template</button>
            </div>
        </div>
       </form>
	
	</div>
@endif


<div class="well">
	<form action="" method="post" enctype="multipart/form-data">
		@if(!isset($hide_periode))
		Periode
		<div class="row">
			<div class="col-sm-4">
				<select name="bulan" class="form-control">
					@foreach(nama_bulan() as $i=>$bln)
					<option value="{{ $i }}" {{$i==$default['bulan'] ? "selected" : ""}} >{{ $bln }}</option>
					@endforeach
				</select>
			</div>
			<div class="col-sm-4">
				<input type="number" name="tahun" min=1900 max=2100 class="form-control" value="{{ $default['tahun'] }}">
			</div>
      <div class="col-sm-4">
        <select name="divisi" class="form-control">
          <option value="">- Pilih Divisi -</option>
          @foreach($list_divisi as $row)
            <option value="{{ $row->id}}">{{ $row->nama_divisi }}</option>
          @endforeach
        </select>
      </div>
		</div>
		@endif
    <br>
    
    Custom Tanggal Mulai
    <br>
    <input type="date" name="tglFrom" class="form-control">
    <br>
    <input type="file" name="presence" class="form-control" required>

		<button class="btn btn-primary">
			<span class="fa fa-save"></span>
			Proses
		</button>
	</form>
</div>


@stop