<div class="slip">
	<h2>Slip Gaji</h2>
	<span class="label label-primary">Detail Karyawan</span>
	<table class="padd">
		<tr>
			<th>Nama Karyawan</th>
			<td>{{ $row->nama }}</td>
		</tr>
		<tr>
			<th>Divisi</th>
			<td>{{ $row->nama_divisi }}</td>
		</tr>
		<tr>
			<th>Jabatan</th>
			<td>{{ $row->nama_jabatan }}</td>
		</tr>
		<tr>
			<th>Periode</th>
			<td>{{ nama_bulan($default['bulan'])." ".$default['tahun'] }}</td>
		</tr>
	</table>

	<span class="label label-primary">Detail Gaji</span>
	<table class="padd">
<?php
/*
		<tr>
			<td>Presensi</td>
			<td>{{ $gaji[$row->id_karyawan]['bobot'] }} / {{ $gaji[$row->id_karyawan]['batas_bobot'] }}</td>
			<td></td>
		</tr>
*/
?>

		@foreach($gaji[$row->id_karyawan][1] as $label=>$nilai)
		<tr>
			<td>{{ isset($listgaji[$label]) ? $listgaji[$label] : ucwords(str_replace("_", " ", $label)) }}</td>
			<td align="right">Rp {{ number_format($nilai, 0, ",", ".") }}</td>
			<td></td>
		</tr>
		@endforeach

		@foreach($gaji[$row->id_karyawan][2] as $label=>$nilai)
		<tr>
			<td>{{ isset($listgaji[$label]) ? $listgaji[$label] : ucwords(str_replace("_", " ", $label)) }}</td>
			<td></td>
			<td align="right">Rp {{ number_format($nilai, 0, ",", ".") }}</td>
		</tr>
		@endforeach

		<tr>
			<td><b>Take Home Pay</b></td>
			<td colspan=2 align="center" style="border:1px solid #000; font-weight:bold;">Rp {{ number_format($gaji[$row->id_karyawan]['total'], 0, ",", ".") }}</td>
		</tr>
	</table>
</div>
