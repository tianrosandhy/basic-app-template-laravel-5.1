@extends('template/t_print')

@section('content')



<div class="slip-pack">
	<?php $x = 0; ?>
	@foreach($rows as $row)
		@if(isset($gaji[$row->id_karyawan]))
			<div class="slip">
				<h2>Slip Gaji</h2>
				<span class="label label-primary">Detail Karyawan</span>
				<table class="padd detail-gaji">
					<tr>
						<th>Nama Karyawan</th>
						<td>{{ $row->nama }}</td>
					</tr>
					<tr>
						<th>Divisi</th>
						<td>{{ $row->nama_divisi }}</td>
					</tr>
					<tr>
						<th>Jabatan</th>
						<td>{{ $row->nama_jabatan }}</td>
					</tr>
					<tr>
						<th>Periode</th>
						<td>{{ nama_bulan($default['bulan'])." ".$default['tahun'] }}</td>
					</tr>
				</table>
				
				<span class="label label-primary">Detail Gaji</span>
				<table class="padd detail-gaji">
<?php
/*
					<tr>
						<td>Presensi</td>
						<td>{{ $gaji[$row->id_karyawan]['bobot'] }} / {{ $gaji[$row->id_karyawan]['batas_bobot'] }}</td>
						<td></td>
					</tr>
*/

$total_override = 0;
?>
					@if(isset($gaji[$row->id_karyawan][1]))
					@foreach($gaji[$row->id_karyawan][1] as $label=>$nilai)
                        <?php
                        $final_label = isset($listgaji[$label]) ? $listgaji[$label] : ucwords(str_replace("_", " ", $label));
						if (strpos(strtolower($final_label), 'tahunan') !== false) {
							continue;
						}
                        ?>
                        @if (strpos(strtolower($final_label), 'gaji') !== false || strpos(strtolower($final_label), 'tunjangan') !== false)
							@if (strpos(strtolower($final_label), 'pph') !== false || strpos(strtolower($final_label), 'penempatan') !== false)
								<?php
								continue;
								?>
							@endif
							<tr>
								<?php
								$total_override += $nilai;
								?>
								<td>{{ $final_label }}</td>
								<td>Rp {{ number_format($nilai, 0, ",", ".") }}</td>
								<td></td>
							</tr>
							@endif
					@endforeach
					@endif
{{--
					@if(isset($gaji[$row->id_karyawan][2]))
					@foreach($gaji[$row->id_karyawan][2] as $label=>$nilai)
					<tr>
						<td>{{ isset($listgaji[$label]) ? $listgaji[$label] : ucwords(str_replace("_", " ", $label)) }}</td>
						<td></td>
						<td>Rp {{ number_format($nilai, 0, ",", ".") }}</td>
					</tr>
					@endforeach
					@endif
					<tr>
						<td><b>Take Home Pay</b></td>
						<td colspan=2 align="center" style="border:1px solid #000; font-weight:bold;">Rp {{ number_format($gaji[$row->id_karyawan]['total'], 0, ",", ".") }}</td>
					</tr>
--}}
					<tr>
						<td><b>Total Gaji + Tunjangan</b></td>
						<td colspan=2 align="center" style="border:1px solid #000; font-weight:bold;">Rp {{ number_format($total_override, 0, ",", ".") }}</td>
					</tr>
				</table>
			</div>
			<?php
			$x++;
			if($x % get_setting('slip_gaji_per_page') == 0){
				echo "</div><div class='slip-pack'>";
			}
			?>
		@endif
	@endforeach
</div>
@stop