@extends('template/t_panel')

@section('content')
<h2>{{ $title }}</h2>

@if(isset($description))
<div class="alert alert-info">{{ $description }}</div>
@endif


@include('template/t_well')


<table class="data table table-hover">
	<thead>
		<tr>
			<th>Nama Karyawan</th>
			<th>Divisi</th>
			<th>Keterangan</th>
			<th>Terlambat</th>
		</tr>
	</thead>
	<tbody>
		@foreach($row as $r)
			<?php
			$jumlah_telat = 0;
			?>
			<tr>
				<td><a href="detail/{{ $r->id_karyawan }}" data-featherlight="ajax">{{ $r->nama }}</a></td>
				<td>{{ $r->nama_divisi }}</td>
				<td>
					@if(isset($content[$r->id_karyawan]))
					@foreach($content[$r->id_karyawan] as $tlt => $true)
						@if(isset($acc[$r->id_karyawan]))
							@if(!in_array(strtotime($tlt), $acc[$r->id_karyawan]))
							<span class="btn btn-sm btn-danger">{{ date('d M Y', strtotime($tlt)) }}</span>
							<?php $jumlah_telat++;?>
							@endif
						@else
						<span class="btn btn-sm btn-danger">{{ date('d M Y', strtotime($tlt)) }}</span>
						<?php $jumlah_telat++;?>
						@endif
					@endforeach
					@endif
				</td>
				<td>{{ $jumlah_telat }}</td>
			</tr>
		@endforeach
	</tbody>
</table>


@stop