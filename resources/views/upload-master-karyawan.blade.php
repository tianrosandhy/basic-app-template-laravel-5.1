@extends('template/t_panel')

@section('content')

<h2>{{ $title }}</h2>

<form action="{{ isset($data['importedData']) ? url('upload-karyawan/process') : '' }}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="well" style="padding:1em;">
        <p class="">Anda dapat mengupload data master karyawan sekaligus dalam format excel. Silakan download contoh format excel, lalu upload sesuai kebutuhan.</p>
        <a href="{{ url('upload-karyawan/example') }}" class="btn btn-info"><i class="fa fa-download"></i> Download Contoh Format Excel</a>
    </div>

    @if(!isset($data['importedData']))
    <div class="well" style="background:#fff; padding:1.5em">
        <div class="form-group">
            <label>Upload Excel Document Disini</label>
            <input type="file" name="excel" class="form-control" accept=".xls, .xlsx">
        </div>

        <button type="submit" class="btn btn-success">Process Data</button>
    </div>
    @else
    <div class="alert alert-info">Dibawah ini adalah <strong>{{ count($data['importedData']) }}</strong> data master karyawan yang akan disimpan . Klik tombol "Proses Sekarang" untuk mengeksekusi import data ini.</div>
    <input type="hidden" name="data" value="{{ json_encode($data) }}">
    <div style="margin-bottom:1.5em;" class="btn-group">
        <button style="font-size:1.25rem;" class="btn btn-primary">Proses Sekarang</button>
        <a href="{{ url('upload-karyawan') }}" style="font-size:1.25rem;" class="btn btn-danger">Batalkan</a>
    </div>
    
    <table class="data table table-hover">
        <thead>
            <tr>
                <td>Nama Lengkap</td>
                <td>NIk</td>
                <td>NPWP</td>
                <td>Email</td>
                <td>Departemen</td>
                <td>Tempat Tanggal Lahir</td>
                <td>JK</td>
                <td>Alamat</td>
                <td>No Telepon</td>
                <td>Tanggal Masuk</td>
                <td>No Rekening</td>
                <td>Status Transfer</td>
                <td>Divisi</td>
                <td>Jabatan</td>
                <td>Golongan</td>
            </tr>
        </thead>
        <tbody>
            @foreach($data['importedData'] as $row)
            <tr>
                <td>{{ $row['nama'] }}</td>
                <td>{{ $row['nik'] }}</td>
                <td>{{ $row['npwp'] }}</td>
                <td>{{ $row['email'] }}</td>
                <td>{{ $row['departemen'] }}</td>
                <td>{{ $row['ttl'] }}</td>
                <td>
                    @if($row['jk'] == 1)
                    <span class="btn btn-primary btn-sm">Laki-laki</span>
                    @elseif($row['jk'] == 0)
                    <span class="btn btn-danger btn-sm">Perempuan</span>
                    @endif
                </td>
                <td>{{ $row['alamat'] }}</td>
                <td>{{ $row['telp'] }}</td>
                <td>{{ $row['tgl_masuk'] }}</td>
                <td>{{ $row['no_rek'] }}</td>
                <td>
                    @if($row['status_transfer'] == 1)
                    TRANSFER
                    @else
                    CASH
                    @endif
                </td>
                <td>
                    <?php
                    $outdivisi = null;
                    if(isset($row['divisi'])){
                        foreach($divisi as $dv){
                            if($dv->id == $row['divisi']){
                                $outdivisi = $dv->nama_divisi;
                            }
                        }
                    }
                    ?>
                    {{ $outdivisi }}
                </td>
                <td>
                    <?php
                    $outjabatan = null;
                    if(isset($row['jabatan'])){
                        foreach($jabatan as $dv){
                            if($dv->id == $row['jabatan']){
                                $outjabatan = $dv->nama_jabatan;
                            }
                        }
                    }
                    ?>
                    {{ $outjabatan }}
                </td>
                <td>
                    <?php
                    $outgolongan = null;
                    if(isset($row['golongan'])){
                        foreach($golongan as $dv){
                            if($dv->id == $row['golongan']){
                                $outgolongan = $dv->kode_golongan;
                            }
                        }
                    }
                    ?>
                    {{ $outgolongan }}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

    <div style="margin-top:1.5em;" class="btn-group">
        <button style="font-size:1.25rem;" class="btn btn-primary">Proses Sekarang</button>
        <a href="{{ url('upload-karyawan') }}" style="font-size:1.25rem;" class="btn btn-danger">Batalkan</a>
    </div>

    @endif
</form>

@stop