<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>e-PaySlip CWA</title>
    <style>
    * {margin: 0 auto;}
    html, body{
        font-family: sans-serif;
        font-size:13px;
    }

    .detail-gaji th{
        letter-spacing:4px;
        font-weight:bold;
        text-align:left;
        padding:.75rem;
        border-top:2px solid #000;
        border-bottom:2px solid #000;
    }

    .detail-gaji .border-left{
        border-left:2px solid #000;
    }
    .detail-gaji tfoot>tr>td{
        border-top:2px solid #000;
        border-bottom:3px double #000;
        font-weight:bold;
    }
    .heading{font-size:15px; font-weight:bold;}

    </style>
</head>
<body>
<div class="paper">
    <table class="header" style="width:100%;">
        <tr>
            <td width="12%">
                @if (isset($is_console))
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFIAAABQCAIAAAAFhrXHAAAAAXNSR0IB2cksfwAAAAlwSFlzAAALEwAACxMBAJqcGAAADxJJREFUeJztmwd4FNUWx58oRX0PJEbx6ZNQk5DQEoHQQUCkIwIhNAWUKigKGkQUBVRAqohgQeHBo/cgmkJXQHpJkCCQQICQZMvszmyZmve/O+tkdzNbkkwIqOc7H19C7uzc373nnnLv3X/k/yXlH2XdgbKRv7Hz87Oysk6fOqWJnj93TpKksqLyK27Yo0eOeqhiRU20WnCwzWYrKyq/4oY9KC7ugfvu00T/+eCDtJkuKyq/4oY9dPBgrbArP/wwwzBlReVX3LBHDBumFXbVypWtVmtZUfmVv7H/xv5rYa8cM25MlSBXHVclKL5q8JSi6/SnQ7h7BZuePCWvZpgmqqsfLd0rcdv81jt3CTZNl27Md8ee8ObdgH3lypW42Nj169ZpAagu7tjjHdih9dnkPWzKXn3TlviVGvgSu2ef5esVMg89ZRr+RPUf5MRrEM2nX+IzM83uC6TY2Lezs5tERcMpVnzggR9379YI01PUsGuGAQOFhGn0a/jZ+t0q/CzoDbrwhviVO3NOtFiM3XrLLc0TJ0sOYY/+ivEqIXbG1asRYWFKOKhUvvyG9eu1gy0QdWzriu9BYlnyZV6dCO70GYnnJUEw9uqra9hEpGlMLyaZtKwVzh0/KbEsdy5VpChq2KslwT516lTL5s09AuHjjz66fNkyQRA0pXbHNo0eL3fa2LsfUO2JyfqmrYQbN7ljJ0DFfDzb2DcOw2Hbuj2vFmlmaNdJEkX20C/0h7PwA+yi2NiY58LMf5AHb9+2TRTFUsMeMcrZ79r1yIq99Ds1dLhosZrfmMRf+M3+UxKzYLFos5nffNu5zmd+StbC2AmweUGn53+7qIuMKga2kJG5rEnMQ+XKeUt+gqpU+WHXLg3JvWA7lrRoNFq++U5kGEOXnmDmMzLte/fxVzP0rdoTMHi+g4ckjjP26GPo8Dx7+KhIM8buvYuKDWZD++dyaoa9/ejjPsiffvLJXQkJWpG7Y496TcGmBg+DncPChdw8fYt2lqXLsLxFs9melIIlLS8E4fZt2LaQpxNuZcMoMPOKncP/SYzF7+thRPgc+ZHrNUKnBz9RyTv5v6tVW7VypfbYiksj/Y5sDO8FKvQMpmueFC97bMviL+QGls+XYly44ycsS5dDbRu3YFz4zGu66BjyeFgDyV+9Ld64aew7QB5EWTNrhI4PeswH+ZPVqiXs3FmK2FB2/0HiwDZsInPbrbeMTcUNJVT1GmHxK79C9c+0YI8dBznMJBBsMeuG4fkeziGOilFeertm2MiqwT7IH6xQ4eCBA5piuyenxp4v0tNnGNp2JL/WiYB7o4aOyKsbKVNRg16iBr9c0L5WuOG5bqbhI/Wtn/WLDeenj2ntHK9W7UWDkYodrHxUds2wKcHVfJR3D1eqlJiYqBk2PfV9zZLTiEaSRb0C4478igFytqwTAa9JnMKqNa6PY87jg6v5mHN4OGQyxfZwbtjM7HlaYeujm+fzfOH3YZ4NnboWGFS/gcQdGgz6lu1kk3Gd82FVgir6IH/qqaTExOJtS7th29Zu0LkkmCVRLJDCLxMQ/GLaFDRDCDxwCO6AnvqB7D6sazcYOnd3IQ8d4ZP8saCg5KSkYsy5G7ao01NxQzTADmtg37mrMDPis2sz0/BRSH6QzGNFmN99X8jJxdRxqWm6qGZKm8shdV+pElTe+zqvWb06yEuETTp37XoJyQ0du9i378x3tz3+5Glj115uiz+6OZd2Abkd88kcdv8Byc4SbEEgAbJOhGvLqyF1fc851vmelJQSYUPE3FzEZH3L9kUF1jV4hp42XbiYnu9eOUhWKzXoZdd1C2XmzifpUE6OcPMWMiL6o1mI+XzqBTgFYvBdexW4Pcec9/1XFR/ktUJCUs+fLxG2U3geKZex+wse3VVVVCzMzE/EW7fUP+ncecR5N4t4tjPcGEkDUPAkpegaNYUnF00m1EJoiQlHRozJV4IcNKtG6IDKj1Twbu2o1dJSU0uMLYsgcMeOU/3ivMHrGje1rV0v+dwDYvcd8LBbXeNm3MnTws2b1JBh+BP8H/itX6+ghgxH5o/RQEkPc8Cad30K5K8+EuQNu3y5cju2b9cIWxaeZ5P3GDr3cOt6ZBQd/56Ym+f3adStyF48xysyCtU7sZRnmqPUE61WJIWi3S7k5lpXrERtozrKIMc6LzznjwU9unXLlgCZA8Z2iH1HgmsPUHiJ2bcDeRDN9M1aq2KgwmXmLSTWjijEMPbkPSTbdTEN2DxqFdRCruRY567kwVWD1qxeHThI0bCt337vtp6btxEuXwnoSZ5X9t48tU6EddVqOHDu1BkSQUIjC/5UN5KKHcT+fBhBjiSzco7s0Js1Qwf+sc6xpNetXVsk5qJhm8e97mal4Q25EycDfJaEpdr1vPj/aNOY8cr+BPmfiMbUgCFwdQCWWFZiOdiC6dUxrk+lh9SNrfxI1cqVN23cyKulgxphc5y8u+Cq9oQfAnxayLzmkauoaK1wWBA9azZ39hyAobaNm5lP5sL4+Yvp8IIe7S/Vjzq4tZibTYFiSwZDXiG3BN8b6HtE0Txxkm9s0yuj+bQLEsfJS52e8TEWNsglXqDf/8jTRiIaWxYtUU37tcTmL/xWuKP09JmBv4n9Kck3NoI5VjiXlmbbtFWkaeF6Fkxdstv5qxmeU10rHI6mGLSKBIrNJiarzk/gb5IYi65REz92jmLe4QJMYydgwuHq8K+yn6Mw29ZtyC/ZfaBAsa3Lv1GZn+d7FOll5vET/WC7qGXh5ySLgw938SkkO1q/seiYnhIoNv3BDBUnXD86P2CPIiH3HDM+UOw6EfaUvWTrblHBVCOxsa0pcqxSlUCxPeKHokJWlv+HMWmpaW47UP4U8QzeBCmqvtWzTuZmrdik5MBH2bcEhi2KyrauhyLf9vMsio2EH/Qt2gbO7ORs3YGZv8g5Cg2bkBeV0vGANxF1etedIFe1LvvKx4Oo3ukp0+QzQw+3JG9F+tHa4Y55bu1g1vJOY0DY6L2hbSfVntHxU709hdKN2Ihackb1HcAd+sW2bqNp9Guo0n3NeYu27O6fNJxnWQLCxjLzZqUowlQeEEX75q3e5lPfpgN/vqAwRg1nmb+IlFyFalvkJOze/Vqhukpgs30x3bmzWRj7gxkejSWz2fz2uz4mkE3Zq/IOUUSssn75FTkzgIHUrmcaPlLIuFZyQlUpsZFPd8GGx0674GNPAv9vWbzEb6aBzCyfZUtC5VcCc2m5uYaOXVRJLHPmORuB+fQZb81kNb44QLLeFdeXAgtgjqsMqiS2tY5LFpLEHT4iHwN5dU6t2ovZ2cXvKWxEEGAFks2uojAQlCUOFXl1KTo2OQMepwrDnTyNv/Jnz/muK1FL2bdsKw4sZbJt2GSeONk0dAQVO9jYJ9bY80UV7d2P6j9I1pRefdq1aeOhz7Ztu2FdwTWYQLEtcxc4Y8/AoebJ8fCxsqfFMIt6vXKDR11r17MsXS4VfblKJjM5P/WyP+FN1z/xH9U9xhkfflhkbPuOXXmObEm4cVO0WuV7HMYXY0kh/fpbvvtBDR0hGY1FZc6XjyID2K5GG1Lb/dFSS2xUvygPUPeKFCXxvHwLjZn9mT1ht+8+kS3xnNxiMPNnzvphrhtp6NKTmTufO31GNJmUXRAtseFRqP4DiWdq2c7QtRcxvLAGWHWue2AqzJ278xfTi8FMnGjfOHU3EdHYHD/VnpQi3MomNblDMBnKrThNseXtEZfhRzohmzfSdfNb7+jbdFRhTr9UHGYktoePqMYC+85dWGKEk6b5zGv2PfssS740T3jT9ehKY2x4L2OPPs4hD2uA9xnadCDO/MQpjDq5rObK3Kkrf+n34jFDmI8+LoxtfKG/kJMjTy+XmsZ8tsDQrTe5AOKeBWuMne84rCWb9U1aWBZ/YVm0RPax9pS9otVm/f6/it+mBr6EDKfYzPAjqqcixLzfmGTbuQtprKDTw8XgvWjMHjvOzFuoOPwtT4ZUUDskLD52viOQilk3sPbQA2dvGjVF5NTVj5Z7xsyZj5CWTy5mWEWDUbx9W8jMFC5fwZChgCVhzF85RTbPfJSl8NsRjZEmmEaORbIEbHKiYjIrLm3n07Uq3X+/xtiKeNzCRldMYyfYtmyzrVnHfDjTNHwUigoUW6iWEfbwV12DaCxO+F7EM3ryFMuCxbb/rWf3H8TUiQaDkqgj/y+8Ia/YEfyFOf4966rV7OEjfEamaDaT23J5OntisnKEVLrY9qRk0r/a9cittXFvMDM/NY0Zb2jXqfApn1cNjcSIIDQYu/aiBr+MLMgybyG5r+slbuF13MlT5JyE4/CDbf1GetanphGjMJSwOKVZ6WJDJJoWfr8sURSSR2df60bqm7UKaOek6Kpv25G/miGfinPHT1qWLgMzqYjdT5FLHVsW5qNZzleGNbDvO4A+2TZvLQ3sPMfVD2rEKNuOBPmupzwEAow8ZY8y1ncIW9lL1oU3ZH8+jJ7Y1c4SNOZv2ISKG8Is/Jz95TC54mU03iGXpgjsHBmo8/5t9xdIOHW/naOZhtbXN2/jeTEivCFeSu5P3hmX5kZ+7To9bTpZ1YFUDupIkbqoZvrWHeQrDqqKoMWdO0+O/ocM87HxeOewHeiCeOOmbc1aqt9AfUxrcq3P2xDUjUSnSW7fuTsyXGbOPPvOBFTsGDtEMjZlr8f9HkWJS7t8xfHFFI678Bszex65D1vIfd5ZbEWQPOXk8KlpbFIKYrj16xXWr75FJst8Mte6YiX7YyJ/8rQjddGp37NHeTdxkvqQ1YnA8mHmLyI5OTJzZCkYpiNHmfmLXTezyghbVZCNBLzXjeDkdkfTw9Sf6wZsZu4C5C0weHLpHfHsfOpdiV1EQZ7nGxs5IooQJEhI18i06/V/Bmzh8mV1O68VLn87ScjJJTdbHPceUN6axo7/M2BjUbh+X0fx5Ozho+RLaYA1mbmz5y1fLEM1mud+K/pexs7PZ/cf8JhwY59YAfXcrWzbxs0IAfKXUso6gGkugkBm0jWARcegevN6s+9Pgg3w9Ev6Ji2Kmvnc89gQFJjmSfHGbr0NjtJdHxWjrtExyH9kTWrWsk5IjRrVq3voogULlI+927HzyXV0m5h9W8jIhHsXLqara/olpECy0leuXoKkp3uoTqdTPvMewC4N+T8VzBX/Evj0vgAAAABJRU5ErkJggg==" style="width:60px;" alt="Logo">
                @else
                <img src="{{ public_path('images/wawawa.png') }}" style="width:60px;" alt="Logo">
                @endif
            </td>
            <td width="88%">
                <table style="width:100%; font-weight:bold; font-size:18px;" cellspacing=0>
                    <tr>
                        <td style="border-bottom:2px solid #000;">PT. WARNA ABADI MITRA BERSAMA</td>
                        <td style="border-bottom:2px solid #000; text-align:right">SLIP GAJI</td>
                    </tr>
                    <tr>
                        <td>
                            <span style="color:#d00; font-size:13px; font-weight:normal;">~ private & confidential ~</span>
                        </td>
                        <td style="text-align:right;">{{ nama_bulan($default['bulan']) }} {{ $default['tahun'] }}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <table class="detail-karyawan" style="width:100%;">
        <tr>
            <td style="width:50%;" valign="bottom">
                <table style="width:100%">
                    <tr>
                        <td style="width:42%;">Nama Karyawan</td>
                        <td style="width:3%;"> : </td>
                        <td style="width:55%;">{{ $row->nama }}</td>
                    </tr>
                    <tr>
                        <td>Jabatan</td>
                        <td> : </td>
                        <td>{{ $row->nama_jabatan ? $row->nama_jabatan : '-' }}</td>
                    </tr>
                    <tr>
                        <td>Departemen</td>
                        <td> : </td>
                        <td>{{ $row->departemen ? $row->departemen : '-' }}</td>
                    </tr>
                    <tr>
                        <td>Cabang</td>
                        <td> : </td>
                        <td>{{ isset($listdiv[$row->id_divisi]) ? $listdiv[$row->id_divisi] : '-' }}</td>
                    </tr>
                </table>
            </td>
            <td style="width:50%;" valign="bottom">
                <table style="width:100%">
                    <tr>
                        <td style="width:32%;"></td>
                        <td style="width:3%;"></td>
                        <td style="width:65%;"></td>
                    </tr>
                    <tr>
                        <td>NIK</td>
                        <td> : </td>
                        <td>{{ $row->nik ? $row->nik : '-' }}</td>
                    </tr>
                    <tr>
                        <td>Golongan</td>
                        <td> : </td>
                        <td>{{ $row->kode_golongan }}</td>
                    </tr>
                    <tr>
                        <td>NPWP</td>
                        <td> : </td>
                        <td>{{ strlen($row->npwp) > 0 ? $row->npwp : '-' }}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <?php
    $penerimaan = isset($gaji[$row->id_karyawan][1]) ? $gaji[$row->id_karyawan][1] : [];
    $potongan = isset($gaji[$row->id_karyawan][2]) ? $gaji[$row->id_karyawan][2] : [];

    $total_penerimaan = array_sum($penerimaan);
    $total_potongan = array_sum($potongan);
    $pph_penerimaan = 0;
    $pph_potongan = 0;
    $thr_penerimaan = 0;
    $thr_potongan = 0;
    ?>
    <table class="detail-gaji" style="width:100%;" cellspacing=0>
        <thead>
            <tr>
                <th width="50%" class="heading">PENERIMAAN</th>
                <th width="50%" class="heading border-left">POTONGAN</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="padding:.75rem;" valign="top">
                    <table class="penerimaan" style="width:100%;">
                        @foreach($penerimaan as $label=>$nilai)
                            <?php
                            $current_label = isset($listgaji[$label]) ? $listgaji[$label] : ucwords(str_replace("_", " ", $label));
                            if (strpos(strtolower($current_label), 'pph') !== false && strpos(strtolower($current_label), '21') !== false) {
                                $pph_penerimaan = $nilai;
                                continue;
                            }
                            if (strpos(strtolower($current_label), 'ke-13') !== false || (strpos(strtolower($current_label), 't h r') !== false || strpos(strtolower($current_label), 'thr') !== false)) {
                                $thr_penerimaan = $nilai;
                                continue;
                            }                            
                            ?>
                            <tr>
                                <td>{{ $current_label }}</td>
                                <td style="text-align:right">{{ number_format($nilai, 0, ',', '.') }}</td>
                            </tr>
                        @endforeach
                        @if ($thr_penerimaan <> 0)
                            <tr>
                                <td>Gaji Ke-13 / T H R</td>
                                <td style="text-align:right">{{ number_format($nilai, 0, ',', '.') }}</td>
                            </tr>
                        @endif
                    </table>
                </td>
                <td style="padding:.75rem;" class="border-left" valign="top">
                    <table class="penerimaan" style="width:100%;">
                        @foreach($potongan as $label=>$nilai)
                            <?php
                            $current_label = isset($listgaji[$label]) ? $listgaji[$label] : ucwords(str_replace("_", " ", $label));
                            if (strpos(strtolower($current_label), 'pph') !== false && strpos(strtolower($current_label), '21') !== false) {
                                $pph_potongan = $nilai;
                                continue;
                            }
                            if (strpos(strtolower($current_label), 'ke-13') !== false || (strpos(strtolower($current_label), 't h r') !== false || strpos(strtolower($current_label), 'thr') !== false)) {
                                $thr_potongan = $nilai;
                                continue;
                            }                            
                            ?>
                            <tr>
                                <td>{{ isset($listgaji[$label]) ? $listgaji[$label] : ucwords(str_replace("_", " ", $label)) }}</td>
                                <td style="text-align:right">{{ number_format($nilai, 0, ',', '.') }}</td>
                            </tr>
                        @endforeach
                    </table>

                </td>
            </tr>
            @if ($pph_penerimaan <> 0 || $pph_potongan <> 0 || $thr_penerimaan <> 0 || $thr_potongan <> 0)
            <tr>
                <td style="padding:.75rem;" valign="top">
                    @if ($pph_penerimaan <> 0 || $pph_potongan <> 0)
                    <table class="penerimaan" style="width:100%;">
                        <tr>
                            <td>Tunjangan Pph Pasal 21 By Co.</td>
                            <td style="text-align:right">{{ number_format($pph_penerimaan, 0, ',', '.') }}</td>
                        </tr>
                    </table>
                    @endif
                </td>
                <td style="padding:.75rem;" class="border-left" valign="top">
                    @if ($pph_penerimaan <> 0 || $pph_potongan <> 0)
                    <table class="penerimaan" style="width:100%;">
                        <tr>
                            <td>Pph Pasal 21 By Co. paid <span style="color:#d00">*</span></td>
                            <td style="text-align:right">{{ number_format($pph_potongan, 0, ',', '.') }}</td>
                        </tr>
                    </table>
                    @endif

                    @if ($thr_penerimaan <> 0 || $thr_potongan <> 0)
                    <table class="penerimaan" style="width:100%;">
                        <tr>
                            <td>Gaji Ke-13 / T H R paid <span style="color:#d00">*</span></td>
                            <td style="text-align:right">{{ number_format($thr_potongan, 0, ',', '.') }}</td>
                        </tr>
                    </table>
                    @endif

                </td>
            </tr>
            @endif

        </tbody>
        <tfoot>
            <tr>
                <td style="padding:.75rem; border-top: 2px solid #000; border-bottom:3px double #000;" valign="top">
                    <table class="penerimaan" style="width:100%; font-weight:bold;">
                        <tr>
                            <td>Total Penerimaan</td>
                            <td style="text-align:right">{{ number_format($total_penerimaan, 0, ',', '.') }}</td>
                        </tr>
                    </table>
                </td>
                <td style="padding:.75rem; border-top: 2px solid #000; border-bottom:3px double #000;" class="border-left" valign="top">
                    <table class="penerimaan" style="width:100%; font-weight:bold;">
                        <tr>
                            <td>Total Potongan</td>
                            <td style="text-align:right">{{ number_format($total_potongan, 0, ',', '.') }}</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tfoot>
    </table>


    <table style="width:100%; margin-top:1rem;" class="take-home-pay">
        <tr>
            <td style="width:50%;">
                <table style="width:100%;">
                    <tr>
                        <td class="heading" style="font-weight:bold; padding-left:.75rem">Take Home Pay</td>
                        <td class="heading" style="text-align:right; border:1px solid #000; font-weight:bold;">
                            {{ number_format($total_penerimaan - $total_potongan, 0, ',', '.') }}
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width:50%; text-align:center;">
                Badung, {{ date('d F Y') }}
            </td>
        </tr>
    </table>
    <table class="footer" style="width:100%; margin-top:2rem;">
        <tr>
            <td style="width:50%;" valign="bottom">
                @if (strlen($row->no_rek) > 0 && $row->status_transfer == 1)
                    Ditransfer ke : <br>
                    Bank BCA<br>
                    a/c <span class="no-rekening">{{ $row->no_rek }}</span><br>
                    a.n. <span class="nama-rekening">{{ $row->nama }}</span>
                @else
                    Dibayar : Cash <br><br><br><br>
                @endif
            </td>
            <td style="width:50%; text-align:center;" valign="bottom">
                <u>Katania Prasetya</u>
                <br>
                HRD
            </td>
        </tr>
    </table>

    <br>
    <br>
    <hr>
</div>
</body>
</html>