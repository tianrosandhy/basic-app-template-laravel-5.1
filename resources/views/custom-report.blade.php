@extends('template/t_panel')

@section('content')
<h2>{{ $title }}</h2>

<div class="well">
	<h3>Filter Data</h3>
	<form action="" method="get">
		<div class="row">
			<div class="col-sm-3">Divisi</div>
			<div class="col-sm-6">
				<select name="divisi" class="form-control">
					<option value="">- Seluruh Divisi -</option>
					@foreach($list_divisi as $iddiv => $nmdiv)
					<option value="{{ $iddiv }}" {{ $default['divisi'] == $iddiv ? 'selected' : '' }}>{{ $nmdiv }}</option>
					@endforeach
				</select>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3">Bulan</div>
			<div class="col-sm-6">
				<select name="bulan" id="" class="form-control">
				@foreach(nama_bulan() as $idbul => $nmbul)
					<option value="{{ $idbul }}" {{ $default['bulan'] == $idbul ? 'selected' : '' }} >{{ $nmbul }}</option>
				@endforeach
				</select>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3">Tahun</div>
			<div class="col-sm-6">
				<input type="number" name="tahun" min=1970 max=2100 class="form-control" value="{{ $default['tahun'] }}">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3"></div>
			<div class="col-sm-6">
				<button class="btn btn-primary">Proses</button>
				<a href="report/finance" class="btn btn-danger">Refresh</a>
			</div>
		</div>
	</form>
</div>

<a href="{{ url('report/finance/print?') . $_SERVER['QUERY_STRING'] }}" class="btn btn-primary">Print</a>

<table class="table table-hover">
	<thead>
		<tr>
			<th>Cabang</th>
			<th>Gaji Pokok & Tunjangan</th>
			<th>Pt Absensi</th>
			<th>Insentif Overtime</th>
			<th>Biaya Gaji</th>
			<th>Insentif</th>
			<th>Bonus Produk Unggulan</th>
			<th>Gaji Kotor</th>
			<th>BP Jamsostek</th>
			<th>BPJS Kesehatan</th>
			<th>Iuran Suka Duka</th>
			<th>Pt Kasbon</th>
			<th>Pt Denda</th>
			<th>Gaji Bersih</th>
		</tr>
	</thead>
	<tbody>
	@foreach($sorted_divisi as $dv)
		<?php
		$iddivisi = $dv->id;
		$content = $data[$iddivisi] ?? [];
		?>
		<?php
		$tmpinse = isset($content['Insentive Overtime']) ? $content['Insentive Overtime'] : 0;
		$tmplem = isset($content['Lembur']) ? $content['Lembur'] : 0;

		$tlt = isset($content['Potongan Terlambat']) ? $content['Potongan Terlambat'] : 0;
		$ptg = isset($content['Potongan Denda']) ? $content['Potongan Denda'] : 0;
		$lain = isset($content['Potongan Lain-Lain']) ? $content['Potongan Lain-Lain'] : 0;


		$gjtj = 0;
		$gjtj += (isset($content['Gaji Pokok']) ? ($content['Gaji Pokok']) : 0);
		$gjtj += (isset($content['Tunjangan Jabatan']) ? ($content['Tunjangan Jabatan']) : 0);
		$gjtj += (isset($content['Tunjangan Golongan']) ? ($content['Tunjangan Golongan']) : 0);
		$gjtj += (isset($content['Tunjangan Lain']) ? ($content['Tunjangan Lain']) : 0);
		$gjtj += (isset($content['Tunjangan Penempatan']) ? ($content['Tunjangan Penempatan']) : 0);		
		$gjtj += (isset($content['Tunjangan Tahunan']) ? ($content['Tunjangan Tahunan']) : 0);		
		
        $pu = isset($content['Bonus PU']) ? $content['Bonus PU'] : 0;
        $potongan_pu = isset($content['Potongan Bonus PU']) ? $content['Potongan Bonus PU'] : 0        
		?>
		<tr>
			<td>{{ $list_divisi[$iddivisi]}}</td>
			<td>{{ number_format(round($gjtj)) }}</td>
			<td>{{ isset($content['Potongan Absensi']) ? number_format($content['Potongan Absensi']) : 0 }}</td>
			<td>{{ number_format(round(($tmpinse + $tmplem))) }}</td>
			<td>{{ isset($content['Biaya Gaji']) ? number_format($content['Biaya Gaji']) : 0 }}</td>

			<td>{{ isset($content['Insentif']) ? number_format($content['Insentif']) : 0 }}</td>
			<td>{{ number_format($pu - $potongan_pu) }}</td>
			<td>{{ isset($content['Gaji Kotor']) ? number_format($content['Gaji Kotor']) : 0 }}</td>
			<td>{{ isset($content['BP Jamsostek']) ? number_format($content['BP Jamsostek']) : 0 }}</td>
			<td>{{ isset($content['BPJS Kesehatan']) ? number_format($content['BPJS Kesehatan']) : 0 }}</td>
			<td>{{ isset($content['Iuran Suka Duka']) ? number_format($content['Iuran Suka Duka']) : 0 }}</td>
			<td>{{ isset($content['Potongan Kasbon']) ? number_format($content['Potongan Kasbon']) : 0 }}</td>
			<td>{{ number_format($tlt + $ptg + $lain) }}</td>
			<td>{{ isset($content['Gaji Bersih']) ? number_format($content['Gaji Bersih']) : 0 }}</td>
		</tr>
	@endforeach
		<tr>
			<?php
            $tim = isset($total_kesimpulan['Insentive Overtime']) ? $total_kesimpulan['Insentive Overtime'] : 0;
            $tle = isset($total_kesimpulan['Lembur']) ? $total_kesimpulan['Lembur'] : 0;

            $atlt = isset($total_kesimpulan['Potongan Terlambat']) ? $total_kesimpulan['Potongan Terlambat'] : 0;
            $akes = isset($total_kesimpulan['Potongan Denda']) ? $total_kesimpulan['Potongan Denda'] : 0;
            $alain = isset($total_kesimpulan['Potongan Lain-Lain']) ? $total_kesimpulan['Potongan Lain-Lain'] : 0;

            $ksgjtj = 0;
            $ksgjtj += (isset($total_kesimpulan['Gaji Pokok']) ? ($total_kesimpulan['Gaji Pokok']) : 0);
            $ksgjtj += (isset($total_kesimpulan['Tunjangan Jabatan']) ? ($total_kesimpulan['Tunjangan Jabatan']) : 0);
            $ksgjtj += (isset($total_kesimpulan['Tunjangan Golongan']) ? ($total_kesimpulan['Tunjangan Golongan']) : 0);
            $ksgjtj += (isset($total_kesimpulan['Tunjangan Lain']) ? ($total_kesimpulan['Tunjangan Lain']) : 0);
            $ksgjtj += (isset($total_kesimpulan['Tunjangan Penempatan']) ? ($total_kesimpulan['Tunjangan Penempatan']) : 0);	
            $ksgjtj += (isset($total_kesimpulan['Tunjangan Tahunan']) ? ($total_kesimpulan['Tunjangan Tahunan']) : 0);	
			
            $total_pu = isset($total_kesimpulan['Bonus PU']) ? $total_kesimpulan['Bonus PU'] : 0;
            $total_potongan_pu = isset($total_kesimpulan['Potongan Bonus PU']) ? $total_kesimpulan['Potongan Bonus PU'] : 0;
			?>
			<th>Total</th>
			<th>{{ number_format($ksgjtj) }}</th>
			<th>{{ isset($total_kesimpulan['Potongan Absensi']) ? number_format($total_kesimpulan['Potongan Absensi']) : 0 }}</th>
			<th>{{ number_format($tim + $tle) }}</th>
			<th>{{ isset($total_kesimpulan['Biaya Gaji']) ? number_format($total_kesimpulan['Biaya Gaji']) : 0 }}</th>

			<th>{{ isset($total_kesimpulan['Insentif']) ? number_format($total_kesimpulan['Insentif']) : 0 }}</th>
			<th>{{ number_format($total_pu - $total_potongan_pu) }}</th>
			<th>{{ isset($total_kesimpulan['Gaji Kotor']) ? number_format($total_kesimpulan['Gaji Kotor']) : 0 }}</th>

			<th>{{ isset($total_kesimpulan['BP Jamsostek']) ? number_format($total_kesimpulan['BP Jamsostek']) : 0 }}</th>
			<th>{{ isset($total_kesimpulan['BPJS Kesehatan']) ? number_format($total_kesimpulan['BPJS Kesehatan']) : 0 }}</th>
			<th>{{ isset($total_kesimpulan['Iuran Suka Duka']) ? number_format($total_kesimpulan['Iuran Suka Duka']) : 0 }}</th>
			<th>{{ isset($total_kesimpulan['Potongan Kasbon']) ? number_format($total_kesimpulan['Potongan Kasbon']) : 0 }}</th>
			<th>{{ number_format($atlt + $akes + $alain) }}</th>
			<th>{{ isset($total_kesimpulan['Gaji Bersih']) ? number_format($total_kesimpulan['Gaji Bersih']) : 0 }}</th>
		</tr>

	</tbody>
</table>

@endsection

