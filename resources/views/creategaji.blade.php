<div style="width:600px">
	<h2>{{ $title }}</h2>

	<div class="panel panel-primary">
		<div class="panel-heading">Detail Karyawan</div>
		<div class="panel-body">
			<table>
				<tr>
					<th>Nama Karyawan</th>
					<td> : </td>
					<td>{{ $selected->nama }}</td>
				</tr>
				<tr>
					<th>TTL</th>
					<td> : </td>
					<td>{{ $selected->ttl }}</td>
				</tr>
				<tr>
					<th>Jenis Kelamin</th>
					<td> : </td>
					<td>{{ ($selected->jk == 1) ? "Pria" : "Wanita" }}</td>
				</tr>
				<tr>
					<th>Alamat</th>
					<td> : </td>
					<td>{{ $selected->alamat }}</td>
				</tr>
				<tr>
					<th>Tanggal Masuk Kerja</th>
					<td> : </td>
					<td>{{ indo_date($selected->tgl_masuk) }} </td>
				</tr>

			</table>		
		</div>
	</div>

	<div class="well">
		<form action="{{ $action_button }}" method="post" class='form-horizontal'>
			<input type="hidden" name="id" value="{{ Input::get('id') }}">
			<input type="hidden" name="qs" value="{{ $_SERVER['QUERY_STRING'] }}">
			<div class="form-group">
				<div class="col-sm-4">Periode Berlaku</div>
				<div class="col-sm-8">
					<div class="row">
						<div class="col-xs-7">
							<select name="bulan" class="form-control">
								@foreach(nama_bulan() as $i=>$bln)
								<option value="{{ $i }}" {{$i==$default['bulan'] ? "selected" : ""}} >{{ $bln }}</option>
								@endforeach
							</select>
						</div>
						<div class="col-xs-5">
								<input type="number" name="tahun" min=1900 max=2100 class="form-control" value="{{ $default['tahun'] }}">
						</div>
					</div>
				</div>
			</div>
			@foreach($master_gaji as $msg)

				@if (get_priviledge()[0] >= 3 && in_array(strtolower($msg['label']), config('app.hr_ignore')))
				@else
				<div class="form-group" style="border-bottom:1px solid #ccc; padding-bottom:.75em; {{ ($msg['label'] == 'Tunjangan Lain' && Auth::user()->priviledge > 1) ? 'display:none' : '' }}">
					<div class="col-sm-4">{{ $msg['label'] }}</div>
					<div class="col-sm-8">
						<?php
						$active_until_period = $default_gaji[$id_karyawan][$msg['id']]['active_until_period'] ?? null;
						$value_must_resetted = true;
						if (empty($active_until_period)) {
							$active_until_period = date('Y-m-01');
							$value_must_resetted = false;
						}

						if ($value_must_resetted) {
							// check active_until_period vs default[bulan] default[tahun]
							if (strtotime($active_until_period) >= strtotime($default['tahun'].'-'.$default['bulan'].'-01')) {
								$value_must_resetted = false;
							} else {
								$active_until_period = date('Y-m-d', strtotime($default['tahun'].'-'.$default['bulan'].'-01'));
							}
						}
						?>
						<div class="input-group">
							<div class="input-group-addon">Rp</div>
							<input type="number" name="gaji[{{ $msg['id'] }}]" class="form-control" value="{{ $value_must_resetted ? null : (isset($default_gaji[$id_karyawan][$msg['id']]) ? $default_gaji[$id_karyawan][$msg['id']]['value'] : 0) }}">
						</div>
						@if (intval($msg['is_ranged_period'] ?? 0) == 1)
						<div>
							Berlaku hingga 
							<div class="row">
								<div class="col-xs-7">
									<select name="active_until_period_bulan[{{ $msg['id'] }}]" class="form-control">
										@foreach(nama_bulan() as $i=>$bln)
										<option value="{{ $i }}" {{ $i==intval(date('m', strtotime($active_until_period))) ? "selected" : "" }} >{{ $bln }}</option>
										@endforeach
									</select>
								</div>
								<div class="col-xs-5">
										<input type="number" name="active_until_period_tahun[{{ $msg['id'] }}]" min=1900 max=2100 class="form-control" value="{{ date('Y', strtotime($active_until_period)) }}">
								</div>
							</div>
							
						</div>
						@endif
					</div>
				</div>
				@endif
			@endforeach

			@if(isset($apply_to))
			<div class="form-group">
				<div class="col-sm-4">
					Apply ke Divisi
				</div>
				<div class="col-sm-8">
					<?php $apply_to[""] = "- Karyawan ini saja -"; ?>
					<select name="apply" class="form-control">
						@foreach($apply_to as $id=>$dv)
						<option value="{{ $id }}" {{ $id == $default['divisi'] ? "selected" : "" }} >{{ $dv }}</option>
						@endforeach
					</select>
				</div>
			</div>
			@endif

			<div align="center">
				<button class="btn btn-primary">Simpan Data Gaji</button>
			</div>
		</form>	
	</div>
	
</div>
