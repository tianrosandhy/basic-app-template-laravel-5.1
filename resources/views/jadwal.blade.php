@extends('template/t_panel')

@section('content')
<h2>{{ $title }}</h2>

@if(isset($description))
<div class="alert alert-info">{{ $description }}</div>
@endif


@include('template/t_well')


<div class="shift_button_holder">
	<ul class="nav nav-pills">
	@foreach($list_kode as $inc=>$list)
		<li data-background="{!! $list['background'] !!}" data-unique="{{ $list['type'].$list['pk'] }}" data-type="{{ $list['type'] }}">
			<a>
			{{ $list['label'] }}
			<span class="label label-primary">{{ $list['kode'] }}</span>
			</a>
		</li>
	@endforeach
	</ul>
</div>


<table class="table compact">
	<thead>
		<tr>
			<th rowspan=2>Nama Karyawan</th>
			<th rowspan=2>Divisi</th>
			@foreach($month_control as $ind=>$jmlhari)
				<th colspan="{{ $jmlhari }}">{{ nama_bulan($ind) }}</th>
			@endforeach
			<th rowspan=2></th>
		</tr>
		<tr>
			@foreach($calendar as $cl)
				@if(isset($holiday[strtotime($cl)]))
					@if($holiday[strtotime($cl)]['type'] == "HR")
						<th style="background:#d00" title="{{ $holiday[strtotime($cl)]['label'] }}">{{ date("d", strtotime($cl)) }}</th>
					@else 
						<th style="background:#fc0" title="{{ $holiday[strtotime($cl)]['label'] }}">{{ date("d", strtotime($cl)) }}</th>
					@endif
				@else
					<th>{{ date("d", strtotime($cl)) }}</th>
				@endif
			@endforeach
		</tr>
	</thead>
	<tbody>
		@foreach($mutasi as $data)
		<tr>
			<td><a href="detail/{{ $data->id_karyawan }}" data-featherlight="ajax">{{ $data->nama }}</a></td>
			<td>{{ $data->nama_divisi }}</td>
			@foreach($calendar as $cl)
				<td>
					@if(in_array($cl, $data->tgl))
						@if(strlen($data->type) > 0 && strtotime($data->tgl_start) <= strtotime($cl) && strtotime($data->tgl_end) >= strtotime($cl))
							{{ $data->type }}
						@else

							{{-- */$index = strtotime($cl)."-".$data->id_karyawan."-".$data->id_divisi; /* --}}
							@if(isset($content[$index]))
							<div class="shift_box" data-tanggal="{{ $cl }}" data-karyawan="{{ $data->id_karyawan }}" data-divisi="{{ $data->id_divisi }}" style="background:{{ $content[$index]['bg'] }}">{{ $content[$index]['ctn'] }}</div>
							@else
							<div class="shift_box" data-tanggal="{{ $cl }}" data-karyawan="{{ $data->id_karyawan }}" data-divisi="{{ $data->id_divisi }}" >
							</div>
							@endif

						
						@endif

					@endif
				</td>
			@endforeach
		</tr>
		@endforeach
	</tbody>
</table>


<div class="shift_button_holder">
	<ul class="nav nav-pills">
	@foreach($list_kode as $inc=>$list)
		<li data-background="{!! $list['background'] !!}" data-unique="{{ $list['type'].$list['pk'] }}" data-type="{{ $list['type'] }}">
			<a>
			{{ $list['label'] }}
			<span class="label label-primary">{{ $list['kode'] }}</span>
			</a>
		</li>
	@endforeach
	</ul>
</div>


<script>
	var APP_URL = {!! json_encode(url('/')) !!}
	$(function(){
		$(".nav-pills li").click(function(){
			var uniq = $(this).attr("data-unique");

			$(".nav-pills li").removeClass("active");
			$("[data-unique="+uniq+"]").addClass("active");
		});

		$(".shift_box").click(function(){
			var bef = $(this).html();
			var act = $(".nav-pills li.active");
			var isi = $(".nav-pills li.active span.label").html();
			var vartipe = $(".nav-pills li.active").attr("data-type");

			if(bef !== isi){
				//run ajax
				//prepare data
				vartgl = $(this).attr("data-tanggal");
				varkar = $(this).attr("data-karyawan");
				vardiv = $(this).attr("data-divisi");

				$(this).html(isi);
				$(this).css("background-color", act.attr("data-background"));

				$.ajax({
					method : "GET",
					url : APP_URL+"/api/jadwal",
					dataType : "json",
					data : {
						tanggal : vartgl,
						id_karyawan : varkar,
						id_divisi : vardiv,
						type : vartipe,
						nilai : isi
					}
				}).done(function(data){
					if(data['error'])
						alertify.error(data['error'], 1);
					if(data['success'])
						alertify.success(data['success'], 1);
				});
			}

		});

	});
</script>

@stop


