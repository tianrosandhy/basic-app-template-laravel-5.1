<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => 'ccf264f9fd3e7b48dfd4527b2934b889ab333c65',
    'name' => 'laravel/laravel',
  ),
  'versions' => 
  array (
    'classpreloader/classpreloader' => 
    array (
      'pretty_version' => '3.1.0',
      'version' => '3.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bc7206aa892b5a33f4680421b69b191efd32b096',
    ),
    'cordoval/hamcrest-php' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'danielstjules/stringy' => 
    array (
      'pretty_version' => '1.10.0',
      'version' => '1.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4749c205db47ee5b32e8d1adf6d9aff8db6caf3b',
    ),
    'davedevelopment/hamcrest-php' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'dnoegel/php-xdg-base-dir' => 
    array (
      'pretty_version' => '0.1',
      'version' => '0.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '265b8593498b997dc2d31e75b89f053b5cc9621a',
    ),
    'doctrine/inflector' => 
    array (
      'pretty_version' => 'v1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e11d84c6e018beedd929cff5220969a3c6d1d462',
    ),
    'doctrine/instantiator' => 
    array (
      'pretty_version' => '1.0.5',
      'version' => '1.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '8e884e78f9f0eb1329e445619e04456e64d8051d',
    ),
    'facebook/graph-sdk' => 
    array (
      'pretty_version' => '5.6.1',
      'version' => '5.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2f9639c15ae043911f40ffe44080b32bac2c5280',
    ),
    'fzaninotto/faker' => 
    array (
      'pretty_version' => 'v1.7.1',
      'version' => '1.7.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd3ed4cc37051c1ca52d22d76b437d14809fc7e0d',
    ),
    'hamcrest/hamcrest-php' => 
    array (
      'pretty_version' => 'v1.2.2',
      'version' => '1.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b37020aa976fa52d3de9aa904aa2522dc518f79c',
    ),
    'illuminate/auth' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.1.46',
      ),
    ),
    'illuminate/broadcasting' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.1.46',
      ),
    ),
    'illuminate/bus' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.1.46',
      ),
    ),
    'illuminate/cache' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.1.46',
      ),
    ),
    'illuminate/config' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.1.46',
      ),
    ),
    'illuminate/console' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.1.46',
      ),
    ),
    'illuminate/container' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.1.46',
      ),
    ),
    'illuminate/contracts' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.1.46',
      ),
    ),
    'illuminate/cookie' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.1.46',
      ),
    ),
    'illuminate/database' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.1.46',
      ),
    ),
    'illuminate/encryption' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.1.46',
      ),
    ),
    'illuminate/events' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.1.46',
      ),
    ),
    'illuminate/exception' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.1.46',
      ),
    ),
    'illuminate/filesystem' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.1.46',
      ),
    ),
    'illuminate/hashing' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.1.46',
      ),
    ),
    'illuminate/html' => 
    array (
      'pretty_version' => 'v5.0.0',
      'version' => '5.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '3d1009bb8e0f25720c914af5c1f4015dd373c9ef',
    ),
    'illuminate/http' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.1.46',
      ),
    ),
    'illuminate/log' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.1.46',
      ),
    ),
    'illuminate/mail' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.1.46',
      ),
    ),
    'illuminate/pagination' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.1.46',
      ),
    ),
    'illuminate/pipeline' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.1.46',
      ),
    ),
    'illuminate/queue' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.1.46',
      ),
    ),
    'illuminate/redis' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.1.46',
      ),
    ),
    'illuminate/routing' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.1.46',
      ),
    ),
    'illuminate/session' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.1.46',
      ),
    ),
    'illuminate/support' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.1.46',
      ),
    ),
    'illuminate/translation' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.1.46',
      ),
    ),
    'illuminate/validation' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.1.46',
      ),
    ),
    'illuminate/view' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.1.46',
      ),
    ),
    'jakub-onderka/php-console-color' => 
    array (
      'pretty_version' => '0.1',
      'version' => '0.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e0b393dacf7703fc36a4efc3df1435485197e6c1',
    ),
    'jakub-onderka/php-console-highlighter' => 
    array (
      'pretty_version' => 'v0.3.2',
      'version' => '0.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '7daa75df45242c8d5b75a22c00a201e7954e4fb5',
    ),
    'jeremeamia/superclosure' => 
    array (
      'pretty_version' => '2.3.0',
      'version' => '2.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '443c3df3207f176a1b41576ee2a66968a507b3db',
    ),
    'kodova/hamcrest-php' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'laravel/framework' => 
    array (
      'pretty_version' => 'v5.1.46',
      'version' => '5.1.46.0',
      'aliases' => 
      array (
      ),
      'reference' => '7f2f892e62163138121e8210b92b21394fda8d1c',
    ),
    'laravel/laravel' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => 'ccf264f9fd3e7b48dfd4527b2934b889ab333c65',
    ),
    'league/flysystem' => 
    array (
      'pretty_version' => '1.0.41',
      'version' => '1.0.41.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f400aa98912c561ba625ea4065031b7a41e5a155',
    ),
    'maatwebsite/excel' => 
    array (
      'pretty_version' => '2.1.23',
      'version' => '2.1.23.0',
      'aliases' => 
      array (
      ),
      'reference' => '8682c955601b6de15a8c7d6e373b927cc8380627',
    ),
    'mockery/mockery' => 
    array (
      'pretty_version' => '0.9.9',
      'version' => '0.9.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '6fdb61243844dc924071d3404bb23994ea0b6856',
    ),
    'monolog/monolog' => 
    array (
      'pretty_version' => '1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fd8c787753b3a2ad11bc60c063cff1358a32a3b4',
    ),
    'mpdf/mpdf' => 
    array (
      'pretty_version' => 'v8.1.1',
      'version' => '8.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e511e89a66bdb066e3fbf352f00f4734d5064cbf',
    ),
    'mtdowling/cron-expression' => 
    array (
      'pretty_version' => 'v1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9504fa9ea681b586028adaaa0877db4aecf32bad',
    ),
    'myclabs/deep-copy' => 
    array (
      'pretty_version' => '1.11.0',
      'version' => '1.11.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '14daed4296fae74d9e3201d2c4925d1acb7aa614',
    ),
    'nesbot/carbon' => 
    array (
      'pretty_version' => '1.22.1',
      'version' => '1.22.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '7cdf42c0b1cc763ab7e4c33c47a24e27c66bfccc',
    ),
    'nikic/php-parser' => 
    array (
      'pretty_version' => 'v2.1.1',
      'version' => '2.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '4dd659edadffdc2143e4753df655d866dbfeedf0',
    ),
    'niklasravnsborg/laravel-pdf' => 
    array (
      'pretty_version' => 'v4.1.0',
      'version' => '4.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a5f5c22dd5e10d8f536102cec01c21282e18ebae',
    ),
    'paragonie/random_compat' => 
    array (
      'pretty_version' => 'v1.4.2',
      'version' => '1.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '965cdeb01fdcab7653253aa81d40441d261f1e66',
    ),
    'php-http/message-factory' => 
    array (
      'pretty_version' => 'v1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a478cb11f66a6ac48d8954216cfed9aa06a501a1',
    ),
    'phpdocumentor/reflection-common' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '21bdeb5f65d7ebf9f43b1b25d404f87deab5bfb6',
    ),
    'phpdocumentor/reflection-docblock' => 
    array (
      'pretty_version' => '4.1.1',
      'version' => '4.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2d3d238c433cf69caeb4842e97a3223a116f94b2',
    ),
    'phpdocumentor/type-resolver' => 
    array (
      'pretty_version' => '0.4.0',
      'version' => '0.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9c977708995954784726e25d0cd1dddf4e65b0f7',
    ),
    'phpoffice/phpexcel' => 
    array (
      'pretty_version' => '1.8.1',
      'version' => '1.8.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '372c7cbb695a6f6f1e62649381aeaa37e7e70b32',
    ),
    'phpspec/php-diff' => 
    array (
      'pretty_version' => 'v1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '30e103d19519fe678ae64a60d77884ef3d71b28a',
    ),
    'phpspec/phpspec' => 
    array (
      'pretty_version' => '2.5.8',
      'version' => '2.5.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd8a153dcb52f929b448c0bf2cc19c7388951adb1',
    ),
    'phpspec/prophecy' => 
    array (
      'pretty_version' => 'v1.7.2',
      'version' => '1.7.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c9b8c6088acd19d769d4cc0ffa60a9fe34344bd6',
    ),
    'phpunit/php-code-coverage' => 
    array (
      'pretty_version' => '2.2.4',
      'version' => '2.2.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'eabf68b476ac7d0f73793aada060f1c1a9bf8979',
    ),
    'phpunit/php-file-iterator' => 
    array (
      'pretty_version' => '1.4.2',
      'version' => '1.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '3cc8f69b3028d0f96a9078e6295d86e9bf019be5',
    ),
    'phpunit/php-text-template' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '31f8b717e51d9a2afca6c9f046f5d69fc27c8686',
    ),
    'phpunit/php-timer' => 
    array (
      'pretty_version' => '1.0.9',
      'version' => '1.0.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '3dcf38ca72b158baf0bc245e9184d3fdffa9c46f',
    ),
    'phpunit/php-token-stream' => 
    array (
      'pretty_version' => '1.4.11',
      'version' => '1.4.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e03f8f67534427a787e21a385a67ec3ca6978ea7',
    ),
    'phpunit/phpunit' => 
    array (
      'pretty_version' => '4.8.36',
      'version' => '4.8.36.0',
      'aliases' => 
      array (
      ),
      'reference' => '46023de9a91eec7dfb06cc56cb4e260017298517',
    ),
    'phpunit/phpunit-mock-objects' => 
    array (
      'pretty_version' => '2.3.8',
      'version' => '2.3.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ac8e7a3db35738d56ee9a76e78a4e03d97628983',
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '4ebe3a8bf773a19edfe0a84b6585ba3d401b724d',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0.0',
      ),
    ),
    'psy/psysh' => 
    array (
      'pretty_version' => 'v0.7.2',
      'version' => '0.7.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e64e10b20f8d229cac76399e1f3edddb57a0f280',
    ),
    'sebastian/comparator' => 
    array (
      'pretty_version' => '1.2.4',
      'version' => '1.2.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '2b7424b55f5047b47ac6e5ccb20b2aea4011d9be',
    ),
    'sebastian/diff' => 
    array (
      'pretty_version' => '1.4.3',
      'version' => '1.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '7f066a26a962dbe58ddea9f72a4e82874a3975a4',
    ),
    'sebastian/environment' => 
    array (
      'pretty_version' => '1.3.8',
      'version' => '1.3.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'be2c607e43ce4c89ecd60e75c6a85c126e754aea',
    ),
    'sebastian/exporter' => 
    array (
      'pretty_version' => '1.2.2',
      'version' => '1.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '42c4c2eec485ee3e159ec9884f95b431287edde4',
    ),
    'sebastian/global-state' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bc37d50fea7d017d3d340f230811c9f1d7280af4',
    ),
    'sebastian/recursion-context' => 
    array (
      'pretty_version' => '1.0.5',
      'version' => '1.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b19cc3298482a335a95f3016d2f8a6950f0fbcd7',
    ),
    'sebastian/version' => 
    array (
      'pretty_version' => '1.0.6',
      'version' => '1.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '58b3a85e7999757d6ad81c787a1fbf5ff6c628c6',
    ),
    'setasign/fpdi' => 
    array (
      'pretty_version' => 'v2.3.6',
      'version' => '2.3.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '6231e315f73e4f62d72b73f3d6d78ff0eed93c31',
    ),
    'swiftmailer/swiftmailer' => 
    array (
      'pretty_version' => 'v5.4.8',
      'version' => '5.4.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '9a06dc570a0367850280eefd3f1dc2da45aef517',
    ),
    'symfony/console' => 
    array (
      'pretty_version' => 'v2.7.34',
      'version' => '2.7.34.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a288d36702dc27144b5e1a3a25dd1c776737c939',
    ),
    'symfony/css-selector' => 
    array (
      'pretty_version' => 'v2.8.27',
      'version' => '2.8.27.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ba3204654efa779691fac9e948a96b4a7067e4ab',
    ),
    'symfony/debug' => 
    array (
      'pretty_version' => 'v2.7.34',
      'version' => '2.7.34.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f45a9086343c608d5c704ab450580fde6da0fa76',
    ),
    'symfony/dom-crawler' => 
    array (
      'pretty_version' => 'v2.7.34',
      'version' => '2.7.34.0',
      'aliases' => 
      array (
      ),
      'reference' => '217ad29b2a7eb8c1546947f7216c38f7128d12f5',
    ),
    'symfony/event-dispatcher' => 
    array (
      'pretty_version' => 'v2.8.27',
      'version' => '2.8.27.0',
      'aliases' => 
      array (
      ),
      'reference' => '1377400fd641d7d1935981546aaef780ecd5bf6d',
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => 'v2.7.34',
      'version' => '2.7.34.0',
      'aliases' => 
      array (
      ),
      'reference' => '936529dea4ebedcfe577700b6079a75eb918d21f',
    ),
    'symfony/http-foundation' => 
    array (
      'pretty_version' => 'v2.7.34',
      'version' => '2.7.34.0',
      'aliases' => 
      array (
      ),
      'reference' => '8561a9988c161160ee89766088a4579abebcc04b',
    ),
    'symfony/http-kernel' => 
    array (
      'pretty_version' => 'v2.7.34',
      'version' => '2.7.34.0',
      'aliases' => 
      array (
      ),
      'reference' => '4ff31d88d8cd25e96aab46f338d64ef7fd0a0ba5',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.5.0',
      'version' => '1.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7c8fae0ac1d216eb54349e6a8baa57d515fe8803',
    ),
    'symfony/polyfill-php56' => 
    array (
      'pretty_version' => 'v1.5.0',
      'version' => '1.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e85ebdef569b84e8709864e1a290c40f156b30ca',
    ),
    'symfony/polyfill-util' => 
    array (
      'pretty_version' => 'v1.5.0',
      'version' => '1.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '67925d1cf0b84bd234a83bebf26d4eb281744c6d',
    ),
    'symfony/process' => 
    array (
      'pretty_version' => 'v2.7.34',
      'version' => '2.7.34.0',
      'aliases' => 
      array (
      ),
      'reference' => '95c5ed0b9ec2a03fc6247d0455c008e96e97fbb4',
    ),
    'symfony/routing' => 
    array (
      'pretty_version' => 'v2.7.34',
      'version' => '2.7.34.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ebb0b3eb0be5a9f418e12e9234d1644f65d4c791',
    ),
    'symfony/translation' => 
    array (
      'pretty_version' => 'v2.7.34',
      'version' => '2.7.34.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd52cb2413f339a9eb19e625c6a0ee9476fc7e2af',
    ),
    'symfony/var-dumper' => 
    array (
      'pretty_version' => 'v2.7.34',
      'version' => '2.7.34.0',
      'aliases' => 
      array (
      ),
      'reference' => '57f5057702ba67eccb02a941d17a6db19a772bd5',
    ),
    'symfony/yaml' => 
    array (
      'pretty_version' => 'v3.3.6',
      'version' => '3.3.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ddc23324e6cfe066f3dd34a37ff494fa80b617ed',
    ),
    'tijsverkoyen/css-to-inline-styles' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ab03919dfd85a74ae0372f8baf9f3c7d5c03b04b',
    ),
    'vlucas/phpdotenv' => 
    array (
      'pretty_version' => 'v1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '0cac554ce06277e33ddf9f0b7ade4b8bbf2af3fa',
    ),
    'webmozart/assert' => 
    array (
      'pretty_version' => '1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '2db61e59ff05fe5126d152bd0655c9ea113e550f',
    ),
  ),
);
