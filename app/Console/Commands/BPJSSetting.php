<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Exception;

class BPJSSetting extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'setting:bpjs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initialize BPJS setting';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $current_setting = DB::table('cwa_setting')->get();
        $stored = [];
        foreach ($current_setting as $row) {
            $stored[$row->param] = true;
        }

        foreach ($this->data() as $row) {
            if (!isset($stored[$row['param']])) {
                if (empty($row['value'])) {
                    $row['value'] = $row['def_value'];
                }
                DB::table('cwa_setting')->insert($row);
                $this->info("Create new setting row : " . $row['param']);
            } else {
                $this->info("Setting row " . $row['param'] . " already exists");
            }
        }
    }


    public function data()
    {
        return [
            [
                'tag' => 'bpjs',
                'param' => 'batas_bawah',
                'label' => 'Batas Bawah Gaji',
                'value' => null,
                'def_value' => 3165000,
            ],
            [
                'tag' => 'bpjs',
                'param' => 'batas_atas',
                'label' => 'Batas Atas Gaji',
                'value' => null,
                'def_value' => 12000000,
            ],
            [
                'tag' => 'bpjs',
                'param' => 'jkk_pk',
                'label' => 'JKK PK (%)',
                'value' => null,
                'def_value' => 0.24,
            ],
            [
                'tag' => 'bpjs',
                'param' => 'jkm_pk',
                'label' => 'JKM PK (%)',
                'value' => null,
                'def_value' => 0.3,
            ],
            [
                'tag' => 'bpjs',
                'param' => 'jht_pk',
                'label' => 'JHT PK (%)',
                'value' => null,
                'def_value' => 3.7,
            ],
            [
                'tag' => 'bpjs',
                'param' => 'jp_pk',
                'label' => 'JP PK (%)',
                'value' => null,
                'def_value' => 2,
            ],
            [
                'tag' => 'bpjs',
                'param' => 'jht_tk',
                'label' => 'JHT TK (%)',
                'value' => null,
                'def_value' => 2,
            ],
            [
                'tag' => 'bpjs',
                'param' => 'jp_tk',
                'label' => 'JP TK (%)',
                'value' => null,
                'def_value' => 1,
            ],

            [
                'tag' => 'bpjs',
                'param' => 'kes_pk',
                'label' => 'Kesehatan PK (%)',
                'value' => null,
                'def_value' => 5,
            ],
            [
                'tag' => 'bpjs',
                'param' => 'kes_tk',
                'label' => 'Kesehatan TK (%)',
                'value' => null,
                'def_value' => 1,
            ],
            
        ];
    }

}
