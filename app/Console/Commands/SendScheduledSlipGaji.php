<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use App\Model\MdReport;
use App\Model\ModelGajiResign;
use App\Model\MdMtGaji;
use App\Model\MdMtKaryawan;
use Exception;
use Cache;

class SendScheduledSlipGaji extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'slipgaji:blast {batch?} {--id=0} {--bulan=0} {--tahun=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Blast slip gaji ke karyawan yang sudah diassign';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // --- SINGLE BYPASS MODE

        $bypassid = $this->option('id');
        if (intval($bypassid) > 0) {
            $tahun = intval($this->option('tahun')) > 2000 ? $this->option('tahun') : date('Y');
            $bulan = intval($this->option('bulan')) > 0 && intval($this->option('bulan')) <= 12 ? $this->option('bulan') : intval(date('m'));

            $karyawan = DB::table('cwa_karyawan')->where('id', $bypassid)->first();
            if (empty($karyawan)) {
                return $this->logError("ID Karyawan " . $bypassid . " tidak ditemukan");
            }
            
            $this->logInfo("Try bypass manually send email slip gaji to : " . $karyawan->nama . " - " . $karyawan->email . " (Tahun=".$tahun.", Bulan=".$bulan.")");
            $row_email_gaji = $this->getEmailGajiInstance($karyawan, $bulan, $tahun);

            $pdf = $this->generatePdf($row_email_gaji->karyawan_id, $row_email_gaji->bulan, $row_email_gaji->tahun);
            $send_result = $this->sendEmail($row_email_gaji, $pdf);
            if ($send_result) {
                $this->logInfo("MANUAL EMAIL SLIP GAJI FOR " . $row_email_gaji->email . " IS SUCCESSFULLY SENT");
            } else {
                $this->logError("MANUAL EMAIL SLIP GAJI FOR " . $row_email_gaji->email . " IS FAILED TO BE SENT");
            }

            return;
        }


        // --- BATCH MODE

        $batch = $this->argument('batch');
        if (intval($batch) == 0) {
            $batch = 3; //default per batch
        }

        $grabbed = DB::table('cwa_email_gaji')
            ->where('is_sent', 0)
            ->take($batch)
            ->get();

        if (count($grabbed) == 0) {
            return $this->logInfo("No pending email-scheduled slip gaji data. Nothing to do");
        } else {
            foreach ($grabbed as $row) {
                // handle blast email to each one
                $pdf = $this->generatePdf($row->karyawan_id, $row->bulan, $row->tahun);
                $send_result = $this->sendEmail($row, $pdf);
                if ($send_result) {
                    $this->logInfo("EMAIL SLIP GAJI FOR " . $row->email . " IS SUCCESSFULLY SENT");
                }
            }
        }
        // finish
        return true;
    }

    protected function getEmailGajiInstance($karyawan, $bulan, $tahun)
    {
        $get = DB::table('cwa_email_gaji')
            ->where('karyawan_id', $karyawan->id)
            ->where('bulan', $bulan)
            ->where('tahun', $tahun)
            ->first();
        
        if (empty($get)) {
            $this->logInfo("(getEmailGajiInstance) Email Gaji Instance data is empty, so the data ".$karyawan->id." is created", [
                'karyawan' => $karyawan,
                'bulan' => $bulan,
                'tahun' => $tahun,
            ]);
            $savedata = [
                'karyawan_id' => $karyawan->id,
                'bulan' => $bulan,
                'tahun' => $tahun,
                'email' => $karyawan->email,
                'is_sent' => 0,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            DB::table('cwa_email_gaji')->insert($savedata);

            $get = DB::table('cwa_email_gaji')
                ->where('karyawan_id', $karyawan->id)
                ->where('bulan', $bulan)
                ->where('tahun', $tahun)
                ->first();

            return $get;
        } 
        $this->logInfo("(getEmailGajiInstance) Instance Email Gaji is found!", [
            'data' => $get
        ]);
        return $get;
    }

    protected function sendEmail($row_email_gaji, $pdf)
    {
        $dummyCacheKey = 'DUMMY-EMAIL-TESTING';

        if (strlen($row_email_gaji->email) < 4) {
            DB::table('cwa_email_gaji')->where('id', $row_email_gaji->id)->update([
                'is_sent' => 9,
                'note' => "Invalid email address",
            ]);
            return false;
        }

        $karyawan = DB::table('cwa_karyawan')->where('id', $row_email_gaji->karyawan_id)->first();        
        $subject = 'Slip Gaji CWA Periode ' . nama_bulan($row_email_gaji->bulan) . ' ' . $row_email_gaji->tahun;
        $param = [
            'title' => $subject,
            'content' => view('mail.slip-gaji-content', [
                'karyawan' => $karyawan,
                'row_email_gaji' => $row_email_gaji,
            ])->render(),
        ];

        try {
            if (config('mail.dummy_email')) {
                $this->logInfo("Try send slip gaji to dummy email (".config('mail.dummy_email').") instead of " . $row_email_gaji->email);
            } else {
                $this->logInfo("Try send slip gaji to " . $row_email_gaji->email);
            }

            if (config('mail.dummy_email') && Cache::has($dummyCacheKey)) {
                // email gausa dikirim beneran
                $this->logInfo("APP_DUMMY_EMAIL detected, and cache is still active. Just update the mail status without send real time");
            } else {
                Mail::send('mail.template', $param, function ($message) use($row_email_gaji, $pdf, $subject, $dummyCacheKey) {
                    $message->from(config('mail.from.address'), config('mail.from.name'));
                    if (config('mail.dummy_email')) {
                        $message->to(config('mail.dummy_email'));
                        Cache::add($dummyCacheKey, 1, 60 * 8); // cache utk 8 jam 
                    } else {
                        $message->to($row_email_gaji->email);
                    }
                    $message->subject($subject);
                    $message->attach(storage_path('app/' . $pdf));
                });
            }
        } catch (Exception $e) {
            $errid = strtoupper(substr(sha1(rand(1, 999999) . uniqid() . time()), 0, 5)) . date('ymdHis');
            $this->logError($e->getMessage(), [
                'ERRID' => $errid
            ]);
            DB::table('cwa_email_gaji')->where('id', $row_email_gaji->id)->update([
                'is_sent' => 9,
                'note' => "Mail Server Error (".$errid.")",
            ]);
            return false;
        }

        DB::table('cwa_email_gaji')->where('id', $row_email_gaji->id)->update([
            'is_sent' => 1,
        ]);
        return true;
    }

    /**
     * @source from \App\Http\Controllers\SliGaji @ detailOnline
     */
    public function generatePdf($idkaryawan, $bulan, $tahun)
    {
        $report = new MdReport([
            'bulan' => $bulan,
            'tahun' => $tahun,
        ]);

        //month field
        $row = $report->make_mutasi($idkaryawan, "id_karyawan");
        foreach($row as $r){
        	$row = $r;
        }


        if(count($row) == 0){
            //siapa tahu data karyawan resign
            $data_resign = ModelGajiResign::with('getKaryawan')
            ->where('id_karyawan', $idkaryawan)
            ->whereIn('tgl_berlaku', $report->calendar)
            ->first();

            $row = (object)[
                'id' => $data_resign->id_karyawan,
                'id_karyawan' => $data_resign->id_karyawan,
                'nama' => $data_resign->getKaryawan->nama,
                'nama_divisi' => '-resigned-',
                'nama_jabatan' => '',
            ];
        }

        $report->get_rancangan($idkaryawan);
        $report->get_presensi($idkaryawan);

        $content = $report->prepare_object();


        $gaji = new MdMtGaji([
            'bulan' => $bulan,
            'tahun' => $tahun,
        ]);

        $detail = $report->gaji_maker($content, $gaji->build_gaji(1), $gaji->build_gaji(2), $row);


        $ls = MdReport::list_gaji();
        foreach($ls as $rls){
            $listgaji[$rls->id] = $rls->label;
        }

        $pdf = \PDF::loadView("slip-epay", [
            'title' => 'Rekap Gaji Karyawan',
            'curmenu' => 3,
            'cursubmenu' => 33,
            'default' => [
                'bulan' => $bulan,
                'tahun' => $tahun,
                'id_karyawan' => $idkaryawan
            ],
            'listdiv' => MdMtKaryawan::filter("divisi"),
            'listkode' => MdReport::list_kode_absen(),
            'listgaji' => $listgaji,
            'calendar' => $report->calendar,
            'content' => $content,
            'row' => $row,
            'gaji' => $detail,
            'is_console' => true
        ]);

        $pass = str_replace('-', '', $row->nik);
        $pdf->SetProtection(['print', 'copy'], $pass, $pass);

        $filename = 'epayslip/' . $row->nama . ' - Slip Gaji ' . $tahun . '-' . $bulan . '.pdf';
        
        $pdf_output = $pdf->output();
        Storage::put($filename, $pdf_output);
        return $filename;
    }

    protected function logInfo($message, $param=[])
    {
        $this->info($message);
        Log::info($message, $param);
    }

    protected function logError($message, $param=[])
    {
        $this->error($message);
        Log::error($message, $param);
    }

    protected function logWarning($message, $param=[])
    {
        $this->warning($message);
        Log::warning($message, $param);
    }
}
