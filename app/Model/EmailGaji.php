<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\MdReport;
use DB;

class EmailGaji extends Model
{
    public $table = 'cwa_email_gaji';

    public static function getCurrentReport($bulan, $tahun)
    {
        $data = DB::table('cwa_email_gaji')
            ->where('bulan', $bulan)
            ->where('tahun', $tahun)
            ->get();

        $out = [];
        foreach ($data as $row) {
            $out[$row->karyawan_id] = $row;
        }

        return $out;
    }

    public static function generateCurrentReport($bulan, $tahun)
    {
        $report = new MdReport([
            'bulan' => $bulan,
            'tahun' => $tahun,
        ]);

        $mutasi = $report->make_mutasi(null);

        $current_data = self::where('bulan', $bulan)->where('tahun', $tahun)->get(['karyawan_id']);

        $batch_data = [];
        foreach ($mutasi as $row) {
            if ($current_data->where('karyawan_id', $row->id_karyawan)->count() > 0) {
                // data is already saved, skip scheduling
                continue;
            }

            $batch_data[] = [
                'karyawan_id' => $row->id_karyawan,
                'bulan' => $bulan,
                'tahun' => $tahun,
                'email' => $row->email,
                'is_sent' => (strlen($row->email) < 3) ? 9 : 0,
                'note' => (strlen($row->email) < 3) ? 'Invalid email provided' : '',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
        }

        if (!empty($batch_data)) {
            return DB::table('cwa_email_gaji')->insert($batch_data);
        }
        // do nothing
        return false;
    }
}