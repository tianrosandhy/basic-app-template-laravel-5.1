<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Model\MdJadwal;

class MdUpload extends Model
{
    //
	var $boleh,
		$filename,
		$ext,
		$location,
		$data_absen;

	public function __construct(){
		$this->boleh = ['xls','xlsx','csv'];
		$location = '';
		$filename = '';
	}

	public function process($file){
		$this->ext = $file->guessExtension();
		if($file->isValid() and in_array($this->ext, $this->boleh)){
			$this->filename = date("YmdHis").".".$this->ext;
			$file->move('tmp', $this->filename);
			$this->location = 'tmp/'.$this->filename;
			return true;
		}
		else
			return false;
	}


	//method untuk merapikan index data
	public function clean($inp){
		$lastval = "";
        $inp = (array)$inp;
        $items = $inp["\x00*\x00items"];
        $data = [];
        foreach($items as $row=>$item){
            foreach($item as $col=>$val){
                if(!empty($val)){
                    if(is_numeric($val)){
                        $data[$val] = [];
                        $lastval = $val;
                    }

                    if(isset($data)){
                        $data[$lastval][$row][$col] = $val;
                    }
                }
            }
        }
        return $data;
	}

	public function analyze($data){
		$bukan_absen = [];
		$data_absen = [];

		$curr = "";
		foreach($data as $id_absen => $detail){
			foreach($detail as $row => $dt){
				foreach($dt as $col=>$val){
					if(strlen($val) <= 6 ){
						//bukan data absen
						$bukan_absen[$row][$col] = $val;
						if(is_numeric($val))
							$curr = $val;
					}
					else{
						$data_absen[$curr][$row][$col] = $val;
					}
				}
			}
		}

		//mestinya datanya sudah cukup rapi

		$tgl = array_values($data_absen[""][0]);
		$range_tgl = $tgl[(count($tgl)-1)];
		$exp_tg = preg_split("/\s?s\/d\s?/", $range_tgl);
		$tmp_year[date("m", strtotime($exp_tg[0]))] = date("Y",strtotime($exp_tg[0]));
		$tmp_year[date("m", strtotime($exp_tg[1]))] = date("Y",strtotime($exp_tg[1]));

		$final = [];
		$tmp_mo = 0;
		foreach($data_absen as $id_absen => $detail){
			foreach($detail as $row => $dt){
				foreach($dt as $col=> $nilai){
					if(is_numeric(substr($nilai, -2))){
						//ini jam absen
						$jam_absen = $nilai;
						if(isset($data_absen[$id_absen][($row-1)][$col])){
							$tgl_absen = substr($data_absen[$id_absen][($row-1)][$col], 0, 5);

							$curmo = substr($tgl_absen, 0, 2);
							$tgl_absen = $tmp_year[$curmo]."/".$tgl_absen;

							$final[$id_absen][$tgl_absen] = explode_jam($jam_absen);
						}
					}
				}
			}
		}
		$this->data_absen = $final;

		return $final;

	}




	//fungsi utk absen model baru yg sompret berantakan
	public function force_clean($excel){
		$excel = json_decode(json_encode($excel));
		$save = [];
		foreach($excel as $idrow=>$row){
			foreach($row as $ind=>$dt){
				if(!is_null($dt))
					$save[$idrow][$ind] = $dt;
			}
		}
		return $save;
	}

	public function force_analyze($cleaned, $list_date){
		$active_id = '';
		$saved = [];
		foreach($cleaned as $row){
			$mix = array_values($row);
			if(strpos($mix[0], 'No:') !== false){
				$active_id = $mix[1];
				continue;
			}
			if(isset($mix[1])){
				if(strpos($mix[1], 'No:') !== false){
					$active_id = $mix[2];
					continue;
				}
			}


			if(strlen($active_id) > 0){
				foreach($row as $ind_date => $jam){
					if(!isset($list_date[$ind_date])){
						continue;
					}
					$tgl = $list_date[$ind_date];

					$jams = explode("\n", $jam);
					for($i=0; $i<=(count($jams)-1); $i++){
						if(strlen($jams[$i]) == 0){
							unset($jams[$i]);
						}
					}

					if(count($jams) > 2){
						$jams[1] = max($jams);
						$jams = [$jams[0], $jams[1]];
					}
					if(count($jams) == 1){
						$jams[1] = $jams[0];
					}

					$saved[$active_id][$tgl] = $jams;
				}
			}

		}
		$this->data_absen = $saved;
		return $saved;
	}



	//fungsi untuk absen paling baru babik
	public function clean_baru($excel){
		$out = [];
		$data = json_decode(json_encode($excel));
		return $out;
	}

	public function analyze_baru($cleaned){
		$out = [];
		$current_id = 0;
		foreach($cleaned as $row){
			//cari id kode absen dulu
			if(!is_array($row)){
				continue;
			}
			$nih = array_values($row); //mengabaikan index array saat ini

			$first_col = null;
			if(isset($row[0])){
				$first_col = $row[0];
			}

			if(strpos($first_col, 'PIN') !== false){
				$idpos = $nih[1];
				$trimId = preg_replace("/[\/\&%#\:\$ ]/", "", $idpos);
				$current_id = $trimId;
			}

			if($current_id > 0){
				$tgl = self::isTanggal($nih[0]);
				if($tgl){
					if(!isset($nih[1])){
						continue;
					}
					$jam_a = $nih[1];
					$jam_b = $nih[count($nih)-1];

					//jamnya masih salah format, mesti diubah ke H:i
					if(is_string($jam_a)){
						$jam_a = substr(str_replace(".", ":", $jam_a), 0, -3);
					}
					else{
						$jam_a = date('H:i', strtotime($jam_a->date));
					}

					if(is_string($jam_b)){
						$jam_b = substr(str_replace(".", ":", $jam_b), 0, -3);
					}
					else{
						$jam_b = date('H:i', strtotime($jam_b->date));
					}

					$out[$current_id][$tgl] = [$jam_a, $jam_b];
				}
			}
		}
		$this->data_absen = $out;
		return $out;
	}

	protected function isTanggal($string=''){
		$pecah = explode(', ', $string);
		if(count($pecah) != 2)
			return false;

		$tglString = $pecah[1];
		//kalo formatnya masih garing, ubah ke strip bangsat
		$tglString = trim(str_replace('/', '-', $tglString));

		return date('Y-m-d', strtotime($tglString));
	}




	public function migrate_code($mutasi=[]){
		$cari = [];
		foreach($this->data_absen as $ind=>$dt){
			$cari[] = $ind;
		}

		$idk = [];
		foreach($mutasi as $mt){
			$idk[] = $mt->id_karyawan;
		}

		$sql = DB::table("cwa_control_code")
		->where("stat","<>",9);

		if(count($idk) > 0){
			$sql = $sql
			->whereIn('id_karyawan', $idk);
		}

		$sql = $sql
		->whereIn("code", $cari)
		->get();

		$ret = [];
		if(count($sql) > 0){
			foreach($sql as $row){
				$ret[$row->code] = $row->id_karyawan;
			}
		}

		return $ret;
	}

	public function rancangan($list_date){
		$sql = DB::table("cwa_rancangan_jadwal AS a")
		->leftJoin("cwa_shift AS b", "a.id_shift", "=", "b.id")
		->selectRaw("a.*, b.jam_masuk, b.jam_pulang")
		->whereBetween("tanggal", [$list_date[0], max($list_date)])
		->get();
			
		$ret = [];
		foreach($sql as $row){
			if(!empty($row->jam_masuk) and !empty($row->jam_pulang)){
				$ret[$row->id_karyawan][date("Y-m-d", strtotime($row->tanggal))] = [
					'id' => $row->id,
					'must_masuk' => $row->jam_masuk,
					'must_pulang' => $row->jam_pulang
				];
			}
		}
		return $ret;
	}





	public function olah_absen($absen){
		$reserved = ['Divisi', 'Bulan', 'Tahun', 'Nama Karyawan'];
		foreach($absen as $row){
			if(in_array(trim($row[0]), $reserved)){
				$var = $row[0];
				unset($row[0]);
				$meta[strtolower($var)] = implode("-",$row);
			}
			else{
				$var = $row[0];
				unset($row[0]);
				$nama_karyawan[$var] = $row;
			}
		}

		//seluruh data sudah dapat
		$tanggal = $meta['tahun']."-".$meta['bulan'];

		$oke = 0;

		$periode = self::periode_mutasi($meta['divisi'], $meta['bulan'], $meta['tahun']);

		$terproses = [];
		foreach($nama_karyawan as $nm => $arr_tgl){
			foreach($arr_tgl as $tg => $absen){
				$real_tgl = date("Y-m-d", strtotime($tanggal."-".$tg));
				
				if(!isset($periode[$nm]))
					continue;
				if(in_array($real_tgl, $periode[$nm]['tgl'])){
					$terproses['id_karyawan'] = $periode[$nm]['id_karyawan'];
					$terproses['id_divisi'] = $periode[$nm]['id_divisi'];
				}
				else{
					continue;
				}

				$imp = MdUpload::check_kode($absen);
				if(count($imp) == 0){
					continue;
				}
				$terproses['data'][$real_tgl] = $imp;


			}

			$action = self::simpan_rancangan($terproses, $meta['bulan'], $meta['tahun']);


			if($action){
				foreach($action['update'] as $tgl){
					//update
					if(isset($terproses['data'][$tgl])){

						DB::table('cwa_rancangan_jadwal')
						->where('id_karyawan', $terproses['id_karyawan'])
						->where('id_divisi', $terproses['id_divisi'])
						->where('tanggal', $tgl)
						->where('stat','<>',9)

						->update($terproses['data'][$tgl]);

						$oke++;
					}
				}
				foreach($action['insert'] as $tgl){
					//insert
					if(isset($terproses['data'][$tgl])){
						DB::table('cwa_rancangan_jadwal')
						->insert([
							'id_karyawan' => $terproses['id_karyawan'],
							'id_divisi' => $terproses['id_divisi'],
							'id_kode_absen' => $terproses['data'][$tgl]['id_kode_absen'],
							'id_shift' => $terproses['data'][$tgl]['id_shift'],
							'tanggal' => $tgl,
							'stat' => 1
						]);
						$oke++;
					}
				}			
			}



			$terproses = []; //balik ke semula lagi

		}


		



		if($oke > 0){
			$msg = ['success' => 'Berhasil menyimpan data jadwal karyawan'];
		}
		else{
			$msg = ['error' => 'Tidak ada data yang diolah. Pastikan inputan file sudah dalam format yang tepat'];
		}

		return $msg;

	}

	static function periode_mutasi($nm_divisi, $bulan, $tahun){
		$bln = intval($bulan);
		$jdw = new MdJadwal([
			'bulan' => $bln,
			'tahun' => $tahun
		]);


		foreach($jdw->calendar as $tgl){
			$mutasi = MdMtKaryawan::queryGetMutasi([
				'tgl_berlaku' => $tgl,
				'nama_divisi' => $nm_divisi,
			]);

			foreach($mutasi as $mut){
				$ret[$mut->nama]['id_karyawan'] = $mut->id_karyawan;
				$ret[$mut->nama]['id_divisi'] = $mut->id_divisi;
				$ret[$mut->nama]['tgl'][] = $tgl;
			}

		}
		return $ret;
	}

	static function simpan_rancangan($data, $bulan, $tahun){
		$bln = explode("-", $bulan);
		$jdw = new MdJadwal([
			'bulan' => $bulan,
			'tahun' => $tahun
		]);

		$a = $jdw->calendar[0];
		$b = max($jdw->calendar);

		if(!isset($data['id_karyawan'])){
			return false;
		}
		//cari data yang sudah tersimpan
		$cari = DB::table('cwa_rancangan_jadwal')
		->where('id_karyawan', $data['id_karyawan'])
		->where('id_divisi', $data['id_divisi'])
		->whereBetween('tanggal', [$a, $b])
		->where('stat', '<>', 9)
		->get();

		$ret['update'] = [];
		
		foreach($cari as $row){
			$ret['update'][] = $row->tanggal;
		}
		$ret['insert'] = array_diff($jdw->calendar, $ret['update']);


		return $ret;

	}



	static function save_rancangan($data){
		$cek = DB::table('cwa_rancangan_jadwal')
		->where('id_karyawan', $data['id_karyawan'])
		->where('id_divisi', $data['id_divisi'])
		->where('tanggal', $data['tanggal'])
		->where('stat', '<>', 9)
		->get();

		if(count($cek) > 0){
			//update
			DB::table('cwa_rancangan_jadwal')
			->where('id_karyawan', $data['id_karyawan'])
			->where('id_divisi', $data['id_divisi'])
			->where('tanggal', $data['tanggal'])
			->where('stat', '<>', 9)
			->update([
				'id_kode_absen' => $data['id_kode_absen'],
				'id_shift' => $data['id_shift']
			]);
		}
		else{
			//insert
			DB::table('cwa_rancangan_jadwal')
			->insert($data);
		}
	}



	static function check_kode($kd){
		$list = MdUpload::list_kode();
		if(in_array($kd, $list['absen'])){
			$key = array_search($kd, $list['absen']);
			return [
				'id_kode_absen' => $key,
				'id_shift' => null
			];
		}
		else if(in_array($kd, $list['shift'])){
			$key = array_search($kd, $list['shift']);
			return [
				'id_kode_absen' => null,
				'id_shift' => $key
			];
		}
	}

	static function list_kode(){
		$sql = DB::table('cwa_kode_absen')
		->where('stat','<>',9)
		->get();

		foreach($sql as $row){
			$row = (array)$row;
			$absen[$row['id']] = $row['kode_absen'];
		}

		$sql = DB::table('cwa_shift')
		->where('stat','<>',9)
		->get();

		foreach($sql as $row){
			$row = (array)$row;
			$shift[$row['id']] = $row['kode_shift'];
		}

		return ['absen' => $absen, 'shift' => $shift];
	}



	public function formatUlang($absen, $tgl){
		$out = [];
		$kosong = [];
		foreach($absen as $kode => $data){
			foreach($tgl as $dt){
				if(!isset($data[$dt])){
					$out[$kode][$dt] = null;
					$kosong[$kode][] = $dt;
				}
				else{
					$out[$kode][$dt] = $data[$dt];
				}
			}
		}
		return $kosong;
	}


	public function removeDependency($format, $listKaryawan, $listRancangan){
		foreach($listKaryawan as $absen => $lk){
			if(!isset($listRancangan[$lk]))
				continue;
			foreach($listRancangan[$lk] as $tgl => $data){
				$idRancangan = $data['id'];
				if(!isset($format[$absen])){
					continue;
				}
				if(in_array($tgl, $format[$absen])){
					//kalau ada data di bagian ini, artinya ada data absen yg kosong dan di tanggal bersangkutan karyawan punya rancangan jadwal.
					//data kehadiran di bagian ini boleh dihapus
					$cek = DB::table('cwa_presensi')
					->where('id_rancangan_jadwal', $data['id'])->first();
					if(count($cek) > 0){
						//hapus data lembur (rancangan)
						DB::table('cwa_lembur')->where('id_presensi', $data['id'])->delete(); 
					}
					DB::table('cwa_presensi')
					->where('id_rancangan_jadwal', $data['id'])->delete();

				}
			}
		}
	}

}
