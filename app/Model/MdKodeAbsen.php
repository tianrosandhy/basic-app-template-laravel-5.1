<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;


class MdKodeAbsen extends Model
{
    static function structure(){
    	return [
    		"#" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "no",
    			"type" => "number",
    			"attr" => [],
    			"value" => null    			
    		),
    		"Kode" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "kode_absen",
    			"type" => "text",
    			"attr" => ["required" => true, "class"=>"col-sm-2", "maxlength"=>10],
    			"value" => null
    		),
    		"Label" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "label",
    			"type" => "text",
    			"attr" => ["required" => true],
    			"value" => null
    		),
    		"Bobot" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "bobot",
    			"type" => "select",
    			"list" => array(
    				null => "",
    				0 => "Hitung Tidak Absen",
    				1 => "Hitung Absen"
    			),
    			"attr" => ["required" => true],
    			"value" => null
    		),
    		"Color" => array(
                "table" => true,
                "form" => true,
                "field" => "color",
                "type" => "color",
                "attr" => ["required" => true],
                "value" => "FFFFFF"
            ),
            "Inc. Jadwal" => array(
                "table" => false,
                "form" => true,
                "field" => "include_jadwal",
                "type" => "checkbox",
                "list" => [1 => "<b>Daftarkan ke opsi rencana jadwal</b>"],
                "attr" => [],
                "value" => null
            ),

    		"" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "btn",
    			"type" => "action",
    			"attr" => [],
    			"value" => null
    		)

    	];
    }


    static function content(){

    	#from tb
    	$tb = DB::table("cwa_kode_absen")
    		->where("stat", '<>', 9)
    		->orderBy("id", "desc")
    		->get();

    	$result = MdKodeAbsen::manage_table_content($tb);
    	return $result;
    }

    static function manage_table_content($fromtb){
    	#Mengubah dan menyesuaikan format data yang ditampilkan ke tabel

    	$result = array_map("toArray", $fromtb);

    	$no = 1;
    	$bbt = array("<span class='label label-danger'>Hitung Tidak Absen</span>", "<span class='label label-success'>Hitung Absen</span>");
    	foreach($result as $ind=>$data){

    		$result[$ind]['no'] = $no;
    		$result[$ind]['bobot'] = $bbt[$data['bobot']];
    		$result[$ind]['color'] = "<div class='color-holder' style='padding:1em; background:".$data['color']."'></div>";
    		$result[$ind]['btn'] = '<a href="master/kode_absensi/edit/'.$data['id'].'" class="btn btn-info btn-sm">Edit</a> <a href="master/kode_absensi/destroy/'.$data['id'].'" class="btn btn-danger btn-sm delete-button">Delete</a>';
    		$no++;
    	}
    	return $result;
    }

    static function editable($id){
        $tb = DB::table("cwa_kode_absen")
            ->where("id", $id)
            ->first();


        //jika ada reformat data edit, bisa diatur ulang sebelum direturn

        return (array)$tb;
    }

    static function validate($post, $except=null){
        if(strlen(trim($post['kode_absen'])) == 0)
            return "Mohon mengisi nama / kode hr dengan tepat";

        if($except > 0){
            $sql = DB::table("cwa_kode_absen")
            ->where("kode_absen", $post['kode_absen'])
            ->where("stat", "<>", 9)
			->where("id", "<>", $except)
            ->get();
        }
        else{
            $sql = DB::table("cwa_kode_absen")
            ->where("kode_absen", $post['kode_absen'])
            ->where("stat", "<>", 9)
            ->get();   
        }

        if(count($sql) > 0){
            return "Data kode absensi dengan keterangan tersebut sudah ada";
        }
        return false;
    }
}
