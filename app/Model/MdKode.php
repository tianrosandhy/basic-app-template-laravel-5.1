<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class MdKode extends Model
{
    //
    static function structure(){
    	return [
    		"#" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "no",
    			"type" => "number",
    			"attr" => [],
    			"value" => null    			
    		),
    		"Nama Kode" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "code_name",
    			"type" => "text",
    			"attr" => ["required" => true],
    			"value" => null
    		),
    		"Description" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "code_description",
    			"type" => "textarea",
    			"attr" => [],
    			"value" => null
    		),
    		"Type" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "type",
    			"type" => "select",
    			"list" => array(
    				"" => "",
    				"mandatory" => "Mandatory",
    				"optional" => "Optional"
    			),
    			"attr" => [],
    			"value" => null
    		),
    		"" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "btn",
    			"type" => "action",
    			"attr" => [],
    			"value" => null
    		)

    	];
    }


    static function content(){

    	#from tb
    	$tb = DB::table("cwa_code_type")
    		->where("stat", '<>', 9)
    		->orderBy("id", "desc")
    		->get();

    	$result = MdKode::manage_table_content($tb);
    	return $result;
    }

    static function manage_table_content($fromtb){
    	#Mengubah dan menyesuaikan format data yang ditampilkan ke tabel

    	$result = array_map("toArray", $fromtb);

    	$no = 1;
    	foreach($result as $ind=>$data){
    		$result[$ind]['no'] = $no;
            $result[$ind]['code_name'] = "<a href='master/kode_karyawan/manage/".$data['id']."'>".$data['code_name']."</a>";
    		$result[$ind]['btn'] = '<a href="master/kode_karyawan/edit/'.$data['id'].'" class="btn btn-info btn-sm">Edit</a> <a href="master/kode_karyawan/destroy/'.$data['id'].'" class="btn btn-danger btn-sm delete-button">Delete</a>';
    		$no++;
    	}
    	return $result;
    }

    static function editable($id){
        $tb = DB::table("cwa_code_type")
            ->where("id", $id)
            ->first();


        //jika ada reformat data edit, bisa diatur ulang sebelum direturn

        return (array)$tb;
    }

    static function validate($post, $except=null){
        if(strlen(trim($post['code_name'])) == 0)
            return "Mohon mengisi nama kode karyawan dengan tepat";

        if($except > 0){
            $sql = DB::table("cwa_code_type")
            ->where("code_name", $post['code_name'])
            ->where("stat", "<>", 9)
            ->where("id", "<>", $except)
            ->get();
        }
        else{
            $sql = DB::table("cwa_code_type")
            ->where("code_name", $post['code_name'])
            ->where("stat", "<>", 9)
            ->get();
        }

        if(count($sql) > 0){
            return "Data kode karyawan dengan nama tersebut sudah ada";
        }
        return false;
    }


    static function list_kode($id=null){
        $sql = DB::table("cwa_code_type")
        ->where("stat","<>",9)
        ->where("id",$id)
        ->first();
        return $sql;
    }

    static function get_code($code_name=""){
        $sql = DB::table('cwa_control_code')
        ->where('type', $code_name)
        ->where('stat','<>',9)
        ->get();

        return transform_array($sql);
    }


    static function show_detail_code($karyawan, $data_kode, $idkode){
        $ret = [];
        $kode = [];
        foreach($data_kode as $dtk){
            $kode[$dtk['id_karyawan']] = $dtk;
        }


        foreach($karyawan as $kar){
            $kar = (array)$kar;
            if(isset($kode[$kar['id_karyawan']])){
                $code_name = $kode[$kar['id_karyawan']]['code'];
            }
            else{
                $code_name = "";
            }

            $code_name = "<input type='text' class='kode_box form-control' data-type='".$idkode."' data-karyawan='".$kar['id_karyawan']."' data-default='".$code_name."' value='".$code_name."'>";

            $ret[] = array(
                'nama_karyawan' => $kar['nama'],
                'divisi' => $kar['nama_divisi'],
                'code_name' => $code_name,
                'btn' => ''
            );
        }
        return $ret;
    }
}
