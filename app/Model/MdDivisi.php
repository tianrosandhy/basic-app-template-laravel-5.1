<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class MdDivisi extends Model
{

    static function structure(){
    	return [
			"Sort No" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "sort_no",
    			"type" => "number",
    			"attr" => [],
    			"value" => null
    		),
    		"Nama Cabang" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "nama_divisi",
    			"type" => "text",
    			"attr" => ["required" => true],
    			"value" => null
    		),
    		"Jenis Mesin Absen" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "deskripsi",
    			"type" => "select",
                "list" => [
                    0 => '',
                    1 => 'Baru',
                    2 => 'Lama',
                    3 => 'Paling Baru'
                ],
    			"attr" => [],
    			"value" => null
    		),
    		"" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "btn",
    			"type" => "action",
    			"attr" => [],
    			"value" => null
    		)

    	];
    }


    static function content(){

    	#from tb
    	$tb = DB::table("cwa_divisi")
    		->where("stat", '<>', 9)
    		->orderBy("id", "desc")
    		->get();

    	$result = MdDivisi::manage_table_content($tb);
    	return $result;
    }

    static function manage_table_content($fromtb){
    	#Mengubah dan menyesuaikan format data yang ditampilkan ke tabel

    	$result = array_map("toArray", $fromtb);

    	$no = 1;
    	foreach($result as $ind=>$data){
    		$result[$ind]['no'] = $no;
            $result[$ind]['deskripsi'] = intval($data['deskripsi']) == 1 ? 'Baru' : (intval($data['deskripsi']) == 2 ? 'Lama' : ($data['deskripsi'] == 3 ? 'Paling Baru' : ''));
    		$result[$ind]['btn'] = '<a href="master/divisi/edit/'.$data['id'].'" class="btn btn-info btn-sm">Edit</a> <a href="master/divisi/destroy/'.$data['id'].'" class="btn btn-danger btn-sm delete-button">Delete</a>';
    		$no++;
    	}
    	return $result;
    }

    static function editable($id){
        $tb = DB::table("cwa_divisi")
            ->where("id", $id)
            ->first();


        //jika ada reformat data edit, bisa diatur ulang sebelum direturn

        return (array)$tb;
    }


    static function validate($post, $except=null){
    	if(strlen(trim($post['nama_divisi'])) == 0)
    		return "Mohon mengisi nama divisi dengan tepat";

    	if($except > 0){
	    	$sql = DB::table("cwa_divisi")
	    	->where("nama_divisi", $post['nama_divisi'])
	    	->where("stat", "<>", 9)
	    	->where("id", "<>", $except)
	    	->get();
    	}
    	else{
	    	$sql = DB::table("cwa_divisi")
	    	->where("nama_divisi", $post['nama_divisi'])
	    	->where("stat", "<>", 9)
	    	->get();
    	}

    	if(count($sql) > 0){
    		return "Data divisi dengan nama tersebut sudah ada";
    	}
    	return false;
    }

}
