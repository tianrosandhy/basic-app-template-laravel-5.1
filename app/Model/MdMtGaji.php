<?php

namespace App\Model;

use App\Model\MdGolongan;
use DB;

class MdMtGaji extends MdJadwal
{
    public $karyawan;

    //
    public static function list_jabatan()
    {
        $sql = DB::table('cwa_jabatan')
            ->where("stat", "<>", 9)
            ->get();

        $ret = array();
        foreach ($sql as $row) {
            $ret[$row->id] = $row->nama_jabatan;
        }
        return $ret;
    }

    public static function master_gaji($occurence = 1)
    {
        $sql = DB::table('cwa_master_gaji')
            ->where('occurence', $occurence)
            ->where('stat', '<>', 9)
            ->where('default_value', '=', 0)
            ->where('is_visible', 1)
            ->get();

        $arr = array();
        foreach ($sql as $row) {
            $row = (array) $row;
            $arr[$row['id']] = $row;
        }

        // hardcode urutan 
        $final = array();
        foreach ($arr as $id => $param) {
            if (strtolower($param['label']) == 'potongan bonus pu') {
                $final[$id] = $param;
            }
        }
        foreach ($arr as $id => $param) {
            if (!isset($final[$id])) {
                $final[$id] = $param;
            }
        }

        return $final;
    }

    public function build_gaji($occ = 1)
    {
        //tolong tanggalnnya nanti dicek lagi, sudah bener atau blm?
        //sekarang build gaji include modul golongan
        $master_golongan = MdGolongan::golongan_gaji_by_tanggal(max($this->calendar));

        $gol = [];
        if ($occ == 1) {
            foreach ($master_golongan as $row) {
                $gol[$row->id]['gaji_pokok'] = $row->gaji_pokok;
                $gol[$row->id]['tunjangan_jabatan'] = $row->tunjangan_jabatan;
            }
        }

        $mutt = MdMtKaryawan::queryGetMutasi([
            'tgl_berlaku' => max($this->calendar),
        ]);

        if ($occ == 1) {
            foreach ($mutt as $row) {
                $datagaji[$row->id_karyawan] = isset($gol[$row->id_golongan]) ? $gol[$row->id_golongan] : 0;
            }
        }

        $tgl = min($this->calendar);
        $adate = date("Y", strtotime($tgl)) . "-" . date("m", strtotime($tgl)) . "-01";

        $addQ = "";
        if ($occ == 2) {
            $addQ = " AND a.tanggal = '$adate'";
        }

        $sql = DB::select("
		SELECT 
        a.id, a.active_until_period, a.id_karyawan, a.id_gaji, c.type, c.occurence, c.divide_by_absen, c.invalid_by_alpa, c.default_value, a.tanggal, a.nilai, a.stat
		FROM cwa_detail_gaji a
		INNER JOIN (
			SELECT id_gaji, id_karyawan, MAX(tanggal) AS tanggal FROM cwa_detail_gaji WHERE tanggal <= ? GROUP BY id_karyawan, id_gaji
		) b
		ON a.id_karyawan = b.id_karyawan AND a.tanggal = b.tanggal AND a.id_gaji = b.id_gaji
		LEFT JOIN cwa_master_gaji c
		ON a.id_gaji = c.id AND c.is_visible = 1
		WHERE a.stat <> ? $addQ AND c.stat <> 9

		", [$tgl, 9]);

        $arr = array();
        foreach ($sql as $row) {
            $row = (array) $row;

            if ($row['occurence'] == $occ) {
                $active_until_period = $row['active_until_period'] ?? null;
                if (!empty($active_until_period)) {
                    if (strtotime($this->calendar[0]) > strtotime($active_until_period)) {
                        // force nilai ke 0 jika active_until_period sudah kadaluarsa
                        $row['nilai'] = null;
                    }
                }

                $arr[$row['id_karyawan']][$row['id_gaji']] = [
                    'type' => $row['type'],
                    'occurence' => $row['occurence'],
                    'divide_by_absen' => $row['divide_by_absen'],
                    'invalid_by_alpa' => $row['invalid_by_alpa'],
                    'default_value' => $row['default_value'],
                    'active_until_period' => $active_until_period,
                    'value' => $row['nilai'],
                    'tgl' => $row['tanggal'],
                ];
            }

        }

        //penghitungan total
        //jika type = 1 -> increment, else decrement
        foreach ($arr as $id_karyawan => $gaji) {
            $total = 0;
            foreach ($gaji as $id_gaji => $data_gaji) {
                $key = $id_gaji;
                if ($data_gaji['type'] == 1) {
                    $total += $data_gaji['value'];
                } else {
                    $total -= $data_gaji['value'];
                }

                $tgl = $data_gaji['tgl'];
            }

            if (isset($datagaji[$id_karyawan])) {
                $arr[$id_karyawan]['gaji_pokok'] = $datagaji[$id_karyawan]['gaji_pokok'];
                $arr[$id_karyawan]['tunjangan_jabatan'] = $datagaji[$id_karyawan]['tunjangan_jabatan'];

                $total = $total + intval($datagaji[$id_karyawan]['gaji_pokok']) + intval($datagaji[$id_karyawan]['tunjangan_jabatan']);
            }

            $arr[$id_karyawan]['tgl'] = $tgl;
            $arr[$id_karyawan]['total'] = $total;
        }

        foreach ($datagaji ?? [] as $idkar => $dtgj) {
            if (!isset($arr[$idkar])) {
                $arr[$idkar] = $dtgj;
                $total = intval($dtgj['gaji_pokok']) + intval($dtgj['tunjangan_jabatan']);
                $arr[$idkar]['tgl'] = null;
                $arr[$idkar]['total'] = $total;
            }
        }

        return $arr;
    }

    public function karyawan_gaji($divisi = null)
    {
        $saved = array();
        foreach ($this->calendar as $tgl) {
            $get = MdMtKaryawan::queryGetMutasi([
                'tgl_berlaku' => max($this->calendar),
                'divisi' => $divisi,
            ]);
            foreach ($get as $gt) {
                $saved[$gt->id_karyawan] = $gt;
            }
        }
        $this->karyawan = $saved;
        return $this->karyawan;
    }

    public static function detail_gaji_karyawan($id, $occ = 1, $tgl = null)
    {
        if (!empty($tgl)) {
            $tgl = date("Y-m-d", strtotime($tgl));
        }

        $sql = DB::table("cwa_detail_gaji AS a")
            ->join("cwa_master_gaji AS b", "a.id_gaji", "=", "b.id")
            ->selectRaw("a.*, b.occurence")
            ->where("a.id_karyawan", $id)
            ->where("b.occurence", $occ)
            ->where("a.stat", "<>", 9);

        if (!empty($tgl)) {
            $sql = $sql->where('tanggal', '<', $tgl);
        }
        $sql = $sql->get();

        $arr = array();
        foreach ($sql as $row) {
            $row = (array) $row;
            $arr[$row['tanggal']][$row['id_gaji']] = $row['nilai'];
        }
        return $arr;
    }

    public function build_thr($tgl)
    {
        $out = [];
        $skrg = date("Y-m-d", strtotime($tgl));
        $tahun_lalu = date("Y-m-d", strtotime($skrg . " -1 year"));

        //ketentuan THR :
        /*
        - Masa kerja dibawah setahun,
        - Masa kerja setelah setahun
         */

        $this->karyawan_gaji();
        $datagaji = $this->build_gaji();

        $list_golongan = self::getGajiByGolongan();

        foreach ($this->karyawan as $idk => $data) {
            $thr[$idk] = 0;
            if (strtotime($data->tgl_masuk) > strtotime($tahun_lalu)) {
                $x = floor((strtotime($skrg) - strtotime($data->tgl_masuk)) / (30 * 86400));
                $out[$data->id_karyawan]['lama_bekerja'] = $x . " bulan";
                $index[$data->id_karyawan] = $x / 12;
            } else {
                $index[$data->id_karyawan] = 1;
                $out[$data->id_karyawan]['lama_bekerja'] = "> 1 tahun";
            }

            //simpen data gaji pokok + tunjangan
            $id_golongan = $data->id_golongan;
            #get gaji pokok + tunjangan
            if (isset($list_golongan[$id_golongan])) {
                $thr[$idk] += $list_golongan[$id_golongan]['gaji_pokok'];
                $thr[$idk] += $list_golongan[$id_golongan]['tunjangan_jabatan'];
            }
        }

        foreach ($datagaji as $idk => $datag) {
            foreach ($datag as $idgaji => $dtg) {
                if (in_array($idgaji, [17, 18, 19, 20])) {
                    //skip hardcode tunjangan penempatan & insentif
                    continue;
                }
                if (is_array($dtg)) {
                    if ($dtg['type'] == 1 and $dtg['occurence'] == 1 and $dtg['default_value'] == 0) {
                        if (isset($thr[$idk])) {
                            $thr[$idk] += $dtg['value'];
                        } else {
                            $thr[$idk] = $dtg['value'];
                        }
                    }

                }
            }
        }

        //ketentuan THR : gaji pokok + tunjangan fix
        foreach ($this->karyawan as $id => $karyawan) {
            if (!isset($thr[$id])) {
                $thr[$id] = 0;
            }

            $out[$id]['index'] = $index[$id];
            $out[$id]['status_transfer'] = intval($karyawan->status_transfer);
            $out[$id]['thr'] = $thr[$id];
            $out[$id]['get'] = $thr[$id] * $index[$id];
        }
        return $out;
    }

    protected function getGajiByGolongan()
    {
        #kalau suatu saat golongannya salah gaji, update disini
        $list = DB::table('cwa_golongan_gaji')->where('stat', 1)->get();
        $out = [];
        foreach ($list as $row) {
            $out[$row->id_golongan] = [
                'gaji_pokok' => $row->gaji_pokok,
                'tunjangan_jabatan' => $row->tunjangan_jabatan,
            ];
        }

        return $out;
    }

}
