<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Model\MdJadwal;

class MdTerlambat extends MdJadwal
{
    //
    public function __construct($data=[]){
//      $this->holiday = json_decode(get_setting("holiday"));
        $hols = get_setting("holiday");
        $h = explode(",",$hols);
        foreach($h as $ho){
            $this->holiday[] = trim($ho);
        }

        $this->tgl_awal = get_setting("tgl_awal_gaji");

        if(!isset($data['bulan']))
            $data['bulan'] = date("n");
        if(!isset($data['tahun']))
            $data['tahun'] = date("Y");

        $this->create_calendar($data['bulan'], $data['tahun']);
    }

    public function get_presence($allowed=[]){
    	$a = $this->calendar[0];
    	$b = max($this->calendar);

    	$sql = DB::table("cwa_presensi")
    	->whereBetween("tanggal", [$a, $b])
    	->where("stat","<>", 9)
    	->get();

    	$return = array();
    	foreach($sql as $row){
    		$row = (array)$row;

    		//pembuatan index unique
    		$dtl = MdTerlambat::detail_rancangan($row['id_rancangan_jadwal']);
    		if($dtl){
	    		$uniq = strtotime($row['tanggal'])."-".$dtl['id_karyawan']."-".$dtl['id_divisi'];
    		}
    		else{
    			//index detail rancangan jadwal ga ketemu
    			continue;
    		}


    		$return[$uniq] = $row;
    		$return[$uniq]['id_karyawan'] = $dtl['id_karyawan'];
    		$return[$uniq]['id_divisi'] = $dtl['id_divisi'];

    		$time = MdPresensi::time_process($row);
    		$return[$uniq]['time'] = $time;
    		//hitung selisih
    		$return[$uniq]['selisih'] = [
    			'masuk' => (strtotime($row['must_masuk']) - strtotime($row['real_masuk'])) / 60,
    			'pulang' => (strtotime($row['real_pulang']) - strtotime($row['must_pulang'])) / 60
    		];

    		if($time['bgpulang'] == "err" || $time['bgmasuk'] == "err")
	    		$return[$uniq]['terlambat'] = 1;
    		else
	    		$return[$uniq]['terlambat'] = 0;
    	}

    	//pembuangan index yang tidak berkepentingan
    	if(count($allowed) > 0){
    		foreach($return as $indx => $vale){
    			if(!in_array($indx, $allowed)){
    				unset($return[$indx]);
    			}
    		}
    	}


    	return $return;
    }

    static function detail_rancangan($id){
    	$sql = DB::table("cwa_rancangan_jadwal")
    	->where("id", $id)
    	->where("stat","<>",9)
    	->first();
    	$ret = (array)$sql;
    	return $ret;
    }






    static function structure(){
    	return [
    		"#" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "no",    			
    		),
    		"Nama Karyawan" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "nama",
    		),
    		"Divisi" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "divisi",
    		),
    		"Tanggal" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "tanggal",
    		),
    		"Jam Masuk" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "real_masuk",
    		),
    		"Jam Pulang" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "real_pulang",
    		),
    		"Keterlambatan" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "terlambat",
    		),
    		"Alasan" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "reason",
    		),
    		"Acc" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "btn",
    		)

    	];
    }

    public function content($data){
    	$list_karyawan = tb_list("cwa_karyawan");
    	$list_divisi = tb_list("cwa_divisi");

    	$saved = $this->saved_terlambat();

    	$i = 1;
    	foreach($data as $ind => $val){
    		if($val['selisih']['masuk'] < 0 || $val['selisih']['pulang'] < 0){
                if(!isset($list_karyawan[$val['id_karyawan']])){
                    //skip daripada error
                    continue;
                }
    			$mas = abs($val['selisih']['masuk']);
    			$pul = abs($val['selisih']['pulang']);

    			$data[$ind]['uniq'] = $ind;
    			$data[$ind]['no'] = $i;
	    		$data[$ind]['nama'] = $list_karyawan[$val['id_karyawan']]['nama'];
	    		$data[$ind]['divisi'] = $list_divisi[$val['id_divisi']]['nama_divisi'];
	    		$data[$ind]['tanggal'] = indo_date($val['tanggal']);
	    		$data[$ind]['real_masuk'] = time_format($val['real_masuk']);
	    		$data[$ind]['real_pulang'] = time_format($val['real_pulang']);
	    		$data[$ind]['terlambat'] = ($val['selisih']['masuk'] < 0 ? "<span class='label label-danger'>".x_minute($mas)."</span> " : "")." ".($val['selisih']['pulang'] < 0 ? "<span class='label label-danger'>".x_minute($pul)."</span>" : "");

	    		$data[$ind]['reason'] = '
	    		<div class="input-group">
	    			<input type="text" name="reason" class="form-control ajaxBx" placeholder="Catatan alasan terlambat" data-uniq="'.$ind.'" data-type="reason" value="'.(isset($saved[$ind]) ? $saved[$ind]['reason'] : '') .'" />
	    			<span class="input-group-btn">
	    				<a href="" class="btn btn-primary ajaxBtn">
	    					<i class="fa fa-save"></i>
	    				</a>
	    			</span>
	    		</div>
	    		';
	    		$data[$ind]['btn'] = '
	    		<input type="checkbox" data-uniq="'.$ind.'" name="acc" '.(isset($saved[$ind]) ? $saved[$ind]['acc'] == 1 ? 'checked="checked"' : '' : '').' class="form-control ajaxChk" data-type="acc">
	    		';
	    		$i++;
    		}
    		else
    			unset($data[$ind]);
    	}
    	return $data;
    }

    public function saved_terlambat(){
    	$a = $this->calendar[0];
    	$b = max($this->calendar);

    	$sql = DB::table("cwa_terlambat")
    	->whereBetween("tanggal", [$a, $b])
    	->where("stat","<>",9)
    	->get();

    	$ret = array();
    	foreach($sql as $row){
    		$row = (array)$row;
    		$ret[$row['uniq']]['id'] = $row['id'];
    		$ret[$row['uniq']]['reason'] = $row['reason'];
    		$ret[$row['uniq']]['acc'] = $row['acc'];
    	}

    	return $ret;
    }

    static function getTerlambat($uniq){
    	$cek = DB::table("cwa_terlambat")
    	->where("uniq",$uniq)
    	->where("stat","<>",9)
    	->first();

    	if(isset($cek->id))
    		return (array)$cek;
    	else
    		return false;
    }

}
