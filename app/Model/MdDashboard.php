<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Model\MdTerlambat;

class MdDashboard extends Model
{
	var $tgl,
		$data,
		$bulan,
		$tahun,
		$mutasi;

    public function __construct(){
    	$skrg = date("Y-m-d");
    	$this->tgl = $skrg;
    	$this->bulan = date("n", strtotime($skrg));
    	$this->tahun = date("Y", strtotime($skrg));

    	$this->mutasi = MdMtKaryawan::queryGetMutasi([
    		'tgl_berlaku' => $this->tgl
    	]);

    	$this->get_jumlah_karyawan();
    	$this->get_jumlah_divisi();
    	$this->get_terlambat();
    	$this->get_presence();

//    	dd($this->data);
    }

    public function get_jumlah_karyawan(){
    	$data = $this->mutasi;

    	$master = DB::table("cwa_karyawan")
    	->where("stat","<>",9)
    	->get();

    	$a = count($master);
    	$b = count($data);
        $c = intval($this->get_resign());

        $vals = ($a - $b) - $c;
        if($vals < 0){
            $vals = 0;
        }

    	$this->data['flat']['resign'] = [
    		'label' => "Jumlah Karyawan Belum Mutasi",
    		'value' => $vals,
    	];
    	$this->data['flat']['karyawan'] = [
    		'label' => "Jumlah Karyawan",
    		'value' => $a
    	];


    	//utk jenis kelamin
    	$pria = $wanita = 0;
    	foreach($data as $r){
    		if($r->jk == 1)
    			$pria++;
    		else
    			$wanita++;
    	}


    	$dataLabel = json_encode(["Pria","Wanita"]);
    	$dataValue = json_encode([$pria, $wanita]);
    	$dataBg = json_encode(["rgba(0, 221, 210, .2)", "rgba(225, 84, 126, .2)"]);

    	$this->data['graph']['jk'] = [
    		'label' => "Pembagian Jenis Kelamin",
    		'type' => 'pie',
    		'dataLabel' => $dataLabel,
    		'dataValue' => $dataValue,
    		'dataBg' => $dataBg
    	];
    }


    public function get_resign(){
        $sql = DB::table('cwa_resign')
            ->where('tgl_resign', '<=', $this->tgl)
            ->where('stat', '<>', 9)
            ->count();
        return $sql;
    }



    public function get_jumlah_divisi(){
    	$sql = DB::table("cwa_divisi")
    	->where("stat","<>",9)
    	->get();

    	$this->data['flat']['divisi'] = [
    		'label' => "Jumlah Divisi",
    		'value' => count($sql)
    	];

    	//grouping mutasi
        $divisi = [];
    	foreach($this->mutasi as $row){
    		if(isset($divisi[$row->nama_divisi]))
    			$divisi[$row->nama_divisi]++;
    		else
    			$divisi[$row->nama_divisi] = 1;
    	}

        $outlbl = $outnn = $outbg = [];
        foreach($divisi as $tgx => $nn){
        	$outlbl[] = $tgx;
        	$outnn[] = $nn;
        	$outbg[] = rand_color();
        }

        $outlbl = json_encode($outlbl);
        $outnn = json_encode($outnn);
        $outbg = json_encode($outbg);

    	$this->data['graph']['divisi'] = [
    		'type' => 'pie',
    		'label' => "Pembagian Karyawan di Divisi",
    		'dataLabel' => $outlbl,
    		'dataValue' => $outnn,
    		'dataBg' => $outbg
    	];

    }


    public function get_terlambat(){
    	$telat = new MdTerlambat([
            'bulan' => $this->bulan,
            'tahun' => $this->tahun
        ]);

        $allowed = array();
        $list_date = $telat->calendar;
        foreach($list_date as $tg){
            $mutt = MdMtKaryawan::queryGetMutasi([
                'tgl_berlaku' =>  $tg
            ]);

            foreach($mutt as $arr){
                $uniq = strtotime($tg)."-".$arr->id_karyawan."-".$arr->id_divisi;
                $allowed[] = $uniq;
            }
            //data telat valid apabila termasuk dalam list mutasi yang terdaftar
        }
        $data_telat = $telat->get_presence($allowed);
        $ind_telat = 0;
        foreach($data_telat as $arr){
        	if($arr['terlambat'] <> 0)
        		$ind_telat++;
        }

        $this->data['flat']['terlambat'] = [
        	'label' => "Jumlah Keterlambatan",
        	'value' => $ind_telat
       	];
    }



    public function get_presence(){
    	$out = [];
    	$presence = new MdPresensi([
    		'bulan' => $this->bulan,
    		'tahun' => $this->tahun,
    	]);

    	//build mutasi
    	$mutasi = array();
        foreach($presence->calendar as $tg){
            $mutt = MdMtKaryawan::queryGetMutasi([
                'tgl_berlaku' =>  $tg
            ]);

            foreach($mutt as $arr){
                $uniq = $arr->id_karyawan."-".$arr->id_divisi;
                if(!isset($mutasi[$uniq])){
                    $mutasi[$uniq]->tgl[] = $tg;
                }
                else{
                    $mutasi[$uniq]->tgl[] = $tg;
                }
            }
        }

        $content = $presence->list_presensi($presence->calendar);

        foreach($content as $index=>$gapake){
        	$exp = explode("-", $index);
        	$curtgl = date("Y-m-d",$exp[0]);
        	$uniq = $exp[1]."-".$exp[2];
        	if(!isset($mutasi[$uniq])){
        		unset($content[$index]);
        	}
        	else{
        		if(!in_array($curtgl, $mutasi[$uniq]->tgl))
        			unset($content[$index]);
        		else{
        			if(isset($out[$curtgl]))
        				$out[$curtgl]++;
        			else
        				$out[$curtgl] = 1;
        		}
        	}
        }

        //yang kosong2
        foreach($presence->calendar as $tgll){
        	if(!isset($out[$tgll]))
        		$out[$tgll] = 0;
        }

        //last formatting
        ksort($out);
        foreach($out as $tgx => $nn){
        	$outtgl[] = date("d F", strtotime($tgx));
        	$outnn[] = $nn;
        }

        $this->data['graph']['absensi'] = [
        	'type' => 'bar',
        	'label' => 'Absensi per Hari',
        	'dataLabel' => json_encode($outtgl),
        	'dataValue' => json_encode($outnn)
        ];
    }


    static function check_notif(){
        $out = [];
        $tgl = date("Y-m-d");
        $bulan = date("n");
        $tahun = date("Y");



        $firstq = DB::select("
            SELECT
            (SELECT COUNT(id) FROM cwa_karyawan WHERE stat <> 9) AS a,
            (SELECT COUNT(id) FROM cwa_divisi WHERE stat <> 9) AS b,
            (SELECT COUNT(id) FROM cwa_jabatan WHERE stat <> 9) AS c,
            (SELECT COUNT(id) FROM cwa_kode_absen WHERE stat <> 9) AS d,
            (SELECT COUNT(id) FROM cwa_hr WHERE stat <> 9) AS e,
            (SELECT COUNT(id) FROM cwa_master_gaji WHERE stat <> 9) AS f
        ");

        if($firstq[0]->a == 0)
            $out[] = "Data karyawan masih kosong";
        if($firstq[0]->b == 0)
            $out[] = "Data divisi masih kosong";
        if($firstq[0]->c == 0)
            $out[] = "Data jabatan masih kosong";
        if($firstq[0]->d == 0)
            $out[] = "Data kode absen masih kosong";
        if($firstq[0]->e == 0)
            $out[] = "Data hari raya masih kosong";
        if($firstq[0]->f == 0)
            $out[] = "Data gaji masih kosong";

        $cek_rekening = self::check_rekening();
        if($cek_rekening)
            $out[] = $cek_rekening;

        $jadwal = new MdJadwal([
            'bulan' => $bulan,
            'tahun' => $tahun
        ]);
        $a = $jadwal->calendar[0];
        $b = max($jadwal->calendar);

        $cek = DB::table("cwa_rancangan_jadwal")
        ->whereBetween("tanggal", [$a, $b])
        ->where("stat","<>",9)
        ->get();

        if(count($cek) == 0)
            $out[] = "Rancangan jadwal periode ini belum dibuat.";



        $cek2 = DB::table("cwa_presensi")
        ->where("tanggal", $tgl)
        ->where("stat","<>",9)
        ->get();

        if(count($cek2) == 0)
            $out[] = "Data presensi hari ini belum diisi.";




        return $out;
    }
    
    static function check_rekening(){
        $skrg = strtotime(date("Y-m-d"));
        $limit = 3 * 30 * 24 * 60 * 60;
        
        $data = DB::table('cwa_karyawan')->where('stat',1)->where('no_rek', '')->get();
        $n = 0;
        foreach($data as $row){
            $tg = strtotime($row->tgl_masuk);
            if($skrg - $tg >= $limit){
                $n++;
            }
        }
        
        if($n > 0){
            return 'Masih ada '.$n.' karyawan yang belum membuat rekening';
        }
        return false;
    }
    
    
    
    
    
    
}
