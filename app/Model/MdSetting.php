<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Redirect;

class MdSetting extends Model
{
    //
	var $param,
		$users;

    public function __construct(){
    	$sql = DB::table("cwa_setting")->get();
    	foreach($sql as $row){
    		$this->param[$row->tag][$row->param] = (array)$row;
    	}

    	$this->users = $this->get_user_data();
    }

    public function user_by_id($id){
        $sql = DB::table('cwa_user')
        ->where('id', $id)
        ->first();

        return $sql;
    }

    public function get_user_data($level=null){
    	if(is_null($level)){
    		$priv = get_priviledge();
    		$priv = $priv[0];
    	}
    	else
    		$priv = $level;


    	if($priv > 0){
	    	$sql = DB::table("cwa_user")
	    	->where("priviledge",">=",$priv)
	    	->orderBy('priviledge')
	    	->get();

	    	return $sql;
    	}
    	else if($priv == -1){
	    	$sql = DB::table("cwa_user")
	    	->where("priviledge",">=",$priv)
	    	->orderBy('priviledge')
	    	->get();

	    	return $sql;
    	}
    	else{
    		Auth::logout();
    		return false;
    	}
    }

    public function structure(){
    	$priv = get_priviledge();
    	$idpriv = $priv[0];
    	return [
    		'Username' => [
	    		'form' => true,
	    		'field' => 'username',
	    		'type' => 'text',
	    		'attr' => ['required' => true],
	    		'value' => null
    		],
    		'Email' => [
	    		'form' => true,
	    		'field' => 'email',
	    		'type' => 'email',
	    		'attr' => ['required' => true],
	    		'value' => null
    		],
    		'Priviledge' => [
	    		'form' => true,
	    		'field' => 'priviledge',
	    		'type' => 'select',
	    		'attr' => ['required' => true],
	    		'list' => data_priviledge($idpriv),
	    		'value' => null
    		],

    		'New Password' => [
	    		'form' => true,
	    		'field' => 'pass',
	    		'type' => 'password',
	    		'attr' => [],
	    		'value' => null
    		],
    		'Repeat New Password' => [
	    		'form' => true,
	    		'field' => 'pass2',
	    		'type' => 'password',
	    		'attr' => [],
	    		'value' => null
    		],

    	];
    }


    public function backup_option(){
        return [
            'master' => [
                'cwa_code_type','cwa_control_code','cwa_divisi','cwa_hr','cwa_jabatan','cwa_karyawan','cwa_kode_absen','cwa_setting','cwa_user','cwa_shift'
            ],
            'mutasi & rancangan' => [
                'cwa_mutasi_karyawan','cwa_rancangan_jadwal','cwa_resign'
            ],
            'presensi' => [
                'cwa_presensi','cwa_lembur','cwa_terlambat','cwa_tidak_hadir'
            ],
            'gaji' => [
                'cwa_master_gaji','cwa_detail_gaji'
            ]
        ];
    }

    public function priv(){
        return [
            'master' => 4,
            'mutasi & rancangan' => 3,
            'presensi' => 3,
            'gaji' => 1
        ];
    }
}
