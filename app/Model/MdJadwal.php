<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class MdJadwal extends Model
{
    //
    var $holiday, 
    	$tgl_awal, 
    	$calendar,
        $bgsetting;

    public function __construct($data=[]){
//    	$this->holiday = json_decode(get_setting("holiday"));
        $hols = get_setting("holiday");
        $h = explode(",",$hols);
        foreach($h as $ho){
            $this->holiday[] = trim($ho);
        }

    	$this->tgl_awal = get_setting("tgl_awal_absen");

        if(!isset($data['bulan']))
            $data['bulan'] = date("n");
        if(!isset($data['tahun']))
            $data['tahun'] = date("Y");

    	$this->create_calendar($data['bulan'], $data['tahun']);
    }

    public function create_calendar($bulan, $tahun, $return=false, $customTgl=''){
    	$first_date = $this->tgl_awal;
        if(strlen($customTgl) > 0){
            $first_date = intval(date('d', strtotime($customTgl)));
        }
		$tgl = date("Y-m-d",strtotime("$tahun-$bulan-01"));
		$ret = array();
		if($first_date > 1){
			//ada data 2 bulan yg diambil. bulan n dan bulan n-1
			$y_1 = date("Y",strtotime($tgl." -1 month"));
			$m_1 = date("m",strtotime($tgl." -1 month"));
			$y_2 = date("Y",strtotime($tgl));
			$m_2 = date("m",strtotime($tgl));

			$max_1 = jumlah_hari($m_1, $y_1);

			for($i=$first_date; $i<=$max_1;$i++){
				$ret[] = date("Y-m-d",strtotime($y_1."-".$m_1."-".$i));
			}
			for($i=1;$i<$this->tgl_awal;$i++){
				$ret[] = date("Y-m-d",strtotime($y_2."-".$m_2."-".$i));
			}


		}
		else{
			//perhitungan simpel di 1 bulan yg sama
			$max = jumlah_hari($bulan, $tahun);
			$m = date("m",strtotime($tgl));
			for($i=1;$i<=$max;$i++){
				$ret[] = date("Y-m-d",strtotime($tahun."-".$m."-".$i));
			}
		}

        $this->calendar = $ret;
    }

    //kalender yang sudah ada, perlu diketahui kapan tanggal merah dan liburnya
    public function holiday_data(){
    	$first = $this->calendar[0];
    	$last = max($this->calendar);
    	$return = [];

    	$hr = DB::select("
			SELECT * FROM cwa_hr 
			WHERE 
			(
				(
					DATE_FORMAT(tgl_start, '%m-%d') 
					BETWEEN 
						DATE_FORMAT(?, '%m-%d')
						AND 
						DATE_FORMAT(?, '%m-%d')
					OR
					DATE_FORMAT(tgl_end, '%m-%d') 
					BETWEEN 
						DATE_FORMAT(?, '%m-%d')
						AND 
						DATE_FORMAT(?, '%m-%d')
				) AND occurence = 1 AND stat <> 9
    		)
    		OR
    		(
    			(
    			tgl_start BETWEEN ? AND ?
    			OR
    			tgl_end BETWEEN ? AND ?
    			)
    			AND occurence = 2 AND stat <> 9
    		)

    	", [$first, $last, $first, $last, $first, $last, $first, $last]);

    	foreach($hr as $rx){
    		$libur = date_range($rx->tgl_start, $rx->tgl_end);
    		foreach($libur as $l){
    			$dateindex = strtotime($l);
    			$return[$dateindex]['tgl'] = $l;
    			$return[$dateindex]['type'] = "HR";
    			$return[$dateindex]['label'] = $rx->nama_hr;
    		}
    	}




    	//pengaturan tanggal merah mingguan
    	foreach($this->calendar as $cal){
    		$dtindex = strtotime($cal);
    		$dname = date("D", $dtindex);
    		if(in_array($dname, $this->holiday)){
    			$return[$dtindex]['tgl'] = $cal;
    			$return[$dtindex]['type'] = "L";
    			$return[$dtindex]['label'] = "Libur";
    		}
    	}

    	return $return;
    }





    public function build_kode($mode=true){
    	$kode = [];

    	//isi kode pilihan rencana jadwal : 
    	//Seluruh Shift, Libur, Cuti, Hari Raya
    	$shift = DB::table("cwa_shift")
    	->where("stat", "=", 1)
    	->get();

    	foreach($shift as $row){
            $type = "shift";
    		$kode[] = [
    			"type" => $type,
    			"pk" => $row->id,
    			"kode" => $row->kode_shift,
    			"label" => $row->nama_shift,
    			"background" => "#AAAAAA"
    		];
            $this->bgsetting[$type."-".$row->id] = "#AAAAAA";
    	}

    	//kode absen selebihnya
    	$kd = DB::table("cwa_kode_absen")
    	->where("stat","<>", 9);
        if($mode)
    	   $kd->where("include_jadwal", 1);
    	$kd = $kd->get();

    	foreach($kd as $rk){
            $type = "kode";
    		$kode[] = [
    			"type" => $type,
    			"pk" => $rk->id,
    			"kode" => $rk->kode_absen,
    			"label" => $rk->label,
    			"background" => $rk->color
    		];

            $this->bgsetting[$type."-".$rk->id] = $rk->color;
    	}

    	return $kode;
    }


    static function getValue($type, $kode=null){
        if(empty($kode))
            return null;

        if($type=="shift"){
            $tbname = "cwa_shift";
            $pk = "kode_shift";
        }
        else if($type=="kode"){
            $tbname = "cwa_kode_absen";
            $pk = "kode_absen";
        }

        $qry = DB::table($tbname)
        ->where("id", $kode)
        ->where("stat", "<>", 9)
        ->first();

        if(isset($qry->id)){
            $qry = (array)$qry;
            return $qry[$pk];
        }
        else
            return null;
    }


    static function getPK($type, $kode=null){
        if(empty($kode))
            return null;

        if($type=="shift"){
            $tbname = "cwa_shift";
            $pk = "kode_shift";
        }
        else if($type=="kode"){
            $tbname = "cwa_kode_absen";
            $pk = "kode_absen";
        }

        $qry = DB::table($tbname)
        ->where($pk, $kode)
        ->where("stat", "<>", 9)
        ->first();
        if($qry->id)
            return $qry->id;
        else
            return null;
    }


    static function check_exists($post){
        $cek = DB::table("cwa_rancangan_jadwal")
        ->where("tanggal", $post['tanggal'])
        ->where("id_divisi", $post['id_divisi'])
        ->where("id_karyawan", $post['id_karyawan'])
        ->where("stat", "<>", 9)
        ->first();

        if(count($cek) > 0){
            return $cek;
        }
        else{
            return false;
        }
    }

    public function get_jadwal_range($divisi=null, $calendar=null){
        if(empty($calendar)){
            $a = $this->calendar[0];
            $b = max($this->calendar);
        }
        else{
            $a = $calendar[0];
            $b = max($calendar);
        }

        $qry = DB::table("cwa_rancangan_jadwal")
        ->whereBetween('tanggal', [$a, $b])
        ->where("stat", "<>", 9)
        ->get();

        $return = array();
        foreach($qry as $data){
            $data = (array)$data;
            if(empty($data['id_kode_absen'])){
                $type = "shift";
                $kode = $data['id_shift'];
            }
            else{
                $type = "kode";
                $kode = $data['id_kode_absen'];
            }

            $uniq = strtotime($data['tanggal'])."-".$data['id_karyawan']."-".$data['id_divisi'];

            $return[$uniq] = array(
                "type" => $type,
                "pk" => $kode,
                "ctn" => MdJadwal::getValue($type, $kode),
                "bg" => isset($this->bgsetting[$type."-".$kode]) ? $this->bgsetting[$type."-".$kode] : '000000'
            );
        }
        return $return;

    }



    static function updatePresensiByRancangan($instance, $updateData=[]){
        //cari data shift dulu jika ada
        if($updateData['id_shift'] > 0){
            //data shift = get jam masuk dan pulang
            $shift = DB::table('cwa_shift')->where('id', $updateData['id_shift'])->first();
            if($shift){
                $must_masuk = $shift->jam_masuk;
                $must_pulang = $shift->jam_pulang;
            }

            //update presensi ke jam tsb
            DB::table('cwa_presensi')
                ->where('id_rancangan_jadwal', $instance->id)
                ->update([
                    'must_masuk' => $must_masuk,
                    'must_pulang' => $must_pulang
                ]);

        }
        if($updateData['id_kode_absen'] > 0){
            //data kode absen = hapus data presensi
            DB::table('cwa_presensi')
                ->where('id_rancangan_jadwal', $instance->id)
                ->delete();

            //hapus data terlambat jika ada
            $uniq = strtotime($instance->tanggal).'-'.$instance->id_karyawan.'-'.$instance->id_divisi;
            DB::table('cwa_terlambat')->where('uniq', $uniq)->delete();

            //hapus data lembur jika ada
            DB::table('cwa_lembur')->where('id_presensi', $instance->id)->delete();

            $tdkhadir = DB::table('cwa_tidak_hadir')
                ->where('id_karyawan', $instance->id_karyawan)
                ->where('id_divisi', $instance->id_divisi)
                ->where('tanggal', $instance->tanggal)
                ->delete();

            DB::table('cwa_tidak_hadir')->insert([
                'id_karyawan' => $instance->id_karyawan,
                'id_divisi' => $instance->id_divisi,
                'tanggal' => $instance->tanggal,
                'id_kode_absen' => $instance->id_kode_absen,
                'stat' => 1
            ]);
        }
    }
}
