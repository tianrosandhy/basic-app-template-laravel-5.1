<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class MdHariRaya extends Model
{
    //
    static function structure(){
    	return [
    		"#" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "no",
    			"type" => "number",
    			"attr" => [],
    			"value" => null    			
    		),
    		"Nama Hari Raya" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "nama_hr",
    			"type" => "text",
    			"attr" => ["required" => true],
    			"value" => null
    		),
    		"Tanggal Mulai" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "tgl_start",
    			"type" => "date",
    			"attr" => ["required" => true, "class" => "col-sm-5 tgl_start"],
    			"value" => null
    		),
    		"Tanggal Selesai" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "tgl_end",
    			"type" => "date",
    			"attr" => ["required" => true, "class" => "col-sm-5 tgl_end"],
    			"value" => null
    		),
    		"Occurence" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "occurence",
    			"type" => "select",
    			"list" => array(
    				null => "",
    				1 => "Every year",
    				2 => "One time event"
    				),
    			"attr" => ["required" => true],
    			"value" => null
    		),

    		"" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "btn",
    			"type" => "action",
    			"attr" => [],
    			"value" => null
    		)

    	];
    }


    static function content(){

    	#from tb
    	$tb = DB::table("cwa_hr")
    		->where("stat", '<>', 9)
    		->orderBy("id", "desc")
    		->get();

    	$result = MdHariRaya::manage_table_content($tb);
    	return $result;
    }

    static function manage_table_content($fromtb){
    	#Mengubah dan menyesuaikan format data yang ditampilkan ke tabel

    	$result = array_map("toArray", $fromtb);

    	$no = 1;
    	$occ = array("", "<span class='label label-success'>Every year</span>", "<span class='label label-info'>One time event</span>");
    	foreach($result as $ind=>$data){
    		if($data['occurence'] == 1)
    			$type="quarter";
    		else
    			$type = "half";

    		$result[$ind]['no'] = $no;
    		$result[$ind]['tgl_start'] = indo_date($data['tgl_start'], $type);
    		$result[$ind]['tgl_end'] = indo_date($data['tgl_end'], $type);
    		$result[$ind]['occurence'] = $occ[$data['occurence']];
    		$result[$ind]['btn'] = '<a href="master/hari_raya/edit/'.$data['id'].'" class="btn btn-info btn-sm">Edit</a> <a href="master/hari_raya/destroy/'.$data['id'].'" class="btn btn-danger btn-sm delete-button">Delete</a>';
    		$no++;
    	}
    	return $result;
    }

    static function editable($id){
        $tb = DB::table("cwa_hr")
            ->where("id", $id)
            ->first();


        //jika ada reformat data edit, bisa diatur ulang sebelum direturn

        return (array)$tb;
    }

    static function validate($post, $except=null){
        if(strlen(trim($post['nama_hr'])) == 0)
            return "Mohon mengisi nama / kode hr dengan tepat";

        if($except > 0){
            $sql = DB::table("cwa_hr")
            ->where("nama_hr", $post['nama_hr'])
            ->where("stat", "<>", 9)
			->where("id", "<>", $except)
            ->where("tgl_start",$post['tgl_start'])
            ->where("tgl_end",$post['tgl_end'])
            ->get();
        }
        else{
            $sql = DB::table("cwa_hr")
            ->where("nama_hr", $post['nama_hr'])
            ->where("stat", "<>", 9)
            ->where("tgl_start",$post['tgl_start'])
            ->where("tgl_end",$post['tgl_end'])
            ->get();
        }

        if(count($sql) > 0){
            return "Data hari raya dengan keterangan tersebut sudah ada";
        }
        return false;
    }

}
