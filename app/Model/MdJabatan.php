<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class MdJabatan extends Model
{
    //
    static function structure(){
    	return [
    		"#" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "no",
    			"type" => "number",
    			"attr" => [],
    			"value" => null    			
    		),
    		"Nama Jabatan" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "nama_jabatan",
    			"type" => "text",
    			"attr" => ["required" => true],
    			"value" => null
    		),
    		"Description" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "description",
    			"type" => "textarea",
    			"attr" => [],
    			"value" => null
    		),
    		"" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "btn",
    			"type" => "action",
    			"attr" => [],
    			"value" => null
    		)

    	];
    }


    static function content(){

    	#from tb
    	$tb = DB::table("cwa_jabatan")
    		->where("stat", '<>', 9)
    		->orderBy("id", "desc")
    		->get();

    	$result = MdJabatan::manage_table_content($tb);
    	return $result;
    }

    static function manage_table_content($fromtb){
    	#Mengubah dan menyesuaikan format data yang ditampilkan ke tabel

    	$result = array_map("toArray", $fromtb);

    	$no = 1;
    	foreach($result as $ind=>$data){
    		$result[$ind]['no'] = $no;
    		$result[$ind]['btn'] = '<a href="master/jabatan/edit/'.$data['id'].'" class="btn btn-info btn-sm">Edit</a> <a href="master/jabatan/destroy/'.$data['id'].'" class="btn btn-danger btn-sm delete-button">Delete</a>';
    		$no++;
    	}
    	return $result;
    }

    static function editable($id){
        $tb = DB::table("cwa_jabatan")
            ->where("id", $id)
            ->first();


        //jika ada reformat data edit, bisa diatur ulang sebelum direturn

        return (array)$tb;
    }

    static function validate($post, $except=null){
        if(strlen(trim($post['nama_jabatan'])) == 0)
            return "Mohon mengisi nama jabatan dengan tepat";

        if($except > 0){
            $sql = DB::table("cwa_jabatan")
            ->where("nama_jabatan", $post['nama_jabatan'])
            ->where("stat", "<>", 9)
            ->where("id", "<>", $except)
            ->get();
        }
        else{
            $sql = DB::table("cwa_jabatan")
            ->where("nama_jabatan", $post['nama_jabatan'])
            ->where("stat", "<>", 9)
            ->get();
        }

        if(count($sql) > 0){
            return "Data jabatan dengan nama tersebut sudah ada";
        }
        return false;
    }

}
