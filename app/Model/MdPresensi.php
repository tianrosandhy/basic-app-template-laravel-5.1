<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Model\MdJadwal;

class MdPresensi extends MdJadwal
{
    //
	static function kode_absen(){
		$sql = DB::table("cwa_kode_absen")
		->where("stat", "<>", 9)
		->get();

		$ret = array(""=>["label" => "-", "kode" => null, "bobot" => null, "color" => ""]);
		foreach($sql as $row){
			$ret[$row->id] = array(
				"kode" => $row->kode_absen,
				"label" => $row->label,
				"bobot" => $row->bobot,
				"color" => $row->color
			);
		}
		return $ret;
	}

	static function list_shift(){
		$sql = DB::table("cwa_shift")
		->where('stat', '<>', 9)
		->get();

		$ret = array();
		foreach($sql as $row){
			$ret[$row->id] = [
				"nama_shift" => $row->nama_shift,
				"kode_shift" => $row->kode_shift,
				"jam_masuk" => date("H:i",strtotime($row->jam_masuk)),
				"jam_pulang" => date("H:i",strtotime($row->jam_pulang)),
			];
		}
		return $ret;
	}

	public function check_rancangan_exists($post){
		$idrancangan = MdPresensi::get_rancangan($post);
		if($idrancangan){
			$sql = DB::table("cwa_rancangan_jadwal")
			->where("id", $idrancangan->id)
			->where("stat", "<>", 9)
			->first();
			$sql = (array)$sql;

			if(isset($sql['id']) > 0){
				//ada
				return $sql;
			}
			else{
				return false;
			}
		}
		else{
			return false;
		}
	}

	static function get_rancangan($post){
		$sql = DB::table("cwa_rancangan_jadwal")
		->where("stat","<>",9)
		->where("id_karyawan", $post['id_karyawan'])
		->where("id_divisi", $post['id_divisi'])
		->where("tanggal", $post['tanggal'])
		->first();

		if(count($sql) > 0){
			//ada
			return $sql;
		}
		return false;
	}

	static function cek_presensi_exists($id_rancangan){
		$sql = DB::table("cwa_presensi")
		->where("id_rancangan_jadwal", $id_rancangan)
		->where("stat", "<>", 9)
		->get();
		if(count($sql) > 0)
			return true;
		return false;
	}


	public function list_presensi($list_date){
		$a = $list_date[0];
		$b = max($list_date);
		$sql = DB::table("cwa_presensi AS a")
		->join('cwa_rancangan_jadwal AS b', 'a.id_rancangan_jadwal', '=', 'b.id')
		->select("a.id", "a.id_rancangan_jadwal", "must_masuk", "must_pulang", "real_masuk", "real_pulang", "a.tanggal", "a.stat", "b.id_karyawan", "b.id_divisi", "b.id_kode_absen", "b.id_shift")
		->whereBetween("a.tanggal", [$a, $b])
		->where('a.stat',1)
		->get();

		$return = array();
		foreach($sql as $row){
			$row = (array)$row;
			$uniq = strtotime($row['tanggal'])."-".$row['id_karyawan']."-".$row['id_divisi'];

			$return[$uniq."-masuk"]['jam'] = empty($row['real_masuk']) ? null : date("H:i", strtotime($row['real_masuk']));
			$return[$uniq."-pulang"]['jam'] = empty($row['real_pulang']) ? null : date("H:i", strtotime($row['real_pulang']));

			//pengecekan background'
			$dataa = [
				"must_masuk" => $row['must_masuk'],
				"must_pulang" => $row['must_pulang'],
				"real_masuk" => $row['real_masuk'],
				"real_pulang" => $row['real_pulang']
			];
			$time = MdPresensi::time_process($dataa);
			$return[$uniq."-masuk"]['bg'] = $time['bgmasuk'];
			$return[$uniq."-pulang"]['bg'] = $time['bgpulang'];

		}
		return $return;
	}

	static function time_process($data){
		if(strtotime($data['real_pulang']) >= strtotime($data['must_pulang']))
			$return["bgpulang"] = "fill";
		else
			$return["bgpulang"] = "err";
		
		if(strtotime($data['real_masuk']) <= strtotime($data['must_masuk']))
			$return["bgmasuk"] = "fill";
		else
			$return["bgmasuk"] = "err";

		return $return;
	}

	public function tidak_hadir($list_date){
		$sql = DB::table("cwa_tidak_hadir")
		->whereBetween("tanggal", [$list_date[0], max($list_date)])
		->where("stat","<>",9)
		->get();

		$ret = array();
		if(count($sql) > 0){
			foreach($sql as $row){
				$ret[$row->id_karyawan][$row->id_divisi][strtotime($row->tanggal)] = $row->id_kode_absen;
			}
			return $ret;
		}
		else
			return [];
	}
}
