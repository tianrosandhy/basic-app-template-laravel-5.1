<?php

namespace App\Model\Table;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Redirect;

class ModelJabatan extends Model
{
    public $timestamps = false;
    protected $table = 'cwa_jabatan';
    protected $fillable = [
        'id',
        'nama_jabatan',
        'description',
        'stat'
    ];
}
