<?php

namespace App\Model\Table;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Redirect;

class ModelGolongan extends Model
{
    public $timestamps = false;
    protected $table = 'cwa_golongan';
    protected $fillable = [
        'id',
        'kode_golongan',
        'description',
        'gaji_pokok',
        'tunjangan_jabatan',
        'stat'
    ];


    public function gaji(){
        return $this->hasMany('App\\Model\\Table\\ModelGolonganGaji', 'id_golongan', 'id');
    }
}
