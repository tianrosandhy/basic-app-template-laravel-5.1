<?php

namespace App\Model\Table;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Redirect;

class ModelKaryawan extends Model
{
    public $timestamps = false;
    protected $table = 'cwa_karyawan';
    protected $fillable = [
        'id',
        'nama',
        'nama_panggilan',
        'ktp',
        'ttl',
        'jk',
        'alamat',
        'alamat_asal',
        'telp',
        'no_rek',
        'tgl_masuk',
        'stat'
    ];
}
