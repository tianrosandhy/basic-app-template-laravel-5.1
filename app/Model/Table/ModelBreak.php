<?php

namespace App\Model\Table;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Redirect;

class ModelBreak extends Model
{
    public $timestamps = false;
    protected $table = 'cwa_break';
    protected $fillable = [
        'id',
        'id_karyawan',
        'type',
        'tgl_start',
        'tgl_end',
        'description',
        'stat'
    ];
}
