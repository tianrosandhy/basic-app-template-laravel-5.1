<?php

namespace App\Model\Table;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Redirect;

class ModelGajiResign extends Model
{
    public $timestamps = false;
    protected $table = 'cwa_resign_gaji';
    protected $fillable = [
        'id',
        'id_karyawan',
        'bonus',
        'tgl_berlaku',
        'stat'
    ];


    public function getKaryawan(){
    	return $this->belongsTo('App\Model\Table\ModelKaryawan', 'id_karyawan', 'id');
    }

}
