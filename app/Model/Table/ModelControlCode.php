<?php

namespace App\Model\Table;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Redirect;

class ModelControlCode extends Model
{
    public $timestamps = false;
    protected $table = 'cwa_control_code';
    protected $fillable = [
        'id',
        'id_karyawan',
        'code',
        'type',
        'stat'
    ];

    public function getKaryawan(){
    	return $this->belongsTo('App\\Model\\Table\\ModelKaryawan', 'id_karyawan');
    }
}
