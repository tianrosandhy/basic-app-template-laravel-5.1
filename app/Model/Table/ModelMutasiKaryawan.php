<?php

namespace App\Model\Table;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Redirect;

class ModelMutasiKaryawan extends Model
{
    public $timestamps = false;
    protected $table = 'cwa_mutasi_karyawan';
    protected $fillable = [
        'id',
        'id_karyawan',
        'id_divisi',
        'id_jabatan',
        'id_golongan',
        'tgl_berlaku',
        'stat'
    ];
    
    public function getKaryawan(){
        return $this->belongsTo('App\Model\Table\ModelKaryawan', 'id_karyawan', 'id');
    }

    public function getDivisi(){
        return $this->belongsTo('App\Model\Table\ModelDivisi', 'id_divisi', 'id');
    }

    public function getJabatan(){
        return $this->belongsTo('App\Model\Table\ModelJabatan', 'id_jabatan', 'id');
    }
    
    public function getGolongan(){
        return $this->belongsTo('App\Model\Table\ModelGolongan', 'id_golongan', 'id');
    }

}
