<?php

namespace App\Model\Table;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Redirect;

class ModelDivisi extends Model
{
    public $timestamps = false;
    protected $table = 'cwa_divisi';
    protected $fillable = [
        'id',
        'nama_divisi',
        'deskripsi',
        'stat'
    ];
}
