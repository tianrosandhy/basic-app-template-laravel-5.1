<?php

namespace App\Model\Table;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Redirect;

class ModelGolonganGaji extends Model
{
    public $timestamps = false;
    protected $table = 'cwa_golongan_gaji';
    protected $fillable = [
        'id',
        'id_golongan',
        'gaji_pokok',
        'tunjangan_jabatan',
        'tgl_berlaku',
        'stat'
    ];

}
