<?php

namespace App\Model\Table;

use Illuminate\Database\Eloquent\Model;

class ModelResign extends Model
{
    public $timestamps = false;
    protected $table = 'cwa_resign';
    protected $fillable = [
        'id',
        'id_karyawan',
        'tgl_resign',
        'keterangan',
        'stat'
    ];


    public function karyawan(){
    	return $this->belongsTo('App\Model\Table\ModelKaryawan', 'id_karyawan', 'id');
    }
}
