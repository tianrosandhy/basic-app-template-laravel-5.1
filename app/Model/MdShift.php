<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class MdShift extends Model
{
    //
    static function structure(){
    	return [
    		"#" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "no",
    			"type" => "number",
    			"attr" => [],
    			"value" => null    			
    		),
    		"Nama Shift" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "nama_shift",
    			"type" => "text",
    			"attr" => ["required" => true],
    			"value" => null
    		),
    		"Kode Shift" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "kode_shift",
    			"type" => "text",
    			"attr" => ["required" => true, "maxlength"=>10, "class"=>"col-md-1 col-xs-3"],
    			"value" => null
    		),
    		"Jam Masuk" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "jam_masuk",
    			"type" => "text",
    			"attr" => ["required" => true, "class" => "timepicker"],
    			"value" => null
    		),
    		"Jam Pulang" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "jam_pulang",
    			"type" => "text",
    			"attr" => ["required" => true, "class" => "timepicker"],
    			"value" => null
    		),

    		"Description" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "description",
    			"type" => "textarea",
    			"attr" => [],
    			"value" => null
    		),
            "Status" => array(
                "table" => true,
                "form" => false,
                "field" => "stat",
                "type" => "text",
                "attr" => [],
                "value" => null
            ),
    		"" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "btn",
    			"type" => "action",
    			"attr" => [],
    			"value" => null
    		)

    	];
    }


    static function content(){

    	#from tb
    	$tb = DB::table("cwa_shift")
    		->where("stat", '<>', 9)
    		->orderBy("id", "desc")
    		->get();

    	$result = MdShift::manage_table_content($tb);
    	return $result;
    }

    static function manage_table_content($fromtb){
    	#Mengubah dan menyesuaikan format data yang ditampilkan ke tabel

    	$result = array_map("toArray", $fromtb);

    	$no = 1;
    	foreach($result as $ind=>$data){
    		$result[$ind]['no'] = $no;
            $result[$ind]['stat'] = '<input type="checkbox" data-switchery data-table="cwa_shift" data-field="stat" data-id="'.$data['id'].'" '.($data['stat'] ? 'checked' : '').'>';
    		$result[$ind]['btn'] = '<a href="master/shift/edit/'.$data['id'].'" class="btn btn-info btn-sm">Edit</a> <a href="master/shift/destroy/'.$data['id'].'" class="btn btn-danger btn-sm delete-button">Delete</a>';
    		$no++;
    	}
    	return $result;
    }

    static function editable($id){
        $tb = DB::table("cwa_shift")
            ->where("id", $id)
            ->first();


        //jika ada reformat data edit, bisa diatur ulang sebelum direturn

        return (array)$tb;
    }

    static function validate($post, $except=null){
        if(strlen(trim($post['nama_shift'])) == 0 || strlen(trim($post['kode_shift'])) == 0)
            return "Mohon mengisi nama / kode shift dengan tepat";

        if($except > 0){
            $sql = DB::table("cwa_shift")
            ->where("nama_shift", $post['nama_shift'])
            ->where("stat", "<>", 9)
			->where("id", "<>", $except)
            ->where("jam_masuk",$post['jam_masuk'])
            ->where("jam_pulang",$post['jam_pulang'])
            ->get();

            $sql2 = DB::table("cwa_shift")
            ->where("stat","<>",9)
            ->where("kode_shift", $post['kode_shift'])
            ->where("id", "<>", $except)
            ->get();
        }
        else{
            $sql = DB::table("cwa_shift")
            ->where("nama_shift", $post['nama_shift'])
            ->where("stat", "<>", 9)
            ->where("jam_masuk",$post['jam_masuk'])
            ->where("jam_pulang",$post['jam_pulang'])
            ->get();

            $sql2 = DB::table("cwa_shift")
            ->where("stat","<>",9)
            ->where("kode_shift", $post['kode_shift'])
            ->get();
        }

        if(count($sql) > 0 || count($sql2) > 0){
            return "Data shift dengan keterangan tersebut sudah ada";
        }
        return false;
    }
}
