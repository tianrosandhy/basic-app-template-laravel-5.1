<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Model\MdJadwal;

class MdLembur extends MdJadwal
{
    //
    public function get_lembur($allowed=[]){
    	$a = $this->calendar[0];
    	$b = max($this->calendar);

    	$sql = DB::table("cwa_presensi")
        ->join('cwa_rancangan_jadwal', 'cwa_presensi.id_rancangan_jadwal', '=', 'cwa_rancangan_jadwal.id')
        ->join('cwa_shift', 'cwa_rancangan_jadwal.id_shift', '=', 'cwa_shift.id')
    	->whereBetween("cwa_presensi.tanggal", [$a, $b])
    	->where("cwa_presensi.stat","<>", 9)
        ->select('cwa_presensi.*', 'cwa_rancangan_jadwal.id_shift', 'cwa_shift.jam_masuk', 'cwa_shift.jam_pulang', 'cwa_shift.nama_shift')
    	->get();


        //list shift

    	$return = array();
    	foreach($sql as $row){
    		$row = (array)$row;

    		//pembuatan index unique
    		$dtl = MdTerlambat::detail_rancangan($row['id_rancangan_jadwal']);
    		if($dtl){
	    		$uniq = strtotime($row['tanggal'])."-".$dtl['id_karyawan']."-".$dtl['id_divisi'];
    		}
    		else{
    			//index detail rancangan jadwal ga ketemu
    			continue;
    		}


    		$return[$uniq] = $row;
    		$return[$uniq]['id_karyawan'] = $dtl['id_karyawan'];
    		$return[$uniq]['id_divisi'] = $dtl['id_divisi'];

    		$time = MdPresensi::time_process($row);
    		$return[$uniq]['time'] = $time;
    		//hitung selisih
    		$return[$uniq]['selisih'] = [
    			'masuk' => (strtotime($row['jam_masuk']) - strtotime($row['real_masuk'])) / 60,
    			'pulang' => (strtotime($row['real_pulang']) - strtotime($row['jam_pulang'])) / 60
    		];

    		$limit = get_setting("min_lembur");

    		$sm = $return[$uniq]['selisih']['masuk'];
    		$sp = $return[$uniq]['selisih']['pulang'];
    		if(($sm >= $limit || $sp >= $limit) && ($sm + $sp >= $limit)){
    			$a = ($sm >= 60) ? floor($sm/60) : 0;
    			$b = ($sp >= 60) ? floor($sp/60) : 0;

	    		$return[$uniq]['lembur'] = $a+$b;
    		}
    		else
	    		$return[$uniq]['lembur'] = 0;
    	}

    	//pembuangan index yang tidak berkepentingan
    	if(count($allowed) > 0){
    		foreach($return as $indx => $vale){
    			if(!in_array($indx, $allowed)){
    				unset($return[$indx]);
    			}
    			else if($return[$indx]['lembur'] == 0)
    				unset($return[$indx]);
    		}
    	}


    	return $return;
    }


    static function structure(){
    	return [
            "Tanggal" => array(
                "table" => true,
                "form" => false,
                "field" => "tanggal",
            ),
    		"Nama Karyawan" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "nama",
    		),
    		"Divisi" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "divisi",
    		),
            'Shift' => array(
                "table" => true,
                'form' => false,
                'field' => 'nama_shift',
            ),
    		"Jam Masuk" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "real_masuk",
    		),
    		"Jam Pulang" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "real_pulang",
    		),
    		"Lama Lembur" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "lembur",
    		),
    		"Realisasi" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "realisasi",
    		),
    		"Tipe" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "btn",
    		)

    	];
    }

    public function content($data){
    	$list_karyawan = tb_list("cwa_karyawan");
    	$list_divisi = tb_list("cwa_divisi");

    	$i = 1;
    	$saved = $this->saved_lembur();
    	foreach($data as $ind => $val){
			$mas = abs($val['selisih']['masuk']);
			$pul = abs($val['selisih']['pulang']);

			$data[$ind]['uniq'] = $ind;
    		$data[$ind]['nama'] = $list_karyawan[$val['id_karyawan']]['nama'];
    		$data[$ind]['divisi'] = $list_divisi[$val['id_divisi']]['nama_divisi'];
    		$data[$ind]['tanggal'] = indo_date($val['tanggal']);
    		$data[$ind]['real_masuk'] = time_format($val['real_masuk']);
    		$data[$ind]['real_pulang'] = time_format($val['real_pulang']);
    		

    		$data[$ind]['realisasi'] = '
    		<div class="input-group">
    			<input type="number" name="realisasi" class="form-control ajaxLbr" placeholder="Lama Lembur di-acc" min=0 max="'.$data[$ind]['lembur'].'" data-uniq="'.$ind.'" data-type="reason" value="'.(isset($saved[$ind]) ? $saved[$ind]['lama_lembur'] : '') .'" data-default="'.(isset($saved[$ind]) ? $saved[$ind]['lama_lembur'] : '') .'" />
    			<span class="input-group-addon">
    				jam
    			</span>
    		</div>
    		';
    		$data[$ind]['lembur'] .= " jam";
	    	$select = '
	    	<select class="form-control ajaxChose" data-uniq="'.$ind.'" class="ajaxChose">
	    		<option value="1" '. (isset($saved[$ind]) ? ($saved[$ind]['type'] == 1 ? 'selected' : '' )  : '') .'>Lembur Berbayar</option>
	    		<option value="2" '. (isset($saved[$ind]) ? ($saved[$ind]['type'] == 2 ? 'selected' : '' )  : '') .'>Lembur Tidak Berbayar</option>
	    	</select>
	    	';
	    	$data[$ind]['btn'] = $select;


    		$i++;
    	}
    	return $data;
    }

    public function saved_lembur(){
    	$a = $this->calendar[0];
    	$b = max($this->calendar);

    	$get = DB::table('cwa_lembur AS a')
    	->leftJoin("cwa_rancangan_jadwal AS b", "a.id_presensi", "=", "b.id")
    	->select('a.*', 'b.id_karyawan','b.id_divisi')
    	->whereBetween("a.tgl", [$a, $b])
    	->where("a.stat","<>",9)
    	->get();

    	if(count($get) > 0){
    		$res = [];
    		foreach($get as $row){
    			$row = (array)$row;
    			$uniq = strtotime($row['tgl'])."-".$row['id_karyawan']."-".$row['id_divisi'];
    			$res[$uniq] = $row;
    		}
    		return $res;
    	}
    	else
    		return [];

    }

    public function get_rancang($fetch){
    	$sql = DB::table("cwa_rancangan_jadwal")
    	->where("tanggal",$fetch['tanggal'])
    	->where("id_karyawan",$fetch['id_karyawan'])
    	->where("id_divisi",$fetch['id_divisi'])
    	->where("stat","<>",9)
    	->first();

    	if(isset($sql->id))
    		return $sql;
    	else
    		return false;
    }

}
