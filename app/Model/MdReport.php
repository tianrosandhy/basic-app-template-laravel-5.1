<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Model\Table\ModelGajiResign;

class MdReport extends Model
{
    //
    var $tgl_awal;
	var $calendar;
	var $out;
	var $real;
	var $list_telat;
    var $tidak_hadir;
    var $list_lembur;

    public function __construct($data=[]){
    	$this->tgl_awal = get_setting("tgl_awal_gaji");
    	if(!isset($data['bulan']))
            $data['bulan'] = date("n");
        if(!isset($data['tahun']))
            $data['tahun'] = date("Y");

        $this->out = array();

    	$this->create_calendar($data['bulan'], $data['tahun']);

    	$tlt = DB::table("cwa_terlambat")
    	->where("stat","<>",9)
    	->whereBetween("tanggal",[$this->calendar[0], max($this->calendar)])
    	->get();

    	$this->list_telat = array();
    	foreach($tlt as $row){
    		if($row->acc == 1)
	    		$this->list_telat[$row->id_karyawan][$row->tanggal] = true;
    	}

        $this->get_tidakhadir();
        $this->list_lembur();

    }

    public function create_calendar($bulan, $tahun, $return=false){
    	$first_date = $this->tgl_awal;
		$tgl = date("Y-m-d",strtotime("$tahun-$bulan-01"));
		$ret = array();
		if($first_date > 1){
			//ada data 2 bulan yg diambil. bulan n dan bulan n-1
			$y_1 = date("Y",strtotime($tgl." -1 month"));
			$m_1 = date("m",strtotime($tgl." -1 month"));
			$y_2 = date("Y",strtotime($tgl));
			$m_2 = date("m",strtotime($tgl));

			$max_1 = jumlah_hari($m_1, $y_1);

			for($i=$first_date; $i<=$max_1;$i++){
				$ret[] = date("Y-m-d",strtotime($y_1."-".$m_1."-".$i));
			}
			for($i=1;$i<$first_date;$i++){
				$ret[] = date("Y-m-d",strtotime($y_2."-".$m_2."-".$i));
			}

		}
		else{
			//perhitungan simpel di 1 bulan yg sama
			$max = jumlah_hari($bulan, $tahun);
			$m = date("m",strtotime($tgl));
			for($i=1;$i<=$max;$i++){
				$ret[] = date("Y-m-d",strtotime($tahun."-".$m."-".$i));
			}
		}
        $this->calendar = $ret;
    }

    public function force_calendar($bulan, $tahun, $return=false, $customTgl){
        $first_date = $this->tgl_awal;
        if(strlen($customTgl) > 0){
            $first_date = intval(date('d', strtotime($customTgl)));
        }
        $tgl = date("Y-m-d",strtotime("$tahun-$bulan-01"));
        $ret = array();
        if($first_date > 1){
            //ada data 2 bulan yg diambil. bulan n dan bulan n-1
            $y_1 = date("Y",strtotime($tgl." -1 month"));
            $m_1 = date("m",strtotime($tgl." -1 month"));
            $y_2 = date("Y",strtotime($tgl));
            $m_2 = date("m",strtotime($tgl));

            $max_1 = jumlah_hari($m_1, $y_1);

            for($i=$first_date; $i<=$max_1;$i++){
                $ret[] = date("Y-m-d",strtotime($y_1."-".$m_1."-".$i));
            }
            for($i=1;$i<$this->tgl_awal;$i++){
                $ret[] = date("Y-m-d",strtotime($y_2."-".$m_2."-".$i));
            }


        }
        else{
            //perhitungan simpel di 1 bulan yg sama
            $max = jumlah_hari($bulan, $tahun);
            $m = date("m",strtotime($tgl));
            for($i=1;$i<=$max;$i++){
                $ret[] = date("Y-m-d",strtotime($tahun."-".$m."-".$i));
            }
        }

        $this->calendar = $ret;
    }


    public function list_lembur(){
        $sql = DB::table("cwa_lembur AS a")
        ->leftJoin("cwa_presensi AS b", "a.id_presensi", "=", "b.id_rancangan_jadwal")
        ->leftJoin("cwa_rancangan_jadwal AS c", "b.id_rancangan_jadwal", "=", "c.id")
        ->select("a.*", "c.id_karyawan", "c.id_divisi", "b.id_rancangan_jadwal", "b.must_masuk", "b.real_masuk", "b.must_pulang", "b.real_pulang")
        ->where("a.type",1)
        ->whereBetween("a.tgl", [$this->calendar[0], max($this->calendar)])
        ->where("a.stat","<>",9)
        ->get();

        $arr = [];
        foreach($sql as $row){
            $arr[$row->id_karyawan][$row->tgl] = [
                'id_divisi' => $row->id_divisi,
                'lama_lembur' => $row->lama_lembur,
                'type' => $row->type,
                'must_masuk' => $row->must_masuk,
                'real_masuk' => $row->real_masuk,
                'must_pulang' => $row->must_pulang,
                'real_pulang' => $row->real_pulang,
            ];
        }

        $this->list_lembur = $arr;
    }

    public function make_mutasi($divisi, $type="divisi", $panic=false){
    	$saved = array();
        //tes mutasi by kalender max
        $get = MdMtKaryawan::queryGetMutasi([
            'tgl_berlaku' => max($this->calendar),
            $type => $divisi
        ], true);
        foreach($get as $gt){
            if($panic){
                if(is_panic() && $gt->status_transfer <> 1){
                    continue;
                }
            }
            $saved[$gt->id_karyawan] = $gt;
        }
    	return $saved;
    }



    public function get_rancangan($id=null, $row=[]){
        // generate data mutasi karyawan
        $data_mutasi = DB::table('cwa_mutasi_karyawan')->where('stat', 1);
        if ($id) {
            $data_mutasi = $data_mutasi->where('id_karyawan', $id);
        }
        $all_mutasi = $data_mutasi->get();
        $mutasi_data = [];
        foreach ($all_mutasi as $mut) {
            $mutasi_data[$mut->id_karyawan][strtotime($mut->tgl_berlaku)] = $mut->id_divisi;
        }

    	$sql = DB::table("cwa_rancangan_jadwal AS a")
    	->join("cwa_divisi AS b", "a.id_divisi", "=", "b.id")
    	->leftJoin("cwa_kode_absen AS c", "a.id_kode_absen", "=", "c.id")
    	->leftJoin("cwa_shift AS d", "a.id_shift", "=", "d.id")
        ->leftJoin("cwa_break AS e", "a.id_karyawan", "=", "e.id_karyawan")
    	->selectRaw("a.id, a.id_karyawan, a.id_divisi, b.nama_divisi, a.id_kode_absen, c.kode_absen, c.label, c.bobot, c.color, c.include_jadwal, a.id_shift, d.nama_shift, d.kode_shift, d.jam_masuk, d.jam_pulang, a.tanggal, a.stat")
    	->where("a.stat","<>",9)
/*
        ->where(function($query){
            $query->whereNull('e.tgl_start')
                ->orWhere(function($qry){
                    $qry->where('e.tgl_start', '>', max($this->calendar))
                        ->orWhere('e.tgl_end', '<', min($this->calendar));
                });
        })
*/
    	->whereIn("a.tanggal",$this->calendar);

        if($id > 0)
            $sql->where("a.id_karyawan",$id);
    	$sql = $sql->orderBy('tanggal')->get();

    	$this->structure_rancangan($sql, $mutasi_data);
    }

    public function structure_rancangan($sql, $mutasi_data){
    	foreach($sql as $row){
    		$idkar = $row->id_karyawan;

            // if there is a case when rancangan data is duplicate : check if the current data id_divisi is match
            if (isset($this->out[$idkar][$row->tanggal])) {
                $current_tgl = strtotime($row->tanggal);
                $mtkar = isset($mutasi_data[$idkar]) ? $mutasi_data[$idkar] : [];
                $grabbed = null;
                foreach ($mtkar as $stime => $cdivisi) {
                    if ($stime <= $current_tgl) {
                        $grabbed = $cdivisi;
                    } else {
                        break;
                    }
                }

                // skip data rancangan jadwal jika id divisi tidak cocok
                if ($row->id_divisi != $grabbed) {
                    continue;
                }
            }

    		$this->out[$idkar][$row->tanggal] = array(
    			"id_shift" => $row->id_shift,
    			"kode_shift" => $row->kode_shift,
    			"id_kode_absen" => $row->id_kode_absen,
    			"kode_absen" => $row->kode_absen,
    			"color" => $row->color,
    			"bobot" => $row->bobot,
    			"include_jadwal" => $row->include_jadwal,
    		);
    	}

    	/*
    	OUTPUT : 
    	- Index id_karyawan
    	- Masing-masing terdiri dari tanggal rencana jadwal
    	- Masing-masing tanggal berisi shift / kode absen terencana
    	*/
    	return $this->out;
    }





    public function get_presensi($id=null){
    	$sql = DB::table("cwa_presensi AS a")
    	->leftJoin("cwa_rancangan_jadwal AS b", "a.id_rancangan_jadwal","=","b.id")
    	->selectRaw("a.*, b.id_karyawan")
    	->where("a.stat","<>",9)
    	->whereBetween("a.tanggal",[$this->calendar[0], max($this->calendar)]);
        if($id > 0){
            $sql->where("b.id_karyawan", $id);
        }
    	$sql = $sql->get();

    	foreach($sql as $row){
    		$idkar = $row->id_karyawan;
    		if(isset($this->out[$idkar][$row->tanggal])){
    			//rencana ada!
                $kode_absen = isset($this->out[$idkar][$row->tanggal]['kode_absen']) ? $this->out[$idkar][$row->tanggal]['kode_absen'] : null;
                $kode_shift = isset($this->out[$idkar][$row->tanggal]['kode_shift']) ? $this->out[$idkar][$row->tanggal]['kode_shift'] : null;
    			$this->out[$idkar][$row->tanggal]['hadir'] = true;
    			$this->out[$idkar][$row->tanggal]['terlambat'] = $this->analisis_terlambat($row, $kode_absen, $kode_shift);
    		}
    	}
    	return $sql;
    }

    public function analisis_terlambat($obj, $kode_absen=null, $kode_shift=null){
    	if($obj->must_masuk < $obj->real_masuk){
    		if(!isset($this->list_telat[$obj->id_karyawan][$obj->tanggal]) && !in_array($kode_absen, ['HD', 'HR', 'L']))
    		return true;
    	}
    	else if($obj->must_pulang > $obj->real_pulang){
    		if(!isset($this->list_telat[$obj->id_karyawan][$obj->tanggal]) && !in_array($kode_absen, ['HD', 'HR', 'L']))
    		return true;
    	}
    	return false;
    }


    public function prepare_object(){
    	$obj = $this->out;
    	$saved = array();

    	foreach($obj as $idkar => $data){
            //manage lembur

            $saved[$idkar]['terlambat'] = 0;
    		foreach($data as $tgl => $ket){
                if(isset($ket['terlambat'])){
                    if($ket['terlambat'] == true){
                        $saved[$idkar]['terlambat']++;
                    }
                }

    			if($ket['id_kode_absen'] > 0){
    				if(isset($saved[$idkar]['kode_absen'][$ket['id_kode_absen']])){
	    				$saved[$idkar]['kode_absen'][$ket['id_kode_absen']]['val']++;
    				}
	    			else {
	    				$saved[$idkar]['kode_absen'][$ket['id_kode_absen']]['val'] = 1;
	    				$saved[$idkar]['kode_absen'][$ket['id_kode_absen']]['bg'] = $ket['color'];
	    			}

	    			$saved[$idkar]['kode_absen'][$ket['id_kode_absen']]['bobot'] = $ket['bobot'];
    			}
    			else if($ket['id_shift'] > 0 and isset($ket['hadir'])){
    				if(isset($saved[$idkar]['shift'][$ket['id_shift']])){
	    				$saved[$idkar]['shift'][$ket['id_shift']]['val']++;
    				}
	    			else{
	    				$saved[$idkar]['shift'][$ket['id_shift']]['val'] = 1;
	    				$saved[$idkar]['shift'][$ket['id_shift']]['bg'] = $ket['color'];
	    			}

	    			$saved[$idkar]['shift'][$ket['id_shift']]['bobot'] = 1;
    			}
    			else{
    				//tidak ada di data rencana dan presensi
                    //cek ke data ketidakhadiran sebelum dipaksa alpa
                    if(isset($this->tidak_hadir[$idkar][$tgl])){
                        //ada data ketidakhadiran
                        $rev = $this->tidak_hadir[$idkar][$tgl];
                        
                        if(isset($saved[$idkar]['revisi'][$rev['id_kode_absen']])){
                            $saved[$idkar]['revisi'][$rev['id_kode_absen']]['val']++;
                        }
                        else{
                            $saved[$idkar]['revisi'][$rev['id_kode_absen']]['val'] = 1;
                            $saved[$idkar]['revisi'][$rev['id_kode_absen']]['bg'] = $rev['color'];
                            $saved[$idkar]['revisi'][$rev['id_kode_absen']]['bobot'] = $rev['bobot'];

                        }

                    }
                    else{
                        if(isset($saved[$idkar]['alpa'])){
                            $saved[$idkar]['alpa']['val']++;
                        }
                        else{
                            $saved[$idkar]['alpa']['val'] = 1;
                            $saved[$idkar]['alpa']['bg'] = get_setting("alpa_bg");
                        }
                        $saved[$idkar]['alpa']['bobot'] = 0;    
                    }



    				
    			}
    		}


    	}

        $get_lembur = DB::table('cwa_lembur')
            ->leftJoin('cwa_rancangan_jadwal', 'cwa_lembur.id_presensi', '=', 'cwa_rancangan_jadwal.id')
            ->selectRaw('cwa_lembur.*, cwa_rancangan_jadwal.id_karyawan')->get();

        foreach($get_lembur as $row){
            if(isset($saved[$row->id_karyawan])){
                if(!isset($saved[$row->id_karyawan]['lembur'][$row->type])){
                    $saved[$row->id_karyawan]['lembur'][$row->type] = 0;
                }
                $saved[$row->id_karyawan]['lembur'][$row->type] += $row->lama_lembur;
            }
        }
    	return $this->lebih_rapi_lagi($saved);
    }

    public function get_tidakhadir(){
        $sql = DB::table("cwa_tidak_hadir AS a")
        ->leftJoin("cwa_kode_absen AS b","a.id_kode_absen","=","b.id")
        ->select("a.*", "b.kode_absen", "b.label", "b.bobot", "b.color", "b.include_jadwal")
        ->whereBetween("tanggal",[$this->calendar[0], max($this->calendar)])
        ->where("a.stat","<>",9)
        ->get();


        $arr = array();
        foreach($sql as $row){
            $arr[$row->id_karyawan][$row->tanggal] = array(
                "id_kode_absen" => $row->id_kode_absen,
                "label" => $row->label,
                "bobot" => $row->bobot,
                "color" => $row->color
            );
        }

        $this->tidak_hadir = $arr;
    }



    protected function getIdAbsen($kode_absen){
        $sql = DB::table('cwa_kode_absen')
            ->where('kode_absen', $kode_absen)
            ->where('stat', 1)
            ->first();
        return $sql->id;
    }

    public function lebih_rapi_lagi($prepared){
        //perlu data : hadir, terlambat, seluruh kode shift, dan alpa

        $listkode = MdReport::list_kode_absen();
        $arr = array();

        foreach($prepared as $idk => $data){
            $arr[$idk]['hadir'] = 0;
            $arr[$idk]['terlambat'] = $data['terlambat'];
            $arr[$idk]['bobot'] = 0;
            if(isset($data['shift'])){
                foreach($data['shift'] as $shf){
                    $arr[$idk]['hadir'] += $shf['val'];
                    $arr[$idk]['bobot'] += $shf['val'];
                }
            }

            if(isset($data['kode_absen'])){
                foreach($data['kode_absen'] as $ind => $ka){
                    $arr[$idk][$ind] = $ka['val'];
                    if($ka['bobot'] == 1){
                        $arr[$idk]['bobot'] += $ka['val'];
                    }
                }
            }

            if(isset($data['revisi'])){
                $telat_uniq = self::getIdAbsen('T');
                foreach($data['revisi'] as $ind => $ka){
                    //jika ada kode telat, langsung tambah ke data terlambat.. jangan dibawa ke bobot lagi
                    if($ind == $telat_uniq){
                        $arr[$idk]['terlambat'] += $ka['val'];
                    }
                    else{
                        if(isset($arr[$idk][$ind])){
                            $arr[$idk][$ind] += $ka['val'];
                        }
                        else{
                            $arr[$idk][$ind] = $ka['val'];
                        }

                        if($ka['bobot'] == 1){
                            $arr[$idk]['bobot'] += $ka['val'];
                        }
                    }
                }                
            }

            if(isset($data['alpa']['val']))
                $arr[$idk]['alpa'] = $data['alpa']['val'];
            else
                $arr[$idk]['alpa'] = 0;

            if(isset($data['lembur'][1])){
                $arr[$idk]['lembur'][1] = $data['lembur'][1];
            }
            if(isset($data['lembur'][2])){
                $arr[$idk]['lembur'][2] = $data['lembur'][2];
            }

        }
        return $arr;
    }






    static function list_kode_absen(){
        $sql = DB::table("cwa_kode_absen")
        ->where("stat","<>",9)
        ->get();
        return $sql;
    }



    public function single_gaji($detail, $type="single"){
        $arr = array();
        $list_gaji = MdReport::list_gaji();

        foreach($detail as $idkar=>$data){
            $arr['Presensi'] = $data['bobot']."/".$data['batas_bobot'];
            $arr['Alpa'] = $data['alpa'];

            foreach($list_gaji as $lg){
                if(isset($data['gaji'][$lg->type][$lg->id])){
                    $arr[$lg->label] = rupiah($data['gaji'][$lg->type][$lg->id]);
                }
            }
            $arr['Lembur'] = rupiah($data['lembur']);
            $arr['Ptg. Absensi'] = rupiah($data['potongan']);
            $arr['Ptg. Keterlambatan'] = rupiah($data['terlambat']);
            $arr['Take Home Pay'] = rupiah($data['pay']);

            if($type<>"single")
                $ret[$idkar] = $arr;
            else
                $ret = $arr;
        }
        return $ret;

    }


    public function gaji_resign(){
        $cek = ModelGajiResign::whereIn('tgl_berlaku', $this->calendar)->get();
        $out = [];
        foreach($cek as $row){
            //biar mirip aja formatnya
            $out[$row->id_karyawan] = [
                1 =>[
                    'bonus' => $row->bonus
                ],
                2 => [],
                'surplus' => $row->bonus,
                'bobot' => 0,
                'batas_bobot' => 0,
                'defisit' => 0,
                'total' => $row->bonus,
            ];
        }
        return $out;
    }


    public function gaji_maker($content, $gjFix, $gjBulan, $row=[], $type="single"){

        $maxdate = count($this->calendar);
        $total = [];
        //dari data gaji tsb, dapatkan data masing-masing outputnya berapa
        $limit_telat = get_setting("terlambat_limit");
        $denda_telat = get_setting("terlambat_value");
        $biaya_lembur = get_setting("lembur");


        #cari index kehadiran (0 - 1)
        foreach($content as $idkar=>$param){
            $excl = isset($param[exclude_presence()]) ? $param[exclude_presence()] : 0;

            $bobot = $param['bobot'] - $excl;
            $batas_bobot = ($maxdate - $excl);
            //real bobot : current bobot - libur
            $index_presence[$idkar] = $bobot / $batas_bobot;
            $alpa[$idkar] = $batas_bobot - $bobot;
            $total[$idkar]['bobot'] = $bobot;
            $total[$idkar]['batas_bobot'] = $batas_bobot;
            $total[$idkar]['lembur'] = 0;
            $total[$idkar]['alpa'] = $param['alpa'];
            $total[$idkar]['hadir'] = $param['hadir'];
            $total[$idkar]['terlambat'] = $param['terlambat'];

        }
        $include = [];
        foreach($row as $dd){
            $include[] = $dd;
        }

        $list_gaji = self::list_gaji();
        $list_gaji = json_decode(json_encode($list_gaji), true);

        $hasil = [];
        foreach($total as $idkar => $data){

            $index_bagi_absen = $data['bobot'] / $data['batas_bobot'];
            $hasil[$idkar]['index_bagi_absen'] = $index_bagi_absen;
            $hasil[$idkar]['bobot'] = $data['bobot'];
            $hasil[$idkar]['batas_bobot'] = $data['batas_bobot'];

            //fungsi otomatis dulu
            foreach($list_gaji as $row){
                $row = json_decode(json_encode($row), true);
            }


            // UPDATE 12 Des : Telat tidak ada potongan
            $hasil[$idkar][2]['potongan_terlambat'] = 0;
            // if($data['terlambat'] > $limit_telat){
            //     $hasil[$idkar][2]['potongan_terlambat'] = 0;
            // }
            // else{
            //     //kalau telat dibawah limit, dikalikan jumlah keterlambatan
            //     $hasil[$idkar][2]['potongan_terlambat'] = $denda_telat * $data['terlambat'];
            // }

            //manage lembur
            $lmlembur = 0;
            if(count($this->list_lembur) > 0){
                if(isset($this->list_lembur[$idkar])){
                    foreach($this->list_lembur[$idkar] as $row){
                        if($row['type'] == 1){
                            $lmlembur += $row['lama_lembur'];
                        }
                    }
                    $hasil[$idkar][1]['lembur'] = $lmlembur * $biaya_lembur;

                }
            }




        }

            //gaji fix yg tunjangan lain harus dihidden
            $hideTunjangan = DB::table('cwa_master_gaji')
                ->where('label', 'Tunjangan Lain')
                ->where('stat', 1)
                ->first();
            // $priv = \Auth::user()->priviledge;



            //manage gaji fix
            foreach($gjFix as $idkaryawan => $arr){
                $index_bagi_absen = 0;

                $gjpokok = isset($arr['gaji_pokok']) ? $arr['gaji_pokok'] : 0;

                $hasil[$idkaryawan][1]['gaji_pokok'] = $gjpokok;
                $hasil[$idkaryawan][1]['tunjangan_jabatan'] = isset($arr['tunjangan_jabatan']) ? $arr['tunjangan_jabatan'] : 0;


                if(isset($total[$idkaryawan])){
                    $index_bagi_absen = $total[$idkaryawan]['bobot'] / $total[$idkaryawan]['batas_bobot'];
                }
                if(is_array($arr)){
                    foreach($arr as $idgaji => $dt){
                        // if($priv > 1 && $idgaji == $hideTunjangan->id)
                        //     continue; //tunjangan lain gausah diinclude

                        if(isset($dt['value'])){
                            $n = ($dt['value'] == 0) ? $dt['default_value'] : $dt['value'];

                            if($dt['divide_by_absen'] == 1){
                                $n = $index_bagi_absen * $n;
                            }
                            else if($dt['invalid_by_alpa'] == 1 && $index_bagi_absen < 1){
                                $n = 0;
                            }

                            $hasil[$idkaryawan][$dt['type']][$idgaji] = $n;
                        }
                    }
                }

                //atur potongan berdasarkan kehadiran
                $hasil[$idkaryawan][2]['potongan_absensi'] = $gjpokok - ($gjpokok * $index_bagi_absen);


            }


            //manage gaji bulanan
            foreach($gjBulan as $idkaryawan=>$arr){
                $index_bagi_absen = 0;
                if(isset($total[$idkaryawan])){
                    $index_bagi_absen = $total[$idkaryawan]['bobot'] / $total[$idkaryawan]['batas_bobot'];
                }
                if(is_array($arr)){
                    foreach($arr as $idgaji=> $dt){
                        if(isset($dt['value'])){
                            $n = ($dt['value'] == 0) ? $dt['default_value'] : $dt['value'];

                            if($dt['divide_by_absen'] == 1){
                                $n = $index_bagi_absen * $n;
                            }
                            else if($dt['invalid_by_alpa'] == 1){
                                $n = 0;
                            }

                            $hasil[$idkaryawan][$dt['type']][$idgaji] = $n;
                        }
                    }
                }
            }






        //penghitungan total gaji
        foreach($hasil as $idkaryawan => $index){
            $surplus = isset($index[1]) ? array_sum($index[1]) : 0;
            $defisit = isset($index[2]) ? array_sum($index[2]) : 0;

            $hasil[$idkaryawan]['surplus'] = $surplus;

            if(!isset($hasil[$idkaryawan]['bobot'])){
                $hasil[$idkaryawan]['bobot'] = 0;
                $hasil[$idkaryawan]['batas_bobot'] = 0;
            }

            $hasil[$idkaryawan]['defisit'] = $defisit;
            $hasil[$idkaryawan]['total'] = (($surplus - $defisit) < 0 ? 0 : ($surplus - $defisit));
        }


        //gaji resign
        $gaji_resign = $this->gaji_resign();
        return $gaji_resign + $hasil;

    }

    public function bpjs_kesehatan_maker($gjFix){
        $tj_golongan = DB::table('cwa_master_gaji')->where('label', 'Tunjangan Golongan')->where('stat', 1)->first();
        $tj_golongan_id = 0;

        $batas_bawah = intval(get_setting("batas_bawah"));
        $batas_atas = intval(get_setting("batas_atas"));
        
        $jkk_pk = floatval(get_setting("jkk_pk"));
        $jkm_pk = floatval(get_setting("jkm_pk"));
        $jht_pk = floatval(get_setting("jht_pk"));
        $jp_pk = floatval(get_setting("jp_pk"));

        $jht_tk = floatval(get_setting("jht_tk"));
        $jp_tk = floatval(get_setting("jp_tk"));
        
        $kes_pk = floatval(get_setting("kes_pk"));
        $kes_tk = floatval(get_setting("kes_tk"));

        
        if (isset($tj_golongan->id)) {
            $tj_golongan_id = $tj_golongan->id;
        }        

        // only grab : Gaji Pokok, Tj Jabatan, Tj Golongan
        $out = [];
        foreach ($gjFix as $idkar => $param) {
            $gaji_bpjs = ($param['gaji_pokok'] ?? 0) + ($param['tunjangan_jabatan'] ?? 0);
            if (isset($param[$tj_golongan_id]['value'])) {
                $gaji_bpjs += $param[$tj_golongan_id]['value'];
            }
            $gaji_init = $gaji_bpjs;

            if ($gaji_bpjs < $batas_bawah) {
                $gaji_bpjs = $batas_bawah;
            }
            if ($gaji_bpjs > $batas_atas) {
                $gaji_bpjs = $batas_atas;
            }

            $out[$idkar] = [
                'gaji_init' => $gaji_init,
                'gaji_bpjs' => $gaji_bpjs,
                'bpjs_kesehatan_pk' => $gaji_bpjs * $kes_pk / 100,
                'bpjs_kesehatan_tk' => $gaji_bpjs * $kes_tk / 100,
                'bpjs_kesehatan_all' => ($gaji_bpjs * $kes_pk / 100) + ($gaji_bpjs * $kes_tk / 100),
                'bpjs_tk_pk' => ($jkk_pk + $jkm_pk + $jht_pk + $jp_pk) / 100 * $gaji_bpjs,
                'bpjs_tk_tk' => ($jht_tk + $jp_tk) / 100 * $gaji_bpjs,
            ];
        }

        return $out;
    }

    static function list_gaji(){
        $sql = DB::table("cwa_master_gaji")
        ->where("stat","<>",9)
        ->get();

        return $sql;
    }

}
