<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class MdGaji extends Model
{
    //
    static function structure(){
    	return [
    		"#" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "no",
    			"type" => "number",
    			"attr" => [],
    			"value" => null    			
    		),
    		"Label" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "label",
    			"type" => "text",
    			"attr" => ["required" => true],
    			"value" => null
    		),
    		"Description" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "description",
    			"type" => "textarea",
    			"attr" => [],
    			"value" => null
    		),
    		"Default Value" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "default_value",
    			"type" => "number",
    			"attr" => [],
    			"value" => null
    		),
    		"Update Nilai" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "occurence",
    			"type" => "select",
    			"list" => array(
    				1 => "Fixed",
    				2 => "Monthly"
    			),
    			"attr" => ["required" => true],
    			"value" => null
    		),
    		"Type" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "type",
    			"type" => "select",
    			"list" => array(
    				1 => "Penambahan Gaji",
    				2 => "Pemotongan Gaji"
    			),
    			"attr" => ["required" => true],
    			"value" => null
    		),
    		"Nilai Dibagi Kehadiran" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "divide_by_absen",
    			"type" => "checkbox",
    			"list" => array(
    				1 => "<b>Aktifkan</b> <small><em>(jika diaktifkan, maka ketidakhadiran karyawan akan mengurangi nilai yang diterima)</em></small>"
    			),
    			"attr" => [],
    			"value" => 0
    		),
    		"Batalkan Apabila Alpa" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "invalid_by_alpa",
    			"type" => "checkbox",
    			"list" => array(
    				1 => "<b>Aktifkan</b> <small><em>(jika diaktifkan, maka nilai tidak diberikan apabila karyawan alpa / tidak hadir tanpa alasan)</em></small>"
    			),
    			"attr" => [],
    			"value" => 0
    		),
    		"Ditampilkan" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "is_visible",
    			"type" => "checkbox",
    			"list" => array(
    				1 => "<b>Tampilkan</b> <small><em>(jika dinonaktifkan, kolom master gaji ini tidak akan terlihat di slip gaji)</em></small>"
    			),
    			"attr" => [],
    			"value" => 0
    		),
			"Tampilkan Dalam Rentang Waktu" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "is_ranged_period",
    			"type" => "checkbox",
    			"list" => array(
    				1 => "<b>Ya</b> <small><em>(jika diaktifkan, kolom master gaji ini akan mendukung input rentang waktu aktif)</em></small>"
    			),
    			"attr" => [],
    			"value" => 0
    		),

    		"" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "btn",
    			"type" => "action",
    			"attr" => [],
    			"value" => null
    		)

    	];
    }


    static function content(){

    	#from tb
    	$tb = DB::table("cwa_master_gaji")
    		->where("stat", '<>', 9)
    		->orderBy("id", "desc")
    		->get();

    	$result = MdGaji::manage_table_content($tb);
    	return $result;
    }

    static function manage_table_content($fromtb){
    	#Mengubah dan menyesuaikan format data yang ditampilkan ke tabel

    	$result = array_map("toArray", $fromtb);

    	$no = 1;
    	$upd = array("", "Fix", "Bulanan");
    	$type = array("", "<span title='Plus' class='btn btn-sm btn-success'><i class='fa fa-plus'></i></span>", "<span title='Minus' class='btn btn-sm btn-danger'><i class='fa fa-minus'></i></span>");

    	$check = array("-", "<button type='button' class='btn btn-success'><i class='fa fa-check'></i></button>");

    	foreach($result as $ind=>$data){
    		$result[$ind]['no'] = $no;
    		$result[$ind]['occurence'] = $upd[$data['occurence']];
    		$result[$ind]['type'] = $type[$data['type']];
    		$result[$ind]['divide_by_absen'] = $check[$data['divide_by_absen']];
    		$result[$ind]['invalid_by_alpa'] = $check[$data['invalid_by_alpa']];
    		$result[$ind]['is_visible'] = $check[$data['is_visible']];
    		$result[$ind]['is_ranged_period'] = $check[$data['is_ranged_period'] ?? 0];

    		$result[$ind]['btn'] = '<a href="master/gaji/edit/'.$data['id'].'" class="btn btn-info btn-sm">Edit</a> <a href="master/gaji/destroy/'.$data['id'].'" class="btn btn-danger btn-sm delete-button">Delete</a>';
    		$no++;
    	}
    	return $result;
    }

    static function get_kode($kode){
    	$qry = DB::table("cwa_kode_absen")
    	->where("kode_absen", $kode)
    	->where("stat", "<>", 9)
    	->first();

    	if($qry->label)
    		return $qry->label;
    	else
    		return "<em>{Kode absensi tidak ditemukan}</em>";
    }

    static function editable($id){
        $tb = DB::table("cwa_master_gaji")
            ->where("id", $id)
            ->first();


        //jika ada reformat data edit, bisa diatur ulang sebelum direturn

        return (array)$tb;
    }

    static function validate($post, $except=null){
        if(strlen(trim($post['label'])) == 0)
            return "Mohon mengisi nama / kode shift dengan tepat";

        if($except > 0){
            $sql = DB::table("cwa_master_gaji")
            ->where("label", $post['label'])
            ->where("stat", "<>", 9)
			->where("id", "<>", $except)
            ->get();
        }
        else{
            $sql = DB::table("cwa_master_gaji")
            ->where("label", $post['label'])
            ->where("stat", "<>", 9)
            ->get();
        }

        if(count($sql) > 0){
            return "Data gaji dengan keterangan tersebut sudah ada";
        }
        return false;
    }
}
