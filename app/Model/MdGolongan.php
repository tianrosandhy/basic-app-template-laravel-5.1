<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

use App\Model\Table\ModelGolongan;
use App\Model\Table\ModelGolonganGaji;


class MdGolongan extends Model
{

    static function structure(){
    	return [
    		"#" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "no",
    			"type" => "number",
    			"attr" => [],
    			"value" => null    			
    		),
    		"Kode Golongan" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "kode_golongan",
    			"type" => "text",
    			"attr" => ["required" => true],
    			"value" => null
    		),
    		"Description" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "description",
    			"type" => "textarea",
    			"attr" => [],
    			"value" => null
    		),
    		"Gaji Pokok" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "gaji_pokok",
    			"type" => "number",
    			"attr" => [],
    			"value" => null
    		),
    		"Tunjangan Jabatan" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "tunjangan_jabatan",
    			"type" => "text",
    			"attr" => [],
    			"value" => null
    		),
    		"" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "btn",
    			"type" => "action",
    			"attr" => [],
    			"value" => null
    		)

    	];
    }


    static function content(){
        $tgl = date("Y-m-d");
        $tb = self::golongan_gaji_by_tanggal($tgl);

    	$result = MdGolongan::manage_table_content($tb);
    	return $result;
    }

    static function golongan_gaji_by_tanggal($tgl=null){
        if($tgl==null){
            $tgl = date("Y-m-d");
        }
        $date = date('Y-m-d', strtotime($tgl));
        $tb = DB::select('
        SELECT c.id, c.kode_golongan, c.description, d.gaji_pokok, d.tunjangan_jabatan, d.tgl_berlaku, c.stat FROM cwa_golongan c 
        LEFT JOIN
        (
            SELECT a.id, a.id_golongan, a.gaji_pokok, a.tunjangan_jabatan, b.tgl_berlaku, b.stat FROM cwa_golongan_gaji a
            INNER JOIN
            (
                SELECT id_golongan, MAX(tgl_berlaku) AS tgl_berlaku, stat FROM cwa_golongan_gaji WHERE tgl_berlaku <= "'.$date.'" AND stat <> 9 GROUP BY id_golongan
            ) b ON a.id_golongan = b.id_golongan AND a.tgl_berlaku = b.tgl_berlaku
            ORDER BY a.id_golongan
        ) d ON c.id = d.id_golongan
        WHERE c.stat <> 9 
        ');

        return $tb;
    }




    static function manage_table_content($fromtb){
    	#Mengubah dan menyesuaikan format data yang ditampilkan ke tabel

    	$no = 1;
    	foreach($fromtb as $row){
    		$result[$row->id]['no'] = $no;
            $result[$row->id]['kode_golongan'] = $row->kode_golongan;
            $result[$row->id]['description'] = $row->description;
            $result[$row->id]['gaji_pokok'] = "Rp ".number_format($row->gaji_pokok);
            $result[$row->id]['tunjangan_jabatan'] = "Rp ".number_format($row->tunjangan_jabatan);
    		$result[$row->id]['btn'] = '<a href="master/golongan/update-gaji/'.$row->id.'" data-featherlight="ajax" class="btn btn-warning btn-sm">Update Gaji</a> <a href="master/golongan/edit/'.$row->id.'" class="btn btn-info btn-sm">Edit</a> <a href="master/golongan/destroy/'.$row->id.'" class="btn btn-danger btn-sm delete-button">Delete</a>';
    		$no++;
    	}
    	return $result;
    }

    static function editable($id){
        $tb = DB::table("cwa_golongan")
            ->where("id", $id)
            ->first();


        //jika ada reformat data edit, bisa diatur ulang sebelum direturn

        return (array)$tb;
    }


    static function validate($post, $except=null){
    	if(strlen(trim($post['kode_golongan'])) == 0)
    		return "Mohon mengisi nama divisi dengan tepat";

    	if($except > 0){
	    	$sql = DB::table("cwa_golongan")
	    	->where("kode_golongan", $post['kode_golongan'])
	    	->where("stat", "<>", 9)
	    	->where("id", "<>", $except)
	    	->get();
    	}
    	else{
	    	$sql = DB::table("cwa_golongan")
	    	->where("kode_golongan", $post['kode_golongan'])
	    	->where("stat", "<>", 9)
	    	->get();
    	}

    	if(count($sql) > 0){
    		return "Data golongan dengan kode tersebut sudah ada";
    	}
    	return false;
    }

}
