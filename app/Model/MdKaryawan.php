<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Model\Table\ModelDivisi;
use App\Model\Table\ModelJabatan;
use App\Model\Table\ModelGolongan;

class MdKaryawan extends Model
{

    static function structure(){
    	return [
    		"#" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "no",
    			"type" => "number",
    			"attr" => [],
    			"value" => null    			
    		),
    		"Nama Karyawan" => array(
                "table" => true,
                "form" => true,
                "field" => "nama",
                "type" => "text",
                "attr" => ["required" => true],
                "value" => null
            ),
            "NIK" => array(
                "table" => true,
                "form" => true,
                "field" => "nik",
                "type" => "text",
                "attr" => [],
                "value" => null
            ),
            "NPWP" => array(
                "table" => true,
                "form" => true,
                "field" => "npwp",
                "type" => "text",
                "attr" => [],
                "value" => null
            ),
            "Email" => array(
                "table" => true,
                "form" => true,
                "field" => "email",
                "type" => "email",
                "attr" => [],
                "value" => null
            ),
            'Departemen' => array(
                "table" => true,
                "form" => true,
                "field" => "departemen",
                'type' => 'text',
                'attr' => [],
                'value' => null
            ),

    		"TTL" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "ttl",
    			"type" => "text",
    			"attr" => [],
    			"value" => null
    		),
    		"Gender" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "jk",
    			"type" => "select",
    			"list" => array(
    				"" => "",
    				1 => "Pria",
    				2 => "Wanita"
    			),
    			"attr" => [],
    			"value" => null
    		),
    		"Alamat" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "alamat",
    			"type" => "text",
    			"attr" => [],
    			"value" => null
    		),
    		"Telepon" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "telp",
    			"type" => "tel",
    			"attr" => [],
    			"value" => null
    		),
    		"Tanggal Masuk" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "tgl_masuk",
    			"type" => "date",
    			"attr" => [],
    			"value" => null
    		),
            "No Rekening" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "no_rek",
    			"type" => "tel",
    			"attr" => [],
    			"value" => null
            ),
            "Status Transfer" => array(
                "table" => true,
                "form" => true,
                "field" => 'status_transfer',
                "type" => 'select',
                "list" => (is_panic()) ? (['Cash','Transfer']) : config('app.status_transfer'),
                "attr" => [],
                "value" => null
            ),
            "Divisi" => array(
                "table" => false,
                "form" => true,
                "field" => "divisi",
                "type" => "select",
                "attr" => [],
                "list" => ModelDivisi::where('stat',1)->get()->pluck('nama_divisi', 'id'),
                "value" => null
            ),
            "Jabatan" => array(
                "table" => false,
                "form" => true,
                "field" => "jabatan",
                "type" => "select",
                "attr" => [],
                "list" => ModelJabatan::where('stat',1)->get()->pluck('nama_jabatan', 'id'),
                "value" => null
            ),
            "Golongan Karyawan" => array(
                "table" => false,
                "form" => true,
                "field" => "golongan",
                "type" => "select",
                "attr" => [],
                "list" => ModelGolongan::where('stat',1)->get()->pluck('kode_golongan', 'id'),
                "value" => null
            ),
            "BPJS Kesehatan" => array(
                "table" => true,
                "form" => true,
                "field" => 'is_bpjs_kesehatan',
                "type" => 'select',
                "list" => ['Tidak', 'YA'],
                "attr" => [],
                "value" => null
            ),
            "Tgl Daftar BPJS Kesehatan" => array(
                "table" => true,
                "form" => true,
                "field" => 'tgl_daftar_bpjs_kesehatan',
                "type" => 'date',
                "attr" => [],
                "value" => null
            ),
            "BPJS TK" => array(
                "table" => true,
                "form" => true,
                "field" => 'is_bpjs_tk',
                "type" => 'select',
                "list" => ['Tidak', 'YA'],
                "attr" => [],
                "value" => null
            ),
            "Tgl Daftar BPJS TK" => array(
                "table" => true,
                "form" => true,
                "field" => 'tgl_daftar_bpjs_tk',
                "type" => 'date',
                "attr" => [],
                "value" => null
            ),            
            "Status PTKP" => array(
                "table" => true,
                "form" => true,
                "field" => 'status_ptkp',
                "type" => 'select',
                'list' => config('app.status_ptkp'),
                "attr" => [],
                "value" => null
            ),
            
    		"" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "btn",
    			"type" => "action",
    			"attr" => [],
    			"value" => null
    		)

    	];
    }


    static function content(){
    	#from tb
    	$tb = DB::table("cwa_karyawan")
    		->where("stat", '<>', 9)
    		->orderBy("id", "desc")
    		->get();

    	$result = MdKaryawan::manage_table_content($tb);
    	return $result;
    }

    static function manage_table_content($fromtb){
    	#Mengubah dan menyesuaikan format data yang ditampilkan ke tabel

    	$result = array_map("toArray", $fromtb);

    	$no = 1;
    	foreach($result as $ind=>$data){
    		$result[$ind]['tgl_masuk'] = indo_date($data['tgl_masuk'], "half");
            $result[$ind]['nama'] = '<a href="detail/'.$data['id'].'" data-featherlight="ajax">'.$data['nama'].'</a>';
    		$result[$ind]['no'] = $no;
    		$result[$ind]['jk'] = $result[$ind]['jk'] == 1 ? "Pria" : "Wanita";
    		$result[$ind]['btn'] = '<a href="master/karyawan/edit/'.$data['id'].'" class="btn btn-info btn-sm">Edit</a> <a href="master/karyawan/destroy/'.$data['id'].'" class="btn btn-danger btn-sm delete-button">Delete</a>';
    		$no++;
    	}
    	return $result;
    }

    static function editable($id){
        $tb = DB::table("cwa_karyawan")
            ->where("id", $id)
            ->first();
        

        //jika ada reformat data edit, bisa diatur ulang sebelum direturn

        return (array)$tb;
    }

    static function validate($post, $except=null){
        if(strlen(trim($post['nama'])) == 0)
            return "Mohon mengisi nama karyawan dengan tepat";

        if($except > 0){
            $sql = DB::table("cwa_karyawan")
            ->where("nama", $post['nama'])
            ->where("stat", "<>", 9)
            ->where("id", "<>", $except)
            ->get();
        }
        else{
            $sql = DB::table("cwa_karyawan")
            ->where("nama", $post['nama'])
            ->where("stat", "<>", 9)
            ->get();
        }

        if(count($sql) > 0){
            return "Data karyawan dengan nama tersebut sudah ada";
        }
        return false;
    }

    static function getKaryawan($id){
        $sql = DB::table("cwa_karyawan")
        ->where("id", $id)
        ->first();
        return $sql;
    }

    static function list_karyawan(){
        $sql = DB::table("cwa_karyawan")
        ->where("stat","<>",9)
        ->get();

        return transform_array($sql);
    }
}
