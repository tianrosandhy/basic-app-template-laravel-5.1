<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Model\Table\ModelGolongan;

class MdMtKaryawan extends Model
{
    //
	public function __construct(){

	}

	public static function structure(){
		return [
            "Divisi" => array(
                "table" => true,
                "form" => true,
                "field" => "id_divisi",
                "type" => "select",
                "list" => MdMtKaryawan::filter("divisi"),
                "attr" => ["required" => true],
                "value" => null
            ),
    		"Nama Karyawan" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "nama",
    			"type" => "select",
    			"list" => MdMtKaryawan::karyawan_belum_dimutasi(),
    			"attr" => ["required" => true],
    			"value" => null
    		),
    		"Jabatan" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "id_jabatan",
    			"type" => "select",
    			"list" => MdMtKaryawan::filter("jabatan"),
    			"attr" => ["required" => true],
    			"value" => null
    		),
            "Kode Golongan" => array(
                "table" => true,
                "form" => true,
                "field" => "golongan",
                "type" => "select",
                "list" => ModelGolongan::where('stat',1)->get()->pluck("kode_golongan", "id")->toArray(),
                "attr" => [],
                "value" => null
            ),
    		"Tanggal Berlaku" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "tgl_berlaku",
    			"type" => "date",
    			"attr" => [],
    			"value" => null
    		),

    		"" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "btn",
    			"type" => "action",
    			"attr" => [],
    			"value" => null
    		),
    		" " => array(
    			"table" => true,
    			"form" => false,
    			"field" => "resign",
    		)
            
		];
	}

	//mungkin method ini akan banyak digunakan di class2 lain
	static function queryGetMutasi($get, $icResign=true){
		if(empty($get['tgl_berlaku']))
			$tgl = date("Y-m-d");
		else
			$tgl = $get['tgl_berlaku'];
        
		$addQuery = [];
		if(isset($get['divisi'])){
            if($get['divisi'] > 0){
                $addQuery[] = " a.id_divisi = ".intval($get['divisi']);
            }
        }

        if(isset($get['nama_divisi'])){
            $addQuery[] = " d.nama_divisi = '".$get['nama_divisi']."'";
        }

		if(isset($get['jabatan'])){
			if($get['jabatan'] > 0){
				$addQuery[] = " a.id_jabatan = ".intval($get['jabatan']);
			}
		}
        
        if(isset($get['golongan'])){
			if($get['golongan'] > 0){
				$addQuery[] = " a.id_golongan = ".intval($get['golongan']);
			}
		}
        
		if(isset($get['id'])){
            if($get['id'] > 0){
                $addQuery[] = "a.id = ".intval($get['id']);
            }
        }
        if(isset($get['id_karyawan'])){
            if($get['id_karyawan'] > 0){
                $addQuery[] = "a.id_karyawan = ".intval($get['id_karyawan']);
            }
        }
        if(isset($get['nama'])){
            $addQuery[] = "c.nama = '".($get['nama'])."'";
        }

		$addQuery[] = "(f.stat <> 9 OR f.stat IS NULL)";
		$adq = implode(" AND ", $addQuery);
        $adq .= ' AND c.stat <> 9 ';

		if(strlen($adq) > 0){
			//jika ada parameter, 
			$adq = "WHERE ".$adq;
		}

        if(!$icResign){
            $tgl_resign = date("Y-m-d", strtotime($tgl) + (30 * 86400));
        }
        else{
            $tgl_resign = $tgl;
        }


		$query = DB::select("
		SELECT * FROM (
			SELECT a.id, a.id_karyawan, c.nama, c.email, c.npwp, c.nik, c.tgl_masuk, c.jk, c.no_rek, c.status_transfer, c.departemen, c.is_bpjs_kesehatan, c.is_bpjs_tk, c.tgl_daftar_bpjs_kesehatan, c.tgl_daftar_bpjs_tk, c.status_ptkp, a.id_divisi, d.nama_divisi, d.sort_no, a.id_jabatan, e.nama_jabatan, a.id_golongan, g.kode_golongan, a.tgl_berlaku, f.tgl_resign, h.type, h.tgl_start, h.tgl_end, h.description, a.stat FROM cwa_mutasi_karyawan a 
			INNER JOIN ( 
				SELECT id_karyawan, MAX(tgl_berlaku) AS tgl_berlaku, stat FROM cwa_mutasi_karyawan WHERE tgl_berlaku <= ? AND stat = 1 GROUP BY id_karyawan
			) b ON a.id_karyawan = b.id_karyawan AND a.tgl_berlaku = b.tgl_berlaku AND a.stat = b.stat
			LEFT JOIN cwa_karyawan c
				ON a.id_karyawan = c.id
			LEFT JOIN cwa_divisi d
				ON a.id_divisi = d.id
			LEFT JOIN cwa_jabatan e
				ON a.id_jabatan = e.id
			LEFT JOIN cwa_golongan g
				ON a.id_golongan = g.id
			LEFT JOIN cwa_resign f
				ON a.id_karyawan = f.id_karyawan
            LEFT JOIN (
                SELECT * FROM cwa_break WHERE tgl_end >= '$tgl' AND stat <> 9
            ) h ON a.id_karyawan = h.id_karyawan
			$adq
			ORDER BY d.sort_no ASC, a.id_jabatan, c.nama
		) x
		WHERE x.tgl_resign IS NULL OR x.tgl_resign > ?
        ORDER BY sort_no ASC, id_jabatan, nama
		", [$tgl, $tgl_resign]);	

		return $query;
	}

	static function content($get){
		$query = MdMtKaryawan::queryGetMutasi($get);
		$result = MdMtKaryawan::manage_table_content($query);
		return $result;
	} 

	static function manage_table_content($fromtb){
    	#Mengubah dan menyesuaikan format data yang ditampilkan ke tabel

    	$result = array_map("toArray", $fromtb);

    	$no = 1;
    	foreach($result as $ind=>$data){
    		$btbt = "";
    		$tbl_btbt = "";
            
            
    		$cekres = MdMtKaryawan::cek_resign($data['id_karyawan']);
    		if(!$cekres){
    			$tbl_btbt = ' <a href="mutasi/karyawan/resign/'.$data['id_karyawan'].'" class="btn btn-danger resign-button" data-featherlight="ajax" >Resign</a>';
    		}
    		else{
    			$btbt = '<div>Resign per tanggal <span class="label label-danger"><b>'.indo_date($cekres->tgl_resign).'</b></span></div>';
    			$tbl_btbt = ' <a href="mutasi/karyawan/resign/'.$data['id_karyawan'].'" class="btn btn-danger resign-button" data-featherlight="ajax" >Update Resign</a>';
    		}
            
            if($data['type'] == 'break' || $data['type'] == 'cuti hamil'){
                $btbt .= '<div>'.ucwords($data['type']).' per tanggal <br><span class="label label-warning">'.indo_date($data['tgl_start']).'</span> s/d <span class="label label-warning">'. indo_date($data['tgl_end']) .'</span></div>';
            }
            

            $result[$ind]['nama'] = '<a href="detail/'.$data['id_karyawan'].'" data-featherlight="ajax">'.$data['nama'].'</a>';
    		$result[$ind]['id_divisi'] = $data['nama_divisi'];
    		$result[$ind]['id_jabatan'] = $data['nama_jabatan'];
            $result[$ind]['golongan'] = $data['kode_golongan'];

    		$result[$ind]['tgl_berlaku'] = indo_date($data['tgl_berlaku']);

    		$result[$ind]['no'] = $no;
    		$result[$ind]['btn'] = '
            <a href="mutasi/karyawan/manage/'.$data['id_karyawan'].'" data-featherlight="ajax" class="btn btn-primary">Manage Mutasi</a>
            <a href="mutasi/karyawan/break/'.$data['id_karyawan'].'" data-featherlight="ajax" class="btn btn-warning">Break</a>
            '.$tbl_btbt;
    		$result[$ind]['resign'] = $btbt;
    		$no++;
    	}
    	return $result;
    }


    static function filter($type="divisi"){
    	$tb = DB::table("cwa_".$type)
    	->where("stat", "<>", 9);

        if ($type == 'divisi') {
            $tb = $tb->orderBy('sort_no');
        }

        $tb = $tb->get();

    	$data[null] = "- Semua -";
    	foreach($tb as $row){
    		$row = (array)$row;
    		$data[$row['id']] = $row['nama_'.$type];
    	}
    	return $data;
    }















    public static function list_structure(){
		return [
    		"#" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "no",
    			"type" => "number",
    			"attr" => [],
    			"value" => null    			
    		),
    		"Tanggal Berlaku" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "tgl_berlaku",
    			"type" => "date",
    			"attr" => ["class"=>"col-sm-4", "required" => "required"],
    			"value" => date("Y-m-d")
    		),

    		"Divisi" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "id_divisi",
    			"type" => "select",
    			"list" => MdMtKaryawan::filter("divisi"),
    			"attr" => ["required" => "required"],
    			"value" => null
    		),
    		"Jabatan" => array(
    			"table" => true,
    			"form" => true,
    			"field" => "id_jabatan",
    			"type" => "select",
    			"list" => MdMtKaryawan::filter("jabatan"),
    			"attr" => ["required" => "required"],
    			"value" => null
    		),
            "Kode Golongan" => array(
                "table" => true,
                "form" => true,
                "field" => "golongan",
                "type" => "select",
                "list" => ModelGolongan::where('stat',1)->get()->pluck("kode_golongan", "id")->toArray(),
                "attr" => [],
                "value" => null
            ),
    		"" => array(
    			"table" => true,
    			"form" => false,
    			"field" => "btn",
    			"type" => "action",
    			"attr" => [],
    			"value" => null
    		)
		];
	}

    static function list_content($id){
    	$sql = DB::select("
    	SELECT a.id, a.id_karyawan, c.nama, c.tgl_masuk, a.id_divisi, d.nama_divisi, a.id_jabatan, e.nama_jabatan, a.tgl_berlaku, a.id_golongan, f.kode_golongan, a.stat FROM cwa_mutasi_karyawan a 
		LEFT JOIN cwa_karyawan c
			ON a.id_karyawan = c.id
		LEFT JOIN cwa_divisi d
			ON a.id_divisi = d.id
		LEFT JOIN cwa_jabatan e
			ON a.id_jabatan = e.id
        LEFT JOIN cwa_golongan f
            ON a.id_golongan = f.id
		WHERE a.id_karyawan = ? AND a.stat <> 9
		ORDER BY a.tgl_berlaku
    	", [$id]);

    	$result = array();
    	$i = 1;
    	foreach($sql as $row){
    		$row = (array)$row;
    		$result[$i] = $row;
    		$result[$i]['no'] = $i;
    		$result[$i]["id_karyawan"] = $row['nama'];
    		$result[$i]["id_divisi"] = $row['nama_divisi'];
    		$result[$i]["id_jabatan"] = $row['nama_jabatan'];
            $result[$i]['golongan'] = $row['kode_golongan'];
    		$result[$i]['tgl_berlaku'] = indo_date($row['tgl_berlaku']);
    		$result[$i]['btn'] = "<a href='mutasi/karyawan/editmutasi/$row[id]' class='btn btn-info btn-sm' data-featherlight='ajax'>Update</a> <a href='mutasi/karyawan/dropmutasi/$row[id]' class='btn btn-danger btn-sm delete-button'>Hapus Mutasi</a>";
    		$i++;
    	}

    	return $result;
    }



    static function karyawan_belum_dimutasi(){
    	$sql = DB::table("cwa_karyawan")
    	->whereRaw("id NOT IN (SELECT DISTINCT id_karyawan FROM cwa_mutasi_karyawan WHERE stat <> 9)")
    	->where("stat","<>",9)
    	->get();

    	if(count($sql) == 0){
    		return [];
    	}
    	else{
    		$result = [null => ""];
    		foreach($sql as $data){
    			$row = (array)$data;
    			$result[$row['id']] = $row['nama'];
    		}
    		return $result;
    	}
    }




    static function validate($post, $except=null){
        if(strlen(trim($post['nama'])) == 0)
            return "Mohon mengisi nama karyawan dengan tepat";
        if(!array_key_exists($post['nama'], MdMtKaryawan::karyawan_belum_dimutasi())){
        	return "Karyawan tersebut tidak termasuk dalam daftar yang dapat dimutasi";
        }

        return false;
    }



    static function cek_resign($id){
    	$qry = DB::table("cwa_resign")
    	->where("id_karyawan", $id)
    	->where("stat", "<>", 9)
    	->first();

    	if(isset($qry->tgl_resign)){
    		//karyawan tsb resign
    		return $qry;
    	}
		return false;
    }



    static function fetch_uniq($uniq,$complete=true){
        //format uniq : tanggal-id_karyawan-id_divisi
        $exp = explode("-",$uniq);
        $data['tanggal'] = date("Y-m-d",$exp[0]);
        $data['id_karyawan'] = $exp[1];
        $data['id_divisi'] = $exp[2];

        $fetch_nama = tb_list("cwa_karyawan", $exp[1]);
        $fetch_divisi = tb_list("cwa_divisi", $exp[2]);

        foreach($fetch_nama as $fname){
            $data['nama_karyawan'] = $fname['nama'];
        }
        foreach($fetch_divisi as $fdiv){
            $data['nama_divisi'] = $fdiv['nama_divisi'];
        }

        return $data;
    }
}
