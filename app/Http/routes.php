<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'Home@index');

Route::get('login', 'Home@index');
Route::post('login', 'Login@index');

Route::get('logout', 'Home@logout');

#Route::get('master/karyawan', 'Master\Karyawan@index');

//automatic set route go to controller
Route::get('master', 'Home@index');
Route::get('setting', 'WebSetting@index');
Route::post('setting/store', 'WebSetting@store');
Route::get('setting/user', 'WebSetting@user');
Route::get('setting/update/{id?}', 'WebSetting@update');
Route::post('setting/new', 'WebSetting@new');
Route::get('setting/destroy/{id?}', 'WebSetting@destroy');
Route::post('setting/change/{id?}', 'WebSetting@change');
Route::post('setting/backuprestore', 'WebSetting@backuprestore');
Route::post('setting/restore', 'WebSetting@restore');


Route::post('switch', 'Home@switch');
Route::get('switch', 'Home@switch');


Route::controller('master/karyawan', 'Master\Karyawan');
Route::controller('master/kode_karyawan', 'Master\KodeKaryawan');
Route::controller('master/divisi', 'Master\Divisi');
Route::controller('master/jabatan', 'Master\Jabatan');
Route::controller('master/shift', 'Master\Shift');
Route::controller('master/golongan', 'Master\Golongan');
Route::controller('master/hari_raya', 'Master\HariRaya');
Route::controller('master/kode_absensi', 'Master\KodeAbsensi');
Route::controller('master/gaji', 'Master\Gaji');

Route::controller('mutasi/karyawan', 'Mutasi\Karyawan');
Route::controller('mutasi/jadwal', 'Mutasi\Jadwal');
Route::controller('mutasi/presensi', 'Mutasi\Presensi');
Route::controller('mutasi/rekap-terlambat', 'Mutasi\RekapTerlambat');
Route::controller('mutasi/rekap-lupa-absen', 'Mutasi\RekapLupaAbsen');
Route::controller('mutasi/tidakhadir', 'Mutasi\Tidakhadir');
Route::controller('mutasi/upload', 'Mutasi\Upload');
Route::controller('mutasi/upload2', 'Mutasi\Upload2');
Route::controller('mutasi/terlambat', 'Mutasi\Terlambat');
Route::controller('mutasi/lupa-absen', 'Mutasi\LupaAbsen');
Route::controller('mutasi/lembur', 'Mutasi\Lembur');
Route::controller('mutasi/rekap', 'Mutasi\RekapPresensi');



Route::controller('gaji/mutasi', 'Gaji\MutasiGaji');
Route::controller('gaji/bulanan', 'Gaji\GajiBulanan');
Route::controller('gaji/tahunan', 'Gaji\GajiTahunan');
Route::controller('gaji/rekap', 'Gaji\Rekap');
Route::controller('gaji/thr', 'Gaji\GajiTHR');
Route::get('gaji/resign', 'Gaji\GajiResign@index');
Route::post('gaji/resign/store', 'Gaji\GajiResign@store');
Route::get('gaji/bpjs', 'Gaji\GajiBPJS@index');
Route::post('gaji/bpjs', 'Gaji\GajiBPJS@store');
Route::get('gaji/bpjs/export', 'Gaji\GajiBPJS@export');
//Route::controller('gaji/slip', 'Gaji\SlipGaji');


//Route::get('migrate', 'ExcelMigrate@index');
Route::get('detail/{id?}', 'Master\Karyawan@detail');




Route::get('gaji/slip/{id}/{tahun?}/{bulan?}', [
	'uses' => 'Gaji\\SlipGaji@detail'
]);

Route::get('gaji/slip/{id}', [
	'uses' => 'Gaji\\SlipGaji@detail'
]);

Route::get('gaji/slip-online/{id}/{tahun}/{bulan}', [
	'uses' => 'Gaji\\SlipGaji@detailOnline'
]);
Route::get('gaji/blast-slip-online/{tahun}/{bulan}', [
	'uses' => 'Gaji\\SlipGaji@blast'
])->name('gaji.blast');
Route::get('gaji/blast-single-slip-online/{id}/{tahun}/{bulan}', [
	'uses' => 'Gaji\\SlipGaji@blastSingle'
])->name('gaji.blast-single');


Route::get('gaji/slip', [
	'uses' => 'Gaji\\SlipGaji@index'
]);
Route::get('gaji/excel-slip', 'Gaji\SlipGaji@excelReport');




Route::get('api/jadwal', [
	'uses' => 'Api\\ApiJadwal@store'
	]);
//Route::controller('api/jadwal', 'Api\ApiJadwal');

Route::controller('api/presensi', 'Api\ApiPresensi');
Route::post('api/presensi', [
	'uses' => 'Api\\ApiPresensi@store'
	]);

Route::controller('api/terlambat', 'Api\ApiTerlambat');
Route::post('api/terlambat', [
	'uses' => 'Api\\ApiTerlambat@store'
	]);

Route::controller('api/lembur', 'Api\ApiLembur');
Route::post('api/lembur', [
	'uses' => 'Api\\ApiLembur@store'
	]);

Route::controller('api/tidakhadir', 'Api\ApiTidakhadir');
Route::post('api/tidakhadir', [
	'uses' => 'Api\\ApiTidakhadir@store'
	]);

Route::get('api/kode', [
	'uses' => 'Api\\ApiKode@index'
	]);

Route::get('api/excel', 'Api\\ExcelMaker@index');

Route::get('karyawan/table', 'Master\Karyawan@getPost');




Route::controller('report/finance', 'Report\Finance');
Route::controller('report/amy', 'Report\Amy');
Route::controller('report/payroll', 'Report\Payroll');

//panic route
Route::get('panic-on', function(){
	panic_mode(true);
});

Route::get('panic-off', function(){
	panic_mode(false);
});

Route::get('panic-check', function(){
	if(is_panic()){
		echo 'Panic is enabled';
	}
	else{
		echo 'Panic is disabled';
	}
});

// June 2021 : Tambahan fitur upload gaji bulanan
Route::get('upload-gaji/bulanan', 'Gaji\GajiBulananUpload@index');
Route::post('upload-gaji/bulanan', 'Gaji\GajiBulananUpload@check');
Route::post('upload-gaji/bulanan/process', 'Gaji\GajiBulananUpload@store');
Route::get('upload-gaji/bulanan/example', 'Gaji\GajiBulananUpload@example');

// June 2021 : Tambahan fitur upload data master karyawan
Route::get('upload-karyawan', 'Master\MasterKaryawanUpload@index');
Route::post('upload-karyawan', 'Master\MasterKaryawanUpload@check');
Route::post('upload-karyawan/process', 'Master\MasterKaryawanUpload@store');
Route::get('upload-karyawan/example', 'Master\MasterKaryawanUpload@example');

// April 2023 : Tambahan fitur update data master karyawan
Route::get('update-karyawan', 'Master\MasterKaryawanUpdate@index');
Route::post('update-karyawan', 'Master\MasterKaryawanUpdate@check');
Route::post('update-karyawan/process', 'Master\MasterKaryawanUpdate@store');
Route::get('update-karyawan/example', 'Master\MasterKaryawanUpdate@example');

Route::get('test-mail', function(){
    $mail = \Mail::raw('Test email config', function($msg){
        $msg->to('tianrosandhy@gmail.com');
        $msg->subject('Test IPV4 SMTP');
    });

    dd($mail);
});