<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use Redirect;
use DB;
use Auth;


class Login extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $username = Input::get('username');
        $password = Input::get('password');

        if(Auth::attempt(array('username'=> $username, 'password' => $password))){
            log_user('LOGIN');
            return redirect('/');
        }
        else{
            if(empty($username) or empty($password)){
                $msg = "Blank username or password. Please fill all the required field";
            }
            else{
                $msg = "Wrong login detail, please try again.";
            }
            return redirect('/')->with('error', $msg);
        }
    }


}
