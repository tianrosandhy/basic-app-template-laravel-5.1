<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Excel;
use DB;

class ExcelMigrate extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $file = "tmp/datagaji.xlsx";
        $excel = Excel::load($file)->get();

        $excel = json_decode(json_encode($excel), true);
        $data = $excel[0];

        $sql = DB::table('cwa_karyawan')->get();
        foreach($sql as $row){
            $all[$row->nama] = $row->id;
            $tgll[$row->nama] = $row->tgl_masuk;
        }


        //data gaji
        $sqgaji = DB::table('cwa_master_gaji')->get();
        foreach($sqgaji as $gj){
            $rowgaji[$gj->label] = $gj->id;
        }


        $datarow = [
            $rowgaji['Gaji Pokok'] => 4,
            $rowgaji['Tunjangan Jabatan'] => 5,
            $rowgaji['Tunjangan Golongan'] => 6,
            $rowgaji['Tunjangan Lain'] => 7,
            $rowgaji['Bonus PU'] => 10,
            $rowgaji['Potongan Kasbon'] => 12,
            $rowgaji['Potongan BPJS Tk Kerja'] => 13,
            $rowgaji['Potongan BPJS Kesehatan'] => 14,
            $rowgaji['Iuran Suka Duka'] => 15
        ];



        $out = [];
        $n = 1;

        $hehe = DB::table('cwa_master_gaji')->where('occurence', 1)->get();

        foreach($data as $baris){
            if(isset($all[$baris[1]])){

                if(!isset($all[$baris[1]]) or !isset($tgll[$baris[1]])){
                    continue;
                }
                $nama = $all[$baris[1]];
                $tgl = $tgll[$baris[1]];

                foreach($hehe as $he){
                    if(isset($datarow[$he->id])){
                        $hx = $datarow[$he->id];
                        if(isset($baris[$hx])){
                            if(intval($baris[$datarow[$he->id]]) > 0){
                                $out[] = [
                                    'id_karyawan' => $nama,
                                    'id_gaji' => $he->id,
                                    'tanggal' => $tgl,
                                    'nilai' => $baris[$datarow[$he->id]]
                                ];
                            }

                        }
                    }
                }




            }
        }
        dump($out);
        DB::table('cwa_detail_gaji')->insert($out);
        


        

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
