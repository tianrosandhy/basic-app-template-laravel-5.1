<?php

namespace App\Http\Controllers\Mutasi;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\MdMtKaryawan;
use App\Model\MdTerlambat;
use Auth;
use Redirect;
use Input;
use DB;
use URL;


class Terlambat extends Controller
{
    public function __construct(){
        $this->middleware('log');
    }

    public function getIndex(Request $request){
        log_user();
        $bulan = Input::get("bulan");
        $tahun = Input::get("tahun");
        $divisi = Input::get("divisi");

        if(empty($bulan))
            $bulan = date("n");
        if(empty($tahun))
            $tahun = date("Y");

        $telat = new MdTerlambat([
            'bulan' => $bulan,
            'tahun' => $tahun
        ]);


        $allowed = array();
        $list_date = $telat->calendar;
        foreach($list_date as $tg){
            $mutt = MdMtKaryawan::queryGetMutasi([
                'tgl_berlaku' =>  $tg,
                'divisi' => $divisi
            ]);

            foreach($mutt as $arr){
                $uniq = strtotime($tg)."-".$arr->id_karyawan."-".$arr->id_divisi;
                $allowed[] = $uniq;
            }
        }
        //data telat valid apabila termasuk dalam list mutasi yang terdaftar

        $data_telat = $telat->get_presence($allowed);
        $content = $telat->content($data_telat);
        $pake = [];
        foreach($content as $index => $params){
            if($params['real_masuk'] <> $params['real_pulang'] && !empty($params['real_masuk']) && !empty($params['real_pulang'])){
                $pake[$index] = $params;
            }
        }
        $content = $pake;

        return view('master')->with([
            'title' => "Data Keterlambatan Karyawan",
            'curmenu' => 2,
            'cursubmenu' => 25,
            'table' => MdTerlambat::structure(),
            'content' => $content,
            'getform' => [
                'title' => 'Filter Data Keterlambatan',
                'url' => $request->path(),
                'form' => [
                    [
                    'field' => 'bulan',
                    'type' => 'select',
                    'label' => 'Bulan',
                    'list' => nama_bulan(),
                    'attr' => ["class" => "form-control col-sm-6"],
                    'value' => strlen(Input::get("bulan")) > 0 ? Input::get("bulan") : date("n")
                    ],
                    [
                    'field' => 'tahun',
                    'label' => 'Tahun',
                    'type' => 'number',
                    'attr' => ['class' => 'form-control col-sm-4'],
                    'value' => strlen(Input::get('tahun')) > 0 ? Input::get('tahun') : date("Y")
                    ],
                    [
                    'field' => 'divisi',
                    'label' => 'Divisi',
                    'type' => 'select',
                    'list' => MdMtKaryawan::filter('divisi'),
                    'attr' => [],
                    'value' => strlen(Input::get('divisi')) > 0 ? Input::get('divisi') : ''
                    ],
                    
                ]
            ],
            'script' => true
        ]);

    }
}
