<?php

namespace App\Http\Controllers\Mutasi;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use App\Model\MdKaryawan;
use App\Model\MdMtKaryawan;
use App\Model\MdPresensi;
use App\Model\MdReport;
use Input;
use DB;
use URL;


class RekapPresensi extends Controller
{
    public function __construct(){
        $this->middleware('log');
    }

    public function getIndex(){
        log_user();
        $bulan = Input::get("bulan") ? intval(Input::get("bulan")) : date("n");
        $tahun = Input::get("tahun") ? intval(Input::get("tahun")) : date("Y");
        $divisi = Input::get("divisi");


        $report = new MdReport([
            'bulan' => $bulan,
            'tahun' => $tahun,
        ]);

        //month field
        $row = $report->make_mutasi($divisi);
        $report->get_rancangan();
        $report->get_presensi();


        //NANTI DILANJUTIN -_-

        return view("report")->with([
            'title' => 'Rekap Presensi Karyawan',
            'wellname' => 'Filter Presensi',
            'curmenu' => 4,
            'cursubmenu' => 27,
            'default' => [
                'bulan' => $bulan,
                'tahun' => $tahun,
                'divisi' => $divisi
            ],
            'listdiv' => MdMtKaryawan::filter("divisi"),
            'listkode' => MdReport::list_kode_absen(),
            'calendar' => $report->calendar,
            'content' => $report->prepare_object(),
            'row' => $row,
        ]);        
    }

}
