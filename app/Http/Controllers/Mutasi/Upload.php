<?php

namespace App\Http\Controllers\Mutasi;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use Input;
use DB;
use URL;
use Excel;
use App\Model\MdUpload;
use App\Model\MdPresensi;
use App\Model\MdMtKaryawan;
use App\Model\Table\ModelDivisi;

class Upload extends Controller
{
    public function __construct(){
        $this->middleware('log');
    }

    public function getIndex(Request $req)
    {
        log_user();
        if($req->input("bulan"))
            $bulan = intval($req->input("bulan"));
        else
            $bulan = date("n");

        if($req->input("tahun"))
            $tahun = intval($req->input("tahun"));
        else
            $tahun = date("Y");

        return view("blank")->with([
            'title' => "Upload File Presensi",
            'default' => [
                'bulan' => $bulan,
                'tahun' => $tahun   
            ],
            'list_divisi' => ModelDivisi::orderBy('sort_no')->where('stat', 1)->get(),
            'curmenu' => 2,
            'cursubmenu' => 24
        ]);
    }

    public function postIndex(Request $request){
        $upl = new MdUpload();
        log_user('UPLOAD');
        $presensi = new MdPresensi([
            'bulan' => $request->input("bulan"),
            'tahun' => $request->input("tahun"),
        ]);

        $mutasi = MdMtKaryawan::queryGetMutasi([
            'divisi' => $request->divisi
        ]);


        //pengaturan tipe absen
        $dvv = ModelDivisi::find($request->divisi);
        //1 : Baru - 2 : Lama

        $presensi->tgl_awal = get_setting("tgl_awal_gaji");
        $presensi->create_calendar($request->input("bulan"), $request->input("tahun"), false, $request->tglFrom);
        $list_date = $presensi->calendar;
        $list_rancangan = $upl->rancangan($list_date);


        $upl->process($request->file('presence'));

        $current_year = $request->input("tahun");
        
        $excel = Excel::load($upl->location)->get();


        if(intval($dvv->deskripsi) == 2){
            //utk absen lama
            $cleaned = $upl->clean($excel);
            $absen = $upl->analyze($cleaned);

            //ubah garing ke strip : bug 2/1/2018
            $shittemp = [];
            foreach($absen as $ida => $dta){
                foreach($dta as $tg_acak => $narr){
                    $newtgl = str_replace('/', '-', $tg_acak);
                    $shittemp[$ida][$newtgl] = $narr;
                }
            }
            $absen = $shittemp;
        }
        else if(intval($dvv->deskripsi) == 1){
            //utk absen baru
            $cleaned = $upl->force_clean($excel);
            $absen = $upl->force_analyze($cleaned, $list_date);
        }
        else if(intval($dvv->deskripsi == 3)){
            //buat yang absen sangat baru
            $cleaned = $upl->force_clean($excel);
            $absen = $upl->analyze_baru($cleaned);
        }


        $list_karyawan = $upl->migrate_code($mutasi);

        $process_this = [];
        $exclude = [];

        //tanggal yg kelewatan harus diisi
        $kosong = $upl->formatUlang($absen, $list_date);
        //sekarang ada absen dgn tanggal null. Boleh dihapus.
        //jangan lupa hapus semua dependency yg mengarah kesana juga
        $deleteDependency = $upl->removeDependency($kosong, $list_karyawan, $list_rancangan);

        foreach($list_rancangan as $idr => $dtr){
            $list[$idr] = true;
        }

        foreach($list_karyawan as $id_absen => $id_karyawan){
            //kalau inputan ada di 2 tahun yg berbeda gimana ya?

            foreach($absen[$id_absen] as $tgl => $jam){
                $date = date("Y-m-d",strtotime($tgl));
                if(isset($list_rancangan[$id_karyawan])){
                    if(array_key_exists($date, $list_rancangan[$id_karyawan])){
                        //ada
                        $lk = $list_rancangan[$id_karyawan];
                        $process_this[$id_karyawan][] = [
                            'id_rancangan_jadwal' => $lk[$date]['id'],
                            'must_masuk' => date("H:i",strtotime($lk[$date]['must_masuk'])),
                            'must_pulang' => date("H:i",strtotime($lk[$date]['must_pulang'])),
                            'real_masuk' => $jam[0],
                            'real_pulang' => $jam[1],
                            'tanggal' => $date,
                            'stat' => 1
                        ];

                        $exclude[] = $lk[$date]['id'];
                    }
                }
            }
        }

        $list_ada = DB::table("cwa_presensi")
        ->whereIn("id_rancangan_jadwal", $exclude)
        ->get();
        $upd = [];
        foreach($list_ada as $rada){
            $upd[] = $rada->id_rancangan_jadwal;
        }
        $ins = array_diff($exclude, $upd);


        if(count($process_this) > 0){
            foreach($process_this as $id_karyawan => $dt){
                foreach($dt as $id_array => $detail){
                    if(in_array($detail['id_rancangan_jadwal'], $ins)){
                        //insert
                        DB::table("cwa_presensi")
                        ->insert($detail);
                    }
                    else if(in_array($detail['id_rancangan_jadwal'], $upd)){
                        //update
                        DB::table("cwa_presensi")
                        ->where("id_rancangan_jadwal", $detail['id_rancangan_jadwal'])
                        ->update($detail);

                    }
                }
            }
        }


        //proses terakhir
        #hapus semua data presensi yang tanggal kosong



        return redirect("mutasi/upload")->with([
            'success' => 'Berhasil mengimport data presensi'
        ]);

    }   

}
