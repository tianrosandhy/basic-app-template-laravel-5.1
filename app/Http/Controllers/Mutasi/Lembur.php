<?php

namespace App\Http\Controllers\Mutasi;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\MdMtKaryawan;
use App\Model\MdLembur;
use Auth;
use Redirect;
use Input;
use DB;
use URL;


class Lembur extends Controller
{

    public function __construct(){
        $this->middleware('log');
    }

    public function getIndex(Request $request)
    {
        log_user();
        $bulan = Input::get("bulan");
        $tahun = Input::get("tahun");
        $divisi = Input::get("divisi");

        if(empty($bulan))
            $bulan = date("n");
        if(empty($tahun))
            $tahun = date("Y");

        $lembur = new MdLembur([
            'bulan' => $bulan,
            'tahun' => $tahun
        ]);


        $allowed = array();
        $list_date = $lembur->calendar;
        foreach($list_date as $tg){
            $mutt = MdMtKaryawan::queryGetMutasi([
                'tgl_berlaku' =>  $tg,
                'divisi' => $divisi
            ]);

            foreach($mutt as $arr){
                $uniq = strtotime($tg)."-".$arr->id_karyawan."-".$arr->id_divisi;
                $allowed[] = $uniq;
            }
        }
        //data telat valid apabila termasuk dalam list mutasi yang terdaftar

        $data_lembur = $lembur->get_lembur($allowed);

        return view('master')->with([
            'title' => "Data Lembur Karyawan",
            'curmenu' => 2,
            'cursubmenu' => 26,
            'table' => MdLembur::structure(),
            'content' => $lembur->content($data_lembur),
            'getform' => [
                'title' => 'Filter Data Lembur',
                'url' => $request->path(),
                'form' => [
                    [
                    'field' => 'bulan',
                    'type' => 'select',
                    'label' => 'Bulan',
                    'list' => nama_bulan(),
                    'attr' => ["class" => "form-control col-sm-6"],
                    'value' => strlen(Input::get("bulan")) > 0 ? Input::get("bulan") : date("n")
                    ],
                    [
                    'field' => 'tahun',
                    'label' => 'Tahun',
                    'type' => 'number',
                    'attr' => ['class' => 'form-control col-sm-4'],
                    'value' => strlen(Input::get('tahun')) > 0 ? Input::get('tahun') : date("Y")
                    ],
                    [
                    'field' => 'divisi',
                    'label' => 'Divisi',
                    'type' => 'select',
                    'list' => MdMtKaryawan::filter('divisi'),
                    'attr' => [],
                    'value' => strlen(Input::get('divisi')) > 0 ? Input::get('divisi') : ''
                    ],
                    
                ]
            ],
            'script' => true
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
