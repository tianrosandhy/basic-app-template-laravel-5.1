<?php

namespace App\Http\Controllers\Mutasi;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use App\Model\MdMtKaryawan;
use App\Model\MdKaryawan;
use Input;
use DB;
use URL;
use Session;

use App\Model\Table\ModelGolongan;
use App\Model\Table\ModelBreak;

class Karyawan extends Controller
{
    public function __construct(){
        $this->middleware('log');
    }

    public function getIndex(Request $request)
    {
        log_user();
        $list = array('' => '- Semua -');
        $list += ModelGolongan::where('stat',1)->get()->pluck('kode_golongan','id')->toArray();
        
        return view('master')->with([
            'title' => "Mutasi Karyawan",
            'script' => true,
            'curmenu' => 2,
            'cursubmenu' => 21,
            'table' => MdMtKaryawan::structure(),
            'content' => MdMtKaryawan::content(Input::get()),
            'action_button' => "mutasi/karyawan/create",
            'getform' => [
                'title' => 'Filter Mutasi',
                'url' => $request->path(),
                'form' => [
                    [
                    'field' => 'tgl_berlaku',
                    'type' => 'date',
                    'label' => 'Per Tanggal',
                    'attr' => ["class" => "col-sm-6"],
                    'value' => strlen(Input::get("tgl_berlaku")) > 0 ? Input::get("tgl_berlaku") : date("Y-m-d")
                    ],
                    [
                    'field' => 'divisi',
                    'label' => 'Divisi',
                    'type' => 'select',
                    'list' => MdMtKaryawan::filter('divisi'),
                    'attr' => [],
                    'value' => strlen(Input::get('divisi')) > 0 ? Input::get('divisi') : ''
                    ],
                    [
                    'field' => 'jabatan',
                    'label' => 'Jabatan',
                    'type' => 'select',
                    'list' => MdMtKaryawan::filter('jabatan'),
                    'attr' => [],
                    'value' => strlen(Input::get('jabatan')) > 0 ? Input::get('jabatan') : ''
                    ],
                    [
                    'field' => 'golongan',
                    'label' => 'Kode Golongan',
                    'type' => 'select',
                    'attr' => [],
                    'list' => $list,
                    'value' => strlen(Input::get('golongan')) > 0 ? Input::get('golongan') : ''
                    ]
                ]
            ]
        ]);
    }



    public function getManage($id){
        log_user();
        return view("boxmutasi")->with([
            'title' => "Manage Mutasi Karyawan",
            'type' => 'show',
            'name' => MdKaryawan::getKaryawan($id)->nama,
            'tgl_masuk' => MdKaryawan::getKaryawan($id)->tgl_masuk,
            'table' => MdMtKaryawan::list_structure(),
            'content' => MdMtKaryawan::list_content($id),
            'action_button' => "mutasi/karyawan/update/".$id
        ]);
    }

    public function postDropmutasi($id){
        //proses penghapusan data

        $qry  = DB::table("cwa_mutasi_karyawan")
        ->where("id", $id)
        ->where("stat", "<>", 9)
        ->update(["stat" => 9]);

        log_user('DELETE', $id);

        $arr['message'] = "Berhasil menghapus data mutasi";
        $arr['type'] = "success";
        echo json_encode($arr);
    }

    public function getDropmutasi($id){
        return redirect("mutasi/karyawan")->with("error","Aksi tidak diperkenankan");
    }

    public function getEditmutasi($id){
        log_user();

        $get = DB::table('cwa_mutasi_karyawan')->find($id);
        $get = json_decode(json_encode($get), true);
        foreach($get as $index=>$row){
            $dfvalue[$index] = $row;
        }
        $dfvalue['golongan'] = $get['id_golongan'];
        $id_karyawan = $get['id_karyawan'];

        return view('boxmutasi')->with([
            'title' => 'Update Mutasi Karyawan',
            'type' => 'update',
            'name' => MdKaryawan::getKaryawan($id_karyawan)->nama,
            'tgl_masuk' => MdKaryawan::getKaryawan($id_karyawan)->tgl_masuk,
            'table' => MdMtKaryawan::list_structure(),
            'content' => MdMtKaryawan::list_content($id_karyawan),
            'default' => $get,
            'default_value' => $dfvalue,
            'action_button' => "mutasi/karyawan/boxupdate/".$id
        ]);
    }










    public function getCreate(Request $request)
    {
        log_user();
        $cek = MdMtKaryawan::karyawan_belum_dimutasi();
        if(!$cek){
            return redirect("mutasi/karyawan")->with("error","Tidak ada yang perlu ditambahkan lagi. Seluruh karyawan sudah memiliki divisi dan jabatan.");
        }
        //

        return view('create')->with([
            'title' => 'Insert Data Mutasi Karyawan',
            'curmenu' => 2,
            'cursubmenu' => 21,
            'form' => MdMtKaryawan::structure(),
            'action_button' => "mutasi/karyawan/store"
        ]);
    }

    public function postStore(Request $request)
    {
        //
        $post = $request->all();
        $try = MdMtKaryawan::validate($post);

        if($try){
            $msg['error'] = $try;
            $url = "mutasi/karyawan/create";
            foreach($post as $in=>$ps){
                $request->session()->flash($in, $ps);
            }
        }
        else{
            $dtlkaryawan = MdKaryawan::getKaryawan($post['nama']);
            if(count($dtlkaryawan) > 0){
                $tgl_berlaku = $dtlkaryawan->tgl_masuk;
                $data = [
                    'id_karyawan' => $post['nama'],
                    'id_divisi' => $post['id_divisi'],
                    'id_jabatan' => $post['id_jabatan'],
                    'tgl_berlaku' => $tgl_berlaku,
                    'id_golongan' => $post['golongan'],
                    'stat' => 1
                ];
                DB::table("cwa_mutasi_karyawan")->insert($data);
                log_user('INSERT', $data);
                $msg['success'] = "Berhasil menyimpan data mutasi karyawan";
            }
            else{
                $msg['error'] = "Karyawan tersebut tidak aktif atau tidak ditemukan";
            }

            $url = "mutasi/karyawan";

        }

        return redirect(URL::to($url))->with($msg);
    }

    public function postBoxupdate($id, Request $request){
        $post = $request->all();

        $get = DB::table('cwa_mutasi_karyawan')->find($id);
        $id_karyawan = $get->id_karyawan;

        //pengecekan apakah sudah masuk atau belum
        $data = MdKaryawan::getKaryawan($id_karyawan);
        if(strtotime($data->tgl_masuk) > strtotime($post['tgl_berlaku'])){
            return json_encode(["error" => "Tanggal tidak valid. Karyawan baru bergabung di perusahaan tanggal ".indo_date($data->tgl_masuk)]);
        }

        $cek = MdMtKaryawan::cek_resign($id_karyawan);
        if($cek){
            if(strtotime($cek->tgl_resign) < strtotime($post['tgl_berlaku']))
                return json_encode(["error" => "Tidak dapat menyimpan data mutasi. Karyawan tersebut sudah mengajukan resign pada tanggal sebelumnya (".indo_date($cek->tgl_resign).")"]);
        }

        $new = [
            'id_divisi' => $post['id_divisi'],
            'id_jabatan' => $post['id_jabatan'],
            'id_golongan' => $post['golongan'],
            'tgl_berlaku' => date($post['tgl_berlaku'])
        ];
        DB::table('cwa_mutasi_karyawan')
            ->where('id', $id)
            ->update($new);

        return json_encode([
            'success' => 'Berhasil mengupdate data mutasi karyawan'
        ]);

    }

    public function postUpdate($id, Request $request){
        $post = $request->all();

        //pengecekan apakah sudah masuk atau belum
        $data = MdKaryawan::getKaryawan($id);
        if(strtotime($data->tgl_masuk) > strtotime($post['tgl_berlaku'])){
            return redirect("mutasi/karyawan")->with("error","Tanggal tidak valid. Karyawan baru bergabung di perusahaan tanggal ".indo_date($data->tgl_masuk));
        }

        $cek = MdMtKaryawan::cek_resign($id);
        if($cek){
            if(strtotime($cek->tgl_resign) < strtotime($post['tgl_berlaku']))
                return redirect("mutasi/karyawan")->with("error", "Tidak dapat menyimpan data mutasi. Karyawan tersebut sudah mengajukan resign pada tanggal sebelumnya (".indo_date($cek->tgl_resign).")");
        }

        //pengecekan apakah sudah resign atau belum

        $dataa = [
            'id_karyawan' => $id,
            'id_divisi' => $post['id_divisi'],
            'id_jabatan' => $post['id_jabatan'],
            'tgl_berlaku' => $post['tgl_berlaku'],
            'id_golongan' => $post['golongan'],
            'stat' => 1
        ];
        DB::table("cwa_mutasi_karyawan")->insert($dataa);
        log_user('INSERT', $dataa);

        return redirect("mutasi/karyawan")->with("success","Berhasil mengupdate mutasi karyawan");
    }


    public function getResign($id, Request $request){
        log_user();
        $data = MdKaryawan::getKaryawan($id);
        $cek = MdMtKaryawan::cek_resign($id);

        if($cek){
            $request->session()->flash('tgl_resign', $cek->tgl_resign);
            $request->session()->flash('keterangan', $cek->keterangan);
        }

        return view('boxresign')->with([
            'data' => $data,
            'action_button' => "mutasi/karyawan/resign/".$id,
            'cek' => $cek
        ]);
    }

    public function postResign($id, Request $request){
        $data = MdKaryawan::getKaryawan($id);
        $post = $request->all();

        if(isset($post['batal'])){
            //batal resign
            DB::table("cwa_resign")
            ->where("id_karyawan", $id)
            ->where("stat", "<>", 9)
            ->delete();

            log_user('DELETE', $id);

            return redirect("mutasi/karyawan")->with("success", "Berhasil menghapus proses resign karyawan yang bersangkutan");
        }
        else{
            //proses
            if(strtotime($data->tgl_masuk) > strtotime($post['tgl_resign'])){
                $awal = indo_date($data->tgl_masuk);
                return redirect("mutasi/karyawan")->with("error", "Tanggal resign tidak valid. Karyawan baru bergabung di perusahaan tanggal ".$awal);
            }
            else{
                //hajar
                $cek = MdMtKaryawan::cek_resign($data->id);
                if($cek){
                    //update
                    $dt = [
                        'id_karyawan' => $data->id,
                        'tgl_resign' => $post['tgl_resign'],
                        'keterangan' => $post['keterangan'],
                    ];
                    DB::table("cwa_resign")
                    ->where("id", $cek->id)
                    ->update($dt);

                    log_user('UPDATE', $dt);
                }
                else{
                    $dtt = [
                        'id_karyawan' => $data->id,
                        'tgl_resign' => $post['tgl_resign'],
                        'keterangan' => $post['keterangan'],
                        'stat' => 1
                    ];
                    DB::table("cwa_resign")->insert($dtt);
                    log_user('INSERT', $dtt);
                }

                return redirect("mutasi/karyawan")->with("success","Berhasil mencatat data resign karyawan");
            }
        }

        
    }
    
    
    
    public function getBreak($id){
        log_user();
        
        $break = new ModelBreak();
        
        $cari = $break->where('id_karyawan',$id)->first();
        $old = [
            'type' => '',
            'tgl_start' => '',
            'tgl_end' => '',
            'description' => ''
        ];
        if(count($cari) > 0){
            $old = $cari->toArray();
        }
        
        $data = MdKaryawan::getKaryawan($id);
        return view('boxbreak')->with([
            'data' => $data,
            'action_button' => 'mutasi/karyawan/break',
            'old' => $old
        ]);
    }
    
    public function postBreak(Request $req){
        $post = $req->all();
        
        if(!in_array($post['jenis'], ['break','cuti hamil']) || strlen($post['tgl_start']) <= 0 || strlen($post['tgl_end']) < 0){
            $out['error'] = 'Inputan belum lengkap. Mohon isi dengan lengkap sebelum data dapat disimpan';
            return json_encode($out);
        }
        
        if(strtotime($post['tgl_start']) > strtotime($post['tgl_end'])){
            $out['error'] = 'Inputan tanggal tidak valid. Tanggal mulai break harus lebih awal daripada tanggal akhir break';
            return json_encode($out);
        }
        
        if(strtotime($post['tgl_end']) - strtotime($post['tgl_start']) <= (28 * 24 * 60 * 60)){
            $out['error'] = 'Break yang kurang dari 1 bulan mohon dimasukkan ke data cuti saja.';
            return json_encode($out);
        }
        
        //data siap
                
        $break = new ModelBreak();
        $break->where('id_karyawan', $post['id'])->delete(); //hapus data lama dulu
        
        $break->id_karyawan = $post['id'];
        $break->type = $post['jenis'];
        $break->tgl_start = $post['tgl_start'];
        $break->tgl_end = $post['tgl_end'];
        $break->description = $post['keterangan'];
        $break->stat = 1;
        $break->save();
        
        return json_encode(['success' => 'Berhasil menyimpan data break karyawan']);
    }

    public function getRemovebreak($id){
        $sql = DB::table('cwa_break')
            ->where('id_karyawan', $id)
            ->delete();
        return redirect('mutasi/karyawan')->with([
            'success' => 'Berhasil menghapus data break'
        ]);
    }



}
