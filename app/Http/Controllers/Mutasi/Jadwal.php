<?php

namespace App\Http\Controllers\Mutasi;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use App\Model\MdMtKaryawan;
use App\Model\MdKaryawan;
use App\Model\MdJadwal;
use Input;
use DB;
use URL;

class Jadwal extends Controller
{
    public function __construct(){
        $this->middleware('log');
    }

    public function getIndex(Request $request){
        log_user();
        $bulan = Input::get("bulan") ? intval(Input::get("bulan")) : date("n");
        $tahun = Input::get("tahun") ? intval(Input::get("tahun")) : date("Y");
        $divisi = Input::get("divisi");


        $jadwal = new MdJadwal([
            'bulan' => $bulan,
            'tahun' => $tahun,
        ]);

        //month field
        $mutasi = array();
        foreach($jadwal->calendar as $tg){
            $m = date("n", strtotime($tg));
            if(isset($bln[$m]))
                $bln[$m]++;
            else
                $bln[$m] = 1;

            $mutt = MdMtKaryawan::queryGetMutasi([
                'tgl_berlaku' => $tg,
                'divisi' => $divisi
            ]);
            foreach($mutt as $arr){
                //unique-id
                $uniq = $arr->id_karyawan."-".$arr->id_divisi;
                if(!isset($mutasi[$uniq])){
                    //insert this object to array
                    $mutasi[$uniq] = $arr;
                    $mutasi[$uniq]->tgl[] = $tg;
                }
                else{
                    $mutasi[$uniq]->tgl[] = $tg;
                }
            }

        }

        
        
        return view("jadwal")->with([
            'title' => 'Rancangan Jadwal Karyawan',
            'wellname' => 'Filter Jadwal',
            'curmenu' => 2,
            'cursubmenu' => 22,
            'default' => [
                'bulan' => $bulan,
                'tahun' => $tahun,
                'divisi' => $divisi
            ],
            'action_button' => 'mutasi/jadwal/create',
            'month_control' => $bln,
            'listdiv' => MdMtKaryawan::filter("divisi"),
            'calendar' => $jadwal->calendar,
            'mutasi' => $mutasi,
            'holiday' => $jadwal->holiday_data(),
            'list_kode' => $jadwal->build_kode(),
            'content' => $jadwal->get_jadwal_range($divisi)
        ]);
    }    
}
