<?php

namespace App\Http\Controllers\Mutasi;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Model\MdKaryawan;
use App\Model\MdMtKaryawan;
use App\Model\MdPresensi;
use App\Model\MdReport;
use App\Model\MdTerlambat;


class RekapLupaAbsen extends Controller
{
	public $request;

    public function __construct(Request $req){
        $this->middleware('log');
        $this->request = $req;
    }

    public function getIndex(){
        log_user();
        $bulan = $this->request->bulan ? intval($this->request->bulan) : date('n');
        $tahun = $this->request->tahun ? intval($this->request->tahun) : date('Y');
        $divisi = $this->request->divisi;

        $report = new MdReport([
            'bulan' => $bulan,
            'tahun' => $tahun,
        ]);

        //month field
        $row = $report->make_mutasi($divisi);


        //telat data instance 
        $telat = new MdTerlambat([
            'bulan' => $bulan,
            'tahun' => $tahun
        ]);


        $allowed = array();
        $list_date = $telat->calendar;
        foreach($list_date as $tg){
            $mutt = MdMtKaryawan::queryGetMutasi([
                'tgl_berlaku' =>  $tg,
                'divisi' => $divisi
            ]);

            foreach($mutt as $arr){
                $uniq = strtotime($tg)."-".$arr->id_karyawan."-".$arr->id_divisi;
                $allowed[] = $uniq;
            }
        }
        $data_telat = $telat->get_presence($allowed);
        //
        $tmp = [];
        foreach($data_telat as $uniqid => $ls){
            if($ls['real_masuk'] <> $ls['real_pulang'] && !empty($ls['real_masuk']) && !empty($ls['real_pulang'])){
            }
            else{
                $tmp[$uniqid] = $ls;
            }
        }
        $data_telat = $tmp;

        $karyawanTelat = array_where($data_telat, function($val, $key){
            return $key['terlambat'] == 1 ? true : false;
        });

        //karyawan telat return by id karyawan
        $telatList = [];
        foreach($karyawanTelat as $uniq => $dtaa){
            $telatList[$dtaa['id_karyawan']][$dtaa['tanggal']] = $dtaa['terlambat'];
        }

        $dataTelatOlah = self::olahDataTelat($telat->saved_terlambat());

        return view("report-telat")->with([
            'title' => 'Rekap Lupa Absen',
            'wellname' => 'Filter Presensi',
            'curmenu' => 4,
            'cursubmenu' => 46,
            'default' => [
                'bulan' => $bulan,
                'tahun' => $tahun,
                'divisi' => $divisi
            ],
            'listdiv' => MdMtKaryawan::filter("divisi"),
            'listkode' => MdReport::list_kode_absen(),
            'calendar' => $report->calendar,
            'content' => $telatList,
            'allTelat' => $karyawanTelat,
            'row' => $row,
            'acc' => $dataTelatOlah
        ]);        
    }

    //mengembalikan nilai terlambat yang diACC
    protected function olahDataTelat($data){
        $out = [];
        foreach($data as $uniq => $row){
            if($row['acc'] == 1){
                $pch = explode("-", $uniq);
                $tgl = $pch[0];
                $id_kar = $pch[1];

                $out[$id_kar][] = $tgl;
            }
        }

        return $out;
    }

}
