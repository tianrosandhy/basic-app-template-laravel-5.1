<?php

namespace App\Http\Controllers\Mutasi;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use App\Model\MdKaryawan;
use App\Model\MdMtKaryawan;
use App\Model\MdPresensi;
use Input;
use DB;
use URL;



class Tidakhadir extends Controller
{
   
    public function __construct(){
        $this->middleware('log');
    }

    public function getIndex()
    {
        log_user();
        $tgl = Input::get("tgl") ? Input::get("tgl") : date("Y-m-d");
        $divisi = Input::get("divisi");


        $bulan = date("n", strtotime($tgl));
        $tahun = date("Y", strtotime($tgl));

        //
        $presensi = new MdPresensi([
            'bulan' => $bulan,
            'tahun' => $tahun
        ]);

        $mutasi = array();
        $list_date = n_week($tgl);

        foreach($list_date as $tg){
            $mutt = MdMtKaryawan::queryGetMutasi([
                'tgl_berlaku' =>  $tg,
                'divisi' => $divisi
            ]);

            foreach($mutt as $arr){
                $uniq = $arr->id_karyawan."-".$arr->id_divisi;
                if(!isset($mutasi[$uniq])){
                    $mutasi[$uniq] = $arr;
                    $mutasi[$uniq]->tgl[] = $tg;
                }
                else{
                    $mutasi[$uniq]->tgl[] = $tg;
                }
            }
        }

        $presensi->build_kode(false);

        return view("presensi2")->with([
            'title' => "Data Ketidakhadiran",
            'curmenu' => 2,
            'cursubmenu' => 28,
            'action_button' => 'mutasi/tidakhadir/create',
            'default' => [
                'tgl' => $tgl,
                'divisi' => $divisi
            ],
            'listdiv' => MdMtKaryawan::filter("divisi"),
            'list_date' => $list_date,
            'mutasi' => $mutasi,
            'holiday' => $presensi->holiday_data(),
            'kode_absen' => MdPresensi::kode_absen(),
            'list_shift' => MdPresensi::list_shift(),
            'rencana' => $presensi->get_jadwal_range($divisi, $list_date),
            'tidak_hadir' => $presensi->tidak_hadir($list_date),
            'content' => $presensi->list_presensi($list_date)
        ]);
    }

}
