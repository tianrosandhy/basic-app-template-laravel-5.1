<?php

namespace App\Http\Controllers\Mutasi;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use Input;
use DB;
use URL;
use Excel;
use App\Model\MdUpload;
use App\Model\MdPresensi;
use App\Model\MdMtKaryawan;
use App\Model\Table\ModelDivisi;

class Upload2 extends Controller
{
    public function __construct(){
        $this->middleware('log');
    }

    public function getIndex(Request $req)
    {
        log_user();
        if($req->input("bulan"))
            $bulan = intval($req->input("bulan"));
        else
            $bulan = date("n");

        if($req->input("tahun"))
            $tahun = intval($req->input("tahun"));
        else
            $tahun = date("Y");

        
        return view("blank")->with([
            'title' => "Upload File Jadwal",
            'default' => [
                'bulan' => $bulan,
                'tahun' => $tahun   
            ],
            'list_divisi' => ModelDivisi::where('stat',1)->orderBy('sort_no')->get()->pluck('nama_divisi', 'id')->toArray(),
            'file_template' => true,
            'hide_periode' => true,
            'curmenu' => 2,
            'cursubmenu' => 29
        ]);
    }

    public function postIndex(Request $request){
        $upl = new MdUpload();
        
        $upl->process($request->file('presence'));
        $excel = Excel::load($upl->location)->get();
        $data = json_decode(json_encode($excel), true); //object to array magic
        log_user('UPLOAD');


        $r = 0;
        foreach($data as $row){
            $row = array($row);
            foreach($row as $ind => $item){
                foreach($item as $col=>$val){
                    if(strlen($val) > 0)
                        $return[$r][$col] = $val;
                }
            }
            $r++;
        }

        $proses = $upl->olah_absen($return);
        return redirect('mutasi/upload2')->with($proses);
    }   

}
