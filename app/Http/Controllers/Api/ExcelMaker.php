<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Input;
use DB;
use URL;
use Excel;


use App\Model\MdMtKaryawan;
use App\Model\MdJadwal;
use App\Model\Table\ModelDivisi;
use ZipArchive;

class ExcelMaker extends Controller
{

    public $n, $data, $temp_div;
    public $divisi, $bulan, $tahun;
    
    
    public function __construct(){
        $this->middleware('log');
        $this->n = jumlah_hari(date("m"), date("Y"));
    }
    
    public function index(Request $req){
        $get = $req->all();
        $this->divisi = "";
        if(isset($get['divisi'])){
            $this->divisi = $get['divisi'];
        }
        if(isset($get['bulan'])){
            $this->bulan = intval($get['bulan']);
        }
        if(isset($get['tahun'])){
            $this->tahun = intval($get['tahun']);
        }
                
        
        
        
        //if divisi is included
        if($this->divisi > 0){
            $mutasi[$this->divisi] = $this->run_mutasi($this->divisi);
        }
        else{
            $dv = new ModelDivisi();
            $list = $dv->where('stat',1)->get()->pluck('id')->toArray();
            foreach($list as $id_divisi){
                $mutasi[$id_divisi] = $this->run_mutasi($id_divisi);
            }
        }

        $zipname = [];
        foreach($mutasi as $id_divisi=>$data){
            $zipname[] = $this->process($id_divisi, $data);            
        }
        $this->save_zip($zipname);
        
    }
    
    public function run_mutasi($id_divisi){
        $jadwal = new MdJadwal([
            'bulan' => $this->bulan,
            'tahun' => $this->tahun,
        ]);

        //month field
        $mutasi = array();
        foreach($jadwal->calendar as $tg){
            $m = date("n", strtotime($tg));
            if(isset($bln[$m]))
                $bln[$m]++;
            else
                $bln[$m] = 1;

            $mutt = MdMtKaryawan::queryGetMutasi([
                'tgl_berlaku' => $tg,
                'divisi' => $id_divisi
            ]);
            foreach($mutt as $arr){
                //unique-id
                $uniq = $arr->id_karyawan."-".$arr->id_divisi;
                if(!isset($mutasi[$uniq])){
                    //insert this object to array
                    $mutasi[$uniq] = $arr->nama;
                }
            }
        }        
        return $mutasi;
    }
    
    
    public function save_zip($zipname){
        $dir = 'upload';
        $n = 0;
        $willsave = [];
        $nmfilezip = 'tmp/Template Rancangan Jadwal CWA.zip';
        $zip = new ZipArchive;

        $zip->open($nmfilezip, ZipArchive::CREATE);
        foreach($zipname as $zp){
            $nm = explode("/", $zp);
            $zip->addFile($zp, $nm[(count($nm)-1)]);
        }
        $zip->close();
        header('Content-Type: application/zip');
        header('Content-disposition: attachment; filename='.$nmfilezip);
        header('Content-Length: ' . filesize($nmfilezip));
        readfile($nmfilezip);
    }
    
    public function process($id_dv, $data){
        $this->data = $data;
        
        $dvname = ModelDivisi::where("id",$id_dv)->first();
        $dvname = $dvname->nama_divisi;

        $this->temp_div = $dvname;
        
        $filename = 'Rancangan Jadwal - '.nama_bulan($this->bulan).' '.$this->tahun.' '.$dvname;
        Excel::create($filename, function($excel){
            $excel->sheet('Rancangan Jadwal', function($sheet){
                $sheet->row(1, ['Rancangan Jadwal '.$this->temp_div]);
                $sheet->mergeCells('B2:F2');
                $sheet->mergeCells('B3:F3');
                $sheet->mergeCells('B4:F4');
                $sheet->row(2, ['Divisi', $this->temp_div]);
                $sheet->row(3, ['Bulan', $this->bulan]);
                $sheet->row(4, ['Tahun', $this->tahun]);

                $dt_loop[0] = 'Nama Karyawan';
                for($n=1; $n<=$this->n; $n++){
                    $dt_loop[$n] = $n;
                }
                
                $sheet->row(6, $dt_loop);
                
                $sheet->cell('A1', function($cell){
                    $cell->setFontSize(16);
                    $cell->setFontWeight('bold');
                });
                
                $sheet->cells('A6:AF6', function($cell){
                   $cell->setFontWeight('bold'); 
                });
                
                $sheet->setWidth([
                    'A' => 30,
                    'B' =>4,
                    'C' =>4,
                    'D' =>4,
                    'E' =>4,
                    'F' =>4,
                    'G' =>4,
                    'H' =>4,
                    'I' =>4,
                    'J' =>4,
                    'K' =>4,
                    'L' =>4,
                    'M' =>4,
                    'N' =>4,
                    'O' =>4,
                    'P' =>4,
                    'Q' =>4,
                    'R' =>4,
                    'S' =>4,
                    'T' =>4,
                    'U' =>4,
                    'V' =>4,
                    'W' =>4,
                    'X' =>4,
                    'Y' =>4,
                    'Z' =>4,
                    'AA' =>4,
                    'AB' =>4,
                    'AC' =>4,
                    'AD' =>4,
                    'AE' =>4,
                    'AF' => 3                    
                ]);
                
                
                
                //
                $start_from = 7;
                $n = 0;
                foreach($this->data as $row){
                    $sheet->row(($start_from+$n), [$row]);
                    $n++;
                }
                
                $sheet->setBorder('A6:AF'.($start_from+$n), 'thin');
                
            });
        })->store('xlsx', 'upload');
        
        return "upload/".$filename.".xlsx";
    }

}
