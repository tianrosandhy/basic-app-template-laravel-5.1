<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use App\Model\MdMtKaryawan;
use App\Model\MdKaryawan;
use App\Model\MdPresensi;
use App\Model\MdTerlambat;
use Input;
use DB;
use URL;


class ApiTerlambat extends Controller
{

    public function __construct(){
        $this->middleware('log');
    }

    public function store(Request $req){
        $uniq = Input::get("datauniq");
        $isi = Input::get("datacontent");
        $type = Input::get("type");
        $fetch = MdMtKaryawan::fetch_uniq($uniq);
        $telat = new MdTerlambat([
            'bulan' => date("n",strtotime($fetch['tanggal'])),
            'tahun' => date("Y",strtotime($fetch['tanggal'])),
        ]);
        $saved = $telat->saved_terlambat();

        if($type=="reason"){
            $reason = $isi;
            $acc = null;
            $sv = "reason";
            $isis = $reason;
        }
        else{
            $reason = null;
            $acc = intval($isi);
            $sv = "acc";
            $isis = $acc;
        }

        $simpan = [
            'uniq' => $uniq,
            'tanggal' => $fetch['tanggal'],
            'id_karyawan' => $fetch['id_karyawan'],
            'id_divisi' => $fetch['id_divisi'],
            'reason' => $reason,
            'acc' => $acc,
            'stat' => 1
        ];

//        $fetch = MdMtKaryawan::fetch_uniq($uniq);
        $cek = MdTerlambat::getTerlambat($uniq);
        if($cek){
            //update
            
            DB::table("cwa_terlambat")
            ->where("id",$cek['id'])
            ->where("stat","<>",9)
            ->update([
                $sv => $isis
            ]);
            $msg = "Berhasil mengupdate data keterlambatan";
        }
        else{
            //insert
            DB::table("cwa_terlambat")
            ->insert($simpan);
            $msg = "Berhasil menyimpan data keterlambatan";
        }


        //delete if there is empty / useless row
        $aftercheck = DB::table("cwa_terlambat")
        ->where("uniq", $uniq)
        ->where("reason", NULL)
        ->where("acc",0)
        ->delete();

        return json_encode(['success'=>$msg]);
    }

}
