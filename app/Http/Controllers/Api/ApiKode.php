<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use App\Model\MdMtKaryawan;
use App\Model\MdKaryawan;
use App\Model\MdPresensi;
use App\Model\MdLembur;
use Input;
use DB;
use URL;

class ApiKode extends Controller
{
    public function index(Request $req){
        $arr = array();

        $cek = DB::table('cwa_control_code')
        ->where('id_karyawan','<>',$req->input('karyawan'))
        ->where('type', $req->input('type'))
        ->where('code', $req->input("kode"))
        ->where('stat','<>',9)
        ->get();
/*
        if(count($cek) > 0){
            //sudah ada,, KODE KONFLIK
            $arr['error'] = 'Kode tersebut sudah digunakan untuk karyawan lainnya. Mohon gunakan kode yang lain';
        }
        else{
*/
            if(strlen($req->input('kode')) == 0){
                //blank code : delete current kode
                DB::table('cwa_control_code')
                ->where('id_karyawan',$req->input('karyawan'))
                ->where('stat','<>',9)
                ->delete();
                $msg = "Berhasil menghapus kode karyawan";
            }
            else{
                $cek2 = DB::table('cwa_control_code')
                ->where('id_karyawan',$req->input('karyawan'))
                ->where('type', $req->input('type'))
                ->where('stat','<>',9)
                ->get();


                if(count($cek2) > 0){
                    //update
                    DB::table('cwa_control_code')
                    ->where('id_karyawan',$req->input('karyawan'))
                    ->where('type', $req->input('type'))
                    ->where('stat','<>',9)
                    ->update([
                        'code' => $req->input('kode')
                    ]);
                    $msg = "Berhasil mengupdate kode karyawan";
                }
                else{
                    //insert
                    DB::table('cwa_control_code')
                    ->insert([
                        'id_karyawan' => $req->input('karyawan'),
                        'code' => $req->input('kode'),
                        'type' => $req->input('type'),
                        'stat' => 1
                    ]);
                    $msg = "Berhasil menyimpan kode karyawan";
                }

            }


            //sukses
            $arr['success'] = $msg;
/*
        }
*/
        return json_encode($arr);
    }
}
