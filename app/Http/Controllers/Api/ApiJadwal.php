<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use App\Model\MdMtKaryawan;
use App\Model\MdKaryawan;
use App\Model\MdJadwal;
use Input;
use DB;
use URL;

class ApiJadwal extends Controller
{

    public function __construct(){
        $this->middleware('log');
    }

    public function store(Request $req){
        $return = array();

        if(count($req->all()) > 0){
            $post = $req->all();
            if($post['type'] == "kode"){
                $kode = $post['nilai'];
                $shift = null;
            }
            else if($post['type'] == "shift"){
                $shift = $post['nilai'];
                $kode = null;
            }

            $instance = MdJadwal::check_exists($post);
            if(!$instance){
                //insert
                $instance = DB::table("cwa_rancangan_jadwal")
                ->insert([
                    'id_karyawan' => $post['id_karyawan'],
                    'id_divisi' => $post['id_divisi'],
                    'id_kode_absen' => MdJadwal::getPK($post['type'], $kode),
                    'id_shift' =>  MdJadwal::getPK($post['type'], $shift),
                    'tanggal' => $post['tanggal'],
                    'stat' => 1
                ]);
                $return['type'] = "insert";
                $return['success'] = "Berhasil menyimpan rencana jadwal";
            }
            else{
                $updateData = [
                    'id_kode_absen' => MdJadwal::getPK($post['type'], $kode),
                    'id_shift' =>  MdJadwal::getPK($post['type'], $shift)
                ];
                //update
                DB::table("cwa_rancangan_jadwal")
                ->where("tanggal", $post['tanggal'])
                ->where("id_divisi", $post['id_divisi'])
                ->where("id_karyawan", $post['id_karyawan'])
                ->where("stat", "<>", 9)
                ->update($updateData);
                $return['type'] = "update";
                $return['success'] = "Berhasil mengupdate rencana jadwal";

	            //update data presensi based on instance id;
	            MdJadwal::updatePresensiByRancangan($instance, $updateData);

            }


        }
        else{
            $return['error'] =  "Invalid operation";
        }
            return json_encode($return);

    }
}
