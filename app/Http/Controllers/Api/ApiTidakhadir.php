<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use App\Model\MdMtKaryawan;
use App\Model\MdKaryawan;
use App\Model\MdPresensi;
use Input;
use DB;
use URL;

class ApiTidakhadir extends Controller
{
    public function store(Request $request){
        $out = array();
        if(empty($request->input("datauniq")) or empty($request->input("dataval"))){
            $out['error'] = "Error, input tidak valid";
        }

        $pecah = explode("-",$request->input("datauniq"));
        $tgl = date("Y-m-d", $pecah[0]);
        $idkar = intval($pecah[1]);
        $iddiv = intval($pecah[2]);
        $value = intval($request->input("dataval"));

        $cek = DB::table("cwa_tidak_hadir")
        ->where("id_karyawan", $idkar)
        ->where("id_divisi", $iddiv)
        ->where("tanggal", $tgl)
        ->where("stat","<>",9)
        ->get();

        if(count($cek) > 0){
            //ada, update
            DB::table("cwa_tidak_hadir")
            ->where("id_karyawan", $idkar)
            ->where("id_divisi", $iddiv)
            ->where("tanggal", $tgl)
            ->where("stat","<>",9)
            ->update([
                'id_kode_absen' => $value
            ]);

            $out['success'] = "Berhasil mengupdate data ketidakhadiran karyawan";

        }
        else{
            //insert
            DB::table("cwa_tidak_hadir")
            ->insert([
                'id_karyawan' => $idkar,
                'id_divisi' => $iddiv,
                'tanggal' => $tgl,
                'id_kode_absen' => $value,
                'stat' => 1
            ]);

            $out['success'] = "Berhasil menyimpan data ketidakhadiran karyawan";
        }

        return json_encode($out);
    }
}
