<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use App\Model\MdMtKaryawan;
use App\Model\MdKaryawan;
use App\Model\MdPresensi;
use Input;
use DB;
use URL;


class ApiPresensi extends Controller
{

    public function __construct(){
        $this->middleware('log');
    }

    public function getIndex(Request $req){
        $return = array();
        if(count($req->all() > 0)){
            $post = $req->all();
            if($post['type'] == "masuk"){
                $jam_masuk = empty($post['nilai']) ? null : $post['nilai'];
                $jam_pulang = null;
                $unset = "real_pulang";
            }
            else if($post['type'] == "pulang"){
                $jam_pulang = empty($post['nilai']) ? null : $post['nilai'];
                $jam_masuk = null;
                $unset = "real_masuk";
            }

            $tgl = $post['tanggal'];
            $presensi = new MdPresensi([
                'bulan' => date("n", strtotime($tgl)),
                'tahun' => date("Y", strtotime($tgl))
            ]);

            $rancangan = $presensi->check_rancangan_exists($post);
            if($rancangan){
                //ada
                $id_rancangan = $rancangan['id'];
                $id_shift = $rancangan['id_shift'];
                if(empty($id_shift))
                    $return['error'] = "Input presensi tidak perlu dilakukan untuk absensi tanpa shift kerja";
                else{
                    $list_shift = MdPresensi::list_shift();
                    $must_masuk = $list_shift[$id_shift]['jam_masuk'];
                    $must_pulang = $list_shift[$id_shift]['jam_pulang'];

                    //input ke db
                    $pres = MdPresensi::cek_presensi_exists($id_rancangan);

                    $data_input = array(
                        "id_rancangan_jadwal" => $id_rancangan,
                        "must_masuk" => $must_masuk,
                        "must_pulang" => $must_pulang,
                        "real_masuk" => $jam_masuk,
                        "real_pulang" => $jam_pulang,
                        "tanggal" => $tgl,
                        "stat" => 1
                    );

                    $bg = MdPresensi::time_process($data_input);
                    if($post['type'] == "masuk")
                        $return['bg'] = $bg['bgmasuk'];
                    else
                        $return['bg'] = $bg['bgpulang'];

                    if($pres){
                        //update
                        unset($data_input[$unset]);
                        unset($data_input['stat']);
                        unset($data_input['id_rancangan_jadwal']);

                        $act = DB::table("cwa_presensi")
                        ->where("id_rancangan_jadwal", $id_rancangan)
                        ->where("stat","<>",9)
                        ->update($data_input);
                        $msg = "Berhasil mengupdate data presensi";
                    }
                    else{
                        //insert
                        $act = DB::table("cwa_presensi")
                        ->insert($data_input);
                        $msg = "Berhasil menginput data presensi";
                    }

                    if($act){
                        $return['success'] = $msg;
                    }
                }
            }
            else{
                $return['error'] = "Rancangan presensi tidak ditemukan";
            }

        }
        else{
            $return['error'] = "Invalid operation";
        }

        return json_encode($return);
    }
}


