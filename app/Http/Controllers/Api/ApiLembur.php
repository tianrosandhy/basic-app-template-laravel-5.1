<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use App\Model\MdMtKaryawan;
use App\Model\MdKaryawan;
use App\Model\MdPresensi;
use App\Model\MdLembur;
use Input;
use DB;
use URL;

class ApiLembur extends Controller
{
    public function __construct(){
        $this->middleware('log');
    }

    public function getIndex(Request $req){
        $uniq = Input::get("uniq");
        $isi = Input::get("value");
        $type = Input::get("type");
        $fetch = MdMtKaryawan::fetch_uniq($uniq);
        $lembur = new MdLembur([
            'bulan' => date("n",strtotime($fetch['tanggal'])),
            'tahun' => date("Y",strtotime($fetch['tanggal'])),
        ]);

        $saved_lembur = $lembur->saved_lembur();

        if($type=="reason"){
            $fld = 'lama_lembur';
            $lama_lembur = intval($isi);
            $type = 1;
        }
        else{
            $fld = 'type';
            $lama_lembur = 0;
            $type = intval($isi);
        }

        if(array_key_exists($uniq, $saved_lembur)){
            //ada, update
            $id_lembur = $saved_lembur[$uniq]['id'];
            DB::table("cwa_lembur")
            ->where("id",$id_lembur)
            ->where("stat","<>",9)
            ->update([
                $fld => intval($isi)
            ]);
            $msg = "Berhasil mengupdate data lembur";

        }
        else{
            //tidak ada, insert
            $id_rancangan = $lembur->get_rancang($fetch);
            $data = [
                'id_presensi' => $id_rancangan->id,
                'tgl' => $fetch['tanggal'],
                'lama_lembur' => $lama_lembur,
                'type' => $type,
                'stat' => 1
            ];
            DB::table("cwa_lembur")
            ->insert($data);
            $msg = "Berhasil menyimpan data lembur";
        }

        return json_encode(['success'=>$msg]);
    }
}
