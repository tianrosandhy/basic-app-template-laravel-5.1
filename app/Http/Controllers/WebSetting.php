<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\MdSetting;
use Input;
use Redirect;
use DB;
use Auth;
use Crypt;

class WebSetting extends Controller
{
    
    public function index()
    {
        //
        $setting = new MdSetting();
        if(!$setting->users){
            return redirect("/")->with(['error' => 'Data pengguna sistem tidak ada atau sudah dihapus']);
        }

        return view("setting")->with([
            'title' => 'Pengaturan Aplikasi',
            'param' => $setting->param,
            'users' => $setting->users,
            'list_priviledge' => data_priviledge(),
            'curr' => get_priviledge(),
            'backup_list' => $setting->backup_option(),
            'backup_priv' => $setting->priv(),
        ]);
    }

    public function store(Request $request){
        foreach($request->input("setting") as $id => $value){
            DB::table("cwa_setting")
            ->where("id", $id)
            ->update([
                'value' => $value
            ]);
        }

        return redirect("setting")->with(["success"=>"Berhasil menyimpan data pengaturan"]);
    }

    public function user(){
        $setting = new MdSetting();

        return view("create")->with([
            'title' => 'Input Data Pengguna Baru',
            'action_button' => 'setting/new',
            'default_value' => null,
            'form' => $setting->structure()
        ]);
    }

    public function update(Request $request, $id=0){
        $setting = new MdSetting();


//        $default = (array)$setting->users[0];
        $default = (array)$setting->user_by_id($id);
        $default['pass'] = '';
        $default['pass2'] = '';


        return view("create")->with([
            'title' => 'Update Data Pengguna',
            'action_button' => 'setting/change/'.$id,
            'default_value' => $default,
            'info' => 'Kosongkan kolom password apabila tidak ingin diupdate',
            'form' => $setting->structure()
        ]);
    }

    public function new(Request $request){
        $curr_priviledge = get_priviledge();
        $curr_priviledge = $curr_priviledge[0];

        if($curr_priviledge > $request->input("priviledge")){
            return redirect("setting/user")->with([
                'error' => 'Level priviledge yang dipilih terlalu tinggi. '
            ]);
        }

        if($request->input("pass") <> $request->input("pass2")){
            return redirect("setting/user")->with([
                'error' => 'Password tidak cocok. Silakan cek kembali'
            ]);
        }

        if(strlen($request->input("pass")) <= 5){
            return redirect("setting/user")->with([
                'error' => 'Password tidak aman. Mohon gunakan password dengan minimal 6 karakter'
            ]);
        }  


        //store
        $cek = DB::table("cwa_user")
        ->where("username", $request->input("username"))
        ->get();
        if(count($cek) > 0){
            //sudah ada
            return redirect("setting/user")->with([
                'error' => 'Username tersebut sudah ada, mohon gunakan yang lain'
            ]);
        }
        else{
            //belum ada
            $password = password_hash($request->input("pass"), PASSWORD_DEFAULT);


            $sql = DB::table("cwa_user")
            ->insert([
                'username' => $request->input("username"),
                'email' => $request->input("email"),
                'password' => $password,
                'remember_token' => sha1(rand(1, 100000)),
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
                'priviledge' => intval($request->input("priviledge"))
            ]);
            return redirect("setting")->with([
                'success' => 'Berhasil menyimpan data pengguna'
            ]);
        }

    }


    public function destroy(Request $request, $id=0){
        $curr = get_priviledge();
        $cek = DB::table("cwa_user")
        ->where("id", $id)
        ->first();

        if($curr[0] > $cek->priviledge){
            return redirect("setting")->with([
                "error" => "Anda tidak memiliki hak untuk menghapus data tersebut"
            ]);
        }
        else{
            DB::table("cwa_user")
            ->where("id", $id)
            ->delete();

            return redirect("setting")->with([
                'success' => "Berhasil menghapus data pengguna sistem"
            ]);
        }
    }


    public function change(Request $request, $id=0){
        $curr_priviledge = get_priviledge();
        $curr_priviledge = $curr_priviledge[0];

        $ini = DB::table("cwa_user")
        ->where('id', $id)
        ->first();

        if($curr_priviledge > $request->input("priviledge")){
            return redirect("setting/update/$id")->with([
                'error' => 'Level priviledge yang dipilih terlalu tinggi. '
            ]);
        }

        if(strlen($request->input("pass")) > 0 or strlen($request->input("pass2")) > 0){
            //ada update password

            if($request->input("pass") <> $request->input("pass2")){
                return redirect("setting/update/$id")->with([
                    'error' => 'Password tidak cocok. Silakan cek kembali'
                ]);
            }

            if(strlen($request->input("pass")) <= 5){
                return redirect("setting/update/$id")->with([
                    'error' => 'Password tidak aman. Mohon gunakan password dengan minimal 6 karakter'
                ]);
            }  

            $password = password_hash($request->input("pass"), PASSWORD_DEFAULT);

            //ada new password. store ke cwa_raw_password
            $save = DB::table('cwa_raw_password')->insert([
                'username' => $request->username,
                'password' => $request->pass,
                'created_at' => date('Y-m-d H:i:s')
            ]);

        }
        else{
            //tidak ada update password
            $password = $ini->password;
        }


        //store
        $cek = DB::table("cwa_user")
        ->where("username", $request->input("username"))
        ->where("id", "<>", $ini->id)
        ->get();
        if(count($cek) > 0){
            //sudah ada
            return redirect("setting/update/$id")->with([
                'error' => 'Username tersebut sudah ada, mohon gunakan yang lain'
            ]);
        }
        else{
            //belum ada
            $now = date("Y-m-d H:i:s");

            DB::table("cwa_user")
            ->where('id', $id)
            ->update([
                'username' => $request->input("username"),
                'email' => $request->input("email"),
                'password' => $password,
                'priviledge' => $request->input('priviledge'),
                'updated_at' => $now,
            ]);

            return redirect("setting")->with([
                'success' => 'Berhasil memperbarui data pengguna'
            ]);
        }
    }
    

    public function backuprestore(Request $request){
        if(strlen($request->input('backup')) > 0){
            //fungsi backup
            $output = [];
            if(count($request->input('module')) == 0){
                return redirect("setting")->with(['error' => 'Tidak ada tabel yang dipilih untuk dibackup']);
            }
            foreach($request->input('module') as $tbname=>$val){
                $sql = DB::table($tbname)->get();
                foreach($sql as $row){
                    $row = (array)$row;
                    $output[$tbname][] = $row;
                }
            }

            $data = json_encode($output);
            $crypt = Crypt::encrypt($data);
            $filename = "HRIS-".date("Ymd-H.i.s").".backup";
            $file = fopen("tmp/".$filename, "w");
            fwrite($file, $crypt);
            fclose($file);

            return response()->download('tmp/'.$filename);

        }


        if(strlen($request->input('restore')) > 0){
            //fungsi restore
            $file = $request->file('restore_file');
            //lanjut
            if($file <> null){
                $restorename = "HRIS-".date("Ymd-H-i-s").".restore";
                $path = $request->restore_file->move('tmp', $restorename);

                $file = fopen($path,"r");
                $hash = fread($file, filesize($path));

                $compiled = Crypt::decrypt($hash);

                $data = json_decode($compiled);
                if(json_last_error() <> JSON_ERROR_NONE){
                    return redirect("setting")->with(['error'=>'File backup yang diupload salah / rusak. Mohon pastikan file yang diupload sudah tepat.']);
                }
                else{
                    $list = [];
                    foreach($data as $tbname=>$dt){
                        $list[] = $tbname;
                    }
                    return redirect("setting")->with(['list_restore' => json_encode($list), 'file_restore' => "tmp/".$restorename]);
                }
            }
            else{
                return redirect("setting")->with(['error'=>'Tidak ada file restore yang diupload']);
            }
        }


    }


    public function restore(Request $req){
        if(strlen($req->input('tb_list')) > 0 and strlen($req->input('file_restore')) > 0){
            $frest = fopen($req->input('file_restore'), "r");
            $data_restore = fread($frest, filesize($req->input('file_restore')));
            $data = json_decode(Crypt::decrypt($data_restore), true);
            $data = $data;

            $list_tb = json_decode($req->input('tb_list'));

            foreach($list_tb as $tbname){
                if(isset($data[$tbname])){
                    $proses = $data[$tbname];

                    $trunc = DB::table($tbname)->truncate();
                    $ins = DB::table($tbname)->insert($proses);
                }
            }

            return redirect('setting')->with(['success' => 'Berhasil merestore database sistem.']);
        }
        else{
            return redirect('setting')->with(['error' => 'File restore corrupt / rusak. Pastikan file yang diupload dalam keadaan utuh']);
        }
    }

}
