<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use App\Model\MdKaryawan;
use App\Model\MdMtKaryawan;
use App\Model\MdMtGaji;
use Input;
use DB;
use URL;

use App\Model\Table\ModelMutasiKaryawan;
use App\Model\DataTableModel;
use App\Traits\KaryawanTrait;


class Karyawan extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    #table name used in this controller
    protected $tb = "cwa_karyawan";
    public $request;

    use KaryawanTrait;


    public function __construct(Request $req){
        $this->middleware('log');
        $this->register_column();
        $this->request = $req;
    }

    public function getIndex()
    {
        log_user();
        return view('master_new')->with([
            'title' => "Master Karyawan",
            'curmenu' => 1,
            'cursubmenu' => 11,
            'table' => MdKaryawan::structure(),
            'content' => MdKaryawan::content(),
            'columns' => $this->columns,
            'datatable_search' => DataTableModel::generate_search($this->columns),
            'datatable_ajax' => [
                'tb' => 'karyawan/table',
            ],
            'script' => true,
            'action_button' => "master/karyawan/create"
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate()
    {
        //
        log_user();
        return view('create')->with([
            'title' => 'Insert Data Karyawan',
            'curmenu' => 1,
            'cursubmenu' => 11,
            'form' => MdKaryawan::structure(),
            'action_button' => "master/karyawan/store"
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postStore(Request $request)
    {
        //
        
        $post = $request->all();
        $try = MdKaryawan::validate($post);

        if($try){
            $msg['error'] = $try;
            $url = "master/karyawan/create";
            foreach($post as $in=>$ps){
                $request->session()->flash($in, $ps);
            }
        }
        else{
            $data = [
                'nama' => $post['nama'],
                'nik' => $post['nik'],
                'npwp' => $post['npwp'],
                'email' => $post['email'],
                'departemen' => $post['departemen'],
                'ttl' => $post['ttl'],
                'jk' => $post['jk'],
                'alamat' => $post['alamat'],
                'telp' => $post['telp'],
                'no_rek' => $post['no_rek'],
                'tgl_masuk' => $post['tgl_masuk'],
                'status_transfer' => $post['status_transfer'],
                'is_bpjs_kesehatan' => $post['is_bpjs_kesehatan'] ?? null,
                'is_bpjs_tk' => $post['is_bpjs_tk'] ?? null,
                'tgl_daftar_bpjs_kesehatan' => $post['tgl_daftar_bpjs_kesehatan'] ?? null,
                'tgl_daftar_bpjs_tk' => $post['tgl_daftar_bpjs_tk'] ?? null,
                'status_ptkp' => $post['status_ptkp'] ?? null,
                'stat' => 1
            ];
            $idk = DB::table($this->tb)->insertGetId($data);

            log_user('INSERT', $data);
            
            //data mutasi diinsert juga

            $sv = new ModelMutasiKaryawan();
            $sv->id_karyawan = $idk;
            $sv->id_divisi = $post['divisi'];
            $sv->id_jabatan = $post['jabatan'];
            $sv->id_golongan = $post['golongan'];
            $sv->tgl_berlaku = $post['tgl_masuk'];
            $sv->stat = 1;
            $sv->save();
            

            $msg['success'] = "Berhasil menyimpan data master karyawan";
            $url = "master/karyawan";

        }

        return redirect(URL::to($url))->with($msg);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getEdit($id)
    {
        log_user();
        
        $struktur = MdKaryawan::structure();
        unset($struktur['Divisi']);
        unset($struktur['Jabatan']);
        unset($struktur['Golongan Karyawan']);
        
        return view('create')->with([
            'title' => 'Update Data Karyawan',
            'curmenu' => 1,
            'cursubmenu' => 11,
            'form' => $struktur,
            'default_value' => MdKaryawan::editable($id),
            'action_button' => "master/karyawan/update/".$id
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postUpdate(Request $request, $id)
    {
        //
        $post = $request->all();
        $try = MdKaryawan::validate($post, $id);

        if($try){
            $msg['error'] = $try;
            $url = "master/karyawan/edit/".$id;
            foreach($post as $in=>$ps){
                $request->session()->flash($in, $ps);
            }
        }
        else{
            $data = [
                'nama' => $post['nama'],
                'nik' => $post['nik'],
                'npwp' => $post['npwp'],
                'email' => $post['email'],
                'departemen' => $post['departemen'],
                'ttl' => $post['ttl'],
                'jk' => $post['jk'],
                'alamat' => $post['alamat'],
                'telp' => $post['telp'],
                'no_rek' => $post['no_rek'],
                'tgl_masuk' => $post['tgl_masuk'],
                'status_transfer' => $post['status_transfer'],
                'is_bpjs_kesehatan' => $post['is_bpjs_kesehatan'] ?? null,
                'is_bpjs_tk' => $post['is_bpjs_tk'] ?? null,
                'tgl_daftar_bpjs_kesehatan' => strlen($post['tgl_daftar_bpjs_kesehatan']) > 0 ? $post['tgl_daftar_bpjs_kesehatan'] : null,
                'tgl_daftar_bpjs_tk' => strlen($post['tgl_daftar_bpjs_tk']) > 0 ? $post['tgl_daftar_bpjs_tk'] : null,
                'status_ptkp' => $post['status_ptkp'] ?? null,
            ];
            
            DB::table($this->tb)
            ->where("id", $id)
            ->update($data);
            log_user('UPDATE', $data);

            $url = "master/karyawan";
            if(isset($_GET['page'])){
                $url .= '?page='.intval($_GET['page']);
            }
            $msg['success'] = "Berhasil mengupdate data karyawan";
        }
        

        return redirect(URL::to($url))->with($msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getDestroy($id)
    {
        //
        $query = DB::table($this->tb)
            ->where("id", $id)
            ->update(["stat" => 9]);

        log_user('DELETE', $id);

        return redirect("master/karyawan")->with("success","Berhasil menghapus data karyawan");
    }








    public function detail($id_karyawan){
        $skrg = date("Y-m-d");

        $master = MdKaryawan::getKaryawan($id_karyawan);
        $data = array(
            'id' => $master->id,
            'nama' => $master->nama,
            'ttl' => $master->ttl,
            'jk' => ($master->jk == 1) ? "Pria" : "Wanita",
            'alamat' => $master->alamat,
            'telp' => $master->telp,
            'tgl_masuk' => indo_date($master->tgl_masuk, "half")
        );

        $mutasi = MdMtKaryawan::queryGetMutasi([
            'id_karyawan' => $master->id
        ]);
        $mutasi = MdMtKaryawan::list_content($master->id);

        $ms_gaji = MdMtGaji::master_gaji();
        $gaji = MdMtGaji::detail_gaji_karyawan($master->id, 1, $skrg);
        $key = array_keys($gaji);
        if(count($key) > 1){
            $ambil = count($key) - 1;
            $tgl_gaji = $key[$ambil];
            $gaji = $gaji[$key[$ambil]];
        }
        else if(count($key) == 1){
            $tgl_gaji = key($gaji);
            $gaji = $gaji[$tgl_gaji];
        }
        else{
            $tgl_gaji = '';
            $gaji = [];
            $outgaji = [];
        }

        $total_gaji = 0;
        foreach($gaji as $idgj => $nilai){
            $outgaji[$ms_gaji[$idgj]['label']] = $nilai;
            if($ms_gaji[$idgj]['type'] == 1)
                $total_gaji += $nilai;
            else
                $total_gaji -= $nilai;
        }

        return view('detailkaryawan')->with([
            'data' => $data,
            'mutasi' => $mutasi,
            'tgl_gaji' => $tgl_gaji,
            'gaji' => $outgaji,
            'total_gaji' => $total_gaji
        ]);
    }
}
