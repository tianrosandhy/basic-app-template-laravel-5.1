<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use App\Model\MdGaji;
use Input;
use DB;
use URL;

class Gaji extends Controller
{
    protected $tb = "cwa_master_gaji";

    public function __construct(){
        $this->middleware('log');
        $this->middleware('authlev:1');
    }

    public function getIndex()
    {
        //
        log_user();
        return view('master')->with([
            'title' => "Master Gaji",
            'curmenu' => 1,
            'cursubmenu' => 18,
            'table' => MdGaji::structure(),
            'content' => MdGaji::content(),
            'action_button' => "master/gaji/create"
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate()
    {
        //
        log_user();
        return view('create')->with([
            'title' => 'Insert Data Gaji',
            'curmenu' => 1,
            'cursubmenu' => 18,
            'form' => MdGaji::structure(),
            'action_button' => "master/gaji/store"
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postStore(Request $request)
    {
        //
        $post = $request->all();
        $try = MdGaji::validate($post);

        if($try){
            $msg['error'] = $try;
            $url = "master/gaji/create";
            foreach($post as $in=>$ps){
                $request->session()->flash($in, $ps);
            }
        }
        else{
            $data = [
                'label' => $post['label'],
                'description' => $post['description'],
                'default_value' => $post['default_value'],
                'type' => $post['type'],
                'occurence' => $post['occurence'],
                'divide_by_absen' => (isset($post['divide_by_absen']) ? $post['divide_by_absen'] : 0),
                'invalid_by_alpa' => (isset($post['invalid_by_alpa']) ? $post['invalid_by_alpa'] : 0),
                'is_visible' => (isset($post['is_visible']) ? $post['is_visible'] : 0),
                'is_ranged_period' => (isset($post['is_ranged_period']) ? $post['is_ranged_period'] : 0),
                'stat' => 1,
            ];
            DB::table($this->tb)->insert($data);

            log_user('INSERT',$data);

            $url = "master/gaji";
            $msg['success'] = "Berhasil menyimpan Gaji";
        }


        return redirect(URL::to($url))->with($msg);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getEdit($id)
    {
        //
        log_user();
         return view('create')->with([
            'title' => 'Update Data Gaji',
            'curmenu' => 1,
            'cursubmenu' => 18,
            'form' => MdGaji::structure(),
            'default_value' => MdGaji::editable($id),
            'action_button' => "master/gaji/update/".$id
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postUpdate(Request $request, $id)
    {
        //
        $post = $request->all();
        $try = MdGaji::validate($post, $id);

        if($try){
            $msg['error'] = $try;
            $url = "master/gaji/edit/".$id;
            foreach($post as $in=>$ps){
                $request->session()->flash($in, $ps);
            }
        }
        else{
            $data = [
                'label' => $post['label'],
                'description' => $post['description'],
                'default_value' => $post['default_value'],
                'type' => $post['type'],
                'occurence' => $post['occurence'],
                'divide_by_absen' => (isset($post['divide_by_absen']) ? $post['divide_by_absen'] : 0),
                'invalid_by_alpa' => (isset($post['invalid_by_alpa']) ? $post['invalid_by_alpa'] : 0),
                'is_visible' => (isset($post['is_visible']) ? $post['is_visible'] : 0),
                'is_ranged_period' => (isset($post['is_ranged_period']) ? $post['is_ranged_period'] : 0),
            ];
            DB::table($this->tb)
            ->where("id", $id)
            ->update($data);

            log_user('UPDATE',$data);

            $url = "master/gaji";
            $msg['success'] = "Berhasil menyimpan data Gaji";
        }


        return redirect(URL::to($url))->with($msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getDestroy($id)
    {
        //
        $query = DB::table($this->tb)
            ->where('id', $id)
            ->update(['stat' => 9]);

        log_user('DELETE', $id);

        return redirect("master/gaji")->with("success","Berhasil menghapus data Gaji");
    }
}
