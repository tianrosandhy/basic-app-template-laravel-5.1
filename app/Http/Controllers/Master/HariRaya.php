<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use App\Model\MdHariRaya;
use Input;
use DB;
use URL;

class HariRaya extends Controller
{
    protected $tb = "cwa_hr";

    public function __construct(){
        $this->middleware('log');
    }

    public function getIndex()
    {
        log_user();
        return view('master')->with([
            'title' => "Master Hari Raya",
            'curmenu' => 1,
            'cursubmenu' => 16,
            'table' => MdHariRaya::structure(),
            'content' => MdHariRaya::content(),
            'action_button' => "master/hari_raya/create"
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate()
    {
        log_user();
        return view('create')->with([
            'title' => 'Insert Data Hari Raya',
            'curmenu' => 1,
            'cursubmenu' => 16,
            'form' => MdHariRaya::structure(),
            'action_button' => "master/hari_raya/store"
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postStore(Request $request)
    {
        //
        $post = $request->all();
        $try = MdHariRaya::validate($post);

        if($try){
            $msg['error'] = $try;
            $url = "master/hari_raya/create";
            foreach($post as $in=>$ps){
                $request->session()->flash($in, $ps);
            }
        }
        else{
            $data = [
                'nama_hr' => $post['nama_hr'],
                'tgl_start' => $post['tgl_start'],
                'tgl_end' => $post['tgl_end'],
                'occurence' => $post['occurence'],
                'stat' => 1
            ];
            DB::table($this->tb)->insert($data);
            log_user('INSERT', $data);
            $url = "master/hari_raya";
            $msg['success'] = "Berhasil menyimpan Hari Raya";
        }


        return redirect(URL::to($url))->with($msg);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getEdit($id)
    {
        log_user();
         return view('create')->with([
            'title' => 'Update Data Hari Raya',
            'curmenu' => 1,
            'cursubmenu' => 16,
            'form' => MdHariRaya::structure(),
            'default_value' => MdHariRaya::editable($id),
            'action_button' => "master/hari_raya/update/".$id
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postUpdate(Request $request, $id)
    {
        //
        $post = $request->all();
        $try = MdHariRaya::validate($post, $id);

        if($try){
            $msg['error'] = $try;
            $url = "master/hari_raya/edit/".$id;
            foreach($post as $in=>$ps){
                $request->session()->flash($in, $ps);
            }
        }
        else{
            $data = [
                'nama_hr' => $post['nama_hr'],
                'tgl_start' => $post['tgl_start'],
                'tgl_end' => $post['tgl_end'],
                'occurence' => $post['occurence']
            ];
            DB::table($this->tb)
            ->where("id", $id)
            ->update($data);

            log_user('UPDATE', $data);

            $url = "master/hari_raya";
            $msg['success'] = "Berhasil menyimpan data Hari Raya";
        }


        return redirect(URL::to($url))->with($msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getDestroy($id)
    {
        //
        $query = DB::table($this->tb)
            ->where('id', $id)
            ->update(['stat' => 9]);

        log_user('DELETE', $id);

        return redirect("master/hari_raya")->with("success","Berhasil menghapus data Hari Raya");
    }
}
