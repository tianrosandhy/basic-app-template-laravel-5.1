<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use App\Model\MdKode;
use App\Model\MdKaryawan;
use App\Model\MdMtKaryawan;
use Input;
use DB;
use URL;



class KodeKaryawan extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    #table name used in this controller
    protected $tb = "cwa_code_type";

    public function __construct(){
        $this->middleware('log');
    }

    public function getIndex()
    {
        //
        log_user();
        return view('master')->with([
            'title' => "Master Kode Karyawan",
            'description' => 'Masing-masing karyawan bisa saja memiliki lebih dari 1 macam kode. Silakan mengatur kode apa saja yang bisa dimiliki oleh karyawan',
            'curmenu' => 1,
            'cursubmenu' => 12,
            'table' => MdKode::structure(),
            'content' => MdKode::content(),
            'action_button' => "master/kode_karyawan/create"
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate()
    {
        //
        log_user();
        return view('create')->with([
            'title' => 'Insert Data Kode Karyawan',
            'curmenu' => 1,
            'cursubmenu' => 12,
            'form' => MdKode::structure(),
            'action_button' => "master/kode_karyawan/store"
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postStore(Request $request)
    {
        //
        $post = $request->all();
        $try = MdKode::validate($post);

        if($try){
            $msg['error'] = $try;
            $url = "master/kode_karyawan/create";
            foreach($post as $in=>$ps){
                $request->session()->flash($in, $ps);
            }
        }
        else{
            $data = [
                'code_name' => $post['code_name'],
                'code_description' => $post['code_description'],
                'type' => $post['type'],
                'stat' => 1
            ];
            DB::table($this->tb)->insert($data);

            log_user('INSERT', $data);

            $url = "master/kode_karyawan";
            $msg['success'] = "Berhasil menyimpan kode karyawan";
        }


        return redirect(URL::to($url))->with($msg);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getEdit($id)
    {
        //
        log_user();
         return view('create')->with([
            'title' => 'Update Data Kode Karyawan',
            'curmenu' => 1,
            'cursubmenu' => 12,
            'form' => MdKode::structure(),
            'default_value' => Mdkode::editable($id),
            'action_button' => "master/kode_karyawan/update/".$id
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postUpdate(Request $request, $id)
    {
        //
        $post = $request->all();
        $try = MdKode::validate($post, $id);

        if($try){
            $msg['error'] = $try;
            $url = "master/kode_karyawan/edit/".$id;
            foreach($post as $in=>$ps){
                $request->session()->flash($in, $ps);
            }
        }
        else{
            $data = [
                'code_name' => $post['code_name'],
                'code_description' => $post['code_description'],
                'type' => $post['type']
            ];
            DB::table($this->tb)
            ->where("id", $id)
            ->update($data);

            log_user('UPDATE',$data);

            $url = "master/kode_karyawan";
            $msg['success'] = "Berhasil menyimpan kode karyawan";
        }


        return redirect(URL::to($url))->with($msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getDestroy($id)
    {
        //
        $query = DB::table($this->tb)
            ->where('id', $id)
            ->update(['stat' => 9]);

        log_user('DELETE', $id);

        return redirect("master/kode_karyawan")->with("success","Berhasil menghapus data kode karyawan");
    }









    public function getManage(Request $req, $id=0){
        log_user();

        //karyawan based on current mutasi
        $tgl = date("Y-m-d");
        $mutasi = MdMtKaryawan::queryGetMutasi([
            'tgl_berlaku' => $tgl
        ]);


        $list_kode = MdKode::list_kode($id);
        $cari = $list_kode->code_name;

        $data_kode = MdKode::get_code($cari);

        return view('master')->with([
            'title' => "Detail Kode Karyawan",
            'description' => 'Masing-masing karyawan bisa saja memiliki lebih dari 1 macam kode. Silakan mengatur kode apa saja yang bisa dimiliki oleh karyawan',
            'curmenu' => 1,
            'back_button' => true,
            'cursubmenu' => 12,
            'script' => true,
            'table' => [
                'Divisi' => array(
                    'table' => true,
                    'field' => 'divisi',
                ),
                'Nama Karyawan' => array(
                    'table' => true,
                    'field' => 'nama_karyawan',
                ),
                $cari => array(
                    'table' => true,
                    'field' => 'code_name',
                ),
                '' => array(
                    'table' => true,
                    'field' => 'btn'
                )

            ],
            'content' => MdKode::show_detail_code($mutasi, $data_kode, $list_kode->code_name),
            'action_button' => "master/kode_karyawan/create"
        ]);
    }
}
