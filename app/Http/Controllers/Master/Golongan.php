<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use App\Model\MdGolongan;
use Input;
use DB;
use URL;
use Session;
use Validator;

use App\Model\Table\ModelGolongan;
use App\Model\Table\ModelGolonganGaji;


class Golongan extends Controller
{

    #table name used in this controller
    protected $tb = "cwa_golongan";


    public function __construct(){
        $this->middleware('log');
        $this->middleware('authlev:3');
    }

    public function getIndex()
    {
        //
        log_user();
        return view('master')->with([
            'title' => "Master Data Golongan",
            'description' => 'List golongan terkait dapat dicatat di bagian ini',
            'curmenu' => 1,
            'cursubmenu' => 19,
            'table' => MdGolongan::structure(),
            'content' => MdGolongan::content(),
            'action_button' => "master/golongan/create"
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate()
    {
        //
        log_user();
        return view('create')->with([
            'title' => 'Insert Data Golongan',
            'curmenu' => 1,
            'cursubmenu' => 13,
            'form' => MdGolongan::structure(),
            'action_button' => "master/golongan/store"
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postStore(Request $request)
    {
        //
        $post = $request->all();
        $try = MdGolongan::validate($post);
        if($try){
            $msg['error'] = $try;
            $url = "master/golongan/create";
            foreach($post as $in=>$ps){
                $request->session()->flash($in, $ps);
            }
        }
        else{
            $data = [
                'kode_golongan' => $post['kode_golongan'],
                'description' => $post['description'],
                "gaji_pokok" => $post['gaji_pokok'],
                "tunjangan_jabatan" => $post['tunjangan_jabatan'],
                'stat' => 1
            ];
            DB::table($this->tb)->insert($data);
            log_user('INSERT', $data);
            $msg['success'] = "Berhasil menyimpan data master golongan karyawan";
            $url = "master/golongan";
        }
        return Redirect::to(URL::to($url))->with($msg);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getEdit($id)
    {
        //
        log_user();
         return view('create')->with([
            'title' => 'Update Data Golongan',
            'curmenu' => 1,
            'cursubmenu' => 13,
            'form' => MdGolongan::structure(),
            'default_value' => MdGolongan::editable($id),
            'action_button' => "master/golongan/update/".$id
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postUpdate(Request $request, $id)
    {
        //
        $post = $request->all();
        $try = MdGolongan::validate($post, $id);

        if($try){
            $msg['error'] = $try;
            $url = "master/golongan/edit/".$id;
            foreach($post as $in=>$ps){
                $request->session()->flash($in, $ps);
            }
        }
        else{
            $data = [
                'kode_golongan' => $post['kode_golongan'],
                'description' => $post['description'],
                "gaji_pokok" => $post['gaji_pokok'],
                "tunjangan_jabatan" => $post['tunjangan_jabatan'],
            ];
            DB::table($this->tb)
            ->where("id", $id)
            ->update($data);
            log_user('UPDATE', $data);
            $url = "master/golongan";
            $msg['success'] = "Berhasil mengupdate data golongan";
        }

        return redirect(URL::to($url))->with($msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getDestroy($id)
    {
        //
        $query = DB::table($this->tb)
            ->where('id', $id)
            ->update(['stat' => 9]);

        log_user('DELETE', $id);

        return redirect("master/golongan")->with("success","Berhasil menghapus data golongan");
    }


    public function getUpdateGaji($id){
        $tgl = date('Y-m-d');

        $get = ModelGolongan::find($id);
        $datagaji = ModelGolonganGaji::where('id_golongan', $id)->get();
        $current_active = MdGolongan::golongan_gaji_by_tanggal($tgl);
        $curr = [];
        foreach($current_active as $ca){
            $curr[$ca->id][] = date("Y-m-d", strtotime($ca->tgl_berlaku));
        }


        return view('boxgolongan')->with([
            'title' => 'Manage Kode Golongan',
            'row' => $get,
            'datagaji' => $datagaji,
            'curr' => $curr,
            'action_button' => 'master/golongan/save-gaji/'.$id,
            'type' => 'update',

        ]);
    }



    public function getDelDetailgaji(Request $req){
        $post = $req->all();

        $sql = ModelGolonganGaji::where('stat','<>',9)
            ->where('id_golongan', $post['id'])
            ->where('gaji_pokok', $post['gaji'])
            ->where('tunjangan_jabatan', $post['tunjangan'])
            ->where('tgl_berlaku', date('Y-m-d', strtotime($post['tgl'])))
            ->get();

        if(count($sql) > 0){
            $sql = ModelGolonganGaji::where('stat','<>',9)
                ->where('id_golongan', $post['id'])
                ->where('gaji_pokok', $post['gaji'])
                ->where('tunjangan_jabatan', $post['tunjangan'])
                ->where('tgl_berlaku', date('Y-m-d', strtotime($post['tgl'])))
                ->delete();
            $out = ['type' => 'success', 'message' => 'Berhasil menghapus data detail gaji'];            
        }
        else{
            $out = ['type' => 'error', 'message' => 'Data gaji tidak ditemukan'];
        }

        return json_encode($out);
    }



    public function getSaveGaji(Request $req, $id){
        $post = $req->all();
        $validate = Validator::make($post, [
            'id_golongan' => 'required|integer',
            'gaji_pokok' => 'required|integer',
            'tgl_berlaku' => 'required|date'
        ]);

        if($validate->fails()){
            $message = array_values(json_decode(json_encode($validate->messages()), true));
            $out['type'] = 'error';
            $out['message'] = $message[0][0];
        }
        else{
            $cek = ModelGolonganGaji::where('stat', '<>', 9)
            ->where('id_golongan', $post['id_golongan'])
            ->where('tgl_berlaku', $post['tgl_berlaku'])
            ->get();

            if(count($cek) > 0){
                ModelGolonganGaji::where('stat', '<>', 9)
                ->where('id_golongan', $post['id_golongan'])
                ->where('tgl_berlaku', $post['tgl_berlaku'])
                ->update([
                    'gaji_pokok' => $post['gaji_pokok'],
                    'tunjangan_jabatan' => $post['tunjangan_jabatan']
                ]);
            }
            else{
                //save
                $gj = new ModelGolonganGaji();
                $gj->id_golongan = $post['id_golongan'];
                $gj->gaji_pokok = $post['gaji_pokok'];
                $gj->tunjangan_jabatan = $post['tunjangan_jabatan'];
                $gj->tgl_berlaku = $post['tgl_berlaku'];
                $gj->stat = 1;
                $gj->save();
            }

            $out['type'] = 'success';
            $out['message'] = 'Berhasil menyimpan data gaji golongan';            
        }
        return json_encode($out);
    }
}
