<?php
namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\MdMtGaji;
use Excel;
use DB;
use Validator;

class MasterKaryawanUpload extends Controller
{
    public function __construct(Request $req){
        $this->request = $req;
        $this->middleware('log');
        $this->middleware('authlev:2');
    }

    public function index(){
        log_user();

        return view("upload-master-karyawan")->with([
            'title' => 'Upload Data Master Karyawan',
            'curmenu' => 1,
            'cursubmenu' => 110,
        ]);
    }

    public function check(){
        $this->validate($this->request, [
            'excel' => 'required|mimes:xls,xlsx'
        ]);
        $excel = $this->request->excel;

        $importedData = [];

        $out = Excel::load($excel)->toArray();

        $mapField = [
            'nama',
            'nik',
            'npwp',
            'email',
            'departemen',
            'ttl',
            'jk',
            'alamat',
            'telp',
            'tgl_masuk',
            'no_rek',
            'status_transfer',
            'divisi',
            'jabatan',
            'golongan'
        ];

        $divisi = DB::table('cwa_divisi')->where('stat', 1)->get();
        $jabatan = DB::table('cwa_jabatan')->where('stat', 1)->get();
        $golongan = DB::table('cwa_golongan')->where('stat', 1)->get();

        foreach($out as $idx => $row){
            if($idx == 0){
                // skip header field
                continue;
            }
            if(count($row) < 14){
                // bad request. invalid file
                abort(400);
            }

            $temp = [];
            foreach($mapField as $fld_index => $fld_name){
                if($fld_name == 'jk'){
                    // translate gender
                    if(in_array(strtolower($row[$fld_index]), ['l', 'laki-laki', 'pria', 'm', 'male', '1'])){
                        $gender = 1;
                    }
                    else if(strlen($row[$fld_index]) > 0){
                        $gender = 2;
                    }
                    else{
                        $gender = null;
                    }
                    $temp[$fld_name] = $gender;
                }
                else if($fld_name == 'tgl_masuk'){
                    $tgl_masuk = null;
                    if(strlen($row[$fld_index]) > 0){
                        $tgl_masuk = date('Y-m-d', strtotime($row[$fld_index]));
                    }
                    $temp[$fld_name] = $tgl_masuk;
                }
                else if($fld_name == 'status_transfer'){
                    $stt_transfer = null;
                    if(in_array(strtolower($row[$fld_index]), ['transfer', 'trf', 't'])){
                        $stt_transfer = 1;
                    }
                    else if(strlen($row[$fld_index]) > 0){
                        $stt_transfer = 0;
                    }
                    $temp[$fld_name] = $stt_transfer;
                }
                else if($fld_name == 'divisi'){
                    $input_divisi = null;
                    if(strlen($row[$fld_index]) > 0){
                        foreach($divisi as $rd){
                            if(trim(strtolower($rd->nama_divisi)) == trim($row[$fld_index])){
                                $input_divisi = $rd->id;
                            }
                        }
                    }
                    $temp[$fld_name] = $input_divisi;
                }
                else if($fld_name == 'jabatan'){
                    $input_jabatan = null;
                    if(strlen($row[$fld_index]) > 0){
                        foreach($jabatan as $rd){
                            if(trim(strtolower($rd->nama_jabatan)) == trim($row[$fld_index])){
                                $input_jabatan = $rd->id;
                            }
                        }
                    }
                    $temp[$fld_name] = $input_jabatan;                    
                }
                else if($fld_name == 'golongan'){
                    $input_golongan = null;
                    if(strlen($row[$fld_index]) > 0){
                        foreach($golongan as $rd){
                            if(trim(strtolower($rd->kode_golongan)) == trim($row[$fld_index])){
                                $input_golongan = $rd->id;
                            }
                        }
                    }
                    $temp[$fld_name] = $input_golongan;                    
                }
                else{
                    // default string value
                    $temp[$fld_name] = strval($row[$fld_index]);
                }
            }
            $importedData[] = $temp;

        }

        return view("upload-master-karyawan")->with([
            'title' => 'Konfirmasi Upload Data Karyawan',
            'curmenu' => 1,
            'cursubmenu' => 110,
            'data' => [
                'importedData' => $importedData,
            ],
            'divisi' => $divisi,
            'jabatan' => $jabatan,
            'golongan' => $golongan,
        ]);
    }

    public function example(){
        log_user();
        //buat file excel
        $filename = "Contoh Format Upload Karyawan";

        Excel::create($filename, function($excel){
          $excel->sheet('Data Master Karyawan', function($sheet){
            $sheet->row(1, [
                'FORM UPLOAD DATA MASTER KARYAWAN'
            ]);
            $n = 2;
            $sheet->row($n, [
              'Nama Karyawan',
              'NIK',
              'NPWP',
              'Email',
              'Departemen',
              'Tempat Tgl Lahir',
              'Gender',
              'Alamat',
              'Telepon',
              'Tanggal Masuk',
              'No Rekening',
              'Status Transfer',
              'Divisi',
              'Jabatan',
              'Golongan'
            ]);
          });
        })->download('xlsx');
    }

    public function store(){
        $data = json_decode($this->request->data, true);
        if(!isset($data['importedData'])){
            return redirect()->back()->with('error', 'Failed to process data. Please try again later.');
        }

        $importedCount = 0;
        foreach($data['importedData'] as $list){
            $forsave = $list;
            $forsave['stat'] = 1;
            unset($forsave['divisi']);
            unset($forsave['jabatan']);
            unset($forsave['golongan']);

            // save data master karyawan dulu
            $idk = DB::table('cwa_karyawan')->insertGetId($forsave);

            // save data mutasi karyawan jika ada
            if(isset($list['divisi']) && isset($list['golongan']) && isset($list['jabatan'])){
                DB::table('cwa_mutasi_karyawan')->insert([
                    'id_karyawan' => $idk,
                    'id_divisi' => $list['divisi'],
                    'id_jabatan' => $list['jabatan'],
                    'id_golongan' => $list['golongan'],
                    'tgl_berlaku' => $list['tgl_masuk'],
                    'stat' => 1,
                ]);
            }
            $importedCount++;
        }

        return redirect('master/karyawan')->with('success',  $importedCount . ' data karyawan berhasil diimport');
    }
}