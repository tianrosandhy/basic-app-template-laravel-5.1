<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use App\Model\MdJabatan;
use Input;
use DB;
use URL;


class Jabatan extends Controller
{

    protected $tb = "cwa_jabatan";

    public function __construct(){
        $this->middleware('log');
    }

    public function getIndex()
    {
        log_user();
        return view('master')->with([
            'title' => "Master Data Jabatan",
            'curmenu' => 1,
            'cursubmenu' => 14,
            'table' => MdJabatan::structure(),
            'content' => MdJabatan::content(),
            'action_button' => "master/jabatan/create"
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate()
    {
        log_user();
        return view('create')->with([
            'title' => 'Insert Data Jabatan',
            'curmenu' => 1,
            'cursubmenu' => 14,
            'form' => MdJabatan::structure(),
            'action_button' => "master/jabatan/store"
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postStore(Request $request)
    {
        //
        $post = $request->all();
        $try = MdJabatan::validate($post);

        if($try){
            $msg['error'] = $try;
            $url = "master/jabatan/create";
            foreach($post as $in=>$ps){
                $request->session()->flash($in, $ps);
            }
        }
        else{
            $data = [
                'nama_jabatan' => $post['nama_jabatan'],
                'description' => $post['description'],
                'stat' => 1
            ];
            DB::table($this->tb)->insert($data);
            log_user('INSERT', $data);
            $url = "master/jabatan";
            $msg['success'] = "Berhasil menyimpan jabatan";
        }


        return redirect(URL::to($url))->with($msg);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getEdit($id)
    {
        log_user();
         return view('create')->with([
            'title' => 'Update Data jabatan',
            'curmenu' => 1,
            'cursubmenu' => 12,
            'form' => MdJabatan::structure(),
            'default_value' => MdJabatan::editable($id),
            'action_button' => "master/jabatan/update/".$id
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postUpdate(Request $request, $id)
    {
        //
        $post = $request->all();
        $try = MdJabatan::validate($post, $id);

        if($try){
            $msg['error'] = $try;
            $url = "master/jabatan/edit/".$id;
            foreach($post as $in=>$ps){
                $request->session()->flash($in, $ps);
            }
        }
        else{
            $data = [
                'nama_jabatan' => $post['nama_jabatan'],
                'description' => $post['description']
            ];
            DB::table($this->tb)
            ->where("id", $id)
            ->update($data);

            log_user('UPDATE', $data);

            $url = "master/jabatan";
            $msg['success'] = "Berhasil menyimpan jabatan";
        }


        return redirect(URL::to($url))->with($msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getDestroy($id)
    {
        //
        $query = DB::table($this->tb)
            ->where('id', $id)
            ->update(['stat' => 9]);

        log_user('DELETE', $id);

        return redirect("master/jabatan")->with("success","Berhasil menghapus data jabatan");
    }

}
