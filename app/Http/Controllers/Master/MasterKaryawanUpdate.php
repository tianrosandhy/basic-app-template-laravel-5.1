<?php
namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\MdMtGaji;
use Excel;
use DB;
use Validator;

class MasterKaryawanUpdate extends Controller
{
    public function __construct(Request $req){
        $this->request = $req;
        $this->middleware('log');
        $this->middleware('authlev:2');
    }

    public function index(){
        log_user();

        return view("update-master-karyawan")->with([
            'title' => 'Update Data Master Karyawan',
            'curmenu' => 1,
            'cursubmenu' => 111,
        ]);
    }

    public function check(){
        $this->validate($this->request, [
            'excel' => 'required|mimes:xls,xlsx'
        ]);
        $excel = $this->request->excel;

        $importedData = [];

        $out = Excel::load($excel)->toArray();

        $errors = [];

        $mandatoryFields = [
            'nik',
        ];

        $mapField = [
            'nik',
            'nama',
            'npwp',
            'email',
            'departemen',
            'ttl',
            'jk',
            'alamat',
            'telp',
            'tgl_masuk',
            'no_rek',
            'status_transfer',
            'bpjs_kesehatan',
            'tgl_daftar_bpjs_kesehatan',
            'bpjs_tk',
            'tgl_daftar_bpjs_tk',
            'status_ptkp',
        ];

        $mapHeader = [];

        foreach($out as $idx => $row){
            if (empty($mapHeader) && empty($errors)) {
                // fill map header first
                foreach ($row as $n => $cell) {
                    foreach ($mapField as $fi => $fname) {
                        $fn = str_replace('_', ' ', $fname);
                        if (strtolower(trim($cell)) == strtolower($fn)) {
                            $mapHeader[$fname] = $n;
                        }
                    }
                }

                foreach ($mandatoryFields as $fld) {
                    if (!isset($mapHeader[$fld])) {
                        $errors[] = 'Empty mandatory field "'. $fld .'"';
                    }
                }

                continue;
            }

            $temp = [];
            foreach ($row as $n => $cell) {
                $fld_name = null;
                $fld_index = null;
                foreach ($mapHeader as $fname => $fi) {
                    if ($fi == $n) {
                        $fld_index = $fi;
                        $fld_name = $fname;
                    }
                }

                if (empty($fld_name)) {
                    continue;
                }

                if($fld_name == 'jk'){
                    // translate gender
                    if(in_array(strtolower($row[$fld_index]), ['l', 'laki-laki', 'pria', 'm', 'male', '1'])){
                        $gender = 1;
                    }
                    else if(strlen($row[$fld_index]) > 0){
                        $gender = 2;
                    }
                    else{
                        $gender = null;
                    }
                    $temp[$fld_name] = $gender;
                }
                else if($fld_name == 'tgl_masuk'){
                    $tgl_masuk = null;
                    if(strlen($row[$fld_index]) > 0){
                        $tgl_masuk = date('Y-m-d', strtotime($row[$fld_index]));
                    }
                    $temp[$fld_name] = $tgl_masuk;
                }
                else if($fld_name == 'status_transfer'){
                    $stt_transfer = null;
                    if(in_array(strtolower($row[$fld_index]), ['transfer', 'trf', 't'])){
                        $stt_transfer = 1;
                    }
                    else if(strlen($row[$fld_index]) > 0){
                        $stt_transfer = 0;
                    }
                    $temp[$fld_name] = $stt_transfer;
                }
                else if ($fld_name == 'bpjs_kesehatan' || $fld_name == 'bpjs_tk') {
                    $input_bpjs = null;
                    if (strlen($row[$fld_index]) > 0) {
                        if (in_array(trim(strtolower($row[$fld_index])), ['1', 'y', 'ya', 'yes'])) {
                            $input_bpjs = 1;
                        } else {
                            $input_bpjs = 0;
                        }
                    }
                    $temp[$fld_name] = $input_bpjs;
                }
                else if ($fld_name == 'tgl_daftar_bpjs_kesehatan' || $fld_name == 'tgl_daftar_bpjs_tk') {
                    $input_tgl = null;
                    if (strlen($row[$fld_index]) > 0) {
                        if (strtotime($row[$fld_index]) > 0) {
                            $input_tgl = date('Y-m-d', strtotime($row[$fld_index]));
                        }
                    }
                    $temp[$fld_name] = $input_tgl;
                }
                else if ($fld_name == 'status_ptkp') {
                    $input_ptkp = null;
                    if (strlen($row[$fld_index]) > 0) {
                        foreach (config('app.status_ptkp') as $ptkp_code => $ptkp_label) {
                            if (in_array(strtolower(trim($row[$fld_index])), [strtolower($ptkp_code), strtolower($ptkp_label)])) {
                                $input_ptkp = $ptkp_code;
                            }
                        }
                    }
                    $temp[$fld_name] = $input_ptkp;
                }
                else{
                    // default string value
                    $temp[$fld_name] = strval($row[$fld_index]);
                }
            }

            $importedData[] = $temp;
        }

        return view("update-master-karyawan")->with([
            'title' => 'Konfirmasi Update Data Karyawan',
            'curmenu' => 1,
            'cursubmenu' => 110,
            'data' => [
                'importedData' => $importedData,
            ],
            'mapHeader' => $mapHeader,
            'err' => $errors,
        ]);
    }

    public function example(){
        log_user();
        //buat file excel
        $filename = "Contoh Format Update Karyawan";

        Excel::create($filename, function($excel){
          $excel->sheet('Data Master Karyawan', function($sheet){
            $sheet->row(1, [
                'FORM UPDATE DATA MASTER KARYAWAN'
            ]);
            $n = 2;
            $sheet->row($n, [
              'NIK',
              'Nama',
              'NPWP',
              'Email',
              'Departemen',
              'TTL',
              'Gender',
              'Alamat',
              'Telepon',
              'Tgl Masuk',
              'No Rekening',
              'Status Transfer',
              'BPJS Kesehatan',
              'Tgl Daftar BPJS Kesehatan',
              'BPJS TK',
              'Tgl Daftar BPJS TK',
              'Status PTKP',
            ]);
          });
        })->download('xlsx');
    }

    public function store(){
        $data = json_decode($this->request->data, true);
        if(!isset($data['importedData'])){
            return redirect()->back()->with('error', 'Failed to process data. Please try again later.');
        }

        $importedCount = 0;
        foreach($data['importedData'] as $list){
            // tweak maaf
            if (array_key_exists('bpjs_kesehatan', $list)) {
                $list['is_bpjs_kesehatan'] = $list['bpjs_kesehatan'];
                unset($list['bpjs_kesehatan']);
            }
            if (array_key_exists('bpjs_tk', $list)) {
                $list['is_bpjs_tk'] = $list['bpjs_tk'];
                unset($list['bpjs_tk']);
            }
            if (array_key_exists('gender', $list)) {
                $list['jk'] = $list['gender'];
                unset($list['gender']);
            }
            if (array_key_exists('no_rekening', $list)) {
                $list['no_rek'] = $list['no_rekening'];
                unset($list['no_rekening']);
            }
            
            $grab = DB::table('cwa_karyawan')->where('nik', $list['nik'])->update($list);
            $importedCount++;
        }

        return redirect('master/karyawan')->with('success',  $importedCount . ' data karyawan berhasil diupdate');
    }
}