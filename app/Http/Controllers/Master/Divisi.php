<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use App\Model\MdDivisi;
use Input;
use DB;
use URL;
use Session;

class Divisi extends Controller
{

    #table name used in this controller
    protected $tb = "cwa_divisi";


    public function __construct(){
        $this->middleware('log');
    }

    public function getIndex()
    {
        //
        log_user();
        return view('master')->with([
            'title' => "Master Data Cabang",
            'description' => 'List divisi terkait dapat dicatat di bagian ini',
            'curmenu' => 1,
            'cursubmenu' => 13,
            'table' => MdDivisi::structure(),
            'content' => MdDivisi::content(),
            'action_button' => "master/divisi/create"
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate()
    {
        //
        log_user();
        return view('create')->with([
            'title' => 'Insert Data Cabang',
            'curmenu' => 1,
            'cursubmenu' => 13,
            'form' => MdDivisi::structure(),
            'action_button' => "master/divisi/store"
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postStore(Request $request)
    {
        //
        $post = $request->all();
        $try = MdDivisi::validate($post);
        if($try){
            $msg['error'] = $try;
            $url = "master/divisi/create";
            foreach($post as $in=>$ps){
                $request->session()->flash($in, $ps);
            }
        }
        else{
            $data = [
                'nama_divisi' => $post['nama_divisi'],
                'deskripsi' => $post['deskripsi'],
                'sort_no' => $post['sort_no'] ?? 0,
                'stat' => 1
            ];
            DB::table($this->tb)->insert($data);
            log_user('INSERT', $data);
            $msg['success'] = "Berhasil menyimpan data master divisi";
            $url = "master/divisi";
        }
        return Redirect::to(URL::to($url))->with($msg);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getEdit($id)
    {
        //
        log_user();
         return view('create')->with([
            'title' => 'Update Data Cabang',
            'curmenu' => 1,
            'cursubmenu' => 13,
            'form' => MdDivisi::structure(),
            'default_value' => MdDivisi::editable($id),
            'action_button' => "master/divisi/update/".$id
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postUpdate(Request $request, $id)
    {
        //
        $post = $request->all();
        $try = Mddivisi::validate($post, $id);

        if($try){
            $msg['error'] = $try;
            $url = "master/divisi/edit/".$id;
            foreach($post as $in=>$ps){
                $request->session()->flash($in, $ps);
            }
        }
        else{
            $data = [
                'nama_divisi' => $post['nama_divisi'],
                'deskripsi' => $post['deskripsi'],
                'sort_no' => $post['sort_no'] ?? 0,
            ];
            DB::table($this->tb)
            ->where("id", $id)
            ->update($data);
            log_user('UPDATE', $data);
            $url = "master/divisi";
            $msg['success'] = "Berhasil menyimpan data divisi";
        }

        return redirect(URL::to($url))->with($msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getDestroy($id)
    {
        //
        $query = DB::table($this->tb)
            ->where('id', $id)
            ->update(['stat' => 9]);

        log_user('DELETE', $id);

        return redirect("master/divisi")->with("success","Berhasil menghapus data cabang");
    }
}
