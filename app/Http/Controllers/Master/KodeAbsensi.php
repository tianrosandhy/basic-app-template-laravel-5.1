<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use App\Model\MdKodeAbsen;
use Input;
use DB;
use URL;

class KodeAbsensi extends Controller
{
    protected $tb = "cwa_kode_absen";

    public function __construct(){
        $this->middleware('log');
    }

    public function getIndex()
    {
        log_user();
        return view('master')->with([
            'title' => "Master Kode Absensi",
            'curmenu' => 1,
            'cursubmenu' => 17,
            'table' => MdKodeAbsen::structure(),
            'content' => MdKodeAbsen::content(),
            'action_button' => "master/kode_absensi/create"
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate()
    {
        log_user();
        return view('create')->with([
            'title' => 'Insert Data Kode Absensi',
            'curmenu' => 1,
            'cursubmenu' => 17,
            'form' => MdKodeAbsen::structure(),
            'action_button' => "master/kode_absensi/store"
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postStore(Request $request)
    {
        //
        $post = $request->all();
        $try = MdKodeAbsen::validate($post);

        if($try){
            $msg['error'] = $try;
            $url = "master/kode_absensi/create";
            foreach($post as $in=>$ps){
                $request->session()->flash($in, $ps);
            }
        }
        else{
            $data = [
                'kode_absen' => $post['kode_absen'],
                'label' => $post['label'],
                'bobot' => $post['bobot'],
                'color' => $post['color'],
                'include_jadwal' => 1,
                'stat' => 1
            ];
            DB::table($this->tb)->insert($data);

            log_user('INSERT', $data);

            $url = "master/kode_absensi";
            $msg['success'] = "Berhasil menyimpan Kode Absensi";
        }


        return redirect(URL::to($url))->with($msg);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getEdit($id)
    {
        log_user();
         return view('create')->with([
            'title' => 'Update Data Kode Absensi',
            'curmenu' => 1,
            'cursubmenu' => 17,
            'form' => MdKodeAbsen::structure(),
            'default_value' => MdKodeAbsen::editable($id),
            'action_button' => "master/kode_absensi/update/".$id
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postUpdate(Request $request, $id)
    {
        //
        $post = $request->all();
        $try = MdKodeAbsen::validate($post, $id);

        if($try){
            $msg['error'] = $try;
            $url = "master/kode_absensi/edit/".$id;
            foreach($post as $in=>$ps){
                $request->session()->flash($in, $ps);
            }
        }
        else{
            $data = [
                'kode_absen' => $post['kode_absen'],
                'label' => $post['label'],
                'bobot' => $post['bobot'],
                'include_jadwal' => 1,
                'color' => $post['color']
            ];

            DB::table($this->tb)
            ->where("id", $id)
            ->update($data);

            log_user('UPDATE', $data);

            $url = "master/kode_absensi";
            $msg['success'] = "Berhasil menyimpan data Kode Absensi";
        }


        return redirect(URL::to($url))->with($msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getDestroy($id)
    {
        //
        $query = DB::table($this->tb)
            ->where('id', $id)
            ->update(['stat' => 9]);

        log_user('DELETE', $id);

        return redirect("master/kode_absensi")->with("success","Berhasil menghapus data Kode Absensi");
    }











}
