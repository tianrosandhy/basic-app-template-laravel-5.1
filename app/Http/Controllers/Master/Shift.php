<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use App\Model\MdShift;
use Input;
use DB;
use URL;


class Shift extends Controller
{
    protected $tb = "cwa_shift";

    public function __construct(){
        $this->middleware('log');
    }

    public function getIndex()
    {
        //
        log_user();
        return view('master')->with([
            'title' => "Master Shift Kerja",
            'curmenu' => 1,
            'cursubmenu' => 15,
            'table' => MdShift::structure(),
            'content' => MdShift::content(),
            'action_button' => "master/shift/create"
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate()
    {
        //
        log_user();
        return view('create')->with([
            'title' => 'Insert Data Shift Kerja',
            'curmenu' => 1,
            'cursubmenu' => 15,
            'form' => MdShift::structure(),
            'action_button' => "master/shift/store"
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postStore(Request $request)
    {
        //
        $post = $request->all();
        $try = MdShift::validate($post);

        if($try){
            $msg['error'] = $try;
            $url = "master/shift/create";
            foreach($post as $in=>$ps){
                $request->session()->flash($in, $ps);
            }
        }
        else{
            $data = [
                'nama_shift' => $post['nama_shift'],
                'kode_shift' => $post['kode_shift'],
                'jam_masuk' => $post['jam_masuk'],
                'jam_pulang' => $post['jam_pulang'],
                'description' => $post['description'],
                'stat' => 1
            ];
            DB::table($this->tb)->insert($data);
            log_user('INSERT', $data);
            $url = "master/shift";
            $msg['success'] = "Berhasil menyimpan Shift Kerja";
        }


        return redirect(URL::to($url))->with($msg);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getEdit($id)
    {
        //
        log_user();
         return view('create')->with([
            'title' => 'Update Data Shift Kerja',
            'curmenu' => 1,
            'cursubmenu' => 15,
            'form' => MdShift::structure(),
            'default_value' => MdShift::editable($id),
            'action_button' => "master/shift/update/".$id
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postUpdate(Request $request, $id)
    {
        //
        $post = $request->all();
        $try = MdShift::validate($post, $id);

        if($try){
            $msg['error'] = $try;
            $url = "master/shift/edit/".$id;
            foreach($post as $in=>$ps){
                $request->session()->flash($in, $ps);
            }
        }
        else{
            $data = [
                'nama_shift' => $post['nama_shift'],
                'kode_shift' => $post['kode_shift'],
                'jam_masuk' => $post['jam_masuk'],
                'jam_pulang' => $post['jam_pulang'],
                'description' => $post['description']
            ];
            DB::table($this->tb)
            ->where("id", $id)
            ->update($data);

            log_user('UPDATE', $data);
            $url = "master/shift";
            $msg['success'] = "Berhasil menyimpan Shift Kerja";
        }


        return redirect(URL::to($url))->with($msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getDestroy($id)
    {
        //
        $query = DB::table($this->tb)
            ->where('id', $id)
            ->update(['stat' => 9]);

        log_user('DELETE', $id);

        return redirect("master/shift")->with("success","Berhasil menghapus data Shift Kerja");
    }
}
