<?php

namespace App\Http\Controllers\Gaji;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use App\Model\MdMtKaryawan;
use App\Model\MdKaryawan;
use App\Model\MdJadwal;
use App\Model\MdMtGaji;
use Input;
use DB;
use URL;

class MutasiGaji extends Controller
{
	public function __construct(){
        $this->middleware('log');
        $this->middleware('authlev:3');
    }

    public function getMigrate(){
        $sql = DB::table('cwa_detail_gaji')
            ->whereIn('id_gaji', [1,3])->get();

        $data = [];
        foreach($sql as $row){
            $data[$row->id_karyawan][$row->tanggal][$row->id_gaji] = $row->nilai;
        }

        $out = [];
        foreach($data as $idkar => $datagaji){
            foreach($datagaji as $tgl => $gaji){
                $gaji_pokok = isset($gaji[1]) ? intval($gaji[1]) : 0;
                $tunjangan = isset($gaji[3]) ? intval($gaji[3]) : 0;

                if($gaji_pokok == 1600000){
                    $out[$idkar][$tgl] = '0';
                }
                else if($gaji_pokok == 1960000){
                    if($tunjangan > 400000){
                        $out[$idkar][$tgl] = '2';
                        if($tunjangan == 400000){
                            $out[$idkar][$tgl] = '2A';
                        }
                        else if($tunjangan == 600000){
                            $out[$idkar][$tgl] = '2B';
                        }
                        else if($tunjangan == 700000){
                            $out[$idkar][$tgl] = '2C';
                        }
                        else if($tunjangan == 800000){
                            $out[$idkar][$tgl] = '2D';
                        }
                    }
                    else{
                        $out[$idkar][$tgl] = '1';
                        if($tunjangan == 0){
                            $out[$idkar][$tgl] = '1A';
                        }
                        else if($tunjangan == 100000){
                            $out[$idkar][$tgl] = '1B';
                        }
                        else if($tunjangan == 200000){
                            $out[$idkar][$tgl] = '1C';
                        }
                        else if($tunjangan == 300000){
                            $out[$idkar][$tgl] = '1D';
                        }
                    }
                }
                else if($gaji_pokok == 2400000){
                    $out[$idkar][$tgl] = '3';
                    if($tunjangan == 500000){
                        $out[$idkar][$tgl] = '3A';
                    }
                    else if($tunjangan == 750000){
                        $out[$idkar][$tgl] = '3C';
                    }
                    else if($tunjangan == 1000000){
                        $out[$idkar][$tgl] = '3D';
                    }
                }
                else if($gaji_pokok == 3300000){
                    $out[$idkar][$tgl] = '4';
                    if($tunjangan == 0){
                        $out[$idkar][$tgl] = '4A';
                    }
                    else if($tunjangan == 500000){
                        $out[$idkar][$tgl] = '4B';
                    }
                    else if($tunjangan == 750000){
                        $out[$idkar][$tgl] = '4C';
                    }
                    else if($tunjangan == 1000000){
                        $out[$idkar][$tgl] = '4D';
                    }
                }


            }
        }


        //prepare list golongan
        $gol = DB::table('cwa_golongan')->get();
        $golongan = [];
        foreach($gol as $row){
            $golongan[$row->kode_golongan] = $row->id;
        }


        //save data
        $n = 0;
        foreach($out as $idkar => $data){
            foreach($data as $tgl => $kode){
                DB::table('cwa_mutasi_karyawan')
                    ->where('id_karyawan', $idkar)
                    ->where('tgl_berlaku', date("Y-m-d", strtotime($tgl)))
                    ->update([
                        'id_golongan' => $golongan[$kode]
                    ]);
            }
        }

    }

    public function getIndex(Request $request){
        log_user();
    	$bulan = Input::get("bulan") ? intval(Input::get("bulan")) : date("n");
        $tahun = Input::get("tahun") ? intval(Input::get("tahun")) : date("Y");
        $divisi = Input::get("divisi");

        $gaji = new MdMtGaji([
        	'bulan' => $bulan,
        	'tahun' => $tahun
        ]);
        
        return view("gaji")->with([
            'title' => 'Mutasi Gaji Karyawan',
            'wellname' => "Filter Penggajian",
            'curmenu' => 3,
            'cursubmenu' => 31,
            'default' => [
                'bulan' => $bulan,
                'tahun' => $tahun,
                'divisi' => $divisi
            ],
            'action_button' => 'gaji/mutasi/create?'.$_SERVER['QUERY_STRING'],
            'month_control' => $bulan,
            'listdiv' => MdMtKaryawan::filter("divisi"),
            'master_gaji' => MdMtGaji::master_gaji(1),
            'mutasi' => $gaji->karyawan_gaji($divisi),
            'data_gaji' => $gaji->build_gaji()
        ]);
    }




    public function getCreate(Request $request){
        log_user();
    	$bulan = Input::get("bulan") ? intval(Input::get("bulan")) : date("n");
        $tahun = Input::get("tahun") ? intval(Input::get("tahun")) : date("Y");
        $divisi = Input::get("divisi");

        $gaji = new MdMtGaji([
        	'bulan' => $bulan,
        	'tahun' => $tahun
        ]);

        $selected = MdKaryawan::getKaryawan(Input::get('id'));
        if(count($selected) == 0){
        	return redirect("gaji/mutasi?".$qs)->with("error","Karyawan tersebut tidak ditemukan.");
        }

        return view("creategaji")->with([
        	'title' => "Tambah Data Gaji Tetap Karyawan",
        	'curmenu' => 3,
        	'cursubmenu' => 31,
        	'action_button' => 'gaji/mutasi/create',
        	'selected' => $selected,
        	'default' => [
                'bulan' => $bulan,
                'tahun' => $tahun,
                'divisi' => $divisi
            ],
            'id_karyawan' => Input::get('id'),
            'master_gaji' => MdMtGaji::master_gaji(1),
            'default_gaji' => $gaji->build_gaji()
        ]);
    }



    public function postCreate(Request $request){
    	//validasi : 
    	# karyawan ada atau tidak
    	# kurang dr tangal masuk atau tidak
    	# sudah resign atau belum
        $qs = Input::get("qs");

    	$cek_karyawan = MdKaryawan::getKaryawan($request->input('id'));
        if(count($cek_karyawan) == 0){
        	return redirect("gaji/mutasi?".$qs)->with("error","Karyawan tersebut tidak ditemukan.");
        }

        $tgl = date("Y-m-d", strtotime($request->input('tahun')."-".$request->input('bulan')."-1"));

        if(count($request->input('gaji')) == 0){
        	return redirect("gaji/mutasi?".$qs)->with("error","Mohon inputkan data gaji yang ingin dimasukkan");
        }
        else{
        	foreach($request->input("gaji") as $ind=>$nilai){
        		//input / update per data
        		$cek = DB::table("cwa_detail_gaji")
        		->where("id_karyawan", $request->input("id"))
        		->where("id_gaji", $ind)
        		->where("tanggal",$tgl)
        		->get();

                $active_until_period = null;
                if (isset($request->active_until_period_bulan[$ind]) && isset($request->active_until_period_tahun[$ind])) {
                    $active_until_period = date('Y-m-d', strtotime($request->active_until_period_tahun[$ind] . '-' . $request->active_until_period_bulan[$ind] . '-01'));
                }

        		if(count($cek) > 0){
        			//update
        			DB::table("cwa_detail_gaji")
	        		->where("id_karyawan", $request->input("id"))
	        		->where("id_gaji", $ind)
	        		->where("tanggal",$tgl)
	        		->update([
                        "nilai" => $nilai,
                        "active_until_period" => $active_until_period,
                    ]);

                    log_user('UPDATE', ['nilai' => $nilai]);

	        		$msg = "Berhasil mengupdate data gaji karyawan";
        		}
        		else{
        			//insert
                    $dtx = [
                        'id_karyawan' => $request->input("id"),
                        'id_gaji' => $ind,
                        'tanggal' => $tgl,
                        'nilai' => $nilai,
                        'active_until_period' => $active_until_period,
                        'stat' => 1
                    ];
        			DB::table("cwa_detail_gaji")
        			->insert($dtx);
                    log_user('INSERT', $dtx);

        			$msg = "Berhasil menginput data gaji karyawan";
        		}
        	}
        	//success

        	return redirect("gaji/mutasi?".$qs)->with("success",$msg);
        }

    }

    public function getHistory(Request $request, $id=0){
        log_user();
    	$cek_karyawan = MdKaryawan::getKaryawan($id);
        if(count($cek_karyawan) == 0){
        	return redirect("gaji/mutasi?")->with("error","Karyawan tersebut tidak ditemukan.");
        }


        return view("boxhistory")->with([
        	'title' => 'History Rekap Gaji',
        	'datakar' => $cek_karyawan,
        	'master_gaji' => MdMtGaji::master_gaji(1),
        	'gaji' => MdMtGaji::detail_gaji_karyawan($id),
        ]);
    }

    public function getRemove(Request $request, $id=0, $tgl=0){
    	$cek = DB::table("cwa_detail_gaji")
    	->where("id_karyawan", $id)
    	->where("tanggal", date("Y-m-d", $tgl))
    	->where("stat","<>", 9)
    	->get();

    	if(count($cek) > 0){
	    	$upd = DB::table("cwa_detail_gaji")
	    	->where("id_karyawan", $id)
	    	->where("tanggal", date("Y-m-d", $tgl))
	    	->where("stat","<>", 9)
	    	->delete();
            log_user('DELETE', ['id_karyawan' => $id, 'tanggal' => date("Y-m-d", $tgl)]);
	    	$type = "success";
    	}
    	else
    		$type = "error";

    	return redirect("gaji/mutasi")->with([$type => "Berhasil menghapus data mutasi gaji"]);

    }

}
