<?php

namespace App\Http\Controllers\Gaji;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use App\Model\MdMtKaryawan;
use App\Model\MdKaryawan;
use App\Model\MdJadwal;
use App\Model\MdMtGaji;
use App\Model\MdReport;
use App\Model\EmailGaji;
use App\Model\Table\ModelGajiResign;

use Input;
use DB;
use URL;
use Excel;
use Artisan;


class SlipGaji extends Controller
{
    public function __construct(){
        $this->middleware('log');
        $this->middleware('authlev:3');
    }

    public function index(Request $request){
        log_user();
        $bulan = $request->input("bulan") == null ? date("n") : intval($request->input("bulan"));
        $tahun = $request->input("tahun") == null ? date("Y") : intval($request->input("tahun"));
        $divisi = $request->input("divisi");


        $report = new MdReport([
            'bulan' => $bulan,
            'tahun' => $tahun,
        ]);

        $row = $report->make_mutasi($divisi);
        $report->get_rancangan();
        $report->get_presensi();
        $content = $report->prepare_object();

        $gaji = new MdMtGaji([
            'bulan' => $bulan,
            'tahun' => $tahun,
        ]);

        //siapa tahu data karyawan resign
        $data_resign = ModelGajiResign::with('getKaryawan')
        ->whereIn('tgl_berlaku', $report->calendar)
        ->get();

        $temp_row = [];
        foreach($data_resign as $dt){
            $temp_row[$dt->id_karyawan] = (object)[
                'id' => $dt->id_karyawan,
                'id_karyawan' => $dt->id_karyawan,
                'nama' => $dt->getKaryawan->nama,
                'nama_divisi' => '-resigned-',
                'nama_jabatan' => '',
            ];
        }

        $row = $temp_row + $row;

        $detail = $report->gaji_maker($content, $gaji->build_gaji(1), $gaji->build_gaji(2), $row, false);

        $ls = MdReport::list_gaji();
        foreach($ls as $rls){
            $listgaji[$rls->id] = $rls->label;
        }

        $data_resign = ModelGajiResign::with('getKaryawan')
        ->whereIn('tgl_berlaku', $report->calendar)
        ->get();

        return view("slippack")->with([
            'title' => 'Rekap Gaji Karyawan',
            'curmenu' => 3,
            'cursubmenu' => 33,
            'default' => [
                'bulan' => $bulan,
                'tahun' => $tahun,
                'divisi' => $divisi
            ],
            'listdiv' => MdMtKaryawan::filter("divisi"),
            'listkode' => MdReport::list_kode_absen(),
            'listgaji' => $listgaji,
            'calendar' => $report->calendar,
            'content' => $content,
            'rows' => $row,
            'gaji' => $detail,
        ]);
    }

    public function excelReport(Request $request){
        log_user();
        $bulan = $request->input("bulan") == null ? date("n") : intval($request->input("bulan"));
        $tahun = $request->input("tahun") == null ? date("Y") : intval($request->input("tahun"));
        $divisi = $request->input("divisi");


        $report = new MdReport([
            'bulan' => $bulan,
            'tahun' => $tahun,
        ]);

        $row = $report->make_mutasi($divisi);
        $report->get_rancangan();
        $report->get_presensi();
        $content = $report->prepare_object();

        $gaji = new MdMtGaji([
            'bulan' => $bulan,
            'tahun' => $tahun,
        ]);

        //siapa tahu data karyawan resign
        $data_resign = ModelGajiResign::with('getKaryawan')
        ->whereIn('tgl_berlaku', $report->calendar)
        ->get();

        $temp_row = [];
        foreach($data_resign as $dt){
            $temp_row[$dt->id_karyawan] = (object)[
                'id' => $dt->id_karyawan,
                'id_karyawan' => $dt->id_karyawan,
                'nama' => $dt->getKaryawan->nama,
                'nama_divisi' => '-resigned-',
                'nama_jabatan' => '',
            ];
        }

        $row = $temp_row + $row;

        $detail = $report->gaji_maker($content, $gaji->build_gaji(1), $gaji->build_gaji(2), $row, false);


        $ls = MdReport::list_gaji();
        foreach(collect($ls)->sortBy('occurence')->sortBy('type') as $rls){
            if ($rls->is_visible <> 1) {
                continue;
            }
            $listgaji[$rls->type][$rls->id] = $rls->label;
        }

        //hardcode -_-
        $listgaji[1]['gaji_pokok'] = 'Gaji Pokok';
        $listgaji[1]['tunjangan_jabatan'] = 'Tunjangan Jabatan';
        $listgaji[2]['potongan_terlambat'] = 'Potongan Terlambat';
        $listgaji[2]['potongan_absensi'] = 'Potongan Absensi';
        //tar tambahin manual disini kalo ada model gaji hardcode lainnya
        uasort($listgaji[1], function($a, $b){
            $order = [
                'Gaji Pokok',
                'Potongan Absensi',
                'Tunjangan Jabatan',
                'Tunjangan Lain',
                'Tunjangan Golongan',
                'Tunjangan Penempatan',
                'Bonus PU',
                'Insentive Overtime',
                'Insentif',
                'BPJS Kesehatan',
                'BP Jamsostek',
                'Iuran Suka Duka',
                'Potongan Kasbon',
                'Potongan Denda',
                'Potongan Terlambat',
                'Potongan Lain-Lain'
            ];

            $index_a = in_array($a, $order) ? array_search($a, $order) : 100;
            $index_b = in_array($b, $order) ? array_search($b, $order) : 100;

            return $index_a < $index_b ? -1 : 1;
        });
        uasort($listgaji[2], function($a, $b){
            $order = [
                'Gaji Pokok',
                'Potongan Absensi',
                'Tunjangan Jabatan',
                'Tunjangan Lain',
                'Tunjangan Golongan',
                'Tunjangan Penempatan',
                'Bonus PU',
                'Insentive Overtime',
                'Insentif',
                'BPJS Kesehatan',
                'BP Jamsostek',
                'Iuran Suka Duka',
                'Potongan Kasbon',
                'Potongan Denda',
                'Potongan Terlambat',
                'Potongan Lain-Lain'
            ];

            $index_a = in_array($a, $order) ? array_search($a, $order) : 100;
            $index_b = in_array($b, $order) ? array_search($b, $order) : 100;

            return $index_a < $index_b ? -1 : 1;
        });


        // $data_resign = ModelGajiResign::with('getKaryawan')
        // ->whereIn('tgl_berlaku', $report->calendar)
        // ->get();


        $content = [];
        foreach($row as $kar){
            $item = [
                'name' => $kar->nama,
                'email' => $kar->email,
                'npwp' => $kar->npwp,
                'nik' => $kar->nik,
                'departemen' => $kar->departemen,
                'status_transfer' => $kar->status_transfer,
                'divisi' => $kar->nama_divisi,
                'jabatan' => $kar->nama_jabatan,
                'kode_golongan' => $kar->kode_golongan,
                'no_rek' => $kar->no_rek,
            ];

            if(isset($detail[$kar->id_karyawan])){
                $used_gaji = $detail[$kar->id_karyawan];
                $item['kesimpulan'] = [
                    'presensi' => $used_gaji['bobot'],
                    'batas_bobot' => $used_gaji['batas_bobot'],
                    'surplus' => $used_gaji['surplus'],
                    'defisit' => $used_gaji['defisit'],
                    'total' => $used_gaji['total'],
                ];
                foreach($listgaji as $type => $dtgj){
                    foreach($dtgj as $key => $value){
                        $isigaji = isset($used_gaji[$type][$key]) ? $used_gaji[$type][$key] : 0;
                        $item['gaji'][$type][$value] = $isigaji;
                    }
                }
            }
            else{
                $item['kesimpulan'] = [
                    'presensi' => 0,
                    'batas_bobot' => 0,
                    'surplus' => 0,
                    'defisit' => 0,
                    'total' => 0,
                ];
                foreach($listgaji as $type => $dtgj){
                    foreach($dtgj as $key => $value){
                        $item['gaji'][$type][$value] = 0;
                    }
                }
            }

            $content[] = $item;
        }

        Excel::create('Data Gaji ' . $bulan .'-'.$tahun, function($excel) use ($content, $listgaji){
            $excel->sheet('Data Gaji', function($sheet) use($content, $listgaji){
                $sheet->loadView('excel-slip-gaji', [
                    'data' => $content,
                    'listgaji' => $listgaji
                ]);
            });
        })->download('xlsx');


    }

    public function detail(Request $request, $idkaryawan, $tahun=null, $bulan=null){
        log_user();
        $bulan = $bulan==null ? date("n") : intval($bulan);
        $tahun = $tahun==null ? date("Y") : intval($tahun);


        $report = new MdReport([
            'bulan' => $bulan,
            'tahun' => $tahun,
        ]);

        //month field
        $row = $report->make_mutasi($idkaryawan, "id_karyawan");
        foreach($row as $r){
        	$row = $r;
        }


        if(count($row) == 0){
            //siapa tahu data karyawan resign
            $data_resign = ModelGajiResign::with('getKaryawan')
            ->where('id_karyawan', $idkaryawan)
            ->whereIn('tgl_berlaku', $report->calendar)
            ->first();

            $row = (object)[
                'id' => $data_resign->id_karyawan,
                'id_karyawan' => $data_resign->id_karyawan,
                'nama' => $data_resign->getKaryawan->nama,
                'nama_divisi' => '-resigned-',
                'nama_jabatan' => '',
            ];
        }



        $report->get_rancangan($idkaryawan);
        $report->get_presensi($idkaryawan);

        $content = $report->prepare_object();


        $gaji = new MdMtGaji([
            'bulan' => $bulan,
            'tahun' => $tahun,
        ]);

        //NANTI DILANJUTIN -_-

        $detail = $report->gaji_maker($content, $gaji->build_gaji(1), $gaji->build_gaji(2), $row);


        $ls = MdReport::list_gaji();
        foreach($ls as $rls){
            $listgaji[$rls->id] = $rls->label;
        }


        return view("slip")->with([
            'title' => 'Rekap Gaji Karyawan',
            'curmenu' => 3,
            'cursubmenu' => 33,
            'default' => [
                'bulan' => $bulan,
                'tahun' => $tahun,
                'id_karyawan' => $idkaryawan
            ],
            'listdiv' => MdMtKaryawan::filter("divisi"),
            'listkode' => MdReport::list_kode_absen(),
            'listgaji' => $listgaji,
            'calendar' => $report->calendar,
            'content' => $content,
            'row' => $row,
            'gaji' => $detail,

        ]);
    }

    public function detailOnline(Request $request, $idkaryawan, $tahun=null, $bulan=null)
    {
        log_user();
        $bulan = $bulan==null ? date("n") : intval($bulan);
        $tahun = $tahun==null ? date("Y") : intval($tahun);


        $report = new MdReport([
            'bulan' => $bulan,
            'tahun' => $tahun,
        ]);

        //month field
        $row = $report->make_mutasi($idkaryawan, "id_karyawan");
        foreach($row as $r){
        	$row = $r;
        }


        if(count($row) == 0){
            //siapa tahu data karyawan resign
            $data_resign = ModelGajiResign::with('getKaryawan')
            ->where('id_karyawan', $idkaryawan)
            ->whereIn('tgl_berlaku', $report->calendar)
            ->first();

            $row = (object)[
                'id' => $data_resign->id_karyawan,
                'id_karyawan' => $data_resign->id_karyawan,
                'nama' => $data_resign->getKaryawan->nama,
                'nama_divisi' => '-resigned-',
                'nama_jabatan' => '',
            ];
        }



        $report->get_rancangan($idkaryawan);
        $report->get_presensi($idkaryawan);

        $content = $report->prepare_object();


        $gaji = new MdMtGaji([
            'bulan' => $bulan,
            'tahun' => $tahun,
        ]);

        //NANTI DILANJUTIN -_-

        $detail = $report->gaji_maker($content, $gaji->build_gaji(1), $gaji->build_gaji(2), $row);


        $ls = MdReport::list_gaji();
        foreach($ls as $rls){
            $listgaji[$rls->id] = $rls->label;
        }

        $pdf = \PDF::loadView("slip-epay", [
            'title' => 'Rekap Gaji Karyawan',
            'curmenu' => 3,
            'cursubmenu' => 33,
            'default' => [
                'bulan' => $bulan,
                'tahun' => $tahun,
                'id_karyawan' => $idkaryawan
            ],
            'listdiv' => MdMtKaryawan::filter("divisi"),
            'listkode' => MdReport::list_kode_absen(),
            'listgaji' => $listgaji,
            'calendar' => $report->calendar,
            'content' => $content,
            'row' => $row,
            'gaji' => $detail,
        ]);

        if (!isset($request->without_password)) {
            $pass = str_replace('-', '', $row->nik);
            $pass = '123456'; //dummy
            $pdf->SetProtection(['print', 'copy'], $pass, $pass);
        }

        return $pdf->stream();
    }

    public function blast($tahun, $bulan)
    {
        $resp = EmailGaji::generateCurrentReport($bulan, $tahun);
        if ($resp) {
            return redirect()->back()->with(['success' => 'Berhasil mengantrikan data slip gaji untuk dikirim ke seluruh pegawai. Informasi pengiriman dapat dilihat updatenya di halaman ini nantinya.']);
        } else {
            return redirect()->back()->with(['error' => 'Seluruh data slip gaji sudah pernah diblast sebelumnya. Informasi pengiriman dapat dilihat updatenya di halaman ini nantinya.']);
        }
    }

    public function blastSingle(Request $request, $idkaryawan, $tahun=null, $bulan=null)
    {
        // cek dulu sebelum kirim
        $karyawan = DB::table('cwa_karyawan')->where('id', $idkaryawan)->first();
        if (empty($karyawan)) {
            return redirect()->back()->with([
                'error' => 'Data karyawan tidak ditemukan / sudah dihapus'
            ]);
        }

        $exitCode = Artisan::call('slipgaji:blast', [
            '--id' => $idkaryawan,
            '--tahun' => $tahun,
            '--bulan' => $bulan,
        ]);

        if ($exitCode == 0) {
            return redirect()->back()->with([
                'success' => 'Email slip gaji berhasil dikirim ke ' . $karyawan->email
            ]);
        } else {
            return redirect()->back()->with([
                'error' => 'Ada kesalahan server sehingga email slip gaji tidak dapat dikirimkan ke ' . $karyawan->email
            ]);
        }

    }
}
