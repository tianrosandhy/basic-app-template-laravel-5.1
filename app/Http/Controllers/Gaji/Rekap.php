<?php

namespace App\Http\Controllers\Gaji;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use App\Model\MdMtKaryawan;
use App\Model\MdKaryawan;
use App\Model\MdJadwal;
use App\Model\MdReport;
use App\Model\MdMtGaji;
use App\Model\EmailGaji;
use App\Model\Table\ModelGajiResign;

use Input;
use DB;
use URL;

class Rekap extends Controller
{
    public function __construct(){
        $this->middleware('log');
        $this->middleware('authlev:3');
    }

    public function getIndex(){
        log_user();
        $bulan = Input::get("bulan") ? intval(Input::get("bulan")) : date("n");
        $tahun = Input::get("tahun") ? intval(Input::get("tahun")) : date("Y");
        $divisi = Input::get("divisi");


        $report = new MdReport([
            'bulan' => $bulan,
            'tahun' => $tahun,
        ]);

        //month field
        $row = $report->make_mutasi($divisi);
        $report->get_rancangan(null, $row);
        $report->get_presensi();
        $content = $report->prepare_object();

        $gaji = new MdMtGaji([
            'bulan' => $bulan,
            'tahun' => $tahun,
        ]);


        $data_resign = ModelGajiResign::with('getKaryawan')
        ->whereIn('tgl_berlaku', $report->calendar)
        ->get();

        $email_report = EmailGaji::getCurrentReport($bulan, $tahun);
        $email_report_summary = [
            0 => 0,
            1 => 0,
            9 => 0,
            'total' => 0,
        ];
        foreach ($email_report as $er) {
            $email_report_summary[$er->is_sent] += 1;
            $email_report_summary['total'] += 1;
        }

        //NANTI DILANJUTIN -_-
        return view("reportgaji")->with([
            'title' => 'Rekap Gaji Karyawan',
            'wellname' => 'Filter Gaji',
            'curmenu' => 4,
            'cursubmenu' => 33,
            'default' => [
                'bulan' => $bulan,
                'tahun' => $tahun,
                'divisi' => $divisi
            ],
            'listdiv' => MdMtKaryawan::filter("divisi"),
            'listkode' => MdReport::list_kode_absen(),
            'calendar' => $report->calendar,
            'content' => $content,
            'row' => $row,
            'data_resign' => $data_resign,
            'email_report' => $email_report,
            'email_report_summary' => $email_report_summary,
            'gaji' => $report->gaji_maker($content, $gaji->build_gaji(1), $gaji->build_gaji(2), $row, false),
        ]);
    }


}
