<?php

namespace App\Http\Controllers\Gaji;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use DB;

use Validator;

use App\Model\Table\ModelGajiResign;
use App\Model\Table\ModelResign;
use App\Model\MdMtKaryawan;
use App\Model\Table\ModelMutasiKaryawan;


class GajiResign extends Controller
{
	public $request;

    public function __construct(Request $req){
        $this->middleware('log');
        $this->middleware('authlev:3');
        $this->request = $req;
    }

    public function index(){
        log_user();
        $data = ModelGajiResign::where('stat', '<>', 9)->get();

     	$resign_data = ModelResign::with('karyawan')
     		->where('stat', '<>', 9)->get();

        $data_karyawan = [];
        foreach($resign_data as $res){
            $cek = ModelMutasiKaryawan::with('getKaryawan', 'getDivisi')
                ->where('id_karyawan', $res->id_karyawan)
                ->orderBy('tgl_berlaku', 'DESC')
                ->take(1)
                ->first();

            $data_karyawan[$cek->id_karyawan] = $cek;
        }

     	return view('resign-gaji')->with([
     		'data_gaji' => $data->pluck('bonus','id_karyawan'),
     		'data_tanggal' => $data->pluck('tgl_berlaku','id_karyawan'),
     		'data_resign' => $resign_data,
     		'current_mutasi' => $data_karyawan
     	]);
    }


    public function store(){
    	$validate = Validator::make($this->request->all(),[
    		'id_karyawan' => 'required',
    		'bonus' => 'integer|required',
    		'tgl_berlaku' => 'date'
    	]);

    	if($validate->fails()){
    		return [
    			'type' => 'error',
    			'message' => $validate->messages()->first()
    		];
    	}
    	else{
    		//auto hapus
    		$cek = ModelGajiResign::where([
    			'id_karyawan' => $this->request->id_karyawan 
    		])->delete();

    		ModelGajiResign::insert([
    			'id_karyawan' => $this->request->id_karyawan,
    			'bonus' => $this->request->bonus,
    			'tgl_berlaku' => date('Y-m-d', strtotime($this->request->tgl_berlaku)),
    			'stat' => 1
    		]);

    		return [
    			'type' => 'success',
    			'message' => 'Berhasil menyimpan data bonus'
    		];
    	}
    }

}
