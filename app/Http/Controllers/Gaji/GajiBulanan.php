<?php

namespace App\Http\Controllers\Gaji;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use App\Model\MdMtKaryawan;
use App\Model\MdKaryawan;
use App\Model\MdJadwal;
use App\Model\MdMtGaji;
use Input;
use DB;
use URL;

class GajiBulanan extends Controller
{
    public function __construct(){
        $this->middleware('log');
        $this->middleware('authlev:3');
    }

    public function getIndex(Request $request){
        log_user();
        $bulan = Input::get("bulan") ? intval(Input::get("bulan")) : date("n");
        $tahun = Input::get("tahun") ? intval(Input::get("tahun")) : date("Y");
        $divisi = Input::get("divisi");

        $gaji = new MdMtGaji([
        	'bulan' => $bulan,
        	'tahun' => $tahun
        ]);

	

        return view("gaji2")->with([
            'title' => 'Bonus dan Potongan Bulanan',
            'wellname' => "Filter Penggajian",
            'curmenu' => 3,
            'cursubmenu' => 32,
            'default' => [
                'bulan' => $bulan,
                'tahun' => $tahun,
                'divisi' => $divisi
            ],
            'action_button' => 'gaji/bulanan/create?'.$_SERVER['QUERY_STRING'],
            'month_control' => $bulan,
            'listdiv' => MdMtKaryawan::filter("divisi"),
            'master_gaji' => MdMtGaji::master_gaji(2),
            'mutasi' => $gaji->karyawan_gaji($divisi),
            'data_gaji' => $gaji->build_gaji(2)
        ]);
    }



    public function getCreate(Request $request){
        log_user();
    	$bulan = Input::get("bulan") ? intval(Input::get("bulan")) : date("n");
        $tahun = Input::get("tahun") ? intval(Input::get("tahun")) : date("Y");
        $divisi = Input::get("divisi");

        $gaji = new MdMtGaji([
        	'bulan' => $bulan,
        	'tahun' => $tahun
        ]);

        $selected = MdKaryawan::getKaryawan(Input::get('id'));
        if(count($selected) == 0){
        	return redirect("gaji/bulanan?".$qs)->with("error","Karyawan tersebut tidak ditemukan.");
        }

        return view("creategaji")->with([
        	'title' => "Tambah Data Gaji Bulanan Karyawan",
        	'curmenu' => 3,
        	'cursubmenu' => 32,
        	'action_button' => 'gaji/bulanan/create',
        	'selected' => $selected,
        	'default' => [
                'bulan' => $bulan,
                'tahun' => $tahun,
                'divisi' => $divisi
            ],
            'id_karyawan' => Input::get('id'),
            'master_gaji' => MdMtGaji::master_gaji(2),
            'default_gaji' => $gaji->build_gaji(2),
            'apply_to' => MdMtKaryawan::filter("divisi")
        ]);
    }

    public function postCreate(Request $request){
    	$qs = Input::get("qs");

    	//prepare data input
    	$cek_karyawan = MdKaryawan::getKaryawan($request->input('id'));
        if(count($cek_karyawan) == 0){
        	return redirect("gaji/bulanan?".$qs)->with("error","Karyawan tersebut tidak ditemukan.");
        }

        $tgl = date("Y-m-d", strtotime($request->input('tahun')."-".$request->input('bulan')."-1"));
        $bulan = date("n", strtotime($tgl));
        $tahun = date("Y", strtotime($tgl));

    	$gaji = new MdMtGaji([
        	'bulan' => $bulan,
        	'tahun' => $tahun
        ]);

        if(count($request->input('gaji')) == 0){
        	return redirect("gaji/bulanan?".$qs)->with("error","Mohon inputkan data gaji yang ingin dimasukkan");
        }
        else{
        	//cek single type or multiple division
        	if($request->input("apply") > 0){
        		$mutasi = MdMtKaryawan::queryGetMutasi(['tgl_berlaku'=>max($gaji->calendar), 'divisi' => $request->input("apply")]);

        		if(count($mutasi) > 0){
        			//ada
        			$arkd = [];
        			foreach($mutasi as $row){
        				$arkd[] = $row->id_karyawan;
        			}
        		}
        		else{
        			return redirect("gaji/bulanan?".$qs)->with("error", "Tidak ada data karyawan pada divisi terpilih. Mohon gunakan kriteria lainnya");
        		}

        	}
        	else{
        		$arkd[] = $request->input("id");
        	}



        	foreach($request->input("gaji") as $ind=>$nilai){

        		foreach($arkd as $idkar){
	        		//input / update per data
	        		$cek = DB::table("cwa_detail_gaji")
	        		->where("id_karyawan", $idkar)
	        		->where("id_gaji", $ind)
	        		->where("tanggal",$tgl)
	        		->get();

	        		if(count($cek) > 0){
	        			//update
	        			DB::table("cwa_detail_gaji")
		        		->where("id_karyawan", $idkar)
		        		->where("id_gaji", $ind)
		        		->where("tanggal",$tgl)
		        		->update(["nilai" => $nilai]);
		        		$msg = "Berhasil mengupdate data gaji karyawan";
	        		}
	        		else{
	        			//insert
	        			DB::table("cwa_detail_gaji")
	        			->insert([
	        				'id_karyawan' => $idkar,
	        				'id_gaji' => $ind,
	        				'tanggal' => $tgl,
	        				'nilai' => $nilai,
	        				'stat' => 1
	        			]);
	        			$msg = "Berhasil menginput data gaji karyawan";
	        		}

        		}
        	}
        	//success

        	return redirect("gaji/bulanan?".$qs)->with("success",$msg);
        }


    }




    public function getHistory(Request $request, $id=0){
        log_user();
    	$cek_karyawan = MdKaryawan::getKaryawan($id);
        if(count($cek_karyawan) == 0){
        	return redirect("gaji/bulanan?")->with("error","Karyawan tersebut tidak ditemukan.");
        }


        return view("boxhistory")->with([
        	'title' => 'History Rekap Gaji',
        	'datakar' => $cek_karyawan,
        	'master_gaji' => MdMtGaji::master_gaji(2),
        	'gaji' => MdMtGaji::detail_gaji_karyawan($id,2),
        ]);
    }



    public function getRemove(Request $request, $id=0, $tgl=0){
    	$cek = DB::table("cwa_detail_gaji")
    	->where("id_karyawan", $id)
    	->where("tanggal", date("Y-m-d", $tgl))
    	->where("stat","<>", 9)
    	->get();

    	if(count($cek) > 0){
	    	$upd = DB::table("cwa_detail_gaji")
	    	->where("id_karyawan", $id)
	    	->where("tanggal", date("Y-m-d", $tgl))
	    	->where("stat","<>", 9)
	    	->delete();
	    	$type = "success";
    	}
    	else
    		$type = "error";

    	return redirect("gaji/bulanan")->with([$type => "Berhasil menghapus data gaji bulanan"]);

    }

}
