<?php

namespace App\Http\Controllers\Gaji;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use App\Model\MdMtKaryawan;
use App\Model\MdKaryawan;
use App\Model\MdJadwal;
use App\Model\MdReport;
use App\Model\MdMtGaji;
use App\Model\Table\ModelGajiResign;

use Input;
use DB;
use URL;

class GajiTahunan extends Controller
{
    public function __construct(){
        $this->middleware('log');
        $this->middleware('authlev:2');
    }

    public function getIndex(){
        if(is_panic()){
            return redirect('/');
        }

        log_user();
        $tahun = Input::get("tahun") ? intval(Input::get("tahun")) : date("Y");
        $divisi = Input::get("divisi");

        //cuma utk dapet list karyawan terbaru aja sih ini

        //month field

        $data_tahunan = [];
        $current_year = date('Y');
        $current_month = date('n');

        for($i=1; $i<=12; $i++){
        	if($tahun >= $current_year && $i > $current_month){
        		continue;
        	}
	        $report = new MdReport([
	            'bulan' => $i,
	            'tahun' => $tahun,
	        ]);
	        $gaji = new MdMtGaji([
	            'bulan' => $i,
	            'tahun' => $tahun,
	        ]);

	        $row = $report->make_mutasi($divisi);
	        $report->get_rancangan(null, $row);
	        $report->get_presensi();
	        $content = $report->prepare_object();
	        $bulanan = $report->gaji_maker($content, $gaji->build_gaji(1), $gaji->build_gaji(2), $row, false);

	        foreach($bulanan as $id_karyawan => $dtbul){
	        	if(isset($dtbul['index_bagi_absen'])){
		        	if($dtbul['index_bagi_absen'] == 0){
		        		$addtotal = 0;
		        	}
	        	}

        		$data_tahunan[$id_karyawan][date('F Y', strtotime($tahun.'-'.$i.'-01'))] = round($dtbul['total'], 0);
	        }
        }

        $report = new MdReport([
            'bulan' => date('n'),
            'tahun' => date('Y'),
        ]);
        $row = $report->make_mutasi($divisi);

        //NANTI DILANJUTIN -_-
        return view("reportgajitahunan")->with([
            'title' => 'Rekap Gaji Tahunan Karyawan',
            'wellname' => 'Ambil Data Tahun',
            'curmenu' => 4,
            'cursubmenu' => 45,
            'default' => [
                'tahun' => $tahun,
                'divisi' => $divisi
            ],
            'listdiv' => MdMtKaryawan::filter("divisi"),
            'listkode' => MdReport::list_kode_absen(),
            'row' => $row,
            'gaji' => $data_tahunan
        ]);
    }


}
