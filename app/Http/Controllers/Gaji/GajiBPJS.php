<?php

namespace App\Http\Controllers\Gaji;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use App\Model\MdMtKaryawan;
use App\Model\MdKaryawan;
use App\Model\MdJadwal;
use App\Model\MdMtGaji;
use App\Model\MdReport;
use App\Model\EmailGaji;
use App\Model\Table\ModelGajiResign;
use Input;
use DB;
use URL;
use Excel;
use Response;

class GajiBPJS extends Controller
{
    public function __construct(){
        $this->middleware('log');
        $this->middleware('authlev:3');
    }

    public function index(Request $request){
        log_user();
        $bulan = Input::get("bulan") ? intval(Input::get("bulan")) : date("n");
        $tahun = Input::get("tahun") ? intval(Input::get("tahun")) : date("Y");
        $divisi = Input::get("divisi");


        $report = new MdReport([
            'bulan' => $bulan,
            'tahun' => $tahun,
        ]);

        //month field
        $row = $report->make_mutasi($divisi);
        $report->get_rancangan(null, $row);
        $report->get_presensi();
        $content = $report->prepare_object();

        $gaji = new MdMtGaji([
            'bulan' => $bulan,
            'tahun' => $tahun,
        ]);


        $data_resign = ModelGajiResign::with('getKaryawan')
        ->whereIn('tgl_berlaku', $report->calendar)
        ->get();


        //NANTI DILANJUTIN -_-
        return view("gajibpjs-kesehatan")->with([
            'title' => 'Rekap Kalkulasi BPJS Kesehatan',
            'wellname' => 'Filter Periode Gaji',
            'curmenu' => 3,
            'cursubmenu' => 36,
            'default' => [
                'bulan' => $bulan,
                'tahun' => $tahun,
                'divisi' => $divisi
            ],
            'listdiv' => MdMtKaryawan::filter("divisi"),
            'listkode' => MdReport::list_kode_absen(),
            'calendar' => $report->calendar,
            'content' => $content,
            'row' => $row,
            'data_resign' => $data_resign,
            'gaji' => $report->bpjs_kesehatan_maker($gaji->build_gaji(1)),
        ]);
    }

    public function export(){
        log_user();

        // copy from index()
        $bulan = Input::get("bulan") ? intval(Input::get("bulan")) : date("n");
        $tahun = Input::get("tahun") ? intval(Input::get("tahun")) : date("Y");
        $divisi = Input::get("divisi");


        $report = new MdReport([
            'bulan' => $bulan,
            'tahun' => $tahun,
        ]);

        //month field
        $row = $report->make_mutasi($divisi);
        $report->get_rancangan(null, $row);
        $report->get_presensi();
        $content = $report->prepare_object();

        $gaji = new MdMtGaji([
            'bulan' => $bulan,
            'tahun' => $tahun,
        ]);
        $gaji = $report->bpjs_kesehatan_maker($gaji->build_gaji(1));

        $data_resign = ModelGajiResign::with('getKaryawan')
        ->whereIn('tgl_berlaku', $report->calendar)
        ->get();


        $period_string = strtoupper(date("M", strtotime("2023-" . $bulan . "-01"))) . " " . $tahun;
        $filename = "Laporan BPJS Kesehatan $divisi - $period_string";

        Excel::create($filename, function($excel) use($period_string, $row, $gaji){
            $excel->sheet("BPJS Kesehatan", function($sheet) use($period_string, $row, $gaji) {
                $sheet->row(1, [
                    "PT CITRA WARNA JAYA ABADI"
                ]);
                $sheet->row(2, [
                    "PERHITUNGAN IURAN BPJS KESEHATAN"
                ]);
                $sheet->row(3, [
                    "PERIODE " . $period_string
                ]);
                
                $sheet->row(5, [
                    '',
                    'Nama Karyawan',
                    'Departemen',
                    'Periode Kepesertaan',
                    'Gaji',
                    'PK',
                    'TK',
                    'ALL',
                ]);

                $n = 7;
                $i = 1;
                foreach($row as $r) {
                    if ($r->is_bpjs_kesehatan <> 1) {
                        continue;
                    }

                    $sheet->row($n, [
                        $i,
                        $r->nama,
                        isset($r->departemen) ? $r->departemen : '-',
                        isset($r->tgl_daftar_bpjs_kesehatan) ? date('Y-m-d', strtotime($r->tgl_daftar_bpjs_kesehatan)) : '-',
                        isset($gaji[$r->id_karyawan]['gaji_bpjs']) ? ($gaji[$r->id_karyawan]['gaji_bpjs']) : 0,
                        isset($gaji[$r->id_karyawan]['bpjs_kesehatan_pk']) ? ($gaji[$r->id_karyawan]['bpjs_kesehatan_pk']) : 0,
                        isset($gaji[$r->id_karyawan]['bpjs_kesehatan_tk']) ? ($gaji[$r->id_karyawan]['bpjs_kesehatan_tk']) : 0,
                        isset($gaji[$r->id_karyawan]['bpjs_kesehatan_all']) ? ($gaji[$r->id_karyawan]['bpjs_kesehatan_all']) : 0,
                    ]);
                    $i++;
                    $n++;
                }    
            });
        })->store('xlsx', 'upload');

        return Response::download("upload/".$filename.".xlsx");
    }

}
