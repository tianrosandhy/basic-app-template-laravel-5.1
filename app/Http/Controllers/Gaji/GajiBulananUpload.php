<?php
namespace App\Http\Controllers\Gaji;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\MdMtGaji;
use Excel;
use DB;
use Validator;

class GajiBulananUpload extends Controller
{
    public function __construct(Request $req){
        $this->request = $req;
        $this->middleware('log');
        $this->middleware('authlev:3');
    }

    public function index(){
        log_user();

        return view("upload-gaji-bulanan")->with([
            'title' => 'Upload Bonus dan Potongan Bulanan',
            'curmenu' => 3,
            'cursubmenu' => 38,
        ]);
    }

    public function check(){
        $this->validate($this->request, [
            'excel' => 'required|mimes:xls,xlsx'
        ]);
        $bulan = $this->request->bulan ?? intval(date('m'));
        $tahun = $this->request->tahun ?? intval(date('Y'));
        $excel = $this->request->excel;

        $importedData = [];

        $out = Excel::load($excel)->toArray();

        $gaji_bulanan = DB::table('cwa_master_gaji')->where('occurence', 2)->where('stat', 1)->get();
        $list_gaji = [];
        foreach($gaji_bulanan as $gj){
            $list_gaji[$gj->id] = $gj->label;
        }

        $karyawan = DB::table('cwa_karyawan')->where('stat', 1)->get(['id', 'nama', 'nik']);
        $mapgaji = [];
        foreach($out as $idx => $row){
            if(count($row) < 3){
                // bad request. invalid file
                abort(400);
            }
            if($idx == 0){
                // configure keys
                foreach($row as $index => $rowlabel){
                    foreach($list_gaji as $id_gaji => $label_gaji){
                        if($label_gaji == $rowlabel){
                            $mapgaji[$id_gaji] = $index;
                        }
                    }
                }
                if(empty($mapgaji)){
                    // bad request. list gaji not found
                    abort(400);
                }
            }
            else{
                // get karyawan based on $row[0] / $row[1]
                foreach($karyawan as $kar){
                    if($row[0] == $kar->nik || strtolower($row[1]) == strtolower($kar->nama)){
                        $temp = [];
                        $containdata = false;
                        foreach($mapgaji as $idgaji => $indexgaji){
                            $temp[$idgaji] = $row[$indexgaji] ?? null;
                            if(!empty($temp[$idgaji])){
                                $containdata = true;
                            }
                        }
                        if($containdata){
                            $importedData[$kar->id] = $temp;
                        }
                    }
                }
            }
        }

        return view("upload-gaji-bulanan")->with([
            'title' => 'Konfirmasi Upload Bonus dan Potongan Bulanan',
            'curmenu' => 3,
            'cursubmenu' => 38,
            'data' => [
                'importedData' => $importedData,
                'bulan' => $bulan,
                'tahun' =>  $tahun,
            ],
            'karyawan' => $karyawan,
            'gaji_bulanan' => $gaji_bulanan,
        ]);
    }

    public function example(){
        log_user();
        //buat file excel
        $filename = "Contoh Format Bonus & Potongan";

        $gaji = new MdMtGaji([
        	'bulan' => $this->request->bulan,
        	'tahun' => $this->request->tahun
        ]);

        $mutasi = $gaji->karyawan_gaji();
        $sorted_divisi = DB::table('cwa_divisi')->orderBy('sort_no', 'ASC')->where('stat', 1)->get(['id', 'nama_divisi']);
        $gaji_bulanan = DB::table('cwa_master_gaji')->where('occurence', 2)->where('stat', 1)->get();
        $list_gaji = [];
        foreach($gaji_bulanan as $gj){
            $list_gaji[] = $gj->label;
        }

        Excel::create($filename, function($excel) use($mutasi, $list_gaji, $sorted_divisi){
          $excel->sheet('Data Bonus & Potongan', function($sheet) use($mutasi, $list_gaji, $sorted_divisi){
            $sheet->row(1, [
                'FORM UPLOAD DATA BONUS DAN POTONGAN'
            ]);
            $n = 2;
            $sheet->row($n, array_merge([
              'ID Karyawan',
              'Nama Karyawan',
              'Divisi',
            ], $list_gaji));

            foreach($sorted_divisi as $dv){
                foreach($mutasi as $idkaryawan => $data){
                    if($data->id_divisi == $dv->id){
                        $n++;
                        $sheet->row($n, [
                            $data->nik,
                            $data->nama,
                            $data->nama_divisi
                        ]);
                    }
                } 
            }


          });
        })->download('xlsx');
    }

    public function store(){
        $data = json_decode($this->request->data, true);
        if(!isset($data['importedData']) || !isset($data['bulan']) || !isset($data['tahun'])){
            return redirect()->back()->with('error', 'Failed to process data. Please try again later.');
        }

        $string_periode = date('Y-m-d', strtotime($data['tahun'].'-'.$data['bulan'].'-01'));
        $listKaryawan = [];
        $listIDGaji = [];
        $insertBatch = [];
        foreach($data['importedData'] as $idkar => $list){
            $listKaryawan[] = $idkar;
            foreach($list as $idgaji => $valuegaji){
                if(!in_array($idgaji, $listIDGaji)){
                    $listIDGaji[] = $idgaji;
                }

                if(!empty($valuegaji)){
                    $insertBatch[] = [
                        'id_karyawan' => $idkar,
                        'id_gaji' => $idgaji,
                        'tanggal' => $string_periode,
                        'nilai' => $valuegaji,
                        'stat' => 1
                    ];
                }
            }
        }
        
        if(empty($listKaryawan) || empty($listIDGaji)){
            return redirect()->back()->with('error', 'No data to be processed. Please try again later.');
        }

        // remove old data gaji
        $remover = DB::table('cwa_detail_gaji')
            ->where('tanggal', $string_periode)
            ->whereIn('id_karyawan', $listKaryawan)
            ->whereIn('id_gaji', $listIDGaji)
            ->delete();

        // execute batch insert
        DB::table('cwa_detail_gaji')->insert($insertBatch);

        return redirect('gaji/bulanan')->with('success', 'Data bonus & potongann bulanan berhasil disimpan');
    }
}