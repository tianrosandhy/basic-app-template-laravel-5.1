<?php

namespace App\Http\Controllers\Gaji;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use App\Model\MdMtKaryawan;
use App\Model\MdKaryawan;
use App\Model\MdJadwal;
use App\Model\MdMtGaji;
use Input;
use DB;
use URL;
use Excel;
use Response;

class GajiTHR extends Controller
{
    public function __construct(){
        $this->middleware('log');
        $this->middleware('authlev:3');
    }

    public function getIndex(Request $request){
        log_user();
        $tgl = Input::get("tgl") ? Input::get("tgl") : date("Y-m-d");
        $divisi = Input::get("divisi");

        $bulan = date("n", strtotime($tgl));
        $tahun = date("Y", strtotime($tgl));

        $gaji = new MdMtGaji([
        	'bulan' => $bulan,
        	'tahun' => $tahun
        ]);
	

        return view("thr")->with([
        	'getform' => 't_well2',
            'title' => 'Tunjangan Hari Raya',
            'wellname' => "Filter Periode",
            'curmenu' => 3,
            'cursubmenu' => 34,
            'default' => [
                'tgl' => $tgl,
                'tahun' => $tahun,
                'divisi' => $divisi
            ],
            'month_control' => $bulan,
            'listdiv' => MdMtKaryawan::filter("divisi"),
            'master_gaji' => MdMtGaji::master_gaji(1),
            'mutasi' => $gaji->karyawan_gaji($divisi),
            'thr' => $gaji->build_thr($tgl)
        ]);
    }

    public function getPrint(){
        log_user();
        $tgl = Input::get("tgl") ? Input::get("tgl") : date("Y-m-d");
        $divisi = Input::get("divisi");

        $bulan = date("n", strtotime($tgl));
        $tahun = date("Y", strtotime($tgl));

        $gaji = new MdMtGaji([
            'bulan' => $bulan,
            'tahun' => $tahun
        ]);
        $mutasi = $gaji->karyawan_gaji($divisi);
        $thr = $gaji->build_thr($tgl);


        $filename = "Laporan THR $divisi - $tgl";

        $hehe = [];
        foreach($thr as $idk => $parm){
            if($parm['status_transfer'] == 0){
                $hehe['cash'][$idk] = $parm;
            }
            else{
                if($parm['status_transfer'] <> 1 && ($parm['get'] > 5500000 && $idk <> 274)){
                    $sisa = $parm['get'] - 5500000;
                    $parm['get'] = 5500000;
                    $hehe['transfer'][$idk] = $parm;
                    $parm['get'] = $sisa;
                    $hehe['cash'][$idk] = $parm;
                }
                else{
                    $hehe['transfer'][$idk] = $parm;
                }
            }

        }

        if(is_panic() && isset($hehe['cash'])){
            unset($hehe['cash']);
        }


        Excel::create($filename, function($excel) use($mutasi, $hehe, $tgl){
            foreach($hehe as $type => $thr){
                $excel->sheet((is_panic() ? 'THR' : 'THR ' . ucfirst($type)), function($sheet) use($mutasi, $thr, $tgl){
                    $n = 1;
                    $sheet->row($n, [
                        'No',
                        'NIK',
                        'Nama',
                        'Bagian',
                        'Total Gaji',
                        'Tgl Gajian',
                        'No. Rek'
                    ]);

                    $n++;
                    $noo = 1;
                    $real_total = 0;
                    foreach($mutasi as $row){
                        if(isset($thr[$row->id_karyawan])){
                            $real_total += $thr[$row->id_karyawan]['get'];
                            $sheet->row($n, [
                                $noo++,
                                $row->id_karyawan,
                                $row->nama,
                                $row->nama_jabatan,
                                round($thr[$row->id_karyawan]['get'], 0),
                                date('d/m/Y', strtotime($tgl)),
                                (isset($row->no_rek) ? $row->no_rek : ''),
                            ]);
                            $n++;
                        }
                    }
                    $sheet->row($n, [
                        '',
                        '',
                        '',
                        '',
                        round($real_total, 0),
                        '',
                        ''
                    ]);
                });
            }
        })->store('xlsx', 'upload');

        return Response::download("upload/".$filename.".xlsx");
    }

    public function getSlip(){
        log_user();
    	$tgl = Input::get("tgl") ? Input::get("tgl") : date("Y-m-d");
        $divisi = Input::get("divisi");

        $bulan = date("n", strtotime($tgl));
        $tahun = date("Y", strtotime($tgl));

        $gaji = new MdMtGaji([
        	'bulan' => $bulan,
        	'tahun' => $tahun
        ]);

        $thr = $gaji->build_thr($tgl);

        $row = MdMtKaryawan::queryGetMutasi([
        	'tgl_berlaku' => $tgl
        ]);

        return view("slipthr")->with([
            'title' => 'Slip THR',
            'curmenu' => 3,
            'cursubmenu' => 34,
            'default' => [
            	'tgl' => $tgl,
            	'divisi' => $divisi
            ],
            'listdiv' => MdMtKaryawan::filter("divisi"),
            'rows' => $row,
            'content' => $thr,
        ]);
    }

}
