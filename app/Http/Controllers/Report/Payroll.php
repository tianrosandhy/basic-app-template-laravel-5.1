<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use DB;
use Input;
use Excel;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\MdMtKaryawan;
use App\Model\MdKaryawan;
use App\Model\MdJadwal;
use App\Model\MdReport;
use App\Model\MdMtGaji;

use App\Model\Table\ModelGajiResign;
use App\Model\Table\ModelDivisi;

class Payroll extends Controller
{
    public $request;


    public function __construct(Request $req){
        $this->middleware('log');
        $this->middleware('authlev:1');
        $this->request = $req;
    }

    public function getIndex()
    {
        log_user();
        $bulan = Input::get("bulan") ? intval(Input::get("bulan")) : date("n");
        $tahun = Input::get("tahun") ? intval(Input::get("tahun")) : date("Y");
        
        return view('report-payroll')->with([
            'title' => 'Payroll Report',
            'default' => [
                'bulan' => $bulan,
                'tahun' => $tahun
            ]
        ]);
    }

    public function getExcel(){
    	log_user();
        $bulan = Input::get("bulan") ? intval(Input::get("bulan")) : date("n");
        $tahun = Input::get("tahun") ? intval(Input::get("tahun")) : date("Y");

        $trx_prefix = date('ym', strtotime($tahun.'-'.$bulan.'-10'));


        //get last date from this periode
        $jml_date = [
        	1 => 31,
        	2 => 28,
        	3 => 31,
        	4 => 30,
        	5 => 31,
        	6 => 30,
        	7 => 31,
        	8 => 31,
        	9 => 30,
        	10 => 31,
        	11 => 30,
        	12 => 31
        ];

        if(isset($jml_date[intval($bulan)])){
        	$last_date = $jml_date[intval($bulan)];
        	$tgl_gajian = strtotime($tahun.'-'.$bulan.'-'.$last_date);
        	$tgl_gajian = date('d/m/Y', $tgl_gajian);
        }

        $data = $this->prepare_gaji($bulan, $tahun, null);
        $group_divisi = $this->group_divisi($data['mutasi']);
        $prepare = $this->laporan_amy($data);
        $final = $this->rapiin_lagi($prepare);

        //untuk yg resign, format datanya cukup sederhana kok
        $data_resign = ModelGajiResign::with('getKaryawan')
        ->whereIn('tgl_berlaku', $data['calendar'])
        ->get();

        $temp_row = $append = [];
        foreach($data_resign as $dt){
            $temp_row[$dt->id_karyawan] = [
                'nama' => $dt->getKaryawan->nama,
                'nik' => $dt->nik,
                'id_karyawan' => $dt->id_karyawan,
                'divisi' => '-resigned-',
                'status' => 'resign',
                'total_gaji' => $dt->bonus,
            ];
        }

/*
        if(count($temp_row) > 0){
            $append['-resign-'] = $temp_row;
            $final[0] = $final[0] + $append;
        }
*/
        //buat file excel
        $filename = "Laporan Payroll - $bulan - $tahun";
        $sheetname = ['Cash', 'Transfer'];
        Excel::create($filename, function($excel) use ($final, $sheetname, $tgl_gajian, $trx_prefix){
    		$save_total_gaji = [];
        	foreach($final as $ind=> $fin){
                if(is_panic() && $ind == 0){
                    continue;
                }
	        	$excel->sheet($sheetname[$ind], function($sheet) use($sheetname, $ind, $fin, $save_total_gaji, $tgl_gajian, $trx_prefix){
	        		$n = 1;
	        		$sheet->row($n, [
                        'Trx ID',
                        'Transfer Type',
                        'Beneficiary ID',
                        'Credited Account',
                        'Receiver Name',
                        'Amount',
                        'NIP',
                        'Remark',
                        'Beneficiary Email',
                        'Swift Code',
                        'Cust Type',
                        'Cust Residence',
	        			// 'No',
	        			// 'NIK',
	        			// 'Nama',
	        			// 'Bagian',
	        			// 'Total Gaji',
	        			// 'Tgl Gajian',
	        			// 'No. Rek'
	        		]);


                    $sheet->setColumnFormat([
                        'D' => '@',
                        'G' => '@'
                    ]);

	        		//prepare data per divisi
	        		$total_gaji = 0;
	        		$noo = 1;
	        		$n++;
	        		foreach($fin as $divisi=>$konten){
	        			$startborder = $n;
	        			foreach($konten as $idkaryawan => $row){
                            $trx_id = $trx_prefix . substr(1000 + $noo++, 1);

	        				$sheet->row($n, [
	        					$trx_id,
                                'BCA',
                                '',
	        					isset($row['no_rek']) ? $row['no_rek'] : '-',
	        					$row['nama'], 
	        					round($row['total_gaji'], 0),
	        					str_replace(',', '.', $row['nik']),
	        				]); 
	        				$n++;
	        				$total_gaji += $row['total_gaji'];
	        			}

	        		}

	        	});
        	}

        })->store('xlsx', 'upload');
        echo json_encode(url("upload/".$filename.".xlsx"));
        exit();
    }



    public function prepare_gaji($bulan, $tahun, $divisi){
        $report = new MdReport([
            'bulan' => $bulan,
            'tahun' => $tahun
        ]);

        //month field
        $row = $report->make_mutasi($divisi);
        $report->get_rancangan();
        $report->get_presensi();
        $content = $report->prepare_object();

        $gaji = new MdMtGaji([
            'bulan' => $bulan,
            'tahun' => $tahun
        ]);

        return [
            'mutasi' => $row,
            'datagaji' => $report->gaji_maker($content, $gaji->build_gaji(1), $gaji->build_gaji(2), $row, false),
            'calendar' => $report->calendar
        ];
    }

    public function group_divisi($data){
        $saved = [];
        //menyimpan list ID karyawan per divisi 
        foreach($data as $idkar => $row){
            $saved[$row->id_divisi][] = $row->id_karyawan;
        }
        return $saved;
    }


    public static function laporan_amy($data){
        //prepare status karyawan
        $out = [];
        foreach($data['mutasi'] as $idmutasi => $obj){
            $total_gaji = isset($data['datagaji'][$obj->id_karyawan]['total']) ? $data['datagaji'][$obj->id_karyawan]['total'] : 0;

            $out[$obj->status_transfer][$obj->id_karyawan]['nama'] = $obj->nama;
            $out[$obj->status_transfer][$obj->id_karyawan]['nik'] = $obj->nik;
            $out[$obj->status_transfer][$obj->id_karyawan]['id_karyawan'] = $obj->id_karyawan;
            $out[$obj->status_transfer][$obj->id_karyawan]['divisi'] = $obj->nama_divisi;
            $out[$obj->status_transfer][$obj->id_karyawan]['status'] = $obj->type;
            $out[$obj->status_transfer][$obj->id_karyawan]['no_rek'] = $obj->no_rek;
            $out[$obj->status_transfer][$obj->id_karyawan]['nama_jabatan'] = $obj->nama_jabatan;
            $out[$obj->status_transfer][$obj->id_karyawan]['total_gaji'] = $total_gaji;
        }

        return $out;
    }

    public static function rapiin_lagi($prepared){
    	$out = [];
    	//prepared[0] => Cash
    	if(isset($prepared[0])){
    		foreach($prepared[0] as $idkaryawan => $row){
    			$out[0][$row['divisi']][$idkaryawan] = $row;
    		}
    	}

    	//prepared[1] => Transfer
        if(isset($prepared[1])){
            foreach($prepared[1] as $idkaryawan => $row){
                $out[1][$row['divisi']][$idkaryawan] = $row;
            }
        }
        
        //prepared[2] => Multi
        if(isset($prepared[2])){
            foreach($prepared[2] as $idkaryawan => $row){
                //gaji dibagi ke cash dan ke transfer
                $tmpp = $row['total_gaji'];


                //ada 3 threshold tai
                /*
                1. Hendri langsung full transfer
                2. Amy : 6,5
                3. Sisanya biasa
                */

                $tai_threshold = 5500000;
                if($idkaryawan == 3){
                    $tai_threshold = 6500000;
                }
                else if($idkaryawan == 274){
                    $tai_threshold = 15000000; //skrg ditransfer 15jt, sisanya 22jt cash.. bagi2 2 juta aja napa
                }
                if($idkaryawan == 431){
                    $tai_threshold = 10000000;
                }


                if($tmpp > $tai_threshold){
                    $out[1][$row['divisi']][$idkaryawan] = [
                        'nama' => $row['nama'],
                        'nik' => $row['nik'],
                        'id_karyawan' => $row['id_karyawan'],
                        'divisi' => $row['divisi'],
                        'status' => $row['status'],
                        'no_rek' => $row['no_rek'],
                        'nama_jabatan' => $row['nama_jabatan'],
                        'total_gaji' => $tai_threshold,
                    ];


                    $sisacash = $tmpp - $tai_threshold;
                    $out[0][$row['divisi']][$idkaryawan] = [
                        'nama' => $row['nama'],
                        'nik' => $row['nik'],
                        'id_karyawan' => $row['id_karyawan'],
                        'divisi' => $row['divisi'],
                        'status' => $row['status'],
                        'total_gaji' => $sisacash,
                    ];

                }
            }
        }
    	return $out;
    }
}
