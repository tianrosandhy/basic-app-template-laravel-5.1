<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use DB;
use Input;
use Excel;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\MdMtKaryawan;
use App\Model\MdKaryawan;
use App\Model\MdJadwal;
use App\Model\MdReport;
use App\Model\MdMtGaji;

use App\Model\Table\ModelDivisi;
use App\Model\Table\ModelGajiResign;

use App\Http\Controllers\Report\Amy;

class Finance extends Controller
{
    public $request;


    public function __construct(Request $req){
        $this->middleware('log');
        $this->middleware('authlev:1');
        $this->request = $req;
    }

    public function getIndex()
    {
        // $sql = DB::table('cwa_divisi')->get();
        // $list = [];
        // foreach($sql as $row){
        //     $list[$row->id] = $row->nama_divisi;
        // }
        // dd($list);
        log_user();
        $bulan = Input::get("bulan") ? intval(Input::get("bulan")) : date("n");
        $tahun = Input::get("tahun") ? intval(Input::get("tahun")) : date("Y");
        $divisi = Input::get("divisi");

        
        $data = $this->prepare_gaji($bulan, $tahun, $divisi);
        $group_divisi = $this->group_divisi($data['mutasi']);

        $hasil_olah_gaji = $this->olah_gaji($group_divisi, $data['datagaji']);
        $merge_report = $this->merge_report($group_divisi, $hasil_olah_gaji);

        //if ada yg resign
        $data_resign = ModelGajiResign::with('getKaryawan')
        ->whereIn('tgl_berlaku', $data['calendar'])
        ->get();

        $temp_row = [];
        // kalo suatu saat ada masalah ga balance jumlah bonus PUnya, kali aja krn ada data karyawan resign yg blm diatur lebih jauh
        foreach($data_resign as $dt){
            $temp_row['resign'][$dt->id_karyawan] = [
                'Surplus' => $dt->bonus,
                'Bonus PU' => $dt->bonus,
                'Bobot' => 0,
                'Batas Bobot' => 0,
                'Defisit' => 0,
                'Total' => $dt->bonus,
            ];
        }

        $merge_report = $merge_report + $temp_row;
        $ringkasan = $this->kesimpulan($merge_report);
        $total_kesimpulan = $this->total_kesimpulan($ringkasan);

        $list_divisi = ModelDivisi::orderBy('sort_no', 'ASC')->get()->pluck('nama_divisi', 'id');
        $sorted_divisi = ModelDivisi::where('stat', 1)->orderBy('sort_no', 'ASC')->get(['id', 'nama_divisi']);
        $list_divisi->prepend('-resigned-', 'resign');

        return view('custom-report')->with([
            'title' => 'Custom Report untuk Cik Amy',
            'list_divisi' => $list_divisi,
            'default' => [
                'divisi' => $divisi,
                'bulan' => $bulan,
                'tahun' => $tahun
            ],
            'sorted_divisi' => $sorted_divisi,
            'data' => $ringkasan,
            'total_kesimpulan' => $total_kesimpulan
        ]);
    }


    public function getPrint(){
        log_user();
        $bulan = Input::get("bulan") ? intval(Input::get("bulan")) : date("n");
        $tahun = Input::get("tahun") ? intval(Input::get("tahun")) : date("Y");
        $divisi = Input::get("divisi");

        
        $data = $this->prepare_gaji($bulan, $tahun, $divisi);
        $group_divisi = $this->group_divisi($data['mutasi']);

        $hasil_olah_gaji = $this->olah_gaji($group_divisi, $data['datagaji']);

        $merge_report = $this->merge_report($group_divisi, $hasil_olah_gaji);

        //if ada yg resign
        $data_resign = ModelGajiResign::with('getKaryawan')
        ->whereIn('tgl_berlaku', $data['calendar'])
        ->get();

        $temp_row = [];
        foreach($data_resign as $dt){
            $temp_row['resign'][$dt->id_karyawan] = [
                'Surplus' => $dt->bonus,
                'Bonus PU' => $dt->bonus,
                'Bobot' => 0,
                'Batas Bobot' => 0,
                'Defisit' => 0,
                'Total' => $dt->bonus,
            ];
        }

        $merge_report = $merge_report + $temp_row;


        //kalo ada yg kasbon, direcord
        $kasbon = [];
        foreach($merge_report as $iddv => $mg){
          foreach($mg as $idkk => $fdata){
            if(isset($fdata['Potongan Kasbon'])){
              if($fdata['Potongan Kasbon'] > 0){
                $kasbon[$idkk] = $fdata['Potongan Kasbon'];
              }
            }
          }
        }

        if(count($kasbon) > 0){
          $a = array_keys($kasbon);
          $kyw = collect(DB::table('cwa_karyawan')->whereIn('id', $a)->get());
          foreach($kasbon as $iddkk => $nilaikasbon){
            if(isset($kyw->where('id', $iddkk)->first()->nama)){
              $nmmm = $kyw->where('id', $iddkk)->first()->nama;
              $kasbono[$nmmm] = $nilaikasbon;
            }
          }
          if(isset($kasbono)){
            $kasbon = $kasbono;
          }
        }

        $ringkasan = $this->kesimpulan($merge_report);
        $total_kesimpulan = $this->total_kesimpulan($ringkasan);

        $list_divisi = ModelDivisi::get()->pluck('nama_divisi', 'id');
        $list_divisi->prepend('-resigned-', 'resign');        


        //data utk tf&cash
        $prepare = Amy::laporan_amy($data);
        $final = Amy::rapiin_lagi($prepare);
        if(count($temp_row) > 0){
            $append['-resign-'] = $temp_row;
            $final[0] = $final[0] + $append;
        }
        //final[0] = cash
        //final[1] = transfer

        $method = [
          0 => 0,
          1 => 0
        ];
        foreach($final as $i => $tai){
          foreach($tai as $dvs => $kyw){
            foreach($kyw as $prm){
              $method[$i] += $prm['total_gaji'];
            }
          }
        }



        //buat file excel
        $filename = "Laporan Gaji Cik Amy - $bulan - $tahun";
        Excel::create($filename, function($excel) use($ringkasan, $total_kesimpulan, $list_divisi, $method, $kasbon){
          $excel->sheet('Laporan Gaji', function($sheet) use($ringkasan, $total_kesimpulan, $list_divisi, $method, $kasbon){
            $n = 1;
            $sheet->row($n, ['Laporan Data Gaji']);
            $sheet->cells('A'.$n, function($cell){
              $cell->setFontWeight('bold');
            });

            $n++;
            $n++;

            $sheet->row($n, [
              'Cabang',
              'Gaji Pokok & Tunjangan',
              'Pt. Absensi',
              'Insentive Overtime',
              'Biaya Gaji',

              'Insentif',
              'Bonus PU',
              'Gaji Kotor',
              'BP Jamsostek',
              'BPJS Kesehatan',
              'Iuran Suka Duka',
              'Pt. Kasbon',
              'Pt. Gaji',
              'Gaji Bersih',
            ]);

            $n++;

            $sorted_divisi = DB::table('cwa_divisi')->orderBy('sort_no', 'ASC')->where('stat', 1)->get(['id', 'nama_divisi']);
            foreach($sorted_divisi as $sdiv){
                $iddiv = $sdiv->id;
                $content = $ringkasan[$iddiv] ?? null;
                if(empty($content)){
                    continue; //skip
                }

                $tmpinse = isset($content['Insentive Overtime']) ? $content['Insentive Overtime'] : 0;
                $tmplem = isset($content['Lembur']) ? $content['Lembur'] : 0;

                $tlt = isset($content['Potongan Terlambat']) ? $content['Potongan Terlambat'] : 0;
                $ptg = isset($content['Potongan Denda']) ? $content['Potongan Denda'] : 0;
                $lain = isset($content['Potongan Lain-Lain']) ? $content['Potongan Lain-Lain'] : 0;

                $bonus_pu = (isset($content['Bonus PU']) ? round($content['Bonus PU']) : 0);
                $potongan_bonus_pu = (isset($content['Potongan Bonus PU']) ? round($content['Potongan Bonus PU']) : 0);

                $gjtj = 0;
                $gjtj += (isset($content['Gaji Pokok']) ? ($content['Gaji Pokok']) : 0);
                $gjtj += (isset($content['Tunjangan Jabatan']) ? ($content['Tunjangan Jabatan']) : 0);
                $gjtj += (isset($content['Tunjangan Golongan']) ? ($content['Tunjangan Golongan']) : 0);
                $gjtj += (isset($content['Tunjangan Lain']) ? ($content['Tunjangan Lain']) : 0);
                $gjtj += (isset($content['Tunjangan Penempatan']) ? ($content['Tunjangan Penempatan']) : 0);
                $gjtj += (isset($content['Tunjangan Tahunan']) ? ($content['Tunjangan Tahunan']) : 0);		

                $sheet->row($n, [
                $list_divisi[$iddiv],
                round($gjtj),
                (isset($content['Potongan Absensi']) ? round($content['Potongan Absensi']) : 0),
                round(($tmpinse + $tmplem)),
                (isset($content['Biaya Gaji']) ? round($content['Biaya Gaji']) : 0),

                (isset($content['Insentif']) ? round($content['Insentif']) : 0),
                ($bonus_pu - $potongan_bonus_pu),
                (isset($content['Gaji Kotor']) ? round($content['Gaji Kotor']) : 0),
                (isset($content['BP Jamsostek']) ? round($content['BP Jamsostek']) : 0),
                (isset($content['BPJS Kesehatan']) ? round($content['BPJS Kesehatan']) : 0),
                (isset($content['Iuran Suka Duka']) ? round($content['Iuran Suka Duka']) : 0),
                (isset($content['Potongan Kasbon']) ? round($content['Potongan Kasbon']) : 0),
                ($tlt + $ptg + $lain),
                (isset($content['Gaji Bersih']) ? round($content['Gaji Bersih']) : 0),
                ]);
                $n++;
            }

            //total kesimpulan
            $tim = isset($total_kesimpulan['Insentive Overtime']) ? $total_kesimpulan['Insentive Overtime'] : 0;
            $tle = isset($total_kesimpulan['Lembur']) ? $total_kesimpulan['Lembur'] : 0;

            $atlt = isset($total_kesimpulan['Potongan Terlambat']) ? $total_kesimpulan['Potongan Terlambat'] : 0;
            $akes = isset($total_kesimpulan['Potongan Denda']) ? $total_kesimpulan['Potongan Denda'] : 0;
            $alain = isset($total_kesimpulan['Potongan Lain-Lain']) ? $total_kesimpulan['Potongan Lain-Lain'] : 0;

            $ksgjtj = 0;
            $ksgjtj += (isset($total_kesimpulan['Gaji Pokok']) ? ($total_kesimpulan['Gaji Pokok']) : 0);
            $ksgjtj += (isset($total_kesimpulan['Tunjangan Jabatan']) ? ($total_kesimpulan['Tunjangan Jabatan']) : 0);
            $ksgjtj += (isset($total_kesimpulan['Tunjangan Golongan']) ? ($total_kesimpulan['Tunjangan Golongan']) : 0);
            $ksgjtj += (isset($total_kesimpulan['Tunjangan Lain']) ? ($total_kesimpulan['Tunjangan Lain']) : 0);
            $ksgjtj += (isset($total_kesimpulan['Tunjangan Penempatan']) ? ($total_kesimpulan['Tunjangan Penempatan']) : 0);

            $total_bonus_pu = (isset($total_kesimpulan['Bonus PU']) ? round($total_kesimpulan['Bonus PU']) : 0);
            $total_potongan_bonus_pu = (isset($total_kesimpulan['Potongan Bonus PU']) ? round($total_kesimpulan['Potongan Bonus PU']) : 0);

            $sheet->row($n, [
              'Total',
              round($ksgjtj),
              (isset($total_kesimpulan['Potongan Absensi']) ? round($total_kesimpulan['Potongan Absensi']) : 0),
              (($tim + $tle)),
              (isset($total_kesimpulan['Biaya Gaji']) ? round($total_kesimpulan['Biaya Gaji']) : 0),

              (isset($total_kesimpulan['Insentif']) ? round($total_kesimpulan['Insentif']) : 0),
              $total_bonus_pu - $total_potongan_bonus_pu,
              (isset($total_kesimpulan['Gaji Kotor']) ? round($total_kesimpulan['Gaji Kotor']) : 0),

              (isset($total_kesimpulan['BP Jamsostek']) ? round($total_kesimpulan['BP Jamsostek']) : 0),
              (isset($total_kesimpulan['BPJS Kesehatan']) ? round($total_kesimpulan['BPJS Kesehatan']) : 0),
              (isset($total_kesimpulan['Iuran Suka Duka']) ? round($total_kesimpulan['Iuran Suka Duka']) : 0),
              (isset($total_kesimpulan['Potongan Kasbon']) ? round($total_kesimpulan['Potongan Kasbon']) : 0),
              ($atlt + $akes + $alain),
              (isset($total_kesimpulan['Gaji Bersih']) ? round($total_kesimpulan['Gaji Bersih']) : 0),
              
            ]);

            $sheet->cells('A'.$n.':O'.$n, function($cell){
              $cell->setFontWeight('bold');
            });

            $n++;
            $n++;
            $n++;
            if(count($kasbon) > 0){
              $sheet->row($n, ['Kasbon']);
              $sheet->cells('A'.$n, function($cell){
                $cell->setFontWeight('bold');
              });
              $n++;
              foreach($kasbon as $nmu => $ttk){
                $sheet->row($n, [$nmu, round($ttk)]);
                $n++;
              }
            }



            //tampilkan total transfer dan total cash
            $n++;
            $n++;
            $n++;
            $sheet->row($n, ['Total Cash', round($method[0])]);
            $sheet->cells('A'.$n.':B'.$n, function($cell){
              $cell->setFontWeight('bold');
            });
            $n++;
            $sheet->row($n, ['Total Transfer', round($method[1])]);
            $sheet->cells('A'.$n.':B'.$n, function($cell){
              $cell->setFontWeight('bold');
            });

            $n++;
            $sheet->row($n, ['Total', round(round($method[0]) + round($method[1]))]);
            $sheet->cells('A'.$n.':B'.$n, function($cell){
              $cell->setFontWeight('bold');
            });

          });
        })->download('xlsx');

//        dd($ringkasan, $total_kesimpulan);
    }




    public function prepare_gaji($bulan, $tahun, $divisi){
        $report = new MdReport([
            'bulan' => $bulan,
            'tahun' => $tahun
        ]);
//        $report->force_calendar($bulan, $tahun, false, 1);

        //month field
        $row = $report->make_mutasi($divisi, 'divisi', true);
        $report->get_rancangan();
        $report->get_presensi();
        $content = $report->prepare_object();

        $gaji = new MdMtGaji([
            'bulan' => $bulan,
            'tahun' => $tahun
        ]);

        return [
            'mutasi' => $row,
            'datagaji' => $report->gaji_maker($content, $gaji->build_gaji(1), $gaji->build_gaji(2), $row, false),
            'calendar' => $report->calendar
        ];
    }

    public function group_divisi($data){
        $saved = [];
        //menyimpan list ID karyawan per divisi 
        foreach($data as $idkar => $row){
            $saved[$row->id_divisi][] = $row->id_karyawan;
        }
        return $saved;
    }


    public function olah_gaji($list_divisi, $data){
        $out = []; //flat data
        $id_tunjangan = $this->get_index_gaji(1,1,0);

        //labeling
        $master_gaji = DB::table('cwa_master_gaji')
            ->where('stat','<>',9)
            ->get();
        $gajie = [];
        foreach($master_gaji as $rmas){
            $gajie[$rmas->id] = $rmas->label;
        }

        //flattening
        foreach($data as $idkaryawan => $row){
            if(isset($row[1])){
                foreach($row[1] as $indx => $vlue){
                    $data[$idkaryawan][$indx] = $vlue;
                }
            }
            unset($data[$idkaryawan][1]);
            if(isset($row[2])){
                foreach($row[2] as $indx => $vlue){
                    $data[$idkaryawan][$indx] = $vlue;
                }
            }
            unset($data[$idkaryawan][2]);
        }

        foreach($data as $idkaryawan => $row){
        	//jangan ada yg diskip, ngerusak struktur data ternyata
//        	if($row['bobot'] == 0 && $row['total'] == 0){
//        		continue;
//        	}

            //biaya gaji = gajipokok + tunjangan - potongan absensi
            $gjpokok = isset($row['gaji_pokok']) ? $row['gaji_pokok'] : 0;
            $tj_jabatan = isset($row['tunjangan_jabatan']) ? $row['tunjangan_jabatan'] : 0;
            $pot_absen = isset($row['potongan_absensi']) ? $row['potongan_absensi'] : 0;

            $tj_tambahan = 0;
            foreach($id_tunjangan as $idtj){
                $tj_tambahan += isset($row[$idtj]) ? $row[$idtj] : 0;
            }

            //penambahan index apabila diperlukan lagi ditaruh disini aja

            $data[$idkaryawan]['biaya_gaji'] = ($gjpokok + $tj_jabatan + $tj_tambahan);

            foreach($row as $idd => $nll){
                if(is_numeric($nll)){
                    if(isset($gajie[$idd])){
                        $label = $gajie[$idd];
                        unset($data[$idkaryawan][$idd]);
                        $data[$idkaryawan][$label] = $nll;
                    }
                    else{
                        $label = $idd;
                        $data[$idkaryawan][$label] = $nll;
                    }

                }
            }

        }

        //last, indexnya direstyle biar ganteng
        $last = [];
        foreach($data as $idkaryawan => $row){
            $row = $this->restyle_index($row);
            $last[$idkaryawan]  = $row;
        }
        return $last;

    }


    //helper get index gaji yg diinginkan
    public function get_index_gaji($type=null, $occ=null, $divide_by_absen=null){
        $sql = DB::table('cwa_master_gaji');

        if($type != null){
            $sql = $sql->where('type', intval($type));
        }
        if($occ != null){
            $sql = $sql->where('occurence', intval($occ));
        }
        if($divide_by_absen != null){
            $sql = $sql->where('divide_by_absen', intval($divide_by_absen));
        }
        
        $sql = $sql->where('stat', 1)->get();

        $out = [];
        foreach($sql as $row){
            $out[] = $row->id;
        }
        return $out;
    }

    public function get_index_by_label($label){
        $text = trim($label);
        $sql = DB::table('cwa_master_gaji')
            ->where('label', $text)
            ->get();
        if(count($sql) > 0)
            return json_decode(json_encode($sql), true);
        else
            return [];
    }

    public function restyle_index($row){
        foreach($row as $index=>$val){
            $exp = explode("_", $index);
            if(count($exp) > 0){
                //ada underscore
                $out = implode(" ", $exp);
                $out = ucwords($out);
            }
            else{
                $out = $index;
            }
            $save[$out] = $val;
        }
        return $save;
    }

    public function merge_report($listdiv, $hasil){
        $out = [];
        foreach($listdiv as $id_divisi => $data){
            foreach($data as $idkaryawan){
                if(isset($hasil[$idkaryawan])){
                    $out[$id_divisi][$idkaryawan] = $hasil[$idkaryawan];
                }
                else{
                    //mestinya error
                    //tapi ntar aja deh.. -_-
                    //cuma 9 juta cuyy
                }
            }
        }
        return $out;
    }

    public function kesimpulan($merge){
        $out = [];
        foreach($merge as $iddivisi => $row){
            foreach($row as $data){
                foreach($data as $index => $value){
                    if(isset($out[$iddivisi][$index])){
                        $out[$iddivisi][$index] += $value;
                    }
                    else{
                        $out[$iddivisi][$index] = $value;
                    }
                }
            }
        }

        $rout = [];
        foreach($out as $id => $data){
            //overwrite total (DEPRECATED)
            $out[$id]['Total'] = 
                (isset($data['Gaji Pokok']) ? $data['Gaji Pokok'] : 0) + 
                (isset($data['Tunjangan Jabatan']) ? $data['Tunjangan Jabatan'] : 0) + 
                (isset($data['Tunjangan Lain']) ? $data['Tunjangan Lain'] : 0) + 
                (isset($data['Tunjangan Penempatan']) ? $data['Tunjangan Penempatan'] : 0) + 
                (isset($data['Tunjangan Golongan']) ? $data['Tunjangan Golongan'] : 0) + 
                (isset($data['Tunjangan Tahunan']) ? $data['Tunjangan Tahunan'] : 0) + 
                (isset($data['Insentive Overtime']) ? $data['Insentive Overtime'] : 0) - 
                (isset($data['Potongan Absensi']) ? $data['Potongan Absensi'] : 0) - 
                (isset($data['BPJS Kesehatan']) ? $data['BPJS Kesehatan'] : 0) - 
                (isset($data['BP Jamsostek']) ? $data['BP Jamsostek'] : 0)
                // (isset($data['Potongan Lain-Lain']) ? $data['Potongan Lain-Lain'] : 0) 
                #ada potongan2 tertentu yg ga dikurangi
                ;

            // sekarang pakai yg dibawah ini utk laporan cik amy yg terbaru
            $biayagaji = 
                (isset($data['Gaji Pokok']) ? $data['Gaji Pokok'] : 0) + 
                (isset($data['Tunjangan Jabatan']) ? $data['Tunjangan Jabatan'] : 0) + 
                (isset($data['Tunjangan Lain']) ? $data['Tunjangan Lain'] : 0) + 
                (isset($data['Tunjangan Penempatan']) ? $data['Tunjangan Penempatan'] : 0) + 
                (isset($data['Tunjangan Golongan']) ? $data['Tunjangan Golongan'] : 0) + 
                (isset($data['Tunjangan Tahunan']) ? $data['Tunjangan Tahunan'] : 0) + 
                (isset($data['Insentive Overtime']) ? $data['Insentive Overtime'] : 0) - 
                (isset($data['Potongan Absensi']) ? $data['Potongan Absensi'] : 0);
            $out[$id]['Biaya Gaji'] = $biayagaji;
            $gajikotor = $biayagaji + 
                (isset($data['Insentif']) ? $data['Insentif'] : 0) + 
                (isset($data['Bonus PU']) ? $data['Bonus PU'] : 0);
            $out[$id]['Gaji Kotor'] = $gajikotor;


            $tlt = isset($data['Potongan Terlambat']) ? $data['Potongan Terlambat'] : 0;
            $ptg = isset($data['Potongan Denda']) ? $data['Potongan Denda'] : 0;
            $lain = isset($data['Potongan Lain-Lain']) ? $data['Potongan Lain-Lain'] : 0;

            $out[$id]['Gaji Bersih'] = $gajikotor - 
                (isset($data['BPJS Kesehatan']) ? $data['BPJS Kesehatan'] : 0) - 
                (isset($data['BP Jamsostek']) ? $data['BP Jamsostek'] : 0) -
                (isset($data['Iuran Suka Duka']) ? $data['Iuran Suka Duka'] : 0) -
                (isset($data['Potongan Kasbon']) ? $data['Potongan Kasbon'] : 0) -
                (isset($data['Potongan Bonus PU']) ? $data['Potongan Bonus PU'] : 0) -
                ($tlt + $ptg + $lain) //potongan gaji = (terlambat + denda + lain2)
                ;

        }
        return $out;
    }

    public function total_kesimpulan($kesimpulan){
        $total = [];
        foreach($kesimpulan as $row){
            foreach($row as $index => $val){
                if(isset($total[$index])){
                    $total[$index] += $val;
                } 
                else{
                    $total[$index] = $val;
                }
            }
        }
        return $total;
    }

}
