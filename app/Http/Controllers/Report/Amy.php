<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use DB;
use Input;
use Excel;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\MdMtKaryawan;
use App\Model\MdKaryawan;
use App\Model\MdJadwal;
use App\Model\MdReport;
use App\Model\MdMtGaji;

use App\Model\Table\ModelGajiResign;
use App\Model\Table\ModelDivisi;

class Amy extends Controller
{
    public $request;


    public function __construct(Request $req){
        $this->middleware('log');
        $this->middleware('authlev:1');
        $this->request = $req;
    }

    public function getIndex()
    {
        // $sql = DB::table('cwa_divisi')->get();
        // $list = [];
        // foreach($sql as $row){
        //     $list[$row->id] = $row->nama_divisi;
        // }
        // dd($list);
        log_user();
        $bulan = Input::get("bulan") ? intval(Input::get("bulan")) : date("n");
        $tahun = Input::get("tahun") ? intval(Input::get("tahun")) : date("Y");
        $divisi = Input::get("divisi");

        
        $data = $this->prepare_gaji($bulan, $tahun, $divisi);
        $group_divisi = $this->group_divisi($data['mutasi']);

        $prepare = $this->laporan_amy($data);

        return view('custom-report-no-table')->with([
            'title' => 'Custom Report untuk Finance',
            'list_divisi' => ModelDivisi::get()->pluck('nama_divisi', 'id'),
            'default' => [
                'divisi' => $divisi,
                'bulan' => $bulan,
                'tahun' => $tahun
            ],
            'data' => $prepare
        ]);
    }

    public function getExcel(){
    	log_user();
        $bulan = Input::get("bulan") ? intval(Input::get("bulan")) : date("n");
        $tahun = Input::get("tahun") ? intval(Input::get("tahun")) : date("Y");
        $divisi = Input::get("divisi");

        
        $data = $this->prepare_gaji($bulan, $tahun, $divisi);
        $group_divisi = $this->group_divisi($data['mutasi']);

        $prepare = $this->laporan_amy($data);
        $final = $this->rapiin_lagi($prepare);

        //untuk yg resign, format datanya cukup sederhana kok
        $data_resign = ModelGajiResign::with('getKaryawan')
        ->whereIn('tgl_berlaku', $data['calendar'])
        ->get();

        $temp_row = $append = [];
        foreach($data_resign as $dt){
            $temp_row[$dt->id_karyawan] = [
                'nama' => $dt->getKaryawan->nama,
                'id_karyawan' => $dt->id_karyawan,
                'divisi' => '-resigned-',
                'status' => 'resign',
                'total_gaji' => $dt->bonus,
            ];
        }

        if(count($temp_row) > 0){
            $append['-resign-'] = $temp_row;
            $final[0] = $final[0] + $append;
        }



        //buat file excel
        $filename = "Laporan Gaji Finance - $bulan - $tahun";

        $sheetname = ['Cash', 'Transfer'];

        Excel::create($filename, function($excel) use ($final, $sheetname){
            $sorted_divisi = ModelDivisi::orderBy('sort_no', 'ASC')->get(['id', 'nama_divisi']);
    		$save_total_gaji = [];
        	foreach($final as $ind=> $fin){
                if(is_panic() && $ind == 0){
                    continue;
                }
	        	$excel->sheet($sheetname[$ind], function($sheet) use($sheetname, $ind, $fin, $save_total_gaji, $sorted_divisi){
	        		$n = 1;
	        		$sheet->row($n, ['Data Gaji '.$sheetname[$ind]]);
	        		$n++;
	        		$n++;
	        		//prepare data per divisi
	        		$total_gaji = [];
                    foreach($sorted_divisi as $dv){
                        $divisi = $dv->nama_divisi;
                        $konten = $fin[$divisi] ?? null;
                        if(empty($konten)){
                            continue;
                        }

	        			$sheet->row($n, [$divisi]); 
	        			$sheet->cell('A'.$n, function($cell){
	        				$cell->setFontWeight('bold');
	        			});
	        			$n++;
	        			$startborder = $n;
	        			$sheet->row($n, ['Nama Karyawan', 'Jumlah Gaji']);
	        			$sheet->cells('A'.$n.':B'.$n, function($cell){
	        				$cell->setFontWeight('bold');
	        			});
	        			$n++;
	        			foreach($konten as $idkaryawan => $row){
	        				$sheet->row($n, [$row['nama'], round($row['total_gaji'])]); $n++;
	        				if(isset($total_gaji[$divisi]))
	        					$total_gaji[$divisi] += $row['total_gaji'];
	        				else
	        					$total_gaji[$divisi] = $row['total_gaji'];
	        			}
	        			$outtotalgaji = isset($total_gaji[$divisi]) ? round($total_gaji[$divisi]) : 0;
	        			$save_total_gaji[$ind][$divisi] = $outtotalgaji;

	        			$sheet->row($n, ['Total', $outtotalgaji]);
	        			$sheet->cells('A'.$n.':B'.$n, function($cell){
	        				$cell->setFontWeight('bold');
	        			});
	        			$endborder = $n;
	        			$sheet->setBorder('A'.$startborder.':B'.$endborder, 'thin');
	        			$n++;
                        $n++;
	        		}

	        		$sheet->cell('A1', function($cell){
	                    $cell->setFontSize(16);
	                    $cell->setFontWeight('bold');
	                });

                    if(session('save_total_gaji')){
                        session()->put('save_total_gaji.'.$sheetname[$ind] , $save_total_gaji);
                    }
                    else{
                        session()->put('save_total_gaji.'.$sheetname[$ind]  , $save_total_gaji);
                    }

	        	});
        	}

        	//last sheet : kesimpulan
        	$excel->sheet('Report', function($sheet) use($sorted_divisi){
        		$n = 1;
                $save_total_gaji = session('save_total_gaji');
                session()->forget('save_total_gaji');

                $total_tai = 0;
        		foreach($save_total_gaji as $dd){
                    foreach($dd as $idtrf=>$data){
                        if($idtrf == 1)
                            $tp = 'Transfer';
                        else
                            $tp = 'Cash';
                        $sheet->row($n, ['Ringkasan Gaji '.$tp]);
                        $sheet->cell('A'.$n, function($cell){
                            $cell->setFontWeight('bold');
                        });
                        $n++;
                        $gtotal = 0;
                        foreach($sorted_divisi as $dv){
                            $divisi = $dv->nama_divisi;
                            $total = $data[$divisi] ?? 0;
                            if($total == 0){
                                continue;
                            }

                            $total = str_replace(',', '', $total);
                            $total = intval($total);
                            $gtotal += $total;
                            $sheet->row($n, [$divisi, round($total)]);
                            $n++;
                        }
                        //total per cash/transfer
                        $sheet->row($n, ['Total '. $tp, round($gtotal)]);
                        $total_tai += $gtotal;
                        $sheet->cell('A'.$n, function($cell){
                            $cell->setFontWeight('bold');
                        });
                        $sheet->cell('B'.$n, function($cell){
                            $cell->setFontWeight('bold');
                        });

                        $n++; 
                        $n++; 
    
                    }

        		}

                $n++;
                $n++;
                $sheet->row($n, ['Total Transfer + Cash', round($total_tai)]);
                $sheet->cell('A'.$n, function($cell){
                    $cell->setFontWeight('bold');
                });
                $sheet->cell('B'.$n, function($cell){
                    $cell->setFontWeight('bold');
                });


        	});
        })->store('xlsx', 'upload');
        echo json_encode(url("upload/".$filename.".xlsx"));
        exit();
    }



    public function prepare_gaji($bulan, $tahun, $divisi){
        $report = new MdReport([
            'bulan' => $bulan,
            'tahun' => $tahun
        ]);

        //month field
        $row = $report->make_mutasi($divisi, 'divisi', true);
        $report->get_rancangan();
        $report->get_presensi();
        $content = $report->prepare_object();

        $gaji = new MdMtGaji([
            'bulan' => $bulan,
            'tahun' => $tahun
        ]);

        return [
            'mutasi' => $row,
            'datagaji' => $report->gaji_maker($content, $gaji->build_gaji(1), $gaji->build_gaji(2), $row, false),
            'calendar' => $report->calendar
        ];
    }

    public function group_divisi($data){
        $saved = [];
        //menyimpan list ID karyawan per divisi 
        foreach($data as $idkar => $row){
            $saved[$row->id_divisi][] = $row->id_karyawan;
        }
        return $saved;
    }


    public static function laporan_amy($data){
        //prepare status karyawan
        $out = [];
        foreach($data['mutasi'] as $idmutasi => $obj){
            $total_gaji = isset($data['datagaji'][$obj->id_karyawan]['total']) ? $data['datagaji'][$obj->id_karyawan]['total'] : 0;

            $out[$obj->status_transfer][$obj->id_karyawan]['nama'] = $obj->nama;
            $out[$obj->status_transfer][$obj->id_karyawan]['id_karyawan'] = $obj->id_karyawan;
            $out[$obj->status_transfer][$obj->id_karyawan]['divisi'] = $obj->nama_divisi;
            $out[$obj->status_transfer][$obj->id_karyawan]['status'] = $obj->type;
            $out[$obj->status_transfer][$obj->id_karyawan]['total_gaji'] = $total_gaji;
        }

        return $out;
    }

    public static function rapiin_lagi($prepared){
    	$out = [];
    	//prepared[0] => Cash
    	if(isset($prepared[0])){
    		foreach($prepared[0] as $idkaryawan => $row){
    			$out[0][$row['divisi']][$idkaryawan] = $row;
    		}
    	}

    	//prepared[1] => Transfer
        if(isset($prepared[1])){
            foreach($prepared[1] as $idkaryawan => $row){
                $out[1][$row['divisi']][$idkaryawan] = $row;
            }
        }
        
        //prepared[2] => Multi
        if(isset($prepared[2])){
            foreach($prepared[2] as $idkaryawan => $row){
                //gaji dibagi ke cash dan ke transfer
                $tmpp = $row['total_gaji'];


                //ada 3 threshold tai
                /*
                1. Hendri langsung full transfer
                2. Amy : 6,5
                3. Sisanya biasa
                */

                $tai_threshold = 5500000;
                if($idkaryawan == 3){
                    $tai_threshold = 6500000;
                }
                else if($idkaryawan == 274){
                    $tai_threshold = 15000000; //skrg ditransfer 12jt, sisanya 22jt cash.. bagi2 2 juta aja napa
                }
                if($idkaryawan == 431){
                    $tai_threshold = 10000000;
                }


                if($tmpp > $tai_threshold){
                    $out[1][$row['divisi']][$idkaryawan] = [
                        'nama' => $row['nama'],
                        'id_karyawan' => $row['id_karyawan'],
                        'divisi' => $row['divisi'],
                        'status' => $row['status'],
                        'total_gaji' => $tai_threshold,
                    ];

                    $sisacash = $tmpp - $tai_threshold;
                    $out[0][$row['divisi']][$idkaryawan] = [
                        'nama' => $row['nama'],
                        'id_karyawan' => $row['id_karyawan'],
                        'divisi' => $row['divisi'],
                        'status' => $row['status'],
                        'total_gaji' => $sisacash,
                    ];
                }
            }
        }
    	return $out;
    }
}
