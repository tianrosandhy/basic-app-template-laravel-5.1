<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\MdDashboard;
use Auth;
use Redirect;
use DB;


class Home extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $dash = new MdDashboard();
        //
        if(Auth::check()){
            return view('dashboard')->with([
                'title' => "CWA Human Resource Information System",
                'curmenu' => 0,
                'cursubmenu' => 0,
                'data' => $dash->data,
                'notif' => MdDashboard::check_notif(),
            ]);
        }
        else
            return view('template/t_login');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/')->with("info","You've successfully logout from application");
    }
    

    public function switch(Request $request){
        $grab = DB::table($request->table)->where('id', $request->id)->first();
        if(!empty($grab)){
            DB::table($request->table)
                ->where('id', $request->id)
                ->update([
                    $request->field => $request->value
                ]);
        }

        return [
            'type' => 'success'
        ];
    }
}
