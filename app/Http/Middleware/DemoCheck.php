<?php

namespace App\Http\Middleware;

use Closure;
use App\Cms;

class DemoCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    protected $except = array("/");

    public function handle($request, Closure $next)
    {
        //hanya dilakukan selain halaman homepage
        $demo = env('DEMO');
        if($demo===true){
            //cek
            if($request->method() == "POST" and $request->segment(1) <> 'login'){
                if($request->segment(1) == "setting" or empty($request->segment(2))){
                    $url = $request->segment(1);
                }
                else if(!empty($request->segment(2)))
                    $url = $request->segment(1)."/".$request->segment(2);


                if($request->ajax()){
                    echo json_encode(['error' => 'Akses halaman demo hanya untuk melihat-lihat saja.. :D']);
                    exit();
                }
                else{
                    return redirect($url)->with(['error' => 'Akses halaman demo hanya untuk melihat-lihat saja.. :D']);
                }

            }


        }

        return $next($request);
    }
}
