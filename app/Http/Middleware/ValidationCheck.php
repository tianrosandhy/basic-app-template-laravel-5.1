<?php

namespace App\Http\Middleware;

use Closure;
use App\Cms;

class ValidationCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    protected $except = array("/");

    public function handle($request, Closure $next)
    {
        //hanya dilakukan selain halaman homepage
        if(!cek_login(null)){
            return redirect("/")->with("error", "Please log in first");
        }

        return $next($request);
    }
}
