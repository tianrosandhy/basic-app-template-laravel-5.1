<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Redirect;

class AuthLevel
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $priv=10)
    {

        if(Auth::check()){
            $priviledge = Auth::user()->priviledge;
            if($priviledge <= $priv){
                return $next($request);
            }
            else
                return Redirect::to('/')->with(["error" => "Access Denied. Anda tidak memiliki akses menuju halaman tersebut. "]);
        }  
        else
            return Redirect::to('/');

    }
}
