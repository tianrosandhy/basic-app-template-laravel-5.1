<?php
//CMS Helper Class

function cek_login($target="/"){
	if(Auth::check()){
		return true;
	}
	else{
		if(is_null($target)){
			return false;
		}
		else{
			#setara dengan helper redirect()...
			return Redirect::to("/")->send()->with("error","Please log in first");
			exit();
		}
	}
}


function rupiah($n){
	return round($n, 0);
	return "Rp ".number_format($n, 0, ",",".");
}

function data_priviledge($show=0){
	$arr = [
		1 => 'Administrator',
		2 => 'HR Leader',
		3 => 'HR Payroll',
		4 => 'HR Staff',
	];
	$ret = [];
	for($i=$show; $i<=4; $i++){
		if(isset($arr[$i]))
			$ret[$i] = $arr[$i];
	}
	return $ret;
}


function get_priviledge(){
	$priv = Auth::user()['attributes']['priviledge'];
	switch($priv){
		case 1 : 
			$label = "Administrator";
			break;
		case 2 : 
			$label = "HR Leader";
			break;
		case 3 : 
			$label = "HR Payroll";
			break;
		case 4 : 
			$label = "HR Staff";
			break;
		default : 
			$label = "Guest";
	}

	return array($priv, $label);
}




function get_setting($param){
	$sql = DB::table('cwa_setting')
		->where('param', $param)
		->first();

	if(isset($sql->value)){
		if(strlen($sql->value) > 0){
			$value = $sql->value;
		}
		else
			$value = $sql->def_value;

		return $value;
	}
	else return null;
}


function dumps($mixed){
	echo "<textarea style=\"width:100%; height:300px\">";
	var_dump($mixed);
	echo "</textarea>";
}

function toArray($obj){
	return (array)$obj;
}



function nama_bulan($n=0){
	$month = array(
		"", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"
	);
	if($n == 0)
		return $month;
	else
		return $month[$n];
}

function indo_date($tgl, $type="half"){
	$month = nama_bulan();

	$tahun = date("Y",strtotime($tgl));
	$bulan = $month[date("n",strtotime($tgl))];
	$tanggal = date("d",strtotime($tgl));

	$fullDate = "$tanggal $bulan $tahun";

	if($type == "quarter"){
		return "$tanggal $bulan";
	}
	else if($type <> "half"){
		$jam = date("H:i:s", strtotime($tgl));
		return $fullDate." ".$jam;
	}
	return $fullDate;
}


function input_helper($label, $data, $value=null){
	$out = "";

	//default class is form-control
	$attr = array("class" => "form-control");
	if(isset($data['attr']['class'])){
		$attr['class'] .= " ".$data['attr']['class'];
	}

	if(count($data['attr']) > 0){
		$attr = array_merge($attr, $data['attr']);
	}


	//penentuan yg akan diisi sbg value
	//1. session
	//2. edit form
	//3. default value

	if(!empty($value)){
		$fill = $value;
	}
	else{
		$fill = $data['value'];
	}


	if($data['type'] == "select"){
		$attr['class'] = 'select2 form-control';
		$out = Form::select($data['field'], $data['list'], $fill, $attr);
	}
	elseif($data['type'] == "textarea"){
		$out = Form::textarea($data['field'], $fill, $attr);
	}
	elseif($data['type'] == "checkbox"){
		if(count($data['list']) == 1){
			$out .= "<label>";
			$out .= Form::checkbox($data['field'], 1, $data['value'] == $fill ? $data['value'] : $fill);
			$out .= " ".$data['list'][1]." </label>";
		}
		else{
			foreach($data['list'] as $val=>$lbl){
				$out .= "<label>";
				$out .= Form::checkbox($data['field']."[]", $val, $val == $fill ? $val : null);
				$out .= " $lbl </label>";
			}
		}
	}
	elseif($data['type'] == "radio"){
		foreach($data['list'] as $val=>$lbl){
			$out .= "<label>";
			$out .= Form::radio($data['field']."[]", $val, $val == $fill ? $val : null);
			$out .= "$lbl </label>";
		}
	}
	else{
		$out = Form::input($data['type'], $data['field'], $fill, $attr);
	}
	
	return $out;	
}

function tb($tbname){
	$prefix = config('database.prefix');
	if(strpos($tbname, $prefix) === 0){
		return $tbname;
	}
	return $prefix.$tbname;
}




function jumlah_hari($bulan, $tahun){
	$def = array(
		1 => 31,
		2 => 0,
		3 => 31,
		4 => 30,
		5 => 31,
		6 => 30,
		7 => 31,
		8 => 31,
		9 => 30,
		10 => 31,
		11 => 30,
		12 => 31
	);
	$current = $def[intval($bulan)];
	if($current == 0){
		if($tahun % 4 == 0)
			$current = 29;
		else
			$current = 28;
	}

	return $current;
}

function date_range($tgl,$tglb){
	$a = strtotime($tgl);
	$b = strtotime($tglb);

	for($i=$a; $i<=$b;$i+=86400){
		$arr[] = date("Y-m-d",$i);
	}

	return $arr;
}


//default start of day  : Sunday
function n_week($tgl){
	$time = strtotime($tgl);
	if(date("N", $time) <> 7){
		$start = strtotime('last sunday, 12pm', $time);
	}
	else
		$start = strtotime($tgl);

	if(date("N", $time) <> 6){
		$end = strtotime('next saturday, 11:59am', $time);
	}
	else
		$end = strtotime($tgl);
	$format = 'Y-m-d';
	$start_day = date($format, $start);
	$end_day = date($format, $end);

	$list = date_range($start_day, $end_day);

	return $list;
}

function tb_list($tbname, $get=null){
	$sql = DB::table($tbname)
	->where("stat","<>",9);

	if(!empty($get))
		$sql->where("id",$get);
	$sql = $sql->get();

	$ret = array();
	foreach($sql as $row){
		$row = (array)$row;
		$ret[$row['id']] = $row;
	}

	return $ret;
}

function time_format($time){
	$x = strtotime($time);
	if(empty($x))
		return null;
	else{
		return date("H:i",$x);
	}
}

function x_minute($x){
	if($x < 60)
		return $x." menit";
	else{
		$jam = floor($x / 60);
		$sisa = $x % 60;
		$out = $jam." jam ";
		if($sisa > 0)
			$out .= $sisa." menit";
		return $out;
	}
}

function exclude_presence(){
	$x = get_setting("exclude_presence");
	$sql = DB::table("cwa_kode_absen")
	->where("kode_absen",$x)
	->where("stat","<>",9)
	->first();

	$id = $sql->id;
	return $id;

}

function dateDifference($startDate, $endDate) 
{ 
    $startDate = strtotime($startDate); 
    $endDate = strtotime($endDate); 
    if ($startDate === false || $startDate < 0 || $endDate === false || $endDate < 0 || $startDate > $endDate) 
        return false; 
        
    $years = date('Y', $endDate) - date('Y', $startDate); 
    
    $endMonth = date('m', $endDate); 
    $startMonth = date('m', $startDate); 
    
    // Calculate months 
    $months = $endMonth - $startMonth; 
    if ($months <= 0)  { 
        $months += 12; 
        $years--; 
    } 
    if ($years < 0) 
        return false; 
    
    // Calculate the days 
                $offsets = array(); 
                if ($years > 0) 
                    $offsets[] = $years . (($years == 1) ? ' year' : ' years'); 
                if ($months > 0) 
                    $offsets[] = $months . (($months == 1) ? ' month' : ' months'); 
                $offsets = count($offsets) > 0 ? '+' . implode(' ', $offsets) : 'now'; 

                $days = $endDate - strtotime($offsets, $startDate); 
                $days = date('z', $days);    
                
    return array($years, $months, $days); 
} 

function rand_color() {
    return sprintf('#%06X', mt_rand(0, 0xFFFFFF));
}

function explode_jam($jam){
	$x = explode("-", $jam);

	if(count($x) == 2){
		$rt[0] = date("H:i", strtotime($x[0]));
		$rt[1] = date("H:i", strtotime($x[1]));
	}
	else{
		$rt[0] = date("H:i",strtotime($jam));
		$rt[1] = null;
	}

	return $rt;
}

function transform_array($mix){
	$ret = [];
	foreach($mix as $arr){
		$ret[] = (array)$arr;
	}
	return $ret;
}

function log_user($action='OPEN', $data=[]){
	$tgl = date("Y-m-d H:i:s");
	$username = Auth::user()['attributes']['username'];
	$uri = Request::url();
	$data = json_encode($data);

	DB::table('log_system')
	->insert([
		'username' => $username,
		'tgl' => $tgl,
		'uri' => $uri,
		'action' => $action,
		'data' => $data,
		'stat' => 1
	]);
}


function demo_hook(){
	if(env("DEMO")==true){
		$curr = Request::segment(1)."/".Request::segment(2);
		return redirect($curr)->with(['error'=> 'Aksi dibatasi untuk halaman demo']);
		exit();
	}
}

function panic_mode($val=true){
	if($val){
		//create file panic di base
		if(!is_panic()){
			$file = fopen('panic', 'w');
		}
	}
	else{
		//hapus file panic di base
		if(is_panic()){
			unlink('panic');
		}
	}
}

function is_panic(){
	return is_file('panic');
}