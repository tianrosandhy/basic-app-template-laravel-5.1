<?php
namespace App\Traits;

use Form;
use App\Model\DataTableModel;
use Request;
use App\Models\MdKaryawan;


trait KaryawanTrait
{

	//Structure Trait

	public function register_column(){
		$this->columns = [
			'Nama' => ['search' => true, 'col' => 'nama'],
			'NIK' => ['search' => true, 'col' => 'nik'],
			'NPWP' => ['search' => true, 'col' => 'npwp'],
			'Email' => ['search' => true, 'col' => 'email'],
			'Departemen' => ['search' => true, 'col' => 'departemen'],
			'TTL' => ['search' => true, 'col' => 'ttl'],
			'JK' => ['search' => true, 'col' => 'jk'],
			'Alamat' => ['search' => true, 'col' => 'alamat'],
			'Telp'	=> ['search' => true, 'col' => 'telp'],
            'No Rek' => ['search' => true, 'col' => 'no_rek'],
            'Status Transfer' => ['search' => true, 'col' => 'status_transfer'],
			'Tgl Masuk'	=> ['search' => true, 'col' => 'tgl_masuk'],
			'BPJS Kes' => ['search' => true, 'col' => 'is_bpjs_kesehatan'],
			'BPJS TK' => ['search' => true, 'col' => 'is_bpjs_tk'],
			'Status PTKP' => ['search' => true, 'col' => 'status_ptkp'],
			'' => ['search' => false, 'col' => 'button'], 
		];
	}

    //
    //Data Table Trait

    public function getPost(){
    	$req = $this->request->all();
    	$post = new DataTableModel('cwa_karyawan', $this->columns);
    	$post->fillable = ['nama','nama_panggilan','ktp','ttl','jk','alamat','alamat_asal','telp','no_rek','status_transfer', 'tgl_masuk','stat', 'is_bpjs_kesehatan', 'is_bpjs_tk', 'status_ptkp'];

    	$post->data = $req;
    	$out = $post->get_data();

    	$out['data'] = $this->table_format($req, $out['result'], $post->blank_action());

    	return json_encode($out);
    }

    public function table_format($data, $result, $blank=[]){
		//method untuk mengatur tampilan output data di datatable
		if(count($result) == 0){
			$ret = $blank;
		}
		else{
            $skrg = strtotime(date("Y-m-d"));
            $warn = 3 * 30 * 24 * 60 * 60;
			foreach($result as $row){
                $stat = ($skrg - strtotime($row->tgl_masuk)) > $warn ? "old" : "new";
                
                $hmmm = ($stat == "old" && strlen($row->no_rek) == 0) ? "<span class='fa fa-warning' style='color:#f00' title='No Rekening belum terdaftar'></span>" : "";
                
				//field index disesuaikan dengan parameter col di this->column
				$ret[] = array(
					'<a href="detail/'.$row->id.'" data-featherlight="ajax">'.$row->nama.'</a> '.$hmmm,
					$row->nik,
					$row->npwp,
					$row->email,
					$row->departemen,
					$row->ttl,
					($row->jk == 1) ? "Laki-laki" : "Perempuan",
					$row->alamat,
					$row->telp,
          $row->no_rek,
          ($row->status_transfer == 0) ? 'Cash' : (($row->status_transfer == 1) ? 'Transfer' : (is_panic() ? 'Transfer' : 'Multi')),
					indo_date($row->tgl_masuk),
					$row->is_bpjs_kesehatan ? ('<span style="padding:.15em .3em; border-radius:5px; color:#fff; background:#33ac40;">YA</span>' . ($row->tgl_daftar_bpjs_kesehatan ? '<br><small>sejak '.date('d M Y', strtotime($row->tgl_daftar_bpjs_kesehatan)).'</small>' : '')) : '<span style="padding:.15em .3em; border-radius:5px; background: #d00; color:#fff;">TIDAK</span>',
					$row->is_bpjs_tk ? ('<span style="padding:.15em .3em; border-radius:5px; color:#fff; background:#33ac40;">YA</span>' . ($row->tgl_daftar_bpjs_tk ? '<br><small>sejak '.date('d M Y', strtotime($row->tgl_daftar_bpjs_tk)).'</small>' : '')) : '<span style="padding:.15em .3em; border-radius:5px; background: #d00; color:#fff;">TIDAK</span>',
					strtoupper($row->status_ptkp), 
					'<div class="btn-group">
						<a href="master/karyawan/edit/'.$row->id.'" class="btn btn-info btn-sm btn-edit">Edit</a>
						<a href="master/karyawan/destroy/'.$row->id.'" class="btn btn-danger btn-sm delete-button">Delete</a>
					</div>'
				);
			}
		}
		return $ret;
	}



}